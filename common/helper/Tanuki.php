<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 10/05/16
 * Time: 4:27 PM
 */

namespace common\helper;

use Yii;
use \common\models\Yahoo;
use \common\models\Amazon;
use \common\models\Rakuten;
use \common\models\YahooAuctions;
class Tanuki
{
    public $RakutenProduct;
    public $AmazonProduct;
    public $YahooProduct;
    public $YahooAuction;

    public function singleProductDetail($code, $shop) {

        switch ($shop) {
            case 'yahoo' :
                $yahoo = new Yahoo;
                $this->YahooProduct = $yahoo->productsDetail($code);
                break;
            case 'amazon' :
                sleep(2);
                $amazon = new Amazon();
                $this->AmazonProduct = $amazon->productDetail(['id'=>$code]);
                break;
            case 'rakuten' :
                $rakuten = new Rakuten();
                $this->RakutenProduct = $rakuten->RakutenAPI(['itemCode'=>$code]);
                break;
            case 'yahoo_auction' :
                $auction = new YahooAuctions();
                $this->YahooAuction = $auction->productsDetailForFavorite($code);
                break;
            default :
                return "Unknown Shop";
                break;
        }
    }

    public function getProductName($shop){
        switch ($shop) {
            case 'yahoo' :
                return $this->yahooProductName();
                break;
            case 'amazon' :
                sleep(2);
                return $this->amazonProductName();
                break;
            case 'rakuten' :
                return $this->rakutenProductName();
                break;
            case 'yahoo_auction' :
                return $this->auctionName();
                break;
            default :
                return "Unknown Shop";
                break;
        }
    }
    public function getProductPrice($shop){
        switch ($shop) {
            case 'yahoo' :
                return $this->yahooProductPrice();
                break;
            case 'amazon' :
                sleep(2);
                return $this->amazonProductPrice();
                break;
            case 'rakuten' :
                return $this->rakutenProductPrice();
                break;
            default :
                return "Unknown Shop";
                break;
        }
    }

    public function getProductImage($shop){
        switch ($shop) {
            case 'yahoo' :
                return $this->yahooImage();
                break;
            case 'amazon' :
                sleep(2);
                return $this->amazonProductImage();
                break;
            case 'rakuten' :
                return $this->rakutenImage();
                break;
            case 'yahoo_auction' :
                return $this->auctionImage();
                break;
            default :
                return "Unknown Shop";
                break;
        }
    }

    public function rakutenProductName() {
        if ($this->RakutenProduct) {
            return (isset($this->RakutenProduct['data']['data'][0]['Item']['itemName'])) ? $this->RakutenProduct['data']['data'][0]['Item']['itemName'] : 'Unknown';
        } else {
            return 'Unknown';
        }
    }

    public function yahooProductName() {
        if ($this->YahooProduct) {
            return (isset($this->YahooProduct['ResultSet'][0]['Result'][0]['Name'])) ? $this->YahooProduct['ResultSet'][0]['Result'][0]['Name'] : 'Unknown';
        } else {
            return 'Unknown';
        }
    }

    public function auctionName() {
        if ($this->YahooAuction) {
            return (isset($this->YahooAuction['ResultSet']['Result']['Title'])) ? $this->YahooAuction['ResultSet']['Result']['Title'] : false;
        } else {
            return false;
        }
    }


    public function rakutenProductPrice() {
        if ($this->RakutenProduct ) {
            return (isset($this->RakutenProduct['data']['data'][0]['Item']['itemPrice'])) ? $this->RakutenProduct['data']['data'][0]['Item']['itemPrice'] : 0.00;
        } else {
            return 0.00;
        }
    }

    public function yahooProductPrice() {
        if ($this->YahooProduct) {
            return (isset($this->YahooProduct['ResultSet'][0]['Result'][0]['PriceLabel']['DefaultPrice'])) ? $this->YahooProduct['ResultSet'][0]['Result'][0]['PriceLabel']['DefaultPrice'] : 'Unknown';
        } else {
            return 0.00;
        }
    }

    public function rakutenImage(){
        $img = false;
        $data = $this->RakutenProduct;
        if (isset($data['data']) && isset($data['data']['data']) && isset($data['data']['data'][0]) && isset($data['data']['data'][0]['Item'])) {

            if (isset($data['data']['data'][0]['Item']['mediumImageUrls']) &&
                isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]) &&
                isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'])
            ) {
                $img = $data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'];
                $img = substr($img, 0, strpos($img, '?'));
            } else if (isset($data['data']['data'][0]['Item']['smallImageUrls']) &&
                isset($data['data']['data'][0]['Item']['smallImageUrls'][0]) &&
                isset($data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'])
            ) {
                $img = $data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'];
                $img = substr($img, 0, strpos($img, '?'));
            } else {
                $img = 'no-image';
            }
        }
        return $img;
    }

    public function yahooImage(){
        $data = $this->YahooProduct;
        $YImg = false;
        if (
            isset($data['ResultSet']) &&
            isset($data['ResultSet'][0]) &&
            isset($data['ResultSet'][0]['Result']) &&
            isset($data['ResultSet'][0]['Result'][0])
        ) {

            if (!isset($data['ResultSet'][0]['Result'][0]['Name'])) {

            }

            if (
                isset($data['ResultSet'][0]['Result'][0]['Image']) &&
                isset($data['ResultSet'][0]['Result'][0]['Image']['Medium'])
            ) {
                $YImg = $data['ResultSet'][0]['Result'][0]['Image']['Medium'];
                $YImg = str_replace('/g/', '/l/', $YImg);
            } else if (isset($data['ResultSet'][0]['Result'][0]['RelatedImages']) &&
                isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]) &&
                isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'])
            ) {
                $YImg = $data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'];
                $YImg = str_replace('/g/', '/l/', $YImg);
            } else {
                $YImg = 'no-image';
            }
        }
        return $YImg;
    }

    public function amazonProductImage(){
        if ($this->AmazonProduct) {
            $xml = simplexml_load_string($this->AmazonProduct);
            $json = json_encode($xml);
            $data = json_decode($json, true);

            if (
                isset($data['Items']) &&
                isset($data['Items']['Item']) &&
                isset($data['Items']['Item']['LargeImage'])
            ) {
                $Img = $data['Items']['Item']['LargeImage']['URL'];
            } else {
                $Img = 'no-image';
            }
            return $Img;
        }
    }

    public function auctionImage() {
        if (isset($this->YahooAuction['ResultSet']['Result']['Img']) && count($this->YahooAuction['ResultSet']['Result']['Img']) > 0) {
            if (trim($this->YahooAuction['ResultSet']['Result']['Img'])) {
                foreach ($this->YahooAuction['ResultSet']['Result']['Img'] as $key => $value) {
                    if (strpos($value, "https://") !== false) {
                        return $value;
                        break;
                    }
                }
            }
        }
    }

    public function amazonProductPrice() {
        if ($this->AmazonProduct) {
            $xml = simplexml_load_string($this->AmazonProduct);
            $json = json_encode($xml);
            $data = json_decode($json, true);
            if (isset($data['Items']['Item']['VariationSummary']['LowestPrice']['Amount']))
            {
                $price = $data['Items']['Item']['VariationSummary']['LowestPrice']['Amount'];
            } else if (isset($data['Items']['Item']['OfferSummary']['LowestNewPrice']['Amount'])) {
                $price = $data['Items']['Item']['OfferSummary']['LowestNewPrice']['Amount'];
            } else {
                $price = 0;
            }
            return $price;
        }
    }

    public function amazonProductName() {
        if ($this->AmazonProduct) {
            $xml = simplexml_load_string($this->AmazonProduct);
            $json = json_encode($xml);
            $data = json_decode($json, true);
            if (isset($data['Items']['Item'])) {
                return $data['Items']['Item']['ItemAttributes']['Title'];
            } else {
                return 'Amazon-Unknown';
            }
        }
    }

    public function shopProductDetail($shop = 'rakuten',$item = 'price') {

        if ($shop == 'rakuten') {
            if ($item == 'price') {
                return $this->rakutenProductPrice();
            } elseif ($item == 'name') {
                return $this->rakutenProductName();
            } elseif ($item == 'image') {
                return $this->rakutenImage();
            }
        } elseif ($shop == 'yahoo') {
            if ($item == 'price') {
                return $this->yahooProductPrice();
            } elseif ($item == 'name') {
                return $this->yahooProductName();
            } elseif ($item == 'image') {
                return $this->yahooImage();
            }
        } elseif ($shop == 'amazon') {
            if ($item == 'name') {
                return $this->amazonProductName();
            } else if ($item == 'price') {
                return $this->amazonProductPrice();
            }
        }
    }

    public function auctionEndTime() {
        if (isset($this->YahooAuction['ResultSet']['Result']['EndTime'])) {
            $format = new \DateTime($this->YahooAuction['ResultSet']['Result']['EndTime']);
            return $format->format('m/d/Y H:i:s A');
        } else {
            return false;
        }
    }

}