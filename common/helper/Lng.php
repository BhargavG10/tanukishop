<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 10/05/16
 * Time: 4:27 PM
 */

namespace common\helper;

use common\models\Category;
use common\models\Labels;
use common\models\Page;
use Yii;
class Lng
{
    public function Category($slug){
        $data = Category::findOne(['slug'=>$slug]);
        if ($data) {
            return $data->title_en;
        } else {
            return $slug;
        }
    }

    public function Label($slug) {
        $data = Labels::findOne(['slug'=>$slug]);
        if ($data) {
            return $data->title_en;
        } else {
            return $slug;
        }
    }

    public function pageTitle($slug) {
        $data = Page::findOne(['slug'=>$slug]);
        if ($data) {
            return $data->title_en;
        } else {
            return $slug;
        }
    }
}