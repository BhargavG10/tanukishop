<?php

# full website emails will come in this file.

namespace common\components;
use yii;

class MailCamp extends \common\components\TBase
{
    public static function adminNewAuctionMail($model){

        return Yii::$app->mailer->compose('admin/new-bid-on-auction', [
            'model' => $model,
        ])
        ->setTo(TBase::TanukiSetting('sales-email'))
        ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
        ->setSubject('Auction Bid Mail On Auction ID #'.$model->auction_id)
        ->send();
    }

    public static function userNewAuctionMail($model){

        return Yii::$app->mailer->compose('user/new-auction-'.$model->user->language, [
            'model' => $model,
        ])
        ->setTo($model->user->email)
        ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
        ->setSubject('Auction Bid Mail On Auction ID #'.$model->auction_id)
        ->send();
    }

    public static function adminNewBuyOutMail($model,$user){
        return Yii::$app->mailer->compose('admin/user-placed-buyout', [
            'model' => $model,
            'user' => $user->first_name.' '.$user->last_name,
            'user_id' => $user->id,
            'date' => date('d-m-Y H:i:s'),
        ])
	        ->setTo(TBase::TanukiSetting('sales-email'))
	        ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
            ->setSubject('Buyout Mail On Auction ID #'.$model->auction_id)
            ->send();
    }

    public static function userNewBuyOutMail($model,$user){

	    $subject = self::mailSubject($user->language,'userNewBuyOutMail');

        return Yii::$app->mailer->compose('auction/buyout-'.$user->language, [
            'model' => $model,
            'user' => $user->first_name.' '.$user->last_name,
            'user_id' => $user->id,
            'date' => date('d-m-Y H:i:s'),
        ])
            ->setTo($user->email)
            ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
            ->setSubject($subject)
            ->send();
    }

	public static function adminOrderNotification($id) {

		$subject = 'New Order - TNK0000' . $id;
		return Yii::$app->mailer->compose('admin/new-shopping-order', [
			'id' => $id
		])
		->setTo(TBase::TanukiSetting('sales-email'))
		->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
		->setSubject($subject)
		->send();
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public static function adminFundNotification($id) {

    	return Yii::$app->mailer->compose('admin/user-added-funds', [
			'id' => $id
		])
		->setTo(TBase::TanukiSetting('sales-email'))
		->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
        ->setSubject("Клиент пополнил счет")
        ->send();
	}

	public static function adminPackageNotification($id) {
		$subject = 'New Package '.$id;
		return Yii::$app->mailer->compose('admin/new-package', [
			'id' => $id
		])
		->setTo(TBase::TanukiSetting('sales-email'))
		->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
        ->setSubject($subject)
        ->send();
	}

	public static function adminNewUserRegistration($user) {

		return Yii::$app
			->mailer
			->compose('admin/new-user-registration' ,['user' => $user] )
			->setFrom([TBase::TanukiSetting('support-email') => \Yii::$app->name])
			->setTo(TBase::TanukiSetting('support-email'))
			->setSubject('New User Registration '.\Yii::$app->name)
			->send();
	}

	/**
	 * @param $model
	 * @param $user
	 *
	 * @return bool
	 */
	public static function adminChatQuery($model,$user) {

    	return Yii::$app
			->mailer
			->compose('admin/chat-query',['model'=>$model,'user'=>$user])
			->setFrom([TBase::TanukiSetting('sales-email') => \Yii::$app->name])
			->setTo(TBase::TanukiSetting('sales-email'))
			->setSubject('Новое сообщение от клиента (через Мои Сообщения)')
			->send();
	}

	public static function adminProductQuery($body,$url) {
		return Yii::$app->mailer->compose('admin/product-query', [
			'body' => $body,
			'url' => $url,
		])
          ->setTo(TBase::TanukiSetting('sales-email'))
          ->setFrom([TBase::TanukiSetting('sales-email') => \Yii::$app->name])
          ->setSubject('запрос от пользователя (на странице товара)')
          ->send();
	}

	public static function adminAddToCartNotification() {
		return Yii::$app->mailer->compose('admin/product-added-in-cart',['user'=>Yii::$app->user->identity])
            ->setTo(TBase::TanukiSetting('sales-email'))
			->setFrom([TBase::TanukiSetting('sales-email') => \Yii::$app->name])
            ->setSubject("Клиент добавил товар в корзину и ждет подтверждения")
            ->send();
	}

	/**
	 * user-registration-en-US
	 * @param $user
	 * @param $link
	 * @param $password
	 *
	 * @return bool
	 */
	public static function userRegistrationEmail($user,$link,$password) {

		$subject = self::mailSubject($user->language,'userRegistrationEmail');

    	return Yii::$app
			->mailer
			->compose(
				'registration/user-registration-'.$user->language,
				['user' => $user,'link' => $link,'password' => $password]
			)
			->setFrom([TBase::TanukiSetting('support-email') => \Yii::$app->name])
			->setTo($user->email)
			->setSubject($subject)
			->send();
	}

	/**
	 * account-activated--en-US
	 * @param $user
	 *
	 * @return bool
	 */
	public static function accountActivatedEmail($user) {
		return Yii::$app
			->mailer
			->compose( 'registration/account-activated-'.$user->language, ['user' => $user] )
			->setFrom([TBase::TanukiSetting('support-email') => \Yii::$app->name . ' robot'])
			->setTo($user->email)
			->setSubject('New Activated Successfully : ' . \Yii::$app->name)
			->send();
	}

	/**
	 * password-reset-token-en-US
	 * @param $user
	 *
	 * @return bool
	 */
	public static function passwordResetEmail($user) {

		$subject = self::mailSubject($user->language,'passwordResetEmail');

		return Yii::$app
			->mailer
			->compose( 'registration/password-reset-token-'.$user->language, ['user' => $user] )
			->setFrom([TBase::TanukiSetting('support-email') => \Yii::$app->name . ' robot'])
			->setTo($user->email)
			->setSubject($subject)
			->send();
	}

	/**
	 * chat-reply-en-US
	 * @param $user
	 * @param $msg
	 *
	 * @return bool
	 */
	public static function userQueryReplyEmail($user,$msg) {
		$subject = self::mailSubject($user->language,'userQueryReplyEmail');
		return Yii::$app
			->mailer
			->compose('registration/chat-reply-'.$user->language,['msg'=>$msg,'user'=>$user])
			->setFrom([TBase::TanukiSetting('sales-email') => \Yii::$app->name])
			->setTo($user->email)
			->setSubject($subject)
			->send();
	}

	/**
	 * cart-notification-en-US
	 * @param $user
	 *
	 * @return bool
	 */
	public static function cartNotifyToUserEmail($user) {

		$subject = self::mailSubject($user->language,'cartNotifyToUserEmail');

		return Yii::$app->mailer->compose('shopping/cart-notification-'.$user->language, [
			'user' => $user
		])
	        ->setTo($user->email)
	        ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
	        ->setSubject($subject)
	        ->send();
	}

	/**
	 * @param $data
	 *
	 * @return bool
	 */
	public static function auctionWinnerMail($data){

		$subject = self::mailSubject($data->user->language,'auctionWinnerMail');

		return Yii::$app->mailer->compose('auction/auction-winner-'.$data->user->language, [
			'model' => $data,
		])
        ->setTo($data->user->email)
        ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
        ->setSubject($subject)
        ->send();
	}

	/**
	 * @param $model
	 * @param $email
	 *
	 * @return bool
	 */
	public static function adminContactEmail($model,$email) {
		return Yii::$app->mailer->compose('admin/contact',['model'=>$model,'email'=>$email])
                ->setTo($email)
				->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
                ->setSubject('Новое сообщение от клиента (страница Контакты)')
                ->setTextBody($model->body)
                ->send();
	}

	public static function mailSubject($language, $type) {
		$txt = '';

		if ($language == 'en-US') {
			switch ($type) {
				case 'userRegistrationEmail' :
					$txt =  "Registration on Tanuki Shop - please confirm your email";
					break;
				case 'passwordResetEmail' :
					$txt =  "Reset password";
					break;
				case 'userQueryReplyEmail' :
					$txt =  "You have a new message";
					break;
				case 'cartNotifyToUserEmail' :
					$txt =  "Selected goods available!";
					break;
				case 'auctionWinnerMail' :
					$txt =  "You have won an auction on Tanuki Shop!";
					break;
				case 'userNewBuyOutMail' :
					$txt =  "You have won an auction on Tanuki Shop!";
					break;

			}
		} else {
			switch ($type) {
				case 'userRegistrationEmail' :
					$txt =  "Регистрация на Tanuki Shop - пожалуйста подтвердите свою электронную почту";
					break;
				case 'passwordResetEmail' :
					$txt =  "Восстановление пароля";
					break;
				case 'userQueryReplyEmail' :
					$txt =  "Новое сообщение";
					break;
				case 'cartNotifyToUserEmail' :
					$txt =  "Товары в наличие!";
					break;
				case 'auctionWinnerMail' :
					$txt =  "Вы выиграли аукционный лот на Tanuki Shop!";
					break;
				case 'userNewBuyOutMail' :
					$txt =  "Вы успешно купили товар на аукционе Yahoo на Tanuki Shop!";
					break;
			}
		}
		return $txt;
	}
}