<?php
/*
author :: Pitt Phunsanit
website :: http://plusmagi.com
change language by get language=EN, language=TH,...
or select on this widget
*/

namespace common\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
class LanguageSwitcher extends Widget
{
    /* ใส่ภาษาของคุณที่นี่ */
    public $languages = [];

    public function init()
    {
        if(php_sapi_name() === 'cli')
        {
            return true;
        }

        parent::init();

        $this->languages = yii\helpers\ArrayHelper::map(\common\models\SiteLanguages::findAll(['status'=>1]), 'code', 'title');

        $cookies = Yii::$app->response->cookies;
        $languageNew = Yii::$app->request->get('language');
        if($languageNew)
        {
            if(isset($this->languages[$languageNew]))
            {
                Yii::$app->language = $languageNew;
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => $languageNew
                ]));
            }
        }
        elseif($cookies->has('language'))
        {
            Yii::$app->language = $cookies->getValue('language');
        }

    }

    public function run()
    {
        $result = \common\models\SiteLanguages::findAll(['status'=>1]);
        if ($result) {
            echo '<ul class="language-ul">';
            foreach ($result as $lang) {
                $img = Html::img($lang->flag,['alt'=>$lang->title]);
                $link = Html::a($img,[Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,'language'=>$lang->code]);
                echo "<li>{$link}</li>";
            }
            echo '</ul>';
        }

    }

}