<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 29/06/16
 * Time: 11:27 AM
 */

namespace common\components;


use common\models\LabelsDetail;
use yii;
use yii\helpers\Html;

trait Language
{
    public static function getLanguage() {
        $result = \common\models\SiteLanguages::findAll(['status'=>1]);
        if ($result) {
            foreach ($result as $lang) {
	            $img = Html::img(yii\helpers\Url::to('@web'.$lang->flag, true),['alt'=>$lang->title]);
                $link = Html::a($img,['site/lang-switch','lang'=>$lang->code]);
                echo "&nbsp;{$link}";
            }
        }
    }

    public static function SetLanguage($lang = 'ru-RU')
    {
        $url = str_ireplace('www.','',$_SERVER['SERVER_NAME']);
        setcookie("googtrans", "", time()-3600,'/','');
        setcookie("googtrans", "", time()-3600,'/','.'.$url);

        $session = Yii::$app->session;
        setcookie("zLang","1", strtotime( '+1 year' ), "/",'.'.$url);
        if ($lang == 'en-US') {
            $session->set('zLang', 'en-US');
            setcookie("googtrans","/en/en", strtotime( '+1 year' ), "/",'.'.$url);
        } else{
            $session->set('zLang', 'ru-RU');
            setcookie("googtrans","/en/ru", strtotime( '+1 year' ), "/",'.'.$url);

        }
        return true;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function ShowLbl($slug)
    {
        $data = LabelsDetail::find()
            ->joinWith('labels', false)  // will not get the related models
            ->where(['tnk_labels.slug'=>$slug,'lang_code'=>self::CLang()])
            ->one();
        if ($data) {
            return $data->title;
        } else {
            return $slug;
        }
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function _x($slug)
    {
        return self::ShowLbl($slug);
    }

    /**
     * @return mixed|string
     */
    public static function CLang()
    {
        $session = Yii::$app->session;
        return ($session->has('zLang')) ? $session->get('zLang') : 'ru-RU';
    }

}