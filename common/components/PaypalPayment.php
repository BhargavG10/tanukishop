<?php
namespace common\components;

use common\models\User;
use frontend\models\OrderPayment;
use PayPal\Api\Order;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Session;
use frontend\models\ShippingDetail;
//use common\components\paypal;
use \common\helper\Tanuki;


use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;

class PaypalPayment {
    public static function index ($CardDetail = [], $OrderID = null) {

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ASxyDZ9_YEUB-tVfAnAB-6_m_5WNmYY0Cnlc3D7tQY0e8oD0iZqfETp6_ftrTto4A-TRgLOvZqD7Q3yY',     // ClientID
                'ENQTezMe9oHDu2TR5870pDojLBrmm3jhinRG9bA_VCDrOnoAxEekHWeQcFVd1obYnIGJl6M65vtG2KVk'      // ClientSecret
            )
        );

        $card = new CreditCard();
        $card->setType(strtolower($CardDetail['type']))
            ->setNumber($CardDetail['number'])
            ->setExpireMonth($CardDetail['expire_month'])
            ->setExpireYear($CardDetail['expire_year'])
            ->setCvv2($CardDetail['security_code'])
            ->setFirstName($CardDetail['first_name'])
            ->setLastName($CardDetail['last_name']);

        $fi = new FundingInstrument();
        $fi->setCreditCard($card);

        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

        $itemList = new ItemList();
        $total = 0;
        $product = [];
        if (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts'])>0){
            foreach(\Yii::$app->session['carts'] as $key => $cartItem) {
                $api = new Tanuki();
                $api->singleProductDetail($cartItem['product_code'], $cartItem['shop']);
                $amount = $api->shopProductDetail($cartItem['shop'], 'price');
                $total += ($amount * $cartItem['quantity']);
                $item = new Item();
                $item->setName($cartItem['product_code'])
                    ->setDescription($cartItem['product_code'])
                    ->setCurrency('JPY')
                    ->setQuantity($cartItem['quantity'])
                    ->setPrice(($amount));
                $product[] = $item;
            }
        }
        $itemList->setItems($product);


        $details = new Details();
        $details->setShipping(250)
            ->setSubtotal($total);
        $total = $total+250;

        $amount = new Amount();
        $amount->setCurrency("JPY")
            ->setTotal($total)
            ->setDetails($details);




        $invoice_number = uniqid();
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber($invoice_number);


        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        $request = clone $payment;
        $invoice = clone $transaction;

        try {
            $payment->create($apiContext);
            $OrderModel = \common\models\Order::findOne($OrderID);
            $OrderModel->paypal_id = $payment->getId();
            $OrderModel->paypal_created_time = $payment->getCreateTime();
            $OrderModel->status = $payment->getState();
            $OrderModel->invoice = $invoice->getInvoiceNumber();
            $OrderModel->response = $payment;
            $OrderModel->save();
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo "<pre> Error";
            print_r($ex->getCode()); // Prints the Error Code
            print_r($ex->getData()); // Prints the detailed error message
            die($ex);
        }
        //\common\helper\ResultPrinter::printResult('Create Payment Using Credit Card', 'Payment', $payment->getId(), $request, $payment);
    }
}