<?php

namespace common\components;

use Yii;
use yii\base\Widget;

class SearchBar extends Widget{

    public function init(){
        parent::init();
    }
    public function run()
    {
        $currentOne = false;
        $data = [];
        $select = 'All';
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

        if ($controller == 'amazon' || $controller == "rakuten" || $controller == 'yahoo' || $controller == 'yahoo-auctions') {
            $data = TBase::getHierarchy($controller);
            if (isset($_REQUEST['title']) && $_REQUEST['title'] != '') {
                $select = $_REQUEST['title'];
            }
        }

        if (isset($_REQUEST['cid'])) {
            $category = \common\models\Category::findOne(['ref_id'=>Yii::$app->request->get('cid')]);
            if ($category) {
	            $select = $category->ref_id;
            } else {
	            $select = '';
	            $currentOne = Yii::$app->request->get('cid');
            }
        }

        return $this->render('search-bar',['data'=>$data,'select'=>$select,'controller'=>$controller,'current'=>$currentOne]);
    }
}
?>
