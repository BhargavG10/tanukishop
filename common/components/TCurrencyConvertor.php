<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 20/11/15
 * Time: 4:05 PM
 */

namespace common\components;
use yii;

class TCurrencyConvertor
{
    /*========================================================================================================================================================*/

    public $url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    public $tablename = 'tnk_currency_conversion_table';

    public function update_rates() {
        return $this->check_url();
    }


    /* URL response header checking */

    public function check_url() {

        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        $result = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($statusCode == 200) {
            $this->create_table();
            return $this->parse_ecb();
        } else {
            return "Unable to parse ECB URL";
        }

    }


    /* Load the data from the ECB into an array. */

    public function parse_ecb() {

        $xml = simplexml_load_file($this->url);

        foreach($xml->Cube->Cube->Cube as $rate) {

            $ecb[] = array(
                "rate" => (float)$rate['rate'],
                "currency" => (string)$rate['currency']
            );

        }

        return $this->make_rates($ecb);
    }



    public function make_rates($ecb) {

        $total = 0;
        foreach($ecb as $row) {
            $this->update_table($row['currency'], $row['rate']);
            $total ++;
        }

        $this->update_table('EUR', 1);
        $total++;
        return $total." Rates Updated";
    }


    public function update_table($currency, $rate) {

        $model = new \common\models\CurrencyConversionTable;
        $model->currency = $currency;
        $model->rate = $rate;
        $model->datetime = date('Y-m-d H:i:s');
        return $model->save(false);
    }

    public function create_table() {
        Yii::$app->db->createCommand()->truncateTable($this->tablename)->execute();
    }


    public function convert($cost, $origin_currency, $destination_currency) {

        $q = "SELECT currency, rate FROM $this->tablename WHERE currency = '$origin_currency' OR currency = '$destination_currency' LIMIT 2";
        $result = Yii::$app->db->createCommand($q)->queryAll();


        if(count($result) == 0)
            return;

        foreach($result as $currency) {

            if($currency['currency'] == $origin_currency)
                $origin_rate = $currency['rate'];

            if($currency['currency'] == $destination_currency)
                $destination_rate = $currency['rate'];

        }

        if($origin_currency == $destination_currency) { // No conversion required
            return $this->get_currency_details($destination_currency, 1) . $this->format_number($cost);
        }

        if($origin_currency != 'EUR') {

            $p = $cost / $origin_rate;
            $p = $p * $destination_rate;
            if ($destination_currency == 'CNY') {
                return $this->format_number($p). $this->get_currency_details($destination_currency, 1);
            } else {
                return $this->get_currency_details($destination_currency, 1) . $this->format_number($p);
            }
//.590224
        }
    }

    public function floatConvert($cost, $origin_currency, $destination_currency) {

        $q = "SELECT currency, rate FROM $this->tablename WHERE currency = '$origin_currency' OR currency = '$destination_currency' LIMIT 2";
        $result = Yii::$app->db->createCommand($q)->queryAll();


        if(count($result) == 0)
            return;

        foreach($result as $currency) {

            if($currency['currency'] == $origin_currency)
                $origin_rate = $currency['rate'];

            if($currency['currency'] == $destination_currency)
                $destination_rate = $currency['rate'];

        }

        if($origin_currency == $destination_currency) { // No conversion required
            return $this->get_currency_details($destination_currency, 1) . $this->format_number($cost);
        }

        if($origin_currency != 'EUR') {

            $p = $cost / $origin_rate;
            $p = $p * $destination_rate;
            if ($destination_currency == 'CNY') {
                return $this->format_number($p). $this->get_currency_details($destination_currency, 1);
            } else {
                return $this->format_number($p,true);
            }

        }
    }

    function format_number($n,$float = false) {

        if (is_numeric($n) || is_float($n)) {
            $n = number_format((float)$n, 2);
            if (!$float) {
	            $n = preg_replace( '/\..*/', '', $n );
            }
            return $n;
        } else {
            return $n;
        }
    }


    /* Get the currency symbol for the currency  */
    public  function get_currency_details($currency_code, $split) {

        switch($currency_code) {

            case('AUD'):
                $cur = "Australian Dollar|&#36;";
                break;

            case('BGN'):
                $cur = "Bulgaria Lev|&#1083;&#1074;";
                break;

            case('BRL'):
                $cur = "Brazil Real|R&#36;";
                break;

            case('CAD'):
                $cur = "Canada Dollar|C&#36;";
                break;

            case('CHF'):
                $cur = "Switzerland Franc|&#165;";
                break;

            case('CZK'):
                $cur = "Czech Republic Koruna|K&#269;";
                break;

            case('DKK'):
                $cur = "Denmark Krone|kr";
                break;

            case('EUR'):
                $cur = "Euro|&#8364;";
                break;

            case('GBP'):
                $cur = "Pound|&#163;";
                break;

            case('HKD'):
                $cur = "Hong Kong Dollar|&#36;";
                break;

            case('HRK'):
                $cur = "Croatia Kuna|kn";
                break;

            case('HUF'):
                $cur = "Hungary Forint|Ft";
                break;

            case('IDR'):
                $cur = "Indonesia Rupiah|Rp";
                break;

            case('ILS'):
                $cur = "Israel Shekel|&#8362;";
                break;

            case('INR'):
                $cur = "India Rupee|&#8377;";
                break;

            case('JPY'):
                $cur = "Japan Yen|&#165;";
                break;

            case('KRW'):
                $cur = "Korea (South) Won|&#8361;";
                break;

            case('LTL'):
                $cur = "Lithuania Litas|Lt";
                break;

            case('LVL'):
                $cur = "Latvia Lat|Ls";
                break;

            case('MXN'):
                $cur = "Mexico Peso|&#36;";
                break;

            case('MYR'):
                $cur = "Malaysia Ringgit|RM";
                break;

            case('NOK'):
                $cur = "Norway Krone|kr";
                break;

            case('NZD'):
                $cur = "New Zealand Dollar|&#36;";
                break;

            case('PHP'):
                $cur = "Philippines Peso|&#8369;";
                break;

            case('PLN'):
                $cur = "Poland Zloty|&#122;&#322;";
                break;

            case('RON'):
                $cur = "Romania New Leu|&#108;&#101;&#105;";
                break;

            case('RUB'):
                $cur = "Russia Ruble|₽";
                break;

            case('SEK'):
                $cur = "Sweden Krona|kr";
                break;

            case('SGD'):
                $cur = "Singapore Dollar|&#36;";
                break;

            case('THB'):
                $cur = "Thailand Baht|&#3647;";
                break;

            case('TRY'):
                $cur = "Turkey Lira|&#8356;";
                break;

            case('USD'):
                $cur = "United States Dollar|&#36;";
                break;

            case('ZAR'):
                $cur = "South Africa Rand|R";
                break;

            case('CNY'):
                    $cur = "Chinese yuan|元";
                break;

        }

        if($split == 0) { // Return just the Currency Name (United States Dollar)
            return substr($cur, 0, strpos($cur, '|'));
        }

        if($split == 1) { // Return just the Currency Symbol ($)

            $symbol = substr($cur, strpos($cur, '|') + 1);
            return $symbol;
        }

    }

}