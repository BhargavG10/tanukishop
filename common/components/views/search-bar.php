<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;
?>
<div class="header-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-4 search-menu-bar-tab">
                <?php
                $cat =  ['rakuten','yahoo','amazon','yahoo-auctions'];
                $actions = ['index','list','detail'];
                $cntr =Yii::$app->controller->id;
                $action =Yii::$app->controller->action->id;
                    if (
                        in_array($cntr,$cat) &&
                        in_array($action,$actions)
                    ) {
                        if ($current) {
	                        $data = ['All'=>TBase::_x('ALL_CATEGORIES'),$current=>TBase::_x('CURRENT_CATEGORY')] + $data;
	                        $select = $current;
                        } else {
	                        $data = ['All'=>TBase::_x('ALL_CATEGORIES')] + $data;
                        }
                ?>
                    <div class="search_bg inner-search">
                        <form id="w1" action="<?=Yii::$app->urlManager->createAbsoluteUrl([$controller.'/list'])?>" method="GET">
                        <div class="row">
                            <div class="col-sm-10 search_wb">
                                <div class="col-sm-4 col-xs-6" style="padding: inherit;">
                                    <?php
                                    if (count($data) > 0){
                                        echo Html::DropDownList('cid',$select,$data,['class'=>'c-select']);
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-5 col-xs-6">
                                    <input class="form-control" name="s" value="<?=(isset($_REQUEST['s']) && $_REQUEST['s'] != '') ? $_REQUEST['s'] : ''; ?>" type="text" placeholder="<?=TBase::ShowLbl('search')?>">
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <label class="lbl"><?=($controller=='yahoo-auctions') ? 'Y!Auctions' : $controller; ?></label>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-6"><input type="submit" class="search_btn" name="search" value=""></div>
                        </div>
                    </form>
                    </div>
                <?php } else {
	                    if(TBase::CLang() == 'en-US') {
		                    $fb = 'facebook_url';
		                    $in = 'instagram_url';
	                    } else {
		                    $fb = 'facebook_url_russian';
		                    $in = 'instagram_url_russian';
	                    }
                        ?>


                    <div class="row">
                        <div class="col-sm-3"><label style="margin-bottom: 0px;"><?=TBase::ShowLbl('join_our_online_community'); ?></label></div>
                        <div class="col-sm-4 col-xs-4" style="padding: 0px;">
                            <a class="button fb" href="<?=TBase::TanukiSetting($fb)?>" target="_blank">
                              <span>
                                <?=TBase::ShowLbl('facebook_like') ?>
                              </span>
                            </a>
                        </div>
                        <div class="col-sm-4 col-xs-4" style="padding: 0px;">
                            <a class="button in" href="<?=TBase::TanukiSetting($in)?>" target="_blank">
                              <span>
                                <?=TBase::ShowLbl('INSTAGRAM_LIKE') ?>
                              </span>
                            </a>
                        </div>

                    </div>
                <?php } ?>
            </div>
            <div class="col-md-8 col-xs-12 menu-tab-link">
                <ul class="product-log">
                    <?php if (TBase::TanukiSetting('Yahoo-Auction') == 'Enable') { ?>
                        <li class="yahoo-auction-menu">
                            <?=Html::a(TBase::ShowLbl('YAHOO_AUCTION'),['yahoo-auctions/index']); ?>
                        </li>
                    <?php } ?>
                    <?php if (TBase::TanukiSetting('Amazon') == 'Enable') { ?>
                        <li class="amazon-menu">
                            <?=Html::a(TBase::ShowLbl('amazon'),['amazon/index']); ?>
                        </li>
                    <?php } ?>
                    <?php if (TBase::TanukiSetting('Yahoo-Shopping') == 'Enable') { ?>
                        <li class="yahoo-shopping-menu">
                            <?=Html::a(TBase::ShowLbl('yahoo_shopping'),['yahoo/index']); ?>
                        </li>
                    <?php } ?>
                    <?php if (TBase::TanukiSetting('Rakuten') == 'Enable') { ?>
                        <li class="rakuten-menu">
                            <?=Html::a(TBase::ShowLbl('rakuten'),['rakuten/index']); ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>