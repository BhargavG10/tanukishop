<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 20/11/15
 * Time: 4:05 PM
 */

namespace common\components;

use common\models\CartProducts;
use common\models\Category;
use common\models\Countries;
use common\models\Setting;
use common\models\User;
use yii\helpers\Html;
use yii;

/**
 * Class TBase
 * @package common\components
 */
class TBase
{
    use Language;


    /**
     * @param $text
     * @return mixed|string
     */
    public static function slug($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }


    /**
     * @param string $shop
     * @return array
     */
    public static function getHierarchy($shop='rakuten') {
        $options = [];
        $lang = self::CLang();
        $shop = ($shop == 'yahoo-auctions') ? 'yahoo_auctions' : $shop;
        $parents = Category::find()->where(["parent_id"=>"0",'shop'=>$shop])->all();
        foreach($parents as $id => $p) {
            $children = Category::find()->where("parent_id=:parent_id AND shop='$shop'", [":parent_id"=>$p->id])->all();
            $child_options = [];
            $title = ($lang == 'en-US') ? htmlspecialchars_decode($p->title_en) : htmlspecialchars_decode($p->title_ru);
            $child_options[$p->ref_id] = htmlspecialchars_decode($title);
            foreach($children as $child) {
                $title = '';
                $title = ($lang == 'en-US') ? htmlspecialchars_decode($child->title_en) : htmlspecialchars_decode($child->title_ru);

                $child_options[$child->ref_id] = htmlspecialchars_decode($title);
            }
            if ($lang == 'en-US') {
                $options[$p->title_en] = $child_options;
            } else {
                $options[$p->title_ru] = $child_options;
            }
        }
        return $options;
    }

    /**
     * @param string $class
     */
    public static function MainMenu($class = '')
    {
        $menuItems = [
            ['label' => self::ShowLbl('home'), 'url' => ['/site/index']],
            ['label' => self::ShowLbl('about'), 'url' => ['/site/about']],
            ['label' => self::ShowLbl('HELP_MENU'), 'url' => ['/site/help']],
            ['label' => self::ShowLbl('fee'), 'url' => ['/site/fee']],
            ['label' => self::ShowLbl('shipping'), 'url' => ['/site/shipping']],
            ['label' => self::ShowLbl('contact_us'), 'url' => ['/site/contact']],
        ];
        echo yii\bootstrap\Nav::widget([
            'options' => ['class' => $class],
            'items' => $menuItems,
        ]);
    }


    /**
     *
     */
    public static function LoginMenu()
    {
        if (Yii::$app->user->isGuest) { ?>
            <div class="row">
                <div class="col-md-12">
                    <ul class="pull-right sign-section">
                        <li>
                            <?= Html::a(self::ShowLbl('sign_in'), ['site/login'], ['class' => 'btn btn-sign']) ?>
                        </li>
                        <li>
                            <?= Html::a(self::ShowLbl('sign_up'), ['site/login'], ['class' => 'btn btn-sign btn-sign-up']) ?>
                        </li>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(['checkout/index'],true)],true) ?>">
                                <?=Html::img(yii\helpers\Url::to('@web/tnk/images/cart.png', true),['alt'=>'cart']);?>
                                <span class="badge">0</span>&nbsp;<?= self::ShowLbl('item_in_shopping_cart') ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php } else { ?>
            <!-- After Login section --->
            <div class="row second-inner">
                <div class="col-md-12">
                    <div class="pull-left welcome-name">
                        <h6><?= self::ShowLbl('welcome') ?>&nbsp;<span>
                                <?php
                                $name = (strlen(Yii::$app->user->identity->fullname) > 15) ? substr(Yii::$app->user->identity->fullname,0,15).'...' : Yii::$app->user->identity->fullname;
                                echo Html::a($name, ['user/update']);
                                ?>
                            </span>
                        </h6>
                    </div>
                    <div class="my-balance welcome-name pull-left">
                        <h6 style="padding:0px 0px 0px 21px!important">
                            <?php
                            $string = '<span style="color: #292929;">'.self::ShowLbl('MY_BALANCE').'</span>';
                            $string .= '&nbsp;&nbsp;'.number_format(self::currentUserBalance());
                            $string .= ' JPY';
                            echo Html::a($string, ['user/add-fund']) ?>
                        </h6>
                    </div>
                    <div class="pull-left" style="margin-top: 3px;">
	                    <?=Html::a('+ '.self::_x('ADD_FUND'),['user/add-fund'],['class'=>'add_fund_btn'])?>
                    </div>
                    <ul class="pull-right sign-section">
                        <li><?= Html::a(self::ShowLbl('my_page'), ['user/my-page']) ?></li>
                        <li><?= Html::a(self::ShowLbl('wishlist'), ['user/fav-products']) ?></li>
                        <li><?= Html::a(self::ShowLbl('sign_out'), ['/user/logout'], ['data' => ['method' => 'post']]) ?></li>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['cart-products/index']) ?>">
                                <?=Html::img(yii\helpers\Url::to('@web/tnk/images/cart.png', true),['alt'=>'cart']);?>
                                <span
                                    class="badge"><?= Yii::$app->user->identity->cartCount; ?></span>&nbsp;<?= self::ShowLbl('item_in_shopping_cart') ?>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        <?php }
    }

    public static function countryList($id = '') {

	    if ($id) {
	        $countries = Countries::findOne($id);
	        if ($countries) {
	            return $countries->title;
            }
        } else {

		    $countries = Countries::find()->all();
		    return yii\helpers\ArrayHelper::map( $countries, 'id', 'title' );
	    }
    }

    public function countryDetail($id) {

	    if ($id) {
	        $countries = Countries::findOne($id);
	        if ($countries) {
	            return $countries->title;
            }
        }
        return '';
    }

    public function saveImage($inPath,$outPath)
    { //Download images from remote server
        $in=    fopen($inPath, "rb");
        $out=   fopen($outPath, "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }

    public function getCurrency($from_Currency, $to_Currency, $amount) {
        $amount = urlencode($amount);
        $from_Currency = urlencode($from_Currency);
        $to_Currency = urlencode($to_Currency);
        $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";exit;
        $ch = curl_init();
        $timeout = 0;
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt ($ch, CURLOPT_USERAGENT,
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $rawdata = curl_exec($ch);
        curl_close($ch);
        $data = explode('bld>', $rawdata);
        $data = explode($to_Currency, $data[1]);

        return round($data[0], 2);
    }

    public static function checkoutSideBarMenu($cart = false) {
        if ($cart) {
            $menuItems = [
                ['label' => '<i class="fa fa-shopping-cart" aria-hidden="true"></i><br/>' . self::ShowLbl('CART_ITEMS'), 'url' => ['/checkout/index']],
                ['label' => '<i class="fa fa-truck"></i><br/>' . self::ShowLbl('SHIPPING_ADDRESS'), 'url' => ['/checkout/shipping']],
                ['label' => '<i class="fa fa-th-list"></i><br/>' . self::ShowLbl('ORDER_CONFIRMATION'), 'url' => ['/checkout/confirm']],
                ['label' => '<i class="fa fa-credit-card"></i><br/>' . self::ShowLbl('PAYMENT_METHOD'), 'url' => ['/checkout/pay']],
            ];
        } else {
            $menuItems = [
                ['label' => '<i class="fa fa-shopping-cart"></i><br/>' . self::ShowLbl('CART_ITEMS'), 'url' => ['/checkout/index']],
                ['label' => '<i class="fa fa-truck"></i><br/>' . self::ShowLbl('SHIPPING_ADDRESS'), 'url' => '#'],
                ['label' => '<i class="fa fa-th-list"></i><br/>' . self::ShowLbl('ORDER_CONFIRMATION'), 'url' => '#'],
                ['label' => '<i class="fa fa-credit-card"></i><br/>' . self::ShowLbl('PAYMENT_METHOD'), 'url' => '#'],
            ];
        }
        return yii\bootstrap\Nav::widget([
            'options' => ['class' => 'test notranslate'],
            'items' => $menuItems,
            'encodeLabels' => false,
        ]);
    }

    public static function TanukiCharges($key='tanuki-charges'){
        $data = Setting::findOne(['setting_key'=>$key]);
        return $data->setting_value;
    }

    public static function TanukiSetting($setting_key){
        $data = Setting::findOne(['setting_key'=>$setting_key]);
        return $data->setting_value;
    }
    public static function settingConst($setting_key){
        $data = Setting::findOne(['setting_key'=>$setting_key]);
        return $data->setting_value;
    }

    /**
     * @param $image
     * @param $id
     * @param $shop
     * @param string $path
     * @return string
     */
    public static function saveOrderImage($image, $id, $shop, $path = false){
        $path = ($path) ? $path : Yii::$app->params['order-image-path'];
        if ($id) {
            if ($image) {
                if (exif_imagetype($image) == IMAGETYPE_PNG) {
                    $ext = 'png';
                } else if (exif_imagetype($image) == IMAGETYPE_JPEG) {
                    $ext = 'jpeg';
                } else if (exif_imagetype($image) == IMAGETYPE_GIF) {
                    $ext = 'gif';
                }
                $name = $id.'_'.basename($image).'.'.$ext;
                $img = $path.$name;
                @copy($image, $img);
                return $name;
            }
        }
        return $image;
    }


    public static function footer(){ ?>

        <div class="loading-extend-text" style="display: none;"></div>
        <div class="loading-text" style="display: none;">
            <i class="fa fa-times" aria-hidden="true" id="close-loading-text"></i>
            <i class="fa fa-check-circle" aria-hidden="true"></i><br/><br/>
            <span id="loading-text-id" class="notranslate"></span>
        </div>

        <footer class="notranslate">
            <div class="container">
                <div class="">
                    <?php $pageData = \common\models\Page::findOne(10); ?>
                    <div class="col-md-6">
                        <?php
                        if (TBase::CLang() == 'ru-RU') {
                            echo $pageData->detail_ru;
                        } else {
                            echo $pageData->detail_en;
                        }

                        ?>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <?=Html::img('@web/img/footer_new-.jpg')?>
                                <p class="text-right">© <?=date('Y'); ?> <?=TBase::ShowLbl('all_rights_reserved');?></p>
                            </div>
                            <div class="col-sm-6 " style="    padding-left: 0px;">
                                <div class="footer-logo">
                                    <a href="/">
                                        <?php
	                                    $key = (TBase::CLang() == 'en-US') ? 'footer_logo_en' : 'footer_logo_ru';
                                        echo Html::img(TBase::TanukiSetting($key));
                                        ?>
                                    </a>
                                </div>
                                <h4 style="margin-top: 20px" class="text-right"><?=TBase::ShowLbl('social_media');?></h4>
                                <ul class="social-ul text-right">
	                                <?php
	                                if(TBase::CLang() == 'en-US') {
	                                    $fb = 'facebook_url';
                                        $in = 'instagram_url';
	                                } else {
		                                $fb = 'facebook_url_russian';
		                                $in = 'instagram_url_russian';
                                    }
	                                //echo Html::img(TBase::TanukiSetting($key));
	                                ?>
                                    <li><a href="<?=self::TanukiSetting($fb); ?>"><i class="fa fa-facebook" aria-hidden="true"></i> <?=TBase::ShowLbl('facebook_like'); ?> </a></li>
                                    <li><a href="<?=self::TanukiSetting($in); ?>"><i class="fa fa-instagram" aria-hidden="true"></i><?=TBase::ShowLbl('INSTAGRAM_LIKE'); ?> </a></li>
                                </ul>
<!--                                <p class="text-right">Web design by: <b><a href="http://www.dokolink.com">DokoLink</a></b>.</p>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <?php }


    public static function header(){ ?>
        <header class="notranslate">
            <div class="m_container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 main-logo clearfix">
                            <a href="<?=Yii::$app->homeUrl; ?>">
                                <?php
                                if(TBase::CLang() == 'en-US') {
                                    echo Html::img(TBase::TanukiSetting('logo_en'));
                                } else {
                                    echo Html::img(TBase::TanukiSetting('logo_ru'));
                                } ?>

                            </a>
                        </div>
                        <div class="col-sm-9 mt10 right-menu clearfix">
                            <div class="row top-menu">
                                <div class="col-md-7 main-menu">
                                <!-- Static navbar -->
                                    <nav class="navbar navbar-default">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div id="navbar" class="navbar-collapse collapse">
                                            <?=self::MainMenu('nav navbar-nav') ?>
                                        </div>
                                        <!--/.nav-collapse -->
                                        <!--/.container-fluid -->
                                    </nav>
                                    <!-- Main component for a primary marketing message or call to action -->
                                </div>
                                <?php /* ?><div class="exchange-rate clearfix">
                                    <label for="1" class="exchange_rate_label pull-left"><?=self::ShowLbl('exchange_rate'); ?></label>
                                    <?php
                                    if (self::CLang() == 'ru-RU') { ?>
                                        <div class="pull-left exchange_rate_div" "> 1 JPY = <?=self::convertCurrency(1,'JPY','RUB'); ?> RUB</div>
                                    <?php } else { ?>
                                        <div class="pull-left exchange_rate_div" "> 1 USD = <?=self::convertCurrency(1,'USD','JPY'); ?> JPY</div>
                                    <?php }
                                    ?>
                                </div><?php */ ?>
                                <div class="language-div">
                                    <?=self::getLanguage() ?>
                                </div>
                            </div>
                            <hr>
                            <?=self::LoginMenu() ?>
                            <!-->
                        </div>
                    </div>
                </div>
                <?php echo \common\components\SearchBar::widget() ?>
            </div>
        </header>
    <?php }


    public static function convertCurrency($amount, $from, $to){
	    $model = new \common\components\TCurrencyConvertor;
	    return $model->floatConvert((float)$amount,$from,$to);
//        if (Yii::$app->request->hostName == "tanukishop.local") {
//            return \common\models\CurrencyTable::convert((float)$amount,$from,$to);
//        }
//	    echo $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from . $to .'=X';
//
//	    $filehandler = @fopen($url, 'r');
//
//	    if ($filehandler) {
//
//		    $data = fgets($filehandler, 4096);
//
//		    fclose($filehandler);
//
//            if ($data) {
//                $InfoData = explode( ',', $data );
//                return number_format($InfoData[1],2);
//            } else {
//                return $amount;
//            }
//	    }
//	    return $amount;

    }

    /**
     * @return string
     */
    public static function userEmail(){
        if (isset(Yii::$app->user->identity->email)) {
            return Yii::$app->user->identity->email;
        } else {
            return 'order@tanukishop.com';
        }
    }

    /**
     * @param $ID
     * @return string
     */
    public static function getNameChatTitle($ID)
    {
        return ($ID == 'admin') ? Html::img(Yii::$app->params['website-url'].'img/favicon.png') : 'ME';
    }

    /**
     * @return string
     */
    public static function uuid()
    {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0010
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * @param array $data
     * @param int $parent
     * @return array
     */
    public static function buildTree(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = self::buildTree($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }

    /**
     * @param $tree
     * @param int $r
     * @param null $p
     */
    public static function printTree($tree, $r = 0, $p = null,$parent = 0,$selfID = 0) {
        foreach ($tree as $i => $t) {

            if ($t['id'] == $selfID) {
                continue;
            }

            $dash = ($t['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ';

            if ((int)$t['id'] === (int)$parent) {

	            printf( "\t<option selected='selected' value='%d'>%s%s</option>\n", $t['id'], $dash, $t['title_en'] );
            } else {
	            printf( "\t<option value='%d'>%s%s</option>\n", $t['id'], $dash, $t['title_en'] );
            }
            if (isset($t['_children'])) {
                self::printTree($t['_children'], $r+1, $t['parent_id'],$parent,$selfID);
            }
        }
        return true;
    }

    /**
     * @return bool|int
     */
    public static function currentUserBalance() {
        if (Yii::$app->user->id) {
            $data = User::findOne(Yii::$app->user->id);
            return $data->credit;
        } else {
            return false;
        }
    }
    public static function sortBy($sort='cbids',$order='a',$srt=1) {
        $params = $_REQUEST;
        if (isset($params['sort'])) {
            unset($params['sort']);
        }
        if (isset($params['order'])) {
            unset($params['order']);
        }
        if (isset($params['srt'])) {
            unset($params['srt']);
        }
	    $params['sort'] = $sort;
	    $params['order'] = $order;
	    $params['srt'] = $srt;
	    $url = http_build_query($params, '', '&amp;');

	    return \yii\helpers\Url::to(['yahoo-auctions/list'],true).'?'.$url;
    }
}