<?php
return [
    'adminEmail' => 'dgutsu@gmail.com',
    'tanukiCharges' => '300',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'website-url'=>'https://www.tanukishop.com/',
    'order-image-url' => 'https://tanukishop.com/order-images/',
    'auction-image-url' => 'https://tanukishop.com/auction-buyout-images/',
    'auction_key' => 'QfC9WPxGzPDFriKD0lgZgm100GAZ9jQfCcjbbQnVvhQ=',
    'no-image'=>'https://tanukishop.com/img/no-image.png',
    'site_ip' => '160.16.198.56',
    'auction-image-path' => '/var/www/html/tanukishop.com/public_html/frontend/web/auction-buyout-images/',
    'order-image-path' => '/var/www/html/tanukishop.com/public_html/frontend/web/order-images/'
];

