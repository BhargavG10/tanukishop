<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_auction_won_list}}".
 *
 * @property integer $id
 * @property string $auction_id
 * @property string $stored_on
 */
class AuctionWonList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_auction_won_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auction_id', 'stored_on'], 'required'],
            [['stored_on'], 'safe'],
            [['auction_id'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auction_id' => Yii::t('app', 'Auction ID'),
            'stored_on' => Yii::t('app', 'Stored On'),
        ];
    }
}
