<?php

namespace common\models;

use common\components\TBase;
use Yii;

/**
 * This is the model class for table "{{%tnk_currency_conversion_table}}".
 *
 * @property string $id
 * @property string $num_code
 * @property double $char_code
 * @property double $nominal
 * @property double $name
 * @property double $value
 * @property datetime $datetime
 */
class CurrencyTable extends \yii\db\ActiveRecord
{
	public $url= 'http://www.cbr.ru/scripts/XML_daily.asp';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_currency_tble}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num_code','char_code','nominal','name','value','datetime'], 'required'],
            [['num_code','char_code','nominal','name','value','datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num_code' => Yii::t('app', 'Num Code'),
            'nominal' => Yii::t('app', 'Nominal'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
            'datetime' => Yii::t('app', 'datetime'),
        ];
    }

	public function check_url($print) {

		$curl = curl_init($this->url);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		$result = curl_exec($curl);
		$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		$print->stdout("status code: \n".$statusCode, \yii\helpers\Console::FG_RED, \yii\helpers\Console::BOLD);
		if($statusCode == 200) {
			$this->create_table();
			return $this->parse_ecb();
		} else {
			return "Unable to parse ECB URL";
		}
	}

	public function create_table() {
		Yii::$app->db->createCommand('delete from tnk_currency_tble where 1')->execute();
	}

	public function parse_ecb() {

		$xml = simplexml_load_file($this->url);
		foreach($xml->Valute as $rate) {

			$ecb[] = array(
				"num_code" => (int)$rate->NumCode,
				"char_code" => (string)$rate->CharCode,
				"nominal" => (int)$rate->Nominal,
				"name" => (string)$rate->Name,
				"value" => (float)str_replace(",",".",(string)$rate->Value),
			);
		}
		$ecb[] = array(
			"num_code" => 111,
			"char_code" => 'RUB',
			"nominal" => '1',
			"name" => 'RUB',
			"value" => '1',
		);

		return $this->make_rates($ecb);
	}

	public function make_rates($ecb) {
		$total = 0;
		foreach($ecb as $row) {
			if (in_array($row['char_code'],['USD', 'EUR', 'CNY', 'RUB','JPY'])) {
				$model            = new self();
				$model->num_code  = $row['num_code'];
				$model->char_code = $row['char_code'];
				$model->nominal   = $row['nominal'];
				$model->name      = $row['name'];
				$model->value     = $row['value'];
				$model->datetime  = date( 'Y-m-d H:i:s' );
				if ( $model->save( false ) ) {
					$total ++;
				}
			}
		}
		$total++;
		return $total." Rates Updated";
	}

	public static function convert($cost, $origin_currency, $destination_currency) {

		$q = "SELECT `nominal`,`char_code` as `currency`, `value` as `rate` FROM `tnk_currency_tble` WHERE `char_code` = '$origin_currency' OR `char_code` = '$destination_currency' LIMIT 2";
		$result = Yii::$app->db->createCommand($q)->queryAll();
		if(count($result) == 0)
			return;
		foreach($result as $currency) {

			if($currency['currency'] == $origin_currency)
				$origin_rate = (($currency['rate']+ self::currencyConversion())/$currency['nominal']);

			if($currency['currency'] == $destination_currency)
				$destination_rate = ($currency['rate']/$currency['nominal']);

		}

		if($origin_currency == $destination_currency) { // No conversion required
			return self::get_currency_details($destination_currency, 1) . self::format_number($cost);
		}

		$p = (float)($cost * ($origin_rate/$destination_rate));
		if ($destination_currency == 'CNY') {
			return self::format_number($p). self::get_currency_details($destination_currency, 1);
		} else {
			return self::get_currency_details($destination_currency, 1) . self::format_number($p);
		}
	}



	public static function floatConvert($cost, $origin_currency, $destination_currency) {

		$q = "SELECT currency, rate FROM `tnk_currency_tble` WHERE currency = '$origin_currency' OR currency = '$destination_currency' LIMIT 2";
		$result = Yii::$app->db->createCommand($q)->queryAll();


		if(count($result) == 0)
			return;

		foreach($result as $currency) {

			if($currency['currency'] == $origin_currency)
				$origin_rate = $currency['rate'];

			if($currency['currency'] == $destination_currency)
				$destination_rate = $currency['rate'];

		}

		if($origin_currency == $destination_currency) { // No conversion required
			return self::get_currency_details($destination_currency, 1) . self::format_number($cost);
		}

		if($origin_currency != 'RUB') {

			$p = $cost / $origin_rate;
			$p = $p * $destination_rate;
			if ($destination_currency == 'CNY') {
				return self::format_number($p). self::get_currency_details($destination_currency, 1);
			} else {
				return self::format_number($p,true);
			}

		}
	}

	public static function format_number($n,$float = false) {

		if (is_numeric($n) || is_float($n)) {
			$n = number_format((float)$n, 2);
			if (!$float) {
				$n = preg_replace( '/\..*/', '', $n );
			}
			return $n;
		} else {
			return $n;
		}
	}


	/* Get the currency symbol for the currency  */
	public static function get_currency_details($currency_code, $split) {

		switch($currency_code) {

			case('AUD'):
				$cur = "Australian Dollar|&#36;";
				break;

			case('BGN'):
				$cur = "Bulgaria Lev|&#1083;&#1074;";
				break;

			case('BRL'):
				$cur = "Brazil Real|R&#36;";
				break;

			case('CAD'):
				$cur = "Canada Dollar|C&#36;";
				break;

			case('CHF'):
				$cur = "Switzerland Franc|&#165;";
				break;

			case('CZK'):
				$cur = "Czech Republic Koruna|K&#269;";
				break;

			case('DKK'):
				$cur = "Denmark Krone|kr";
				break;

			case('EUR'):
				$cur = "Euro|&#8364;";
				break;

			case('GBP'):
				$cur = "Pound|&#163;";
				break;

			case('HKD'):
				$cur = "Hong Kong Dollar|&#36;";
				break;

			case('HRK'):
				$cur = "Croatia Kuna|kn";
				break;

			case('HUF'):
				$cur = "Hungary Forint|Ft";
				break;

			case('IDR'):
				$cur = "Indonesia Rupiah|Rp";
				break;

			case('ILS'):
				$cur = "Israel Shekel|&#8362;";
				break;

			case('INR'):
				$cur = "India Rupee|&#8377;";
				break;

			case('JPY'):
				$cur = "Japan Yen|&#165;";
				break;

			case('KRW'):
				$cur = "Korea (South) Won|&#8361;";
				break;

			case('LTL'):
				$cur = "Lithuania Litas|Lt";
				break;

			case('LVL'):
				$cur = "Latvia Lat|Ls";
				break;

			case('MXN'):
				$cur = "Mexico Peso|&#36;";
				break;

			case('MYR'):
				$cur = "Malaysia Ringgit|RM";
				break;

			case('NOK'):
				$cur = "Norway Krone|kr";
				break;

			case('NZD'):
				$cur = "New Zealand Dollar|&#36;";
				break;

			case('PHP'):
				$cur = "Philippines Peso|&#8369;";
				break;

			case('PLN'):
				$cur = "Poland Zloty|&#122;&#322;";
				break;

			case('RON'):
				$cur = "Romania New Leu|&#108;&#101;&#105;";
				break;

			case('RUB'):
				$cur = "Russia Ruble|₽";
				break;

			case('SEK'):
				$cur = "Sweden Krona|kr";
				break;

			case('SGD'):
				$cur = "Singapore Dollar|&#36;";
				break;

			case('THB'):
				$cur = "Thailand Baht|&#3647;";
				break;

			case('TRY'):
				$cur = "Turkey Lira|&#8356;";
				break;

			case('USD'):
				$cur = "United States Dollar|&#36;";
				break;

			case('ZAR'):
				$cur = "South Africa Rand|R";
				break;

			case('CNY'):
				$cur = "Chinese yuan|元";
				break;

		}

		if($split == 0) { // Return just the Currency Name (United States Dollar)
			return substr($cur, 0, strpos($cur, '|'));
		}

		if($split == 1) { // Return just the Currency Symbol ($)

			$symbol = substr($cur, strpos($cur, '|') + 1);
			return $symbol;
		}
	}

	public function update_rates() {
		return $this->check_url();
	}

	public static function currencyConversion() {
		return TBase::TanukiSetting('currency-adjust');
	}

	public static function convertMoneta($cost, $origin_currency, $destination_currency) {

		$q = "SELECT `nominal`,`char_code` as `currency`, `value` as `rate` FROM `tnk_currency_tble` WHERE `char_code` = '$origin_currency' OR `char_code` = '$destination_currency' LIMIT 2";
		$result = Yii::$app->db->createCommand($q)->queryAll();
		if(count($result) == 0)
			return;
		foreach($result as $currency) {

			if($currency['currency'] == $origin_currency)
				$origin_rate = (($currency['rate']+ self::currencyConversion())/$currency['nominal']);

			if($currency['currency'] == $destination_currency)
				$destination_rate = ($currency['rate']/$currency['nominal']);

		}
		$p = (float)($cost * ($origin_rate/$destination_rate));
		return $p;
	}
}
