<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HomeProduct;

/**
 * HomeProductSearch represents the model behind the search form about `common\models\HomeProduct`.
 */
class HomeProductSearch extends HomeProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'serial'], 'integer'],
            [['shop', 'product_id','section_id', 'status','title','image_url','price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'serial' => $this->serial,
            'section_id' => $this->section_id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'shop', $this->shop])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image_url', $this->image_url])
            ->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
