<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_labels}}".
 *
 * @property integer $id
 * @property string $slug
 *
 * @property TnkLabelsDetail[] $tnkLabelsDetails
 */
class Labels extends \yii\db\ActiveRecord
{

    public $russian;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_labels}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug','title','russian'], 'required'],
            [['slug'], 'unique'],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'title' => Yii::t('app', 'English'),
            'russian' => Yii::t('app', 'Russian'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabelsDetails()
    {
        return $this->hasMany(LabelsDetail::className(), ['labels_id' => 'id']);
    }
}
