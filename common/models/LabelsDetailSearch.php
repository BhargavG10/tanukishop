<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LabelsDetail;

/**
 * LabelsDetailSearch represents the model behind the search form about `common\models\LabelsDetail`.
 */
class LabelsDetailSearch extends LabelsDetail
{
    public $slug;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'labels_id'], 'integer'],
            [['title', 'lang_code','slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LabelsDetail::find();

        $query->joinWith(['labels']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['labels'] = [
            'asc' => ['tnk_labels.slug' => SORT_ASC],
            'desc' => ['tnk_labels.slug' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'labels_id' => $this->labels_id,
        ]);

        $query->andFilterWhere(['like', 'tnk_labels_detail.title', $this->title])
            ->andFilterWhere(['like', 'tnk_labels_detail.lang_code', $this->lang_code])
            ->andFilterWhere(['like', 'tnk_labels.slug', $this->slug]);
        $query->orderBy('id DESC');
        return $dataProvider;
    }
}
