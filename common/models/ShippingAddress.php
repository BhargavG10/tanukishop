<?php

namespace common\models;

use common\components\TBase;
use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_address}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property string $appt_no
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property integer $phone
 * @property string $email
 * @property integer $shipping_weight
 * @property string $shipping_method
 * @property integer $order_id
 */
class ShippingAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_1', 'address_2'], 'string'],
            [['phone', 'order_id','shipping_weight'], 'integer'],
            [['first_name', 'last_name', 'company', 'appt_no', 'city', 'state', 'country', 'zipcode', 'email', 'shipping_method'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', TBase::ShowLbl('FIRST_NAME')),
            'last_name' => Yii::t('app', TBase::ShowLbl('LAST_NAME')),
            'company' => Yii::t('app', TBase::ShowLbl('COMPANY_NAME')),
            'address_1' => Yii::t('app', TBase::ShowLbl('ADDRESS_1')),
            'address_2' => Yii::t('app', TBase::ShowLbl('ADDRESS_2')),
            'appt_no' => Yii::t('app', TBase::ShowLbl('APPT_NO')),
            'city' => Yii::t('app', TBase::ShowLbl('CITY')),
            'state' => Yii::t('app', TBase::ShowLbl('STATE')),
            'country' => Yii::t('app', TBase::ShowLbl('COUNTRY')),
            'zipcode' => Yii::t('app', TBase::ShowLbl('ZIPCODE')),
            'phone' => Yii::t('app', TBase::ShowLbl('PHONE')),
            'email' => Yii::t('app', TBase::ShowLbl('EMAIL')),
            'shipping_weight' => Yii::t('app', TBase::ShowLbl('SHIPPING_WEIGHT')),
            'shipping_method' => Yii::t('app', TBase::ShowLbl('SHIPPING_METHOD')),
            'order_id' => Yii::t('app', TBase::ShowLbl('ORDER_ID')),
        ];
    }

    public function getFullName() {
        $name = '';
        if (isset($this->first_name)) {
            $name .= $this->first_name;
        }

        if (isset($this->last_name)) {
            $name .= ' '.$this->last_name;
        }
        return $name;
    }

    public function getMethod()
    {
        return $this->hasOne(ShippingMethods::className(),['id'=>'shipping_method']);
    }
}
