<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_payments}}".
 *
 * @property integer $id
 * @property double $amount
 * @property string $purpose
 * @property integer $amount_condition
 * @property string $method
 * @property integer $user_id
 * @property string $created_on
 */
class Payments extends \yii\db\ActiveRecord
{
    const BANK_DEPOSIT = 'Bank Deposit';
    const PAYPAL = 'Paypal';
    const CREDIT_CARD = 'Credit Card';
    const PAY_BY_BALANCE = 'Pay By Balance';
    const ADD_PAYMENT = 1;
    const DEDUCT_PAYMENT = 0;
    const USED_PAYMENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_payments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'purpose', 'method', 'user_id', 'created_on'], 'required'],
            [['amount'], 'number'],
            [['amount_condition', 'user_id'], 'integer'],
            [['created_on'], 'safe'],
            [['purpose', 'method'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'amount' => Yii::t('app', 'Amount'),
            'purpose' => Yii::t('app', 'Purpose'),
            'amount_condition' => Yii::t('app', 'Amount Condition'),
            'method' => Yii::t('app', 'Method'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return array
     */
    public static function getPaymentMethod() {
        return [
            self::BANK_DEPOSIT=>self::BANK_DEPOSIT,
            self::PAYPAL=>self::PAYPAL,
            self::CREDIT_CARD=>self::CREDIT_CARD,
            self::PAY_BY_BALANCE=>self::PAY_BY_BALANCE,
            ];
    }
}
