<?php

namespace common\models;

use common\components\MailCamp;
use common\components\TBase;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Link;

/**
 * This is the model class for table "{{%tnk_cart_products}}".
 *
 * @property integer $id
 * @property integer $buyer_id
 * @property string $shop
 * @property string $product_id
 * @property integer $quantity
 * @property string $status
 * @property integer $domestic_shipping
 * @property integer $notify
 * @property string $created_on
 */
class CartProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_cart_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buyer_id', 'shop', 'product_id', 'quantity', 'status', 'created_on'], 'required'],
            [['buyer_id', 'quantity', 'notify','domestic_shipping'], 'integer'],
            [['created_on'], 'safe'],
            [['shop', 'product_id', 'status'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'buyer_id' => Yii::t('app', 'Client'),
            'shop' => Yii::t('app', 'Shop'),
            'product_id' => Yii::t('app', 'Product ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'status' => Yii::t('app', 'Status'),
            'domestic_shipping' => Yii::t('app', 'Domestic Shipping'),
            'notify' => Yii::t('app', 'Notify'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return int|string
     */
    public function totalProduct()
    {
        return self::find()->where(['buyer_id'=>$this->buyer_id])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuyer() {
        return $this->hasOne(User::className(),['id'=>'buyer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(),['id'=>'buyer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParams() {
        return $this->hasMany(CartProductAttributes::className(),['cart_id'=>'id']);
    }

    /**
     * @param $user
     * @param $cartList
     * @return bool
     */
    public static function notifyUser($user) {
    	return MailCamp::cartNotifyToUserEmail($user);
    }

    /**
     * @return int
     */
    public static function deleteCartAfterOrder()
    {
       return CartProducts::deleteAll(['buyer_id'=>Yii::$app->user->id,'status'=>'approve']);
    }

    public static  function checkIfExist($product_id,$shop)
    {
        return CartProducts::findOne(['product_id'=>$product_id,'shop'=>$shop,'buyer_id'=>Yii::$app->user->id]);
    }

	/**
	 * @return string
	 */
    public function getOuterLink()
    {
        switch($this->shop) {
	        case 'amazon':
		        return 'https://www.amazon.co.jp/dp/'.$this->product_id;
	        	break;
	        case 'yahoo':
		        $parts = explode('_',$this->product_id);
		        return 'https://store.shopping.yahoo.co.jp/'.$parts[0].'/'.$parts[1].'.html';
	        	break;
	        case 'rakuten':
		        return ['order/rakuten-url','itemCode'=>$this->product_id];
	        	break;
	        default:
	        	return $this->product_id;
	        	break;
        }
    }

    public function listAttr() {
    	$html = '';
	    if ($this->params) {
	    	foreach ($this->params as $params) {
			    $html .= '<p>'.$params->attribute_name.': '.$params->attribute_value.'</p>';
		    }
		    return $html;
	    }
	    return true;
    }

    public static function notification(){
	    $count = CartProducts::find()->where(['status'=>'pending'])->count();
    	if ($count) {
    		return '<i class="badge">'.$count.'</i>';
	    }
    }

    public function getLatestDate() {
    	return CartProducts::find()
		    ->select('created_on')
            ->where(['buyer_id'=>$this->buyer_id])
		    ->orderBy('id DESC')
    	    ->asArray()
    	    ->one();
    }
}

