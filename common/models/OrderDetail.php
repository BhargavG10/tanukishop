<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_order_detail}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $shop
 * @property string $product_id
 * @property string $product_name
 * @property string $product_image
 * @property integer $quantity
 * @property integer $price
 * @property integer $tanuki_fee
 * @property integer $domestic_shipping
 * @property integer $weight
 * @property integer $bank_fees
 * @property integer $other_fees
 * @property string $currency
 */
class OrderDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_order_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'quantity', 'price','domestic_shipping','weight','bank_fees','other_fees','tanuki_fee'], 'integer'],
            [['shop', 'product_id', 'currency'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'shop' => Yii::t('app', 'Shop'),
            'product_id' => Yii::t('app', 'Product ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'price' => Yii::t('app', 'Price'),
            'tanuki_fee' => Yii::t('app', 'Tanuki Fee'),
	        'domestic_shipping' => Yii::t('app', 'Domestic Shipping'),
	        'weight' => Yii::t('app', 'Weight'),
	        'bank_fees' => Yii::t('app', 'Bank Fees'),
	        'other_fees' => Yii::t('app', 'Other Fees'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }

    public function getProductAttributes()
    {
        return $this->hasMany(OrderProductAttributes::className(),['product_id'=>'product_id']);
    }

    /**
     * move auctions files to main folder
     * @param $name
     */
    public function moveAuctionFiles($name)
    {
        if (!is_null($name) && file_exists(Yii::$app->params['auction-image-path'].$name)) {
            $from = Yii::$app->params['auction-image-path'] . $name;
            $to = Yii::$app->params['order-image-path'] . $name;
            rename($from, $to);
        }
    }

    /**
     * @return string
     */
    public function getImage()
    {
        if (!is_null($this->product_image) && file_exists(Yii::$app->params['order-image-path'].$this->product_image)) {
            $img = Yii::$app->params['order-image-url'] . $this->product_image;
        } else {
            $img = Yii::$app->params['no-image'];
        }
        return Yii::$app->formatter->asImage($img,['width'=>'100','height'=>'100']);
    }

	/**
	 * @return string
	 */
	public function getOuterLink() {
		switch ( $this->shop ) {
			case 'amazon':
				return 'https://www.amazon.co.jp/dp/' . $this->product_id;
				break;
			case 'yahoo':
				$parts = explode( '_', $this->product_id );
				if (count($parts) >1 ) {
					return 'https://store.shopping.yahoo.co.jp/' . $parts[0] . '/' . $parts[1] . '.html';
				} else {
					return 'https://store.shopping.yahoo.co.jp/' . $this->product_id . '.html';
				}
				break;
			case 'rakuten':
				return ['order/rakuten-url','itemCode'=>$this->product_id];
				break;
			case 'Yahoo Auction':
				return 'https://page.auctions.yahoo.co.jp/jp/auction/'.$this->product_id;
				break;
			default:
				return $this->product_id;
				break;
		}
	}
}
