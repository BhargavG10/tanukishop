<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_countries}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $code
 * @property integer $active
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_countries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code'], 'required'],
            [['code', 'active'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'code' => Yii::t('app', 'Code'),
            'active' => Yii::t('app', 'Active'),
        ];
    }
}
