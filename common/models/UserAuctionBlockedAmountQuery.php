<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UserAuctionBlockedAmount]].
 *
 * @see UserAuctionBlockedAmount
 */
class UserAuctionBlockedAmountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserAuctionBlockedAmount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserAuctionBlockedAmount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
