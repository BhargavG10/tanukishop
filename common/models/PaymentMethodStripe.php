<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\components\TBase;
use \Stripe\Stripe;
use \Stripe\Error\Card;
/**
 * ContactForm is the model behind the contact form.
 */
class PaymentMethodStripe extends Model
{

    const PAYMENT_SUCCESS = 1;
    const INVALID_ORDER_DETAIL = 2;
    const INVALID_STRIPE_TOKEN = 3;
    const PAYMENT_DECLINED = 4;

    public $creditCard_number;
    public $creditCard_expirationDate;
    public $creditCard_cvv;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        // can use https://packagist.org/packages/andrewblake1/yii2-credit-card
        return [
            [['creditCard_number','creditCard_expirationDate','creditCard_cvv'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            [['creditCard_number','creditCard_expirationDate','creditCard_cvv'], 'integer','message' => Yii::t('app', TBase::ShowLbl('INTEGER_ONLY'))],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'creditCard_number' => 'Card card Number',
            'creditCard_cvv' => 'Cvv Code',
            'creditCard_expirationDate' => 'Expiration Date'
        ];
    }

    public static function processStripePayment($orderDetail = false,$stripeToken = false) {

        if ($orderDetail == false) {
            return self::INVALID_ORDER_DETAIL;
        }

        if (Yii::$app->params['testmode'] == "on") {
            Stripe::setApiKey(Yii::$app->params['private_test_key']);
        } else {
            Stripe::setApiKey(Yii::$app->params['private_live_key']);
        }

        if ($stripeToken) {
            $amount_cents = str_replace(".","",$orderDetail->total);  // Chargeble amount
            $invoiceid = $orderDetail->invoice;
            $description = "Invoice #" . $invoiceid . " - " . $invoiceid;

            try {
                $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => $orderDetail->currency,
                        "source" => $stripeToken,
                        "metadata" => array("order_id" => $orderDetail->id),
                        "description" => $description)
                );
                $orderDetail->status = 'Success';
                $orderDetail->response = $charge;
                $orderDetail->save(false);
                return self::PAYMENT_SUCCESS; // successfull request

            } catch (Card $e) {
                $error = $e->getMessage();
                $orderDetail->failure_reason = $error;
                $orderDetail->response = 'declined';
                $orderDetail->save(false);
                return self::PAYMENT_DECLINED; // request declined
            }
        } else {
            return self::INVALID_STRIPE_TOKEN; // invalid stripe key
        }
    }
}
