<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_packages_order}}".
 *
 * @property integer $package_id
 * @property integer $order_id
 */
class PackagesOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_packages_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'order_id'], 'required'],
            [['package_id', 'order_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => Yii::t('app', 'Package ID'),
            'order_id' => Yii::t('app', 'Order ID'),
        ];
    }

    public function getOrder() {
        return $this->hasOne(Order::className(),['id'=>'order_id']);
    }
}
