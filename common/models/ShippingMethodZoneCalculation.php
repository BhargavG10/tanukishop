<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_method_zone_calculation}}".
 *
 * @property integer $id
 * @property integer $shipping_method_id
 * @property integer $zone_id
 * @property integer $weight_from
 * @property double $weight_to
 * @property double $cost
 */
class ShippingMethodZoneCalculation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_method_zone_calculation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipping_method_id', 'zone_id', 'weight_from', 'weight_to', 'cost'], 'required'],
            [['shipping_method_id', 'zone_id', 'weight_from'], 'integer'],
            [['weight_to', 'cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shipping_method_id' => Yii::t('app', 'Shipping Method ID'),
            'zone_id' => Yii::t('app', 'Zone ID'),
            'weight_from' => Yii::t('app', 'Weight From'),
            'weight_to' => Yii::t('app', 'Weight To'),
            'cost' => Yii::t('app', 'Cost'),
        ];
    }
}
