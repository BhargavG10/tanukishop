<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_cart_product_attributes}}".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property string $attribute_name
 * @property string $attribute_value
 */
class CartProductAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_cart_product_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cart_id', 'attribute_name', 'attribute_value'], 'required'],
            [['cart_id'], 'integer'],
            [['attribute_name', 'attribute_value'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cart_id' => Yii::t('app', 'Cart ID'),
            'attribute_name' => Yii::t('app', 'Attribute Name'),
            'attribute_value' => Yii::t('app', 'Attribute Value'),
        ];
    }
}
