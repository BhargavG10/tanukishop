<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_category}}".
 *
 * @property integer $id
 * @property integer $ref_id
 * @property string $title_en
 * @property string $title_ru
 * @property string $slug
 * @property string $amazon_search_index
 * @property string $detail_en
 * @property string $detail_ru
 * @property integer $parent_id
 * @property string $shop
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_text
 * @property string $status
 * @property integer $serial_no
 * @property string $created_on
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ref_id', 'parent_id','serial_no'], 'integer'],
            [['title_en', 'slug', 'detail_en', 'status'], 'required'],
            [['detail_en', 'detail_ru', 'status','shop','amazon_search_index','meta_title','seo_text','meta_keywords','meta_description'], 'string'],
            [['created_on','meta_title','seo_text','meta_keywords','meta_description'], 'safe'],
            [['title_en', 'title_ru','amazon_search_index'], 'string', 'max' => 225],
            [['slug'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ref_id' => Yii::t('app', 'Ref ID'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'slug' => Yii::t('app', 'Slug'),
            'amazon_search_index' => Yii::t('app', 'Amazon Search Index'),
            'detail_en' => Yii::t('app', 'Detail En'),
            'detail_ru' => Yii::t('app', 'Detail Ru'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'shop' => Yii::t('app', 'Shop'),
            'status' => Yii::t('app', 'Status'),
            'serial_no' => Yii::t('app', 'Serial No'),
            'created_on' => Yii::t('app', 'Created On'),
	        'meta_title' => Yii::t('app', 'meta title'),
	        'seo_text' => Yii::t('app', 'seo text'),
	        'meta_keywords' => Yii::t('app', 'meta keywords'),
	        'meta_description' => Yii::t('app', 'meta description'),
        ];
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->created_on = date('Y-m-d');

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
