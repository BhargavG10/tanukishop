<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 10/05/16
 * Time: 4:27 PM
 */

namespace common\models;
use Yii;
use \common\components\TBase;
class Yahoo
{
    public $YahooProduct;
    public $ITEM_SEARCH_API_URL;
    public $ITEM_DETAIL_API_URL;
    public $APIID;
	public $URL;

    function __construct() {
	    $this->URL = 'https://shopping.yahooapis.jp/ShoppingWebService/V1/categorySearch';
        $this->ITEM_SEARCH_API_URL = TBase::TanukiSetting('YAHOO_ITEM_SEARCH_API_URL');
        $this->ITEM_DETAIL_API_URL = TBase::TanukiSetting('YAHOO_ITEM_DETAIL_API_URL');
        $this->APIID = TBase::TanukiSetting('YAHOO_API_ID');
    }

    public function urlencode_RFC3986($str)
    {
        return str_replace('%7E', '~', rawurlencode($str));
    }

    public function productsListing($params){
        $params = array_merge($params,['responsegroup'  =>  'large']);
	    $ch = curl_init ( $this->ITEM_SEARCH_API_URL . '?' . Http_build_query ( $params ));
        Curl_setopt_array ( $ch , Array (
            CURLOPT_RETURNTRANSFER =>  True ,
            CURLOPT_USERAGENT       =>  "Yahoo AppID: $this->APIID"
        ));

        $Result = Curl_exec ( $ch );
        curl_close ( $ch );
        return unserialize($Result);
    }

    public function productsDetail($code){
        $params = Array (
            'itemcode'  =>  $code,
            'responsegroup'  =>  'large'
        );
        $ch = curl_init ( $this->ITEM_DETAIL_API_URL . '?' . Http_build_query ( $params ));
        Curl_setopt_array ( $ch , Array (
            CURLOPT_RETURNTRANSFER =>  True ,
            CURLOPT_USERAGENT       =>  "Yahoo AppID: $this->APIID"
        ));

        $Result = Curl_exec ( $ch );
        curl_close ( $ch );
        return unserialize($Result);
    }

	/**
	 * @param $ID
	 * @return mixed
	 */
	public function getCategories($ID) {
		$id = (is_numeric($ID)) ? $ID : 1;
		$params = Array (
			'category_id'  =>  $ID
		);

		$params = array_merge($params,['appid'  =>  $this->APIID]);
		$api_url = $this->URL.'?'.Http_build_query($params);
		$category = simplexml_load_file($api_url);
		return $category = json_decode(json_encode($category), true);

	}
}