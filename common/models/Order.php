<?php

namespace common\models;

use common\components\MailCamp;
use Yii;
use common\components\TBase;
use \common\helper\Tanuki;
use common\components\TBase as LBL;
/**
 * This is the model class for table "{{%tnk_order}}".
 *
 * @property integer $id
 * @property double $type
 * @property double $subtotal
 * @property double $total
 * @property double $paid_amount
 * @property integer $tanuki_charges
 * @property integer $shipping_charges
 * @property string $currency
 * @property integer $user_id
 * @property string $method
 * @property string $status
 * @property string $date
 * @property string $response
 * @property integer $blocked_amount
 * @property string $invoice
 * @property string $failure_reason
 * @property string $token
 * @property integer $order_status
 * @property string $payment_id
 * @property string $consolidation
 * @property string $user_auction_id
 * @property string $auction_id
 * @property integer $external
 */
class Order extends \yii\db\ActiveRecord
{

	const DOMESTIC_SHIPPING = 1;
	const PAYMENT_PENDING = 2;
	const PACKAGE_ORDER_PENDING = 3;
	const PREPARING_PACKAGE = 4;
	const ORDER_SHIPPED = 5;
	const ORDER_CANCELLED_BY_SELLER = 6;

    const ADD_FUND          = 'add_fund';
    const PRODUCT_PURCHASE  = 'product_purchase';
    const PAY_BY_BALANCE  = 'Pay By Balance';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subtotal', 'total'], 'number'],
            [['tanuki_charges'], 'required'],
            [['tanuki_charges', 'user_id','order_status','consolidation','user_auction_id','external'], 'integer'],
            [['date','paid_amount','blocked_amount'], 'safe'],
            [['response', 'failure_reason','type','auction_id'], 'string'],
            [['currency', 'method', 'status'], 'string', 'max' => 45],
            [['invoice','token','payment_id'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => LBL::_x('ORDER_TYPE'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'total' => Yii::t('app', 'Total'),
            'paid_amount' => Yii::t('app', 'Paid Amount'),
            'tanuki_charges' => Yii::t('app', 'Tanuki Charges'),
            'shipping_charges' => Yii::t('app', 'Shipping Charges'),
            'currency' => Yii::t('app', 'Currency'),
            'user_id' => Yii::t('app', 'Client'),
            'method' => Yii::t('app', 'Method'),
            'status' => Yii::t('app', 'Status'),
            'blocked_amount' => Yii::t('app', 'Blocked Amount'),
            'date' => Yii::t('app', 'Date'),
            'response' => Yii::t('app', 'Response'),
            'invoice' => Yii::t('app', 'Invoice'),
            'failure_reason' => Yii::t('app', 'Failure Reason'),
            'token' => Yii::t('app', 'Token'),
            'order_status' => Yii::t('app', 'Order Status'),
            'payment_id' => Yii::t('app', 'Transaction ID'),
            'consolidation' => Yii::t('app', 'Consolidation'),
            'user_auction_id' => Yii::t('app', 'User Auction ID'),
            'auction_id' => Yii::t('app', 'Auction ID'),
            'external' => Yii::t('app', 'External'),
        ];
    }

    public function extraFields() {
	    return [
	    	'orderDetail',
		    'shipping',
		    'user'
	    ];
    }

	public function getOrderDetail($modelClass = '\common\models\OrderDetail')
    {
        return $this->hasMany($modelClass::className(),['order_id'=>'id']);
    }

    public function getShipping($modelClass = '\common\models\ShippingAddress')
    {
        return $this->hasOne($modelClass::className(),['order_id'=>'id']);
    }

    public function getUser($modelClass = '\common\models\User')
    {
        return $this->hasOne($modelClass::className(),['id'=>'user_id']);
    }
    public function getStatusDetail()
    {
        return $this->hasOne(OrderStatus::className(),['id'=>'order_status']);
    }

    public function getStatus()
    {
        if ($this->order_status) {
        	$status = OrderStatus::findOne($this->order_status);
	        if ($status) {
		        if(TBase::CLang() != 'en-US') {
			        return $status->title_ru;
		        } else {
			        return $status->title;
		        }

	        } else {
        		return '-';
	        }
		} else {
        	return '-';
        }
    }

    /**
     * @return string
     */
    public function getFundStatus()
    {
    	if ($this->type == Order::ADD_FUND) {
    	    if ($this->status) {
		        return LBL::_x(strtoupper($this->status));
	        } else {
		        return LBL::_x('BANK_DEPOSIT');
	        }
	    }

        if ($this->order_status == 2 || $this->order_status == '8') {
            return LBL::_x('PAYMENT_PENDING');
        } else {
	        return LBL::_x('PAYMENT_DONE');
        }
    }

    /**
     * @param string $paymentMethod
     * @param string $currency
     * @param string $status
     * @param int $order_status
     * @return bool|int
     */
    public static function savePurchaseOrder($paymentMethod = 'Paypal',$currency = 'JPY',$status = 'Pending Payment',$order_status=Order::PAYMENT_PENDING)
    {
        $TanukiCharges = TBase::TanukiCharges();
        $total = 0;
        $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();
        if (isset($approveCartProducts) && count($approveCartProducts) > 0) {
            foreach ($approveCartProducts as $key => $cartItem) {
                $api = new Tanuki();
                $api->singleProductDetail($cartItem['product_id'], $cartItem['shop']);
                $amount = $api->shopProductDetail($cartItem['shop'], 'price');
                $total += ($amount * $cartItem['quantity']) + $TanukiCharges + $cartItem['domestic_shipping'];
            }

            // paypal tax calculation
            $finalCost = $total;

            $OrderModel = new \common\models\Order();
            $OrderModel->subtotal = $total;
            $OrderModel->total = $finalCost;
            $OrderModel->paid_amount = $finalCost;
            $OrderModel->tanuki_charges = 0;
            $OrderModel->user_id = Yii::$app->user->id;
            $OrderModel->date = date('Y-m-d H:i:s');
            $OrderModel->response = '';
            $OrderModel->failure_reason = '';
            $OrderModel->shipping_charges = 0;
            $OrderModel->currency = $currency;
            $OrderModel->method = $paymentMethod;
            $OrderModel->status = $status;
            $OrderModel->order_status = $order_status;
            $OrderModel->invoice = uniqid();
            if ($OrderModel->save()) {
                \Yii::$app->session['orderID'] = $OrderModel->id;

                if (isset($approveCartProducts) && count($approveCartProducts) > 0) {
                    foreach ($approveCartProducts as $key => $cartItem) {
                        $api->singleProductDetail($cartItem['product_id'], $cartItem['shop']);
                        $amount = $api->shopProductDetail($cartItem['shop'], 'price');
                        $OrderDetailModel = new \common\models\OrderDetail();
                        $OrderDetailModel->order_id = $OrderModel->id;
                        $OrderDetailModel->shop = $cartItem['shop'];
                        $OrderDetailModel->product_id = $cartItem['product_id'];
                        $OrderDetailModel->product_name = $api->shopProductDetail($cartItem['shop'], 'name');
                        $OrderDetailModel->product_image = TBase::saveOrderImage($api->shopProductDetail($cartItem['shop'], 'image'), $OrderModel->id, $cartItem['shop']);
                        $OrderDetailModel->quantity = $cartItem['quantity'];
                        $OrderDetailModel->tanuki_fee = $TanukiCharges;
                        $OrderDetailModel->price = $amount;
                        $OrderDetailModel->domestic_shipping = $cartItem['domestic_shipping'];
                        $OrderDetailModel->currency = 'JPY';
                        if ($OrderDetailModel->save(false)) {
                            if (isset($cartItem['params'])) {  // saving attributes in case if any product have any attributes
                                foreach ($cartItem['params'] as $attribute => $params) {
                                    $modelAttribute = new OrderProductAttributes();
                                    $modelAttribute->attribute_name = $params['attribute_name'];
                                    $modelAttribute->attribute_value = $params['attribute_value'];
                                    $modelAttribute->order_id = $OrderModel->id;
                                    $modelAttribute->product_id = $cartItem['product_id'];
                                    $modelAttribute->save(false);
                                }
                                CartProductAttributes::deleteAll(['cart_id'=>$cartItem['id']]);
                            }
                            CartProducts::deleteAll(['id'=>$cartItem['id']]);
                        }
                    }
                }

                //saving shipping address
                if (\Yii::$app->session['shipping']) {
                    $shipping = \Yii::$app->session['shipping'];
                    $OrderShipping = new \common\models\ShippingAddress();

                    $OrderShipping->first_name = $shipping['first_name'];
                    $OrderShipping->last_name = $shipping['last_name'];
                    $OrderShipping->company = $shipping['company'];
                    $OrderShipping->address_1 = $shipping['address_1'];
                    $OrderShipping->address_2 = $shipping['address_2'];
                    $OrderShipping->appt_no = $shipping['appt_no'];
                    $OrderShipping->city = $shipping['city'];
                    $OrderShipping->state = $shipping['state'];
                    $OrderShipping->country = $shipping['country'];
                    $OrderShipping->zipcode = $shipping['zipcode'];
                    $OrderShipping->phone = $shipping['appt_no'];
                    $OrderShipping->email = TBase::userEmail();
                    $OrderShipping->shipping_method = $shipping['shipping_method'];
                    $OrderShipping->order_id = $OrderModel->id;
                    $OrderShipping->save(false);
                }
            } else {
                return $OrderModel->errors;
            }
            return $OrderModel->id;
        } else {
            return false;
        }
    }

    /**
     * @param string $paymentMethod
     * @param string $currency
     * @param string $status
     * @param int $order_status
     * @param int $amount
     * @return bool|int
     */
    public static function saveFundOrder($paymentMethod = 'Paypal',$currency = 'JPY',$status = '6',$order_status=Order::PAYMENT_PENDING, $amount = 0)
    {
        if ($amount) {
            //$TanukiCharges = TBase::TanukiCharges();
            $TanukiCharges = 0;
            $total = 0;
            // paypal tax calculation
            $finalCost = $total;

            $OrderModel = new \common\models\Order();
            $OrderModel->type = 'add_fund';
            $OrderModel->subtotal = $amount;
            $OrderModel->total = $amount;
	        $OrderModel->paid_amount = $finalCost;
            $OrderModel->tanuki_charges = $TanukiCharges;
            $OrderModel->user_id = Yii::$app->user->id;
            $OrderModel->date = date('Y-m-d H:i:s');
            $OrderModel->response = '';
            $OrderModel->failure_reason = '';
            $OrderModel->shipping_charges = 0;
            $OrderModel->currency = $currency;
            $OrderModel->method = $paymentMethod;
            $OrderModel->status = '';
            $OrderModel->order_status = $order_status;
            $OrderModel->invoice = uniqid();
            if ($OrderModel->save(false)) {
                \Yii::$app->session['orderID'] = $OrderModel->id;
            }
            return $OrderModel->id;
        } else {
            return false;
        }
    }

    public function getOrderType() {
        $data = $this;
        if ($data->type == 'auction') {
            return 'Yahoo Auction';
        } else if ($data->type == 'buyout') {
            return 'Yahoo Buyout';
        } else if ($data->type == 'product_purchase') {
            return 'Shop Order';
        } else {
            return 'Add Fund';
        }
    }

	/**
	 * @param $id
	 * @param string $view
	 *
	 * @return bool
	 */
    public static function orderInvoiceMail($id, $view = false) {

    	$order = Order::findOne($id);
        if ($order) {

        	if ($order->type == Order::ADD_FUND) {
		        MailCamp::adminFundNotification( $id );
	        } else {
        	    MailCamp::adminOrderNotification($id);
	        }

	        $view = (!$view) ? 'shopping/order-'.$order->user->language : $view;

            Yii::$app->mailer->compose($view, [
                'model' => $order,
            ])
            ->setTo($order->user->email)
            ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
            ->setSubject('Order - TNK0000' . $order->id)
            ->send();
        } else {
            return false;
        }
    }

    public static function invoiceMail($id,$type = 'invoice') {
	    $order = Order::findOne($id);
	    $subject = 'Order Invoice - TNK0000' . $order->id;
	    $balance = $order->total - $order->paid_amount - ((isset($order->user->credit)) ? $order->user->credit : 0);
	    if ($type == 'receipt') {
		    $subject = 'Order Receipt - TNK0000' . $order->id;
        }
	    if ($order) {
            return Yii::$app->mailer->compose('shopping/'.$type.'-'.$order->user->language, [
                'model' => $order,
                'balance' => $balance,
            ])
            ->setTo($order->user->email)
//            ->setTo('anilkumar.dhiman1@gmail.com')
            ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
            ->setSubject($subject)
            ->send();
        } else {
            return false;
        }
    }

    public function getImage() {

        if (isset($this->orderDetail[0]) && !is_null($this->orderDetail[0]->product_image) && file_exists(Yii::$app->params['order-image-path'].$this->orderDetail[0]->product_image)) {
            return Yii::$app->params['order-image-url'] . $this->orderDetail[0]->product_image;
        } else {
	        return Yii::$app->params['no-image'];
        }
    }


    public function getProductCode() {

	    if ($this->type == "buyout" || $this->type == "auction") {
		    if (isset($this->orderDetail[0])) {
			    return $this->orderDetail[0]->product_id;
		    } else {
			    return '-';
		    }
	    }

    }

    public function getBlockedAmount()
    {
		if ($this->type == 'auction' || $this->type == 'buyout' && isset($this->orderDetail[0]->product_id)) {
			return UserAuctionBlockedAmount::findOne(['auction_id'=>$this->orderDetail[0]->product_id,'user_id'=>$this->user_id,'is_deleted'=>0]);
		}
		return false;
    }

    public function getUserAuction()
    {
    	return $this->hasOne(UserAuctions::className(),['id'=>'user_auction_id']);
    }
}
