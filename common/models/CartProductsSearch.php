<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CartProducts;

/**
 * CartProductsSearch represents the model behind the search form about `common\models\CartProducts`.
 */
class CartProductsSearch extends CartProducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'quantity', 'notify','domestic_shipping'], 'integer'],
            [['shop', 'buyer_id','product_id', 'status', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CartProducts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'buyer_id' => $this->buyer_id,
            'quantity' => $this->quantity,
            'notify' => $this->notify,
            'created_on' => $this->created_on,
            'domestic_shipping' => $this->domestic_shipping,
        ]);

        $query->andFilterWhere(['like', 'shop', $this->shop])
            ->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'status', $this->status]);


        return $dataProvider;
    }

    public function groupByUserSearch($params)
    {
        $query = CartProducts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
	    $query->joinWith(['user']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quantity' => $this->quantity,
            'notify' => $this->notify,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'shop', $this->shop])
            ->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'status', $this->status])
		    ->andFilterWhere(
			    [
				    'like',
				    'Concat(user.first_name," ", user.last_name)',
				    $this->buyer_id
			    ]
		    );
	    $query->groupBy( 'buyer_id' )
	          ->orderby( 'created_on DESC' );


        return $dataProvider;
    }
}
