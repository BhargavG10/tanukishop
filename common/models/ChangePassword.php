<?php
namespace common\models;

use common\components\TBase;
use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ChangePassword extends Model
{
    public $new_password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_password','confirm_password'], 'required'],
            ['confirm_password', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Passwords don't match" ],
            ['new_password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'new_password' => TBase::_x('NEW_PASSWORD'),
            'confirm_password' => TBase::_x('CONFIRM_PASSWORD'),
        ];
    }
}
