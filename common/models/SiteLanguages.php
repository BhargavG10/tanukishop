<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_site_languages}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $flag
 * @property string $status
 */
class SiteLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_site_languages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'flag'], 'required'],
            [['status'], 'string'],
            [['title', 'flag'], 'string', 'max' => 225],
            [['code'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'code' => Yii::t('app', 'Code'),
            'flag' => Yii::t('app', 'Flag'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
