<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tanuki_charges'], 'integer'],
            [['subtotal', 'total'], 'number'],
            [['currency', 'user_id', 'method', 'status', 'date', 'response', 'invoice', 'payment_id', 'failure_reason','order_status','type','auction_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
	    $query->joinWith(['user as user']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'tnk_order.id' => $this->id,
            'tnk_order.subtotal' => $this->subtotal,
            'tnk_order.total' => $this->total,
            'tnk_order.tanuki_charges' => $this->tanuki_charges,
            'tnk_order.date' => $this->date,
            'tnk_order.order_status' => $this->order_status,
        ]);

        $query->andFilterWhere(['like', 'tnk_order.currency', $this->currency])
            ->andFilterWhere(['like', 'tnk_order.type', $this->type])
            ->andFilterWhere(['like', 'tnk_order.auction_id', $this->auction_id])
            ->andFilterWhere(['like', 'tnk_order.method', $this->method])
            ->andFilterWhere(['like', 'tnk_order.status', $this->status])
            ->andFilterWhere(['like', 'tnk_order.response', $this->response])
            ->andFilterWhere(['like', 'tnk_order.invoice', $this->invoice])
            ->andFilterWhere(['like', 'tnk_order.failure_reason', $this->failure_reason])
            ->andFilterWhere(
	            [
		            'like',
		            'Concat(user.first_name," ", user.last_name)',
		            $this->user_id
	            ]
            );
        if (!isset($_REQUEST['sort'])) {
	        $query->orderBy( 'id DESC' );
        }
        return $dataProvider;
    }

    public function frontSearch($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'total' => $this->total,
            'tanuki_charges' => $this->tanuki_charges,
            'user_id' => $this->user_id,
            'date' => $this->date,
            'order_status' => $this->order_status,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['in', 'type', $this->type])
            ->andFilterWhere(['like', 'method', $this->method])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'response', $this->response])
            ->andFilterWhere(['like', 'invoice', $this->invoice])
            ->andFilterWhere(['like', 'failure_reason', $this->failure_reason]);
        $query->orderBy('id DESC');
        return $dataProvider;
    }
}
