<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_user_auction_blocked_amount}}".
 *
 * @property integer $id
 * @property integer $auction_id
 * @property string $auction_end
 * @property integer $user_id
 * @property double $auction_amount
 * @property double $blocked_amount
 * @property string $auction_type
 * @property integer $user_auctions_id
 * @property integer $is_deleted
 * @property integer $is_refunded
 * @property string $created_date
 */
class UserAuctionBlockedAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_user_auction_blocked_amount}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auction_id', 'user_id', 'auction_amount', 'blocked_amount', 'auction_type', 'created_date'], 'required'],
            [['user_id'], 'integer'],
            [['auction_end', 'created_date'], 'safe'],
            [['auction_amount', 'blocked_amount','user_auctions_id','is_deleted','is_refunded'], 'number'],
            [['auction_id','auction_type'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auction_id' => Yii::t('app', 'Auction ID'),
            'auction_end' => Yii::t('app', 'Auction End'),
            'user_id' => Yii::t('app', 'User ID'),
            'auction_amount' => Yii::t('app', 'Auction Amount'),
            'blocked_amount' => Yii::t('app', 'Blocked Amount'),
            'auction_type' => Yii::t('app', 'Auction Type'),
            'user_auctions_id' => Yii::t('app', 'User Auctions ID'),
            'is_deleted' => Yii::t('app', 'is deleted'),
            'created_date' => Yii::t('app', 'Created Date'),
	        'is_refunded' => Yii::t('app', 'Is Refunded'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserAuctionBlockedAmountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserAuctionBlockedAmountQuery(get_called_class());
    }

    public function getUser()
    {
    	return $this->hasOne(User::className(),['id'=>'user_id']);
    }
}
