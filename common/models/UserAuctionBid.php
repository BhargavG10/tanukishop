<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_user_auction_bid}}".
 *
 * @property string $user_auction_id
 * @property double $bid_amount
 * @property double $prefix_amount
 * @property string $datetime
 */
class UserAuctionBid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_user_auction_bid}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_auction_id', 'bid_amount', 'prefix_amount', 'datetime'], 'required'],
            [['bid_amount', 'prefix_amount'], 'number'],
            [['datetime'], 'safe'],
            [['user_auction_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_auction_id' => Yii::t('app', 'User Auction ID'),
            'bid_amount' => Yii::t('app', 'Bid Amount'),
            'prefix_amount' => Yii::t('app', 'Prefix Amount'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }

	/**
	 * save auction bid history
	 * @param $auction
	 */
    public static function saveDetail($auction){
	    $bidModel                  = new UserAuctionBid();
	    $bidModel->user_auction_id = $auction->id;
	    $bidModel->bid_amount      = $auction->amount;
	    $bidModel->prefix_amount   = $auction->prefix_amount;
	    $bidModel->datetime        = date( 'Y-m-d H:i:s' );
	    $bidModel->save( false );
    }
}
