<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 11/15/16
 * Time: 12:32 PM
 */

namespace common\models;


use yii\base\Model;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\Operations\Lookup;
use ApaiIO\ApaiIO;
use common\models\Yahoo;
use Rakuten\Client;
use common\models\Amazon;
use \common\components\TBase;

class Rakuten
{
    public function RakutenAPI($SearchItems = [], $page = 1, $pageSize = 20)
    {
        //testing
        /*$RAKUTEN_APP_ID = '1078337665598272286';
        $SECRETID = '1721a5b4db9a5fbdaa67c8d32666f6a13f5e360c';
        $AFFID = '14eb7ba7.3296d821.14eb7ba8.4736a620';*/
        //7fed92c99c89c62e8f13357f56610377 testing
        $RAKUTEN_APP_ID = TBase::TanukiSetting('RAKUTEN_APP_ID');
        $SECRETID = TBase::TanukiSetting('RAKUTEN_SECRET_ID');
        $AFFID = TBase::TanukiSetting('RAKUTEN_AFFILIATE_ID');

        $SearchItems = array_merge($SearchItems,['hits'=>$pageSize, 'page'=>$page]);
        $client = new Client();
        $client->setApplicationId($RAKUTEN_APP_ID);
        $client->setAffiliateId($AFFID);

        //$response = $client->execute('IchibaItemSearch', ['keyword'=>$searchKey, 'hits'=>$pageSize, 'page'=>$page]);
        //$response = $client->execute('IchibaItemSearch', ['genreId'=>206878, 'itemCode'=>'10s-clothing: 10022095', 'shopCode'=>'10s-clothing']);
        //$response = $client->execute('IchibaItemSearch', ['itemCode'=>'10s-clothing: 10022095']);
        //$response = $client->execute('IchibaItemSearch', ['genreId' => 206878]); // for category
        ////https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=json&genreId=206878&itemCode=10s-clothing%3A%2010022095&shopCode=10s-clothing&applicationId=1078337665598272286

        $response = $client->execute('IchibaItemSearch', $SearchItems);
        $responseCode = $response->getCode();

        if ($responseCode != 200) {

            $arReturn['message'] = $response->getMessage();

        } else {

            $data = $response->getData();
            $arData = [];
            $arData['totalRecords'] = $data['count'];
            $arData['pageCount'] = $data['pageCount'];
            $arData['data'] = $data['Items'];
            $arReturn['data'] = $arData;
        }

        return $arReturn;
    }

	public static function categorySearch($category_id)
	{
		$RAKUTEN_APP_ID = TBase::TanukiSetting('RAKUTEN_APP_ID');
		$SECRETID = TBase::TanukiSetting('RAKUTEN_SECRET_ID');
		$AFFID = TBase::TanukiSetting('RAKUTEN_AFFILIATE_ID');

		$SearchItems = ['genreId'=>$category_id];
		$client = new Client();
		$client->setApplicationId($RAKUTEN_APP_ID);
		$client->setAffiliateId($AFFID);

		$response = $client->execute('IchibaGenreSearch', $SearchItems);
		$responseCode = $response->getCode();

		if ($responseCode != 200) {

			$data = $response->getMessage();

		} else {

			$data = $response->getData();
		}

		return $data;
	}
}