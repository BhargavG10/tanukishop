<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tnk_shipping_methods}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 */
class ShippingMethods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_methods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function getCountryList($id)
    {
	    $list = [];
        $countryList = ShippingMethodCalculationFinal::find()
	                     ->andWhere(['shipping_method_id'=>$id])
	                     ->groupBy('country_id')
	                     ->all();
	    if ($countryList) {
		    $list = ArrayHelper::map($countryList,'country_id','country.title');
	    }
	    return $list;
    }
}
