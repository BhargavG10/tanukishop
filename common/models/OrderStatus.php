<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_order_status}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $title_ru
 */
class OrderStatus extends \yii\db\ActiveRecord
{

    const DOMESTIC_SHIPPING = 1;
    const PAYMENT_PENDING = 2;
    const PACKAGE_ORDER_PENDING = 3;
    const PREPARING_PACKAGE = 4;
    const ORDER_SHIPPED = 5;
    const ORDER_CANCELLED_BY_SELLER = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru','title'], 'required'],
            [['title'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_ru' => Yii::t('app', 'Title Russia'),
        ];
    }

    public function getStatus($id)
    {
        return self::findOne($id)->title;
    }
}
