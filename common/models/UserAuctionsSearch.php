<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserAuctions;

/**
 * UserAuctionsSearch represents the model behind the search form about `common\models\UserAuctions`.
 */
class UserAuctionsSearch extends UserAuctions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['auction_id', 'ip_address', 'auction_ends', 'auction_type', 'created_on', 'modified_on', 'api_response','image'], 'safe'],
            [['amount','is_deleted'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAuctions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'created_on' => $this->created_on,
            'modified_on' => $this->modified_on,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'auction_id', $this->auction_id])
            ->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'auction_ends', $this->auction_ends])
            ->andFilterWhere(['like', 'auction_type', $this->auction_type])
            ->andFilterWhere(['like', 'api_response', $this->api_response]);
	    if (!isset($_REQUEST['sort'])) {
		    $query->orderBy('ID DESC');
	    }
        return $dataProvider;
    }
}
