<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\components\TBase;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $type;
    public $rememberMe = true;
    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => TBase::ShowLbl('PASSWORD'),
            'email' => TBase::ShowLbl('EMAIL'),
            'rememberMe' => TBase::ShowLbl('REMEMBER_ME'),
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser($this->type);
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, TBase::ShowLbl('INCORRECT_USERNAME_PASSWORD'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser($this->type), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser($type)
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email,$type);
        }

        return $this->_user;
    }
}
