<?php
namespace common\models;

use common\components\TBase;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use app\models\FavoriteProduct;
use common\models\UserAuctions;
use common\models\Packages;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $credit
 * @property integer $language
 * @property integer $last_login
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     *
     */
    const STATUS_DELETED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 10;
    /**
     *
     */
    const STATUS_DEACTIVE = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'email' => TBase::_x('EMAIL'),
			'first_name' => TBase::_x('FIRST_NAME'),
			'last_name' => TBase::_x('LAST_NAME'),
			'language' => TBase::_x('LANGUAGE'),
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'unique'],
            ['credit', 'required', 'on' => 'add-fund'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_DEACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username,$type)
    {
        return static::findOne(['username' => $username, 'type' => $type,'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email,$type)
    {
        return static::findOne(['email' => $email, 'type' => $type,'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    /**
     * @return bool|string
     */
    public function getFullname()
    {
        $profile = User::findone($this->id);
        if ($profile !==null)
            return $profile->first_name.' '.$profile->last_name;
        return false;
    }

    /**
     * @param $id
     * @param $amount
     * @param $method
     */
    public static function updateFund($id, $amount, $method) {
        $user = self::findOne($id);
        $user->credit = (int)$user->credit + (int)$amount;
        $user->save(false);
        $purpose = $amount.' added to user '.$user->email.' account by '.$method.' method';
        self::addPayments($id,$purpose,$amount,$method);
    }

    /**
     * @param $user_id
     * @param $purpose
     * @param $amount
     * @param $method
     */
    public static function addPayments($user_id, $purpose, $amount, $method) {
        self::managePayment($user_id,$amount,$purpose,Payments::ADD_PAYMENT,$method);
    }

    /**
     * @param $user_id
     * @param $purpose
     * @param $amount
     * @param $method
     */
    public static function deductPayments($user_id, $purpose, $amount, $method) {
        self::managePayment($user_id,$amount,$purpose,Payments::DEDUCT_PAYMENT,$method);
    }

    /**
     * @param $user_id
     * @param $purpose
     * @param $amount
     * @param $method
     */
    public static function usedPayments($user_id, $purpose, $amount, $method) {
        self::managePayment($user_id,$amount,$purpose,Payments::USED_PAYMENT,$method);
    }

    /**
     * @param $user_id
     * @param $amount
     * @param $purpose
     * @param $condition
     * @param $method
     */
    public static function managePayment($user_id, $amount, $purpose, $condition, $method) {
        $modal = new Payments();
        $modal->amount = $amount;
        $modal->amount_condition = $condition;
        $modal->purpose = $purpose;
        $modal->method = $method;
        $modal->user_id = $user_id;
        $modal->created_on = date('Y-m-d H:i:s');
        $modal->save(false);
    }

    /**
     * @param $amount
     */
    public static function deductFund($amount){
        $user = self::findOne(Yii::$app->user->id);
        $user->credit = (int)$user->credit - (int)$amount;
        $user->save(false);
    }

    /**
     * @return string
     */
    public function fullName() {
        return $this->first_name .' '. $this->last_name;
    }

    /**
     * @return array
     */
    public static function allUser()
    {
        return ArrayHelper::map(User::find()->all(),'id','fullName');
    }

    /**
     * @return $this
     */
    public function getPendingCartProducts() {
        return $this->hasMany(CartProducts::className(),['buyer_id'=>'id'])->andWhere(['status'=>'pending']);
    }

    /**
     * @return $this
     */
    public function getApproveCartProducts() {
        return $this->hasMany(CartProducts::className(),['buyer_id'=>'id'])->andWhere(['status'=>'approve']);
    }

    /**
     * @return $this
     */
    public function getDisApproveCartProducts() {
        return $this->hasMany(CartProducts::className(),['buyer_id'=>'id'])->andWhere(['status'=>'disapprove']);
    }

    /**
     * @return int|string
     */
    public function getCartCount() {
        return $this->hasMany(CartProducts::className(),['buyer_id'=>'id'])->count();
    }

    /**
     * @param $auction_id
     * @return int|string
     */
    public static function _isUserBidOnAuction($auction_id) {
        return UserAuctions::find()->where(['user_id'=>Yii::$app->user->id,'auction_id'=>$auction_id])->one();
    }

    /**
     * @param $auction_id
     * @return bool
     */
    public static function _isUserTopBidder($auction_id) {
        $bidDetail = self::_isUserBidOnAuction($auction_id);
        if ($bidDetail) {
            $user = UserAuctions::find()
                ->where(['auction_id'=>$auction_id])
                ->andWhere(['>','amount',$bidDetail->amount])
                ->andWhere(['!=','user_id',Yii::$app->user->id])
                ->one();
            if ($user) {
                return false;
            } else {
                return true;
            }
        }
    }

	/**
	 * get user blocked amount
	 * @return int
	 */
    public function getBlockedAmount()
    {
	    $blocked = 0;
    	$auctions = $this->getUserAuction()
                        ->select('sum(blocked_amount) as blocked_amount')
	                    ->where(['user_id'=>$this->id,'is_deleted'=>'0'])->one();

	    $blocked += ($auctions->blocked_amount) ? $auctions->blocked_amount : 0;
	    $order = $this->getOrder()
	                          ->select('sum(blocked_amount) as blocked_amount')
	                                                            ->where(['user_id'=>$this->id])->one();
	    $blocked  += ($order->blocked_amount) ? $order->blocked_amount : 0;
	    return $blocked;
    }

	/**
	 * @return int|string
	 */
	public function getAuctionsCount()
	{
		return UserAuctions::find()->where(['user_id'=>Yii::$app->user->id,'is_deleted'=>0])->count();
	}

	/**
	 * @return int|string
	 */
	public function getYahooWatchListCount()
	{
		return FavoriteProduct::find()->where(['shop'=>'yahoo_auction','user_id'=>Yii::$app->user->id])->count();
	}

	/**
	 * @return int|string
	 */
	public function getWatchListCount()
	{
		return FavoriteProduct::find()->where(['user_id'=>Yii::$app->user->id])->count();
	}

	public function getOrder() {
    	return $this->hasMany(Order::className(),['user_id'=>'id']);
	}


	public function getUserAuction() {
    	return $this->hasMany(UserAuctions::className(),['user_id'=>'id']);
	}

	public function getOrderCount()
	{
		return Order::find()
               ->where(['user_id'=>Yii::$app->user->id])
               ->andWhere(['!=','type','add_fund'])
               ->count();
	}

	public function getPackageCount()
	{
		return Packages::find()->where(['created_by'=>Yii::$app->user->id])->count();
	}

	public static function updateCredit($amount)
	{
		self::updateAll( [ 'credit' => $amount ], [ 'id' => Yii::$app->user->id ] );
	}
}
