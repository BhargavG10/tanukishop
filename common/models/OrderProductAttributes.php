<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_order_product_attributes}}".
 *
 * @property integer $order_id
 * @property integer $product_id
 * @property string $attribute_name
 * @property string $attribute_value
 */
class OrderProductAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_order_product_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'attribute_name', 'attribute_value'], 'required'],
            [['order_id', 'product_id'], 'integer'],
            [['attribute_name', 'attribute_value'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'attribute_name' => Yii::t('app', 'Attribute Name'),
            'attribute_value' => Yii::t('app', 'Attribute Value'),
        ];
    }
}
