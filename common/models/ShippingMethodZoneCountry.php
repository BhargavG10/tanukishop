<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_method_zone_country}}".
 *
 * @property integer $id
 * @property integer $shipping_id
 * @property integer $zone_id
 * @property integer $country_id
 */
class ShippingMethodZoneCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_method_zone_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipping_id', 'zone_id', 'country_id'], 'required'],
            [['shipping_id', 'zone_id', 'country_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shipping_id' => Yii::t('app', 'Shipping ID'),
            'zone_id' => Yii::t('app', 'Zone ID'),
            'country_id' => Yii::t('app', 'Country ID'),
        ];
    }
}
