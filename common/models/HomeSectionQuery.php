<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[HomeSection]].
 *
 * @see HomeSection
 */
class HomeSectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HomeSection[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HomeSection|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
