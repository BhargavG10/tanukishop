<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_category}}".
 *
 * @property int $id
 * @property string $name
 * @property double $delivery_in_japan_cost
 * @property double $tanuki_fee
 * @property double $international_shipping_cost
 * @property int $weight_base
 * @property string $created_on
 * @property string $updated_on
 * @property int $is_active
 */
class ShippingCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'delivery_in_japan_cost', 'tanuki_fee', 'international_shipping_cost', 'is_active'], 'required'],
            [['delivery_in_japan_cost', 'tanuki_fee', 'international_shipping_cost'], 'number'],
            [['weight_base', 'is_active'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'delivery_in_japan_cost' => Yii::t('app', 'Delivery In Japan Cost'),
            'tanuki_fee' => Yii::t('app', 'Tanuki Fee'),
            'international_shipping_cost' => Yii::t('app', 'International Shipping Cost'),
            'weight_base' => Yii::t('app', 'Weight Base'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
}
