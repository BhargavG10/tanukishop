<?php

namespace common\models;

use common\components\MailCamp;
use Yii;

/**
 * This is the model class for table "{{%tnk_messages}}".
 *
 * @property integer $id
 * @property string $message_from
 * @property integer $user_id
 * @property string $date
 * @property string $msg
 * @property integer $is_read_by_admin
 * @property integer $is_read_by_user
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msg'], 'string'],
            [['user_id', 'date', 'msg', 'is_read_by_admin', 'is_read_by_user'], 'required'],
            [['user_id', 'is_read_by_admin', 'is_read_by_user'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'message_from' => Yii::t('app', 'Message From'),
            'user_id' => Yii::t('app', 'Client'),
            'date' => Yii::t('app', 'Date'),
            'msg' => Yii::t('app', 'Message'),
            'is_read_by_admin' => Yii::t('app', 'Is Read By Admin'),
            'is_read_by_user' => Yii::t('app', 'Is Read By User'),
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function sendEmail($msg, $user)
    {
    	return MailCamp::userQueryReplyEmail($user,$msg);
    }

	public function getLatestDate() {
		return self::find()
               ->select('date')
               ->where(['user_id'=>$this->user_id])
               ->orderBy('id DESC')
               ->asArray()
               ->one();
	}
}
