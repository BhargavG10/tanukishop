<?php

namespace common\models;

use common\components\MailCamp;
use common\components\TBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use common\components\TCurrencyConvertor;
/**
 * This is the model class for table "{{%tnk_user_auctions}}".
 *
 * @property integer $id
 * @property string $auction_id
 * @property string $title
 * @property string $image
 * @property double $current_price
 * @property integer $user_id
 * @property double $amount
 * @property string $ip_address
 * @property integer $bids
 * @property string $seller
 * @property string $top_bidder
 * @property string $auction_ends
 * @property integer $blocked_amount
 * @property integer $prefix_amount
 * @property string $auction_type
 * @property string $created_on
 * @property string $modified_on
 * @property string $api_response
 * @property integer $is_deleted
 * @property integer $is_won
 */
class UserAuctions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_user_auctions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auction_id', 'title', 'user_id', 'amount', 'ip_address', 'bids', 'seller','created_on','prefix_amount'], 'required'],
            [['title', 'auction_type'], 'string'],
            [['current_price', 'amount','is_deleted','blocked_amount','prefix_amount'], 'number'],
            [['user_id', 'bids','is_won'], 'integer'],
            [['created_on', 'modified_on'], 'safe'],
            [['auction_id', 'ip_address'], 'string', 'max' => 50],
            [['seller', 'top_bidder'], 'string', 'max' => 225],
            [['auction_ends', 'api_response','image'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auction_id' => Yii::t('app', 'Auction ID'),
            'title' => Yii::t('app', 'Title'),
            'image' => Yii::t('app', 'Image'),
            'current_price' => Yii::t('app', 'Current Price'),
            'user_id' => Yii::t('app', 'Client'),
            'amount' => Yii::t('app', 'Bid Amount'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'bids' => Yii::t('app', 'Bids'),
            'seller' => Yii::t('app', 'Seller'),
            'top_bidder' => Yii::t('app', 'top bidder'),
            'prefix_amount' => Yii::t('app', 'Prefix Amount'),
            'blocked_amount' => Yii::t('app', 'Blocked Amount'),
            'auction_ends' => Yii::t('app', 'Auction Ends'),
            'auction_type' => Yii::t('app', 'Auction Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'api_response' => Yii::t('app', 'Api Response'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'is_won' => Yii::t('app', 'Is Won'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function getAuctionBlockedAmount() {
        return $this->hasOne(UserAuctionBlockedAmount::className(),['user_auctions_id'=>'id']);
    }

    public function getAuctionBids() {
        return $this->hasMany(UserAuctionBid::className(),['user_auction_id'=>'id']);
    }

    /**
     * @return int
     */
    public static function auctionUpdate() {
        $count = 0;
        $auctions = UserAuctions::find()->all();
        foreach ($auctions as $auction) {
            $model = new YahooAuctions();
            $model->productsDetail($auction->auction_id);
            $save = UserAuctions::updateAll(
                [
                    'modified_on'=>date('Y-m-d H:i:s'),
                    'top_bidder'=> $model->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'],
                    'bids'=> $model->YahooAction['ResultSet']['Result']['Bids'],
                    'current_price'=> $model->YahooAction['ResultSet']['Result']['Price']
                    ],'id='.$auction->id
            );
            if ($save) { $count++; }
        }
        return $count;
    }

    /**
     * @param string $auction_id
     * @param string $key
     * @param string $ip
     * @param string $amount
     * @param string $type
     * @return yii\httpclient\Response
     */
    public static function auctionRestCall(
        $auction_id,
        $key,
        $ip,
        $amount='0.0',
        $type='bid'
    ) {

        $client = new \yii\httpclient\Client(['baseUrl' => 'http://osizapi.com']);
        return $client->get('/v1/api/', [
            'auction_id' => $auction_id,
            'key' => $key,
            'ip' => $ip,
            'amount' => $amount,
            'type' => $type,
        ])->send();
    }

    /**
     * @param $data
     * @return bool
     */
    public static function winnerMail($data){
        return MailCamp::auctionWinnerMail($data);
    }

    /**
     * @return string
     */
    public function getFormattedAmount() {
        $tModel = new TCurrencyConvertor;
        return $tModel->convert($this->amount,'JPY','JPY');
    }

    /**
     * @return string
     */
    public function getTimeLeft(){
        $now = new \DateTime();
        if (strtotime($this->auction_ends) >= strtotime('now')) {
//	        $future_date = new \DateTime( $this->auction_ends );
//	        $interval    = $future_date->diff( $now );
//	        return $interval->format( "%ad : %hh: %im: %ss" );
	        return $this->auction_ends;
        } else {
	        return 'Auctions Ended';
        }
    }

    /**
     * @return string
     */
    public function getImage() {
        $path = '/auction-buyout-images/';
        if (!is_null($this->image) && file_exists(Yii::$app->params['auction-image-path'].$this->image)) {
            return Yii::$app->params['auction-image-url'] . $this->image;
        } else {
            return Yii::$app->params['no-image'];
        }
    }

    public function softDelete() {
        $this->is_deleted =1;
        $this->save(false);
    }

	/**
	 * @return \yii\httpclient\Response
	 */
	public static function auctionResultCall() {
		$key = Yii::$app->params['auction_key'];
		$ip =  Yii::$app->params['site_ip'];
		$client = new \yii\httpclient\Client(['baseUrl' => 'http://osizapi.com']);
		return $client->get('/v1/allwinapi/', [
			'ip' => $ip,
			'key' => $key
		])->send();
	}

	/**
	 * @return int
	 */
	public static function storeAuctionResult() {
		$data = self::auctionResultCall();
		if (isset($data->content) && count(json_decode($data->content,true)) > 0) {
			AuctionWonList::deleteAll();
			foreach (json_decode($data->content,true) as $ids) {
				$model             = new AuctionWonList;
				$model->auction_id = $ids;
				$model->stored_on  = date( 'Y-m-d H:i:s' );
				$model->save();
			}
			return self::auctionUserResultsCalculation();
		}
	}

	/**
	 * @return int
	 */
	public static function auctionUserResultsCalculation() {
		$count = 0;
		$auctions = AuctionWonList::find()->all();
		foreach ($auctions as $auction) {
			$record = UserAuctions::find()
	            ->where(['auction_id'=>$auction->auction_id,'is_deleted'=>0,'is_won'=>0])
				->orderBy('amount DESC')
	            ->one();
			if ($record) {
				$result = UserAuctions::markWinner($record->id);
				if ($result) {
					$count ++;
					$record->is_deleted = 1;
					$record->is_won = 1;
					$record->save(false);
					AuctionWonList::deleteAll(['auction_id'=>$record->auction_id]);
				}
			} else {
				$exist = Order::find()->where(['auction_id'=>$auction->auction_id])->exists();
				if (!$exist) {
					$model = new YahooAuctions();
					$model->productsDetail( $auction->auction_id );
					UserAuctions::otherAdminOrders($model,$auction->auction_id);
					$count ++;
				}
			}
		}
		self::deleteExpiryOrder();
		return $count;
	}

	/**
	 * mark winner field
	 * @param $id
	 * @return bool
	 */
	public static function markWinner($id) {
		$data = UserAuctions::findOne($id);

		if ($data) {
			$model = new \common\models\YahooAuctions;
			$data = $model->productsDetail($data->auction_id,'small');
			$model = new Order();
			$model->type = $data->auction_type;
			$model->subtotal = (isset($data['ResultSet']['Result']['Price'])) ? $data['ResultSet']['Result']['Price'] : ($data->auction_type == 'auction') ? $data->amount : $data->current_price;
			$model->total = $model->subtotal;
			$model->paid_amount = $data->blocked_amount;
			$model->tanuki_charges = TBase::settingConst('action-charges');
			$model->user_id = $data->user_id;
			$model->date = $data->created_on;
			$model->response = $data->api_response;
			$model->blocked_amount = $data->blocked_amount;
			$model->failure_reason = '';
			$model->shipping_charges = 0;
			$model->currency = 'JPY';
			$model->method = Order::PAY_BY_BALANCE;
			$model->status = 'Success';
			$model->order_status = Order::PAYMENT_PENDING;
			$model->invoice = uniqid();
			$model->user_auction_id = $data->id;
			$model->auction_id = $data->auction_id;
			if ($model->save(false)) {
				$modelDetail = new OrderDetail();
				$modelDetail->order_id = $model->id;
				$modelDetail->shop = 'Yahoo Auction';
				$modelDetail->product_image = $data->image;
				$modelDetail->product_id = $data->auction_id;
				$modelDetail->product_name = $data->title;
				$modelDetail->quantity = 1;
				$modelDetail->tanuki_fee = TBase::settingConst('action-charges');
				$modelDetail->price = $model->total;
				$modelDetail->currency = $model->currency;
				if ($modelDetail->save(false)) {
					$modelDetail->moveAuctionFiles($data->image);
					$data->softDelete();
					if (UserAuctions::winnerMail($data)) {
						return true;
					}
				}
			}
		}
	}

	public static function bidOnAuction($bidAmount,$auction_id,$apiModel,$regular_bid_block)
	{
		$existing = self::findOne([
			'user_id'=>Yii::$app->user->id,
            'auction_id'=>$auction_id,
			'is_deleted'=>'0'
		]);

		if ($existing) {
			return self::updateBid($bidAmount,$auction_id,$apiModel,$regular_bid_block,$existing);
		} else {
			return self::newBid($bidAmount,$auction_id,$apiModel,$regular_bid_block);
		}
	}

	public static function newBid($bidAmount, $auction_id, $apiModel, $regular_bid_block)
	{
		$bid = $bidAmount;
		$UserBidCost = (int)$bid + (int)$regular_bid_block;
		$response = UserAuctions::auctionRestCall(
			$auction_id,
			Yii::$app->params['auction_key'],
			Yii::$app->params['site_ip'],
			$bid
		);

		$responseData = json_decode($response->content);

		if (isset($responseData->failure) && $responseData->failure != "") {
			return [
				'operation' => 'error',
				'message'   => 'Bidding error has occurred!<br>
								Please contact Tanuki Shop to solve the issue.',
			];
			exit;
		}
		if (isset($responseData->status) && $responseData->status == "BidDone") {

			$model                      = $apiModel;
			$ActionModel                = new UserAuctions();
			$ActionModel->title         = $model->YahooAction['ResultSet']['Result']['Title'];
			$ActionModel->seller        = $model->YahooAction['ResultSet']['Result']['Seller']['Id'];
			$ActionModel->amount        = $bid;
			$ActionModel->blocked_amount = $UserBidCost;
			$ActionModel->prefix_amount  = $regular_bid_block;
			$ActionModel->auction_id    = $auction_id;
			$ActionModel->image         = TBase::saveOrderImage( $model->getImage( 1 ), $ActionModel->auction_id, 'buyout', Yii::$app->params['auction-image-path'] );
			$ActionModel->user_id       = Yii::$app->user->id;
			$ActionModel->ip_address    = Yii::$app->request->userIP;
			$ActionModel->created_on    = date( 'Y-m-d' );
			$ActionModel->api_response  = $response->content;
			if (isset($model->YahooAction['ResultSet']['Result']['EndTime'])) {
				$format                    = new \DateTime( $model->YahooAction['ResultSet']['Result']['EndTime'] );
				$ActionModel->auction_ends = $format->format( 'Y-m-d H:i:s' );
			}

			if ($ActionModel->save(false)) {
				User::updateCredit(( (int) Yii::$app->user->identity->credit - (int) $UserBidCost ));
				UserAuctionBid::saveDetail($ActionModel);
			}
		} else {
			return [
				'operation' => 'error',
				'message'   => 'Error while bidding. Please contact with administrator'
			];
		}
	}

	public static function updateBid($bidAmount,$auction_id,$apiModel,$regular_bid_block,$existing) {

		$bid = $bidAmount;
		$UserBidCost = (int)$bid + (int)$regular_bid_block;

		if ((int)$bidAmount > ((int)Yii::$app->user->identity->credit + (int)$existing->blocked_amount)) {
			return [
				'operation' => 'error',
				'message' => 'INSUFFICIENT_AMOUNT'
			];
		}

		$response = UserAuctions::auctionRestCall(
			$auction_id,
			Yii::$app->params['auction_key'],
			Yii::$app->params['site_ip'],
			$bid
		);

		$responseData = json_decode($response->content);

		if (isset($responseData->failure) && $responseData->failure != "") {
			return [
				'operation' => 'error',
				'message'   => 'Bidding error has occurred!<br>
								Please contact Tanuki Shop to solve the issue.',
			];
			exit;
		}

		if (isset($responseData->status) && $responseData->status == "BidDone") {

			$ActionModel = UserAuctions::findOne([
							'auction_id'=>$auction_id,
						    'user_id'=>Yii::$app->user->id,'is_deleted'=>0
							]);

			// suppose if user bid of auction firstly with 1000 then second time 1200
			// then we need to deduct 1000 + 1000 first time and second time 200 as he already paid 1000 security
			// at first time and second time just 200 as he already paid 1000 initially
			$amountNeedToDeduct = ($UserBidCost - $ActionModel->blocked_amount);


			$ActionModel->bids          = $apiModel->YahooAction['ResultSet']['Result']['Bids'];
			$ActionModel->amount        = $bid;
			$ActionModel->blocked_amount= $UserBidCost;
			$ActionModel->api_response  = $response->content;

			if (isset($apiModel->YahooAction['ResultSet']['Result']['EndTime'])) {
				$format                    = new \DateTime( $apiModel->YahooAction['ResultSet']['Result']['EndTime'] );
				$ActionModel->auction_ends = $format->format( 'Y-m-d H:i:s' );
			}

			if ($ActionModel->save(false)) {

				UserAuctionBid::saveDetail($ActionModel);
				User::updateCredit(( (int) Yii::$app->user->identity->credit - (int) $amountNeedToDeduct ));
				return [
					'operation' => 'success',
					'message'   => 'bit updated successfully'
				];
			}
		} else {
			return [
				'operation' => 'error',
				'message'   => 'Error while bidding. Please contact with administrator'
			];
		}
	}


	public static function buyoutOrder($YahooModel,$auction_id,$UserBidCost) {

		$bidAmount = (isset($YahooModel->YahooAction['ResultSet']['Result']['TaxinBidorbuy'])) ? $YahooModel->YahooAction['ResultSet']['Result']['TaxinBidorbuy'] : $YahooModel->YahooAction['ResultSet']['Result']['Bidorbuy'];

		$response = UserAuctions::auctionRestCall(
			$auction_id,
			Yii::$app->params['auction_key'],
			Yii::$app->params['site_ip'],
			$bidAmount,
			'buy'
		);

		$responseData = json_decode($response->content);

		if (isset($responseData->status) && $responseData->status == "BidDone") {

			$buyout_bid_block        = TBase::TanukiSetting( 'buyout_bid_block' );
			$model                   = new Order();
			$model->type             = 'buyout';
			$model->subtotal         = $bidAmount;
			$model->total            = $model->subtotal;
			$model->paid_amount      = $model->total;
			$model->blocked_amount   = $UserBidCost;
			$model->tanuki_charges   = TBase::settingConst('action-buyout-charges');
			$model->user_id          = Yii::$app->user->id;
			$model->date             = date( 'Y-m-d' );
			$model->response         = $response->content;
			$model->failure_reason   = '';
			$model->shipping_charges = 0;
			$model->currency         = 'JPY';
			$model->method           = Order::PAY_BY_BALANCE;
			$model->status           = 'Success';
			$model->order_status     = Order::PAYMENT_PENDING;
			$model->invoice          = uniqid();
			$model->auction_id      = $auction_id;

			if ( $model->save( false ) ) {
				$modelDetail                = new OrderDetail();
				$modelDetail->order_id      = $model->id;
				$modelDetail->shop          = 'Yahoo Auction';
				$modelDetail->product_id    = $auction_id;
				$modelDetail->product_image = TBase::saveOrderImage( $YahooModel->getImage( 1 ), $auction_id, 'buyout', Yii::$app->params['order-image-path'] );
				$modelDetail->product_name  = $YahooModel->YahooAction['ResultSet']['Result']['Title'];
				$modelDetail->quantity      = 1;
				$modelDetail->tanuki_fee    = TBase::settingConst('action-buyout-charges');;
				$modelDetail->price         = $model->total;
				$modelDetail->currency      = $model->currency;

				if ( $modelDetail->save( false ) ) {

					$ActionModel                = new UserAuctions();
					$ActionModel->title         = $YahooModel->YahooAction['ResultSet']['Result']['Title'];
					$ActionModel->seller        = $YahooModel->YahooAction['ResultSet']['Result']['Seller']['Id'];
					$ActionModel->amount        = $bidAmount;
					$ActionModel->blocked_amount= $model->blocked_amount;
					$ActionModel->prefix_amount  = $buyout_bid_block;
					$ActionModel->auction_id    = $auction_id;
					$ActionModel->image         = TBase::saveOrderImage( $model->getImage( 1 ), $ActionModel->auction_id, 'buyout', 'auction-image-path' );
					$ActionModel->user_id       = Yii::$app->user->id;
					$ActionModel->ip_address    = Yii::$app->request->userIP;
					$ActionModel->created_on    = date( 'Y-m-d' );
					$ActionModel->auction_type  = 'buyout';
					$ActionModel->is_deleted    =   1;
					$ActionModel->is_won        =   1;
					$ActionModel->save(false);

					UserAuctionBid::saveDetail($ActionModel);
					Order::updateAll(['user_auction_id'=>$ActionModel->id],['id'=>$model->id]);
					User::updateCredit(( (int) Yii::$app->user->identity->credit - (int) $UserBidCost ));

					return [
						'operation' => 'success',
						'message'   => 'buyout done successfully'
					];
				} else {
					return [
						'operation' => 'error',
						'message'   => 'Error while bidding. Please contact with administrator'
					];
				}
			}
		}
	}

	/**
	 * save other orders
	 * @param $YahooModel
	 * @param $auction_id
	 * @return array
	 */
	public static function otherAdminOrders($YahooModel,$auction_id) {

		if (isset($YahooModel->YahooAction['ResultSet']['Result']['TaxinBidorbuy'] ) ) {    $bidAmount = $YahooModel->YahooAction['ResultSet']['Result']['TaxinBidorbuy']; } else
		if (isset($YahooModel->YahooAction['ResultSet']['Result']['Bidorbuy'])) {           $bidAmount = $YahooModel->YahooAction['ResultSet']['Result']['Bidorbuy']; } else
		if (isset($YahooModel->YahooAction['ResultSet']['Result']['TaxinPrice'])) {         $bidAmount = $YahooModel->YahooAction['ResultSet']['Result']['TaxinPrice']; } else
		if (isset($YahooModel->YahooAction['ResultSet']['Result']['Price'])) {              $bidAmount = $YahooModel->YahooAction['ResultSet']['Result']['Price']; }
		else { $bidAmount = 0; }

		$model                   = new Order();
		$model->type             = 'auction';
		$model->subtotal         = $bidAmount;
		$model->total            = $model->subtotal;
		$model->paid_amount      = $model->total;
		$model->blocked_amount   = 0;
		$model->tanuki_charges   = 0;
		$model->user_id          = 1;
		$model->date             = date( 'Y-m-d' );
		$model->response         = 'Auction Won Directly';
		$model->failure_reason   = '';
		$model->shipping_charges = 0;
		$model->currency         = 'JPY';
		$model->method           = Order::PAY_BY_BALANCE;
		$model->status           = 'Success';
		$model->order_status     = Order::PAYMENT_PENDING;
		$model->invoice          = uniqid();
		$model->auction_id       = $auction_id;
		$model->external         = 1;

		if ( $model->save( false ) ) {
			$modelDetail                = new OrderDetail();
			$modelDetail->order_id      = $model->id;
			$modelDetail->shop          = 'Yahoo Auction';
			$modelDetail->product_id    = $auction_id;
			$modelDetail->product_image = TBase::saveOrderImage( $YahooModel->getImage( 1 ), $auction_id, 'buyout', Yii::$app->params['order-image-path'] );
			$modelDetail->product_name  = $YahooModel->YahooAction['ResultSet']['Result']['Title'];
			$modelDetail->quantity      = 1;
			$modelDetail->tanuki_fee    = TBase::settingConst('action-charges');
			$modelDetail->price         = $model->total;
			$modelDetail->currency      = $model->currency;

			if ( $modelDetail->save( false ) ) {
				return [
					'operation' => 'success',
					'message'   => 'buyout done successfully'
				];
			} else {
				return [
					'operation' => 'error',
					'message'   => 'Error while bidding. Please contact with administrator'
				];
			}
		}
	}

	/**
	 * delete expiry order
	 */
	public static function deleteExpiryOrder() {
		$user_auctions = UserAuctions::find()
			->where
			([
				'is_deleted' => '0',
				'is_won'=>'0'
			])
			->all();
		if ($user_auctions) {
			foreach ($user_auctions as $auction) {
				if($auction->auction_ends) {
					if (strtotime($auction->auction_ends) < (strtotime('now'))) {

						// refund amount to user
						$user = User::findOne($auction->user_id);
						$user->credit = (int)$user->credit + (int)$auction->blocked_amount;
						if ($user->save(false)) {
							// set auction to be deleted
							UserAuctions::updateAll( [ 'is_deleted' => 1 ], [ 'id'=>$auction->id,'auction_id' => $auction->auction_id ] );
						}
					}
				}
			}
		}
	}
}
