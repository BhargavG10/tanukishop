<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShippingCategory;

/**
 * ShippingCategorySearch represents the model behind the search form of `common\models\ShippingCategory`.
 */
class ShippingCategorySearch extends ShippingCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'weight_base', 'is_active'], 'integer'],
            [['name', 'created_on', 'updated_on'], 'safe'],
            [['delivery_in_japan_cost', 'tanuki_fee', 'international_shipping_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShippingCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'delivery_in_japan_cost' => $this->delivery_in_japan_cost,
            'tanuki_fee' => $this->tanuki_fee,
            'international_shipping_cost' => $this->international_shipping_cost,
            'weight_base' => $this->weight_base,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
