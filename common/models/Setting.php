<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_setting_table}}".
 *
 * @property integer $id
 * @property string $setting_key
 * @property string $setting_value
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_setting_table}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_key', 'setting_value'], 'required'],
            [['setting_key', 'setting_value'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'setting_key' => Yii::t('app', 'Setting Key'),
            'setting_value' => Yii::t('app', 'Setting Value'),
        ];
    }
}
