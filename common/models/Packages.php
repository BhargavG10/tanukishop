<?php

namespace common\models;

use common\components\TBase;
use Yii;

/**
 * This is the model class for table "{{%tnk_packages}}".
 *
 * @property integer $id
 * @property string $name
 * @property double $cost
 * @property string $status
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property integer $appt_no
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property integer $phone
 * @property integer $shipping_methods_id
 * @property string $tracking_number
 * @property string $email
 * @property string $created_on
 * @property integer $created_by
 * @property string $modified_on
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_packages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['cost'], 'number'],
            [['address_1', 'address_2'], 'string'],
            [['appt_no', 'phone', 'created_by','cost','shipping_methods_id'], 'integer'],
            [['created_on', 'modified_on','tracking_number','zipcode'], 'safe'],
            [['name'], 'string', 'max' => 262],
            [['status'], 'string', 'max' => 10],
            [['status', 'address_1', 'country', 'phone', 'first_name', 'last_name', 'company', 'city', 'state', 'country', 'email'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => TBase::_x('PACKAGE_ID'),
            'name' => Yii::t('app', 'Name'),
            'cost' => Yii::t('app', 'Cost'),
            'status' => TBase::_x('STATUS'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'company' => Yii::t('app', 'Company'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'appt_no' => Yii::t('app', 'Appt No'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'country' => Yii::t('app', 'Country'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'created_on' => TBase::_x('CREATED_ON'),
            'created_by' => Yii::t('app', 'Created By'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'shipping_methods_id' => TBase::_x('SHIPPING_METHOD'),
            'tracking_number' => TBase::_x('TRACKING_NUMBER'),
        ];
    }

    public function getPackageOrders()
    {
        return $this->hasMany(PackagesOrder::className(),['package_id'=>'id']);
    }

	public function getShippingMethod()
	{
        return $this->hasOne(ShippingMethods::className(),['id'=>'shipping_methods_id']);
    }

    public function getUser()
    {
    	return $this->hasOne(User::className(),['id'=>'created_by']);
    }

    public function getCountryName()
    {
    	$TBase = new TBase();
    	if ($this->country) {
		    return $TBase->countryDetail( $this->country );
	    } else {
    		return '-';
	    }
    }

    public function getShipping()
    {
    	return $this->hasOne(ShippingMethods::className(),['id'=>'shipping_methods_id']);
    }
}
