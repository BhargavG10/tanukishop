<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_currency_conversion_table}}".
 *
 * @property string $currency
 * @property double $rate
 */
class CurrencyConversionTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_currency_conversion_table}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency'], 'required'],
            [['rate'], 'number'],
            [['currency'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency' => Yii::t('app', 'Currency'),
            'rate' => Yii::t('app', 'Rate'),
        ];
    }
}
