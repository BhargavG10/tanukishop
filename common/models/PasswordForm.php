<?php
namespace common\models;

use common\components\TBase;
use Yii;
use yii\base\Model;
use common\models\User;

class PasswordForm extends Model{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
        ];
    }

    public function findPasswords($attribute, $params){
        $user = User::findOne(Yii::$app->user->getId());
        if(!Yii::$app->security->validatePassword($this->oldpass, $user->password_hash))
            $this->addError($attribute,'Old password is incorrect');
    }

    public function attributeLabels(){
        return [
            'oldpass'=>TBase::_x('OLD_PASSWORD'),
            'newpass'=>TBase::_x('NEW_PASSWORD'),
            'repeatnewpass'=>TBase::_x('CONFIRM_PASSWORD'),
        ];
    }
}