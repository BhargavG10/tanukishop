<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_method_zone_calculation_final}}".
 *
 * @property integer $id
 * @property integer $shipping_method_id
 * @property integer $country_id
 * @property integer $weight_from
 * @property double $weight_to
 * @property double $cost
 */
class ShippingMethodCalculationFinal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_method_zone_calculation_final}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipping_method_id', 'country_id', 'weight_from', 'weight_to', 'cost'], 'required'],
            [['shipping_method_id', 'country_id', 'weight_from'], 'integer'],
            [['weight_to', 'cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shipping_method_id' => Yii::t('app', 'Shipping Method ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'weight_from' => Yii::t('app', 'Weight From'),
            'weight_to' => Yii::t('app', 'Weight To'),
            'cost' => Yii::t('app', 'Cost'),
        ];
    }

    public function getMethod() {
        return $this->hasOne(ShippingMethods::className(),['ID'=>'shipping_method_id']);
    }

    public function getCountry() {
        return $this->hasOne(Countries::className(),['id'=>'country_id']);
    }
}
