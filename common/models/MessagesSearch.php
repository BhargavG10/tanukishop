<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Messages;

/**
 * MessagesSearch represents the model behind the search form about `common\models\Messages`.
 */
class MessagesSearch extends Messages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_read_by_admin', 'is_read_by_user'], 'integer'],
            [['message_from', 'date', 'msg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Messages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
//            'date' => $this->date,
            'is_read_by_admin' => $this->is_read_by_admin,
            'is_read_by_user' => $this->is_read_by_user,
        ]);

        $query->andFilterWhere(['like', 'message_from', $this->message_from])
            ->andFilterWhere(['like', 'msg', $this->msg])
            ->groupBy('user_id');

        if (count($params)==0) {
		    $query->orderBy( 'date DESC' );
	    }

	    $query->andFilterWhere([
            'DATE(date)' => $this->date,
	    ]);
        return $dataProvider;
    }
}
