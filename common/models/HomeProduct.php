<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_home_product}}".
 *
 * @property integer $id
 * @property string $shop
 * @property string $product_id
 * @property string $title
 * @property string $image_url
 * @property double $price
 * @property integer $section_id
 * @property string $status
 * @property integer $serial
 */
class HomeProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_home_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop', 'status','title','image_url','price'], 'string'],
            [['shop','product_id','status','section_id','title','image_url','price'], 'required'],
            [['serial','section_id'], 'integer'],
            [['product_id'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shop' => Yii::t('app', 'Shop'),
            'product_id' => Yii::t('app', 'Product ID'),
            'section_id' => Yii::t('app', 'Section'),
            'status' => Yii::t('app', 'Status'),
            'serial' => Yii::t('app', 'Serial'),
            'title' => Yii::t('app', 'Product Name'),
            'image_url' => Yii::t('app', 'Image Path'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    public function getSection() {
        return $this->hasOne(HomeSection::className(),['id'=>'section_id']);
    }
}
