<?php

namespace common\models;

use Yii;
use common\components\TBase;
/**
 * This is the model class for table "{{%tnk_user_shipping_address}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property string $appt_no
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property integer $phone
 * @property string $email
 * @property string $user_id
 */
class UserShippingAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_user_shipping_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_1', 'city','phone','country','user_id'], 'required'],
            [['email'], 'email'],
            [['address_1', 'address_2'], 'string'],
            [['phone'], 'integer'],
            [['first_name', 'last_name', 'company', 'appt_no', 'city', 'state', 'country', 'zipcode', 'email'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => TBase::_x('FIRST_NAME'),
            'last_name' => TBase::_x('LAST_NAME'),
            'company' => TBase::_x('COMPANY_NAME'),
            'address_1' => TBase::_x('ADDRESS_1'),
            'address_2' => TBase::_x('ADDRESS_2'),
            'appt_no' => TBase::_x('APPT_NO'),
            'city' => TBase::_x('CITY'),
            'state' => TBase::_x('STATE'),
            'country' => TBase::_x('COUNTRY'),
            'zipcode' => TBase::_x('ZIPCODE'),
            'phone' => TBase::_x('PHONE'),
            'email' => TBase::_x('EMAIL'),
            'user_id' => Yii::t('app', 'User'),
        ];
    }
}
