<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 11/15/16
 * Time: 12:32 PM
 */

namespace common\models;


use common\components\TBase;
use yii\base\Model;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\Operations\Lookup;
use ApaiIO\ApaiIO;

class Amazon
{
    public $ACCESS_KEY_ID;
    public $SECRET_ACCESS_KEY;
    public $ASSOCIATE_TAG;
    public $ACCESS_URL;
    public $COUNTRY;
    public $AmazonProduct;
    public $amazonMin;
    public $amazonMax;
    public $amazonPrice;

    /**
     * Amazon constructor.
     * @param $ACCESS_KEY_ID
     * @param $SECRET_ACCESS_KEY
     * @param $ASSOCIATE_TAG
     * @param $ACCESS_URL
     */
    function __construct() {
        $this->ACCESS_KEY_ID = TBase::TanukiSetting('AMAZON_API_ACCESS_KEY_ID');
        $this->SECRET_ACCESS_KEY = TBase::TanukiSetting('AMAZON_API_SECRET_ACCESS_KEY');
        $this->ASSOCIATE_TAG = TBase::TanukiSetting('AMAZON_API_ASSOCIATE_TAG');
        $this->ACCESS_URL = TBase::TanukiSetting('AMAZON_API_ACCESS_URL');
        $this->COUNTRY = TBase::TanukiSetting('AMAZON_API_COUNTRY');
    }

    public function listing($node ='1350380031', $searchIndex = 'Toys', $page = 1){

        $base_param = 'AWSAccessKeyId='.$this->ACCESS_KEY_ID;

        $params = array();

        $params['Service']        = 'AWSECommerceService';
        $params['Version']        = '2009-10-01';
        $params['Operation']      = 'ItemSearch';

        $params['SearchIndex']    = $searchIndex;


        $params['BrowseNode']       = $node;        // search key ( UTF-8 )
        //$params['Keywords']       = 'Windows';        // search key ( UTF-8 )
        $params['AssociateTag']   = $this->ASSOCIATE_TAG;

        $params['ResponseGroup']  = 'Small,Medium,Large,VariationSummary,Reviews';

        $params['SignatureMethod']  = 'HmacSHA256';   // signature format name.
        $params['SignatureVersion'] = 2;              // signature version.

        // time zone (ISO8601 format)
        $params['Timestamp']      = gmdate('Y-m-d\TH:i:s\Z');

        $params['ItemPage']      = $page;


        if ($this->amazonMin != false) {
            $params['MinimumPrice'] = $this->amazonMin; # 100 = 1.00

        }
        if ($this->amazonMax != false) {
            $params['MaximumPrice'] = $this->amazonMax; # 100 = 1.00
        }
        //price	Price: low to high         -price	Price: high to low
        if ($this->amazonPrice != false) {
            $params['Sort']           = $this->amazonPrice;
        } else {
            $params['Sort']           = 'salesrank';
        }

        // sort $param by asc.
        ksort($params);

        // create canonical string.
        $canonical_string = $base_param;
        foreach ($params as $k => $v) {
            $canonical_string .= '&'.$this->urlencode_RFC3986($k).'='.$this->urlencode_RFC3986($v);
        }

        // create signature strings.( HMAC-SHA256 & BASE64 )
        $parsed_url = parse_url($this->ACCESS_URL);
        $string_to_sign = "GET\n{$parsed_url['host']}\n{$parsed_url['path']}\n{$canonical_string}";

        $signature = base64_encode(
            hash_hmac('sha256', $string_to_sign, $this->SECRET_ACCESS_KEY, true)
        );

        // create URL strings.
        $url = $this->ACCESS_URL.'?'.$canonical_string.'&Signature='.$this->urlencode_RFC3986($signature);
        // request to amazon !!
        $response = file_get_contents($url);

        // response to strings.
        $parsed_xml = null;
        if(isset($response)){
            $parsed_xml = simplexml_load_string($response);
        }

        // print out of response.
        if( $response &&
            isset($parsed_xml) &&
            !$parsed_xml->faultstring &&
            !$parsed_xml->Items->Request->Errors  ){
                return $parsed_xml;
        } else {
            return $parsed_xml;
        }
    }

    public function productList($node ='1350380031', $searchIndex = 'Toys', $page = 1)
    {
        $conf = new GenericConfiguration();
        $conf
            ->setCountry($this->COUNTRY)
            ->setAccessKey($this->ACCESS_KEY_ID)
            ->setSecretKey($this->SECRET_ACCESS_KEY)
            ->setAssociateTag($this->ASSOCIATE_TAG); // tanuki

        $search = new Search();
        $search->setPage($page); //page should be less then 10 as per api described
        if ($this->amazonMin != false) {
            $search->setMinimumPrice($this->amazonMin); # 100 = 1.00
        }
        if ($this->amazonMax != false) {
            $search->setMaximumPrice($this->amazonMax);
        }
        //$search->setSort('-price'); //relevancerank , inverseprice
        //price	Price: low to high         -price	Price: high to low
        if ($this->amazonPrice != false) {
            $search->setSort($this->amazonPrice); //relevancerank , inverseprice
        }
        $search->setBrowseNode($node);
        $search->setCategory($searchIndex);
        $search->setResponseGroup(array('Large','VariationSummary','Reviews'));
        //'Request','ItemIds','Small','Medium','Large','Offers','OfferFull','OfferSummary','OfferListings','PromotionSummary','PromotionDetails','VariationMinimum','VariationSummary','VariationMatrix','VariationOffers','Variations','TagsSummary','Tags','ItemAttributes','MerchantItemAttributes','Tracks','Accessories','EditorialReview','SalesRank','BrowseNodes','Images','Similarities','Subjects','Reviews','ListmaniaLists','SearchInside','PromotionalTag','SearchBins','AlternateVersions','Collections','RelatedItems','ShippingCharges','ShippingOptions']ãªã©ã§ã™ã€‚
        $apaiIO = new ApaiIO($conf);
        return $apaiIO->runOperation($search);
    }

    public function productDetail($id)
    {
        $conf = new GenericConfiguration();
        $conf
            ->setCountry($this->COUNTRY)
            ->setAccessKey($this->ACCESS_KEY_ID)
            ->setSecretKey($this->SECRET_ACCESS_KEY)
            ->setAssociateTag($this->ASSOCIATE_TAG); // tanuki

        $lookup = new Lookup();
        $lookup->setItemIds($id);
        //$lookup->setResponseGroup(array('Small','Medium','Large','Offers','OfferFull','OfferSummary','OfferListings','PromotionSummary','VariationSummary','VariationMatrix','VariationOffers','Variations','ItemAttributes','Tracks','Accessories','EditorialReview','SalesRank','BrowseNodes','Images','Similarities','Reviews','SearchInside','PromotionalTag','SearchBins','AlternateVersions','Collections','RelatedItems','ShippingCharges','ShippingOptions'));
        //$lookup->setResponseGroup(array('Small','Medium','Large','VariationSummary','VariationMatrix','VariationOffers','Variations','ItemAttributes','Accessories','EditorialReview','SalesRank','BrowseNodes','Similarities','Reviews','SearchInside','AlternateVersions','Collections','RelatedItems','ShippingCharges','ShippingOptions'));
        //$lookup->setResponseGroup(array('Large','VariationSummary','Variations','ItemAttributes','EditorialReview'));
        $lookup->setResponseGroup(array('Large','VariationSummary','ItemAttributes','EditorialReview'));
        $apaiIO = new ApaiIO($conf);
        return $apaiIO->runOperation($lookup);
    }


    public function search($SearchIndex ='All', $keyword = 'Toys', $BrowseNode = null, $page = 1)
    {
        $conf = new GenericConfiguration();
        $conf
            ->setCountry($this->COUNTRY)
            ->setAccessKey($this->ACCESS_KEY_ID)
            ->setSecretKey($this->SECRET_ACCESS_KEY)
            ->setAssociateTag($this->ASSOCIATE_TAG); // tanuki

        $search = new Search();
        $search->setPage($page);
        if ($BrowseNode != null) {
            $search->setBrowseNode($BrowseNode);
        }
        $search->setKeywords($keyword);
        $search->setCategory($SearchIndex);
        $search->setResponseGroup(array('Large','VariationSummary','VariationMatrix','Variations','ItemAttributes','Accessories','EditorialReview','SalesRank','BrowseNodes','Similarities'));

        $apaiIO = new ApaiIO($conf);
        return $apaiIO->runOperation($search);
    }

    public function urlencode_RFC3986($str)
    {
        return str_replace('%7E', '~', rawurlencode($str));
    }


        public function testing()
    {
        //-------------------------------------------------------------------
        /*
         * $ params [ 'Operation'] = 'ItemLookup';
$ params [ 'ResponseGroup'] = 'Large';
$ params [ 'ItemId'] = 'B0002IVURC';*/
//	define("ACCESS_URL", 'https://aws.amazonaws.jp/onca/xml');
//-------------------------------------------------------------------


//-------------------------------------------------------------------
// this function is encode with RFC3986 format.
//-------------------------------------------------------------------
        function urlencode_RFC3986($str)
        {
            return str_replace('%7E', '~', rawurlencode($str));
        }


//-------------------------------------------------------------------
// Main routine.
//-------------------------------------------------------------------
        $base_param = 'AWSAccessKeyId='.$this->ACCESS_KEY_ID;

        $params = array();

        $params['Service']        = 'AWSECommerceService';
        $params['Version']        = '2009-10-01';
        $params['Operation']      = 'ItemSearch';

        $params['SearchIndex']    = 'Software';
        $params['Sort']           = 'salesrank';

        $params['Keywords']       = 'Windows';        // search key ( UTF-8 )
        $params['AssociateTag']   = $this->ASSOCIATE_TAG;

        $params['ResponseGroup']  = 'ItemAttributes,Offers, Images ,Reviews ';

        $params['SignatureMethod']  = 'HmacSHA256';   // signature format name.
        $params['SignatureVersion'] = 2;              // signature version.

        // time zone (ISO8601 format)
        $params['Timestamp']      = gmdate('Y-m-d\TH:i:s\Z');

// sort $param by asc.
        ksort($params);

// create canonical string.
        $canonical_string = $base_param;
        foreach ($params as $k => $v) {
            $canonical_string .= '&'.urlencode_RFC3986($k).'='.urlencode_RFC3986($v);
        }

// create signature strings.( HMAC-SHA256 & BASE64 )
        $parsed_url = parse_url($this->ACCESS_URL);
        $string_to_sign = "GET\n{$parsed_url['host']}\n{$parsed_url['path']}\n{$canonical_string}";

        $signature = base64_encode(
            hash_hmac('sha256', $string_to_sign, $this->SECRET_ACCESS_KEY, true)
        );

// create URL strings.
        $url = ACCESS_URL.'?'.$canonical_string.'&Signature='.urlencode_RFC3986($signature);

// request to amazon !!
        $response = file_get_contents($url);

// response to strings.
        $parsed_xml = null;
        if(isset($response)){
            $parsed_xml = simplexml_load_string($response);
        }

        $err_count=0;

// print out of response.
        if( $response &&
            isset($parsed_xml) &&
            !$parsed_xml->faultstring &&
            !$parsed_xml->Items->Request->Errors  ){

            // total results num.
            $total_results = $parsed_xml->Items->TotalResults;
            // total results pages.
            $total_pages = $parsed_xml->Items->TotalPages;

            print("All Result Count:".$total_results."  | Pages :".$total_pages );

            print("
<table>");
            foreach($parsed_xml->Items->Item as $current){
                $nerr=0;

                print("
<tr>
<td><font size='-1'>");

//                print('<a href="'.$current->DetailPageURL.'"><img src="'.$current-- alt="" />SmallImage->URL.'" border="0"></a>
//');
                print('<a href="'.$current->DetailPageURL.'">'.$current->ItemAttributes->Title.'</a>
');
                print($current->ItemAttributes->Manufacturer.'
');

                if(isset($current->Offers) && isset($current->Offers->Offer)){
                    print( $current->Offers->Offer->OfferListing->Price->FormattedPrice.'
');
                } else {
                    print($current->ItemAttributes->ListPrice->FormattedPrice.'
');
                }
                if(isset($current->CustomerReviews->TotalReviews) && $current->CustomerReviews->TotalReviews>0){
                    print('Rate:<a href="'.$current->ItemLinks->ItemLink[2]->URL.'">');
                    print($current->CustomerReviews->AverageRating.'('. $current->CustomerReviews->TotalReviews .')'.'
');
                    print('</a><hr />');
                }



                print("</td>
</tr>
");
            }
            print("</table>
");
        }
    }

}