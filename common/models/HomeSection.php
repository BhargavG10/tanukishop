<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_home_section}}".
 *
 * @property integer $id
 * @property string $title_en
 * @property string $title_ru
 * @property integer $serial_no
 * @property integer $status
 */
class HomeSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_home_section}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_en', 'title_ru', 'serial_no'], 'required'],
            [['serial_no', 'status'], 'integer'],
            [['title_en'], 'string', 'max' => 224],
            [['title_ru'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_en' => Yii::t('app', 'Title En'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'serial_no' => Yii::t('app', 'Serial No'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return HomeSectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HomeSectionQuery(get_called_class());
    }

    public function getProducts() {
        return $this->hasMany(HomeProduct::className(),['section_id'=>'id']);
    }
}

