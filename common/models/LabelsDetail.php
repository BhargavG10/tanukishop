<?php

namespace common\models;

use common\components\Language;
use Yii;

/**
 * This is the model class for table "{{%tnk_labels_detail}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $lang_code
 * @property integer $labels_id
 *
 * @property TnkLabels $labels
 */
class LabelsDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_labels_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'lang_code', 'labels_id'], 'required'],
            [['labels_id'], 'integer'],
            [['title'], 'string', 'max' => 225],
            [['lang_code'], 'string', 'max' => 10],
            [['labels_id'], 'exist', 'skipOnError' => true, 'targetClass' => Labels::className(), 'targetAttribute' => ['labels_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'lang_code' => Yii::t('app', 'Language'),
            'labels_id' => Yii::t('app', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabels()
    {
        return $this->hasOne(Labels::className(), ['id' => 'labels_id']);
    }

    public function getLanguage()
    {
        return $this->hasOne(SiteLanguages::className(), ['code' => 'lang_code']);
    }

    /**
     * @param $model
     */
    public function saveEnglish($model) {
        $labelDetail = new LabelsDetail();
        $labelDetail->title = $model->title;
        $labelDetail->lang_code = 'en-US';
        $labelDetail->labels_id = $model->id;
        $labelDetail->save();
    }

    /**
     * @param $model
     */
    public function saveRussian($model) {
        $labelDetail = new LabelsDetail();
        $labelDetail->title = $model->russian;
        $labelDetail->lang_code = 'ru-RU';
        $labelDetail->labels_id = $model->id;
        $labelDetail->save();
    }
}
