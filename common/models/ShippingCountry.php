<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_shipping_country}}".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $is_active
 */
class ShippingCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_shipping_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['is_active'], 'integer'],
            [['name', 'code'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }
}
