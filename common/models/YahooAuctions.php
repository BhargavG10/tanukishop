<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 10/05/16
 * Time: 4:27 PM
 */

namespace common\models;

use Yii;
use \common\components\TBase;
use common\components\TCurrencyConvertor;
//http://tiger4th.com/auc/computer/?q=&s=cbids&o=d&t=1&c=23336
//http://developer.yahoo.co.jp/webapi/auctions/
//http://tiger4th.com/auc/computer/?q=&s=cbids&o=d&t=1&n=3
class YahooAuctions extends TCurrencyConvertor
{
    /**
     * @var
     */
    public $YahooAction;
    public $ITEM_SEARCH_API_URL;
    public $ITEM_DETAIL_API_URL;
    public $APIID;
    public $AUCTION_URL;

    function __construct() {
        $this->AUCTION_URL = 'https://auctions.yahooapis.jp/AuctionWebService/V2/';
        $this->ITEM_SEARCH_API_URL = TBase::TanukiSetting('YAHOO_ITEM_SEARCH_API_URL');
        $this->ITEM_DETAIL_API_URL = TBase::TanukiSetting('YAHOO_ITEM_DETAIL_API_URL');
        $this->APIID = TBase::TanukiSetting('YAHOO_API_ID');
    }

    public function urlencode_RFC3986($str)
    {
        return str_replace('%7E', '~', rawurlencode($str));
    }

    public function productsListing($params){

        $params = array_merge($params,['appid'  =>  $this->APIID]);
        $api_url = $this->AUCTION_URL.'categoryLeaf?'.Http_build_query($params);
        $category = simplexml_load_file($api_url);
        return $category = json_decode(json_encode($category), true);
    }

    public function productsSellerListing($params){

        $params = array_merge($params,['appid'  =>  $this->APIID]);
        $api_url = $this->AUCTION_URL.'sellingList?'.Http_build_query($params);
        $category = simplexml_load_file($api_url);
        return $category = json_decode(json_encode($category), true);
    }

    public function auctionSearch($params){

        $params = array_merge($params,['appid'  =>  $this->APIID]);
        $api_url = $this->AUCTION_URL.'search?'.Http_build_query($params);
        $category = simplexml_load_file($api_url);
        return $category = json_decode(json_encode($category), true);
    }

    /**
     * @param $auctionID
     * @param string $response
     */
    public function productsDetail($auctionID, $response = 'large')
    {
        $params = Array (
            'auctionID'  =>  $auctionID,
            'responsegroup'  =>  $response
        );
        $ch = curl_init ( $this->AUCTION_URL.'php/auctionItem?' . Http_build_query ( $params ));
        Curl_setopt_array ( $ch , Array (
            CURLOPT_RETURNTRANSFER =>  True ,
            CURLOPT_USERAGENT       =>  "Yahoo AppID: $this->APIID"
        ));

        $Result = Curl_exec ( $ch );
        curl_close ( $ch );

        return $this->YahooAction = unserialize($Result);
    }

    /**
     * @param $ID
     * @return mixed
     */
    public function getCategories($ID) {
        $params = Array (
            'category'  =>  $ID,
            'responsegroup'  =>  'large'
        );

        $params = array_merge($params,['appid'  =>  $this->APIID]);
        $api_url = $this->AUCTION_URL.'categoryTree?'.Http_build_query($params);
        $category = simplexml_load_file($api_url);
        return $category = json_decode(json_encode($category), true);

    }

    /**
     * @param $auctionID
     * @param string $response
     * @return mixed
     */
    public function productsDetailForFavorite($auctionID, $response = 'large'){
        $params = Array (
            'auctionID'  =>  $auctionID,
            'responsegroup'  =>  $response
        );
        $ch = curl_init ( $this->AUCTION_URL.'php/auctionItem?' . Http_build_query ( $params ));
        Curl_setopt_array ( $ch , Array (
            CURLOPT_RETURNTRANSFER =>  True ,
            CURLOPT_USERAGENT       =>  "Yahoo AppID: $this->APIID"
        ));

        $Result = Curl_exec ( $ch );
        curl_close ( $ch );

        return unserialize($Result);
    }

    /**
     * @param bool $imageUrl
     * @return string
     */
    public function getImage($imageUrl = false) {
	    $img = '';
        if (
            isset($this->YahooAction['ResultSet']['Result']['Img']['Image1']) &&
            !empty($this->YahooAction['ResultSet']['Result']['Img']['Image1'])
        ) {
            if ($imageUrl) {
                $img = $this->YahooAction['ResultSet']['Result']['Img']['Image1'];
            } else {
	            $img = Yii::$app->formatter->asImage($this->YahooAction['ResultSet']['Result']['Img']['Image1'],['width'=>'70','height'=>'70']);
            }
            return $img;
        } else {
            return  $img;
        }
    }

    /**
     * @return string
     */
    public function getPrice() {
        if (isset($this->YahooAction['ResultSet']['Result']['Price'])) {
            if(isset($this->YahooAction['ResultSet']['Result']['TaxinPrice'])) {
                return $this->convert($this->YahooAction['ResultSet']['Result']['TaxinPrice'],'JPY','JPY');
            } else {
                return $this->convert($this->YahooAction['ResultSet']['Result']['Price'],'JPY','JPY');
            }
        }
    }

    /**
     * @return string
     */
    public function getTimeLeft(){
        $now = new \DateTime();
        $future_date = new \DateTime($this->YahooAction['ResultSet']['Result']['EndTime']);
        $interval = $future_date->diff($now);
        return $interval->format("%ad: %hh: %im: %ss");
    }

    /**
     * @return string
     */
    public function getTopBidder() {
        if (
            isset($this->YahooAction['ResultSet']['Result']['HighestBidders'])  &&
            isset($this->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'])
        ) {
            return $this->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'];
        } else {
            return '-';
        }
    }

    public function myAuctionsList($auctionID, $response = 'large'){
        $params = Array (
            'auctionID'  =>  $auctionID,
            'responsegroup'  =>  $response
        );
        $ch = curl_init ( $this->AUCTION_URL.'php/auctionItem?' . Http_build_query ( $params ));
        Curl_setopt_array ( $ch , Array (
            CURLOPT_RETURNTRANSFER =>  True ,
            CURLOPT_USERAGENT       =>  "Yahoo AppID: $this->APIID"
        ));

        $Result = Curl_exec ( $ch );
        curl_close ( $ch );

        return unserialize($Result);
    }
}