<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_yahoo_connect}}".
 *
 * @property integer $id
 * @property string $access_token
 * @property string $token_type
 * @property string $time_started
 * @property string $time_expired
 * @property string $refresh_token
 */
class YahooConnect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_yahoo_connect}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_token', 'token_type', 'time_started', 'time_expired', 'refresh_token'], 'required'],
            [['access_token', 'refresh_token'], 'string'],
            [['time_started', 'time_expired'], 'safe'],
            [['token_type'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'token_type' => Yii::t('app', 'Token Type'),
            'time_started' => Yii::t('app', 'Time Started'),
            'time_expired' => Yii::t('app', 'Time Expired'),
            'refresh_token' => Yii::t('app', 'Refresh Token'),
        ];
    }
}
