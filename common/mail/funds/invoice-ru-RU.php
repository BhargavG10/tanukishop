<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();
$tanuki = new \common\helper\Tanuki();
?>
<div style="width: 500px; padding: 12px;">
    <section>
        <div>
            <div style="margin-top: 22px;">
                <div style="text-align: left;margin-bottom: 10px;font-size: 20px;"><strong>СЧЁТ</strong></div>
                <div class="order-summary-table col-lg-12">
                    <table width="50%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td>Счет Но:</td>
                            <td><?=$model->id?></td>
                        </tr>
                        <tr>
                            <td>Дата:</td>
                            <td><?=date('Y-m-d',strtotime($model->date));?></td>
                        </tr>
                    </table>
                </div>
            </div><div style="margin-top: 22px;">
                <div class="order-summary-table col-lg-12">
                    <table style="width: 91%;" cellspacing="0" cellpadding="3">
                        <tr>
                            <td style="width:9%;">Клиент (ФИО):</td>
                            <td style="border-bottom:1px solid;width:25%;"><?=Yii::$app->user->identity->first_name?> <?=Yii::$app->user->identity->last_name?></td>
                        </tr>
                        <tr>
                            <td>Адрес:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Город:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Страна:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Тел/Факс:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Эмайл:</td>
                            <td style="border-bottom:1px solid;width:25%;"><?=Yii::$app->user->identity->email?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding: 0px;margin: 20px 0px;">
                <table width="100%" cellspacing="0" cellpadding="10" border="1">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr class="notranslate">
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;">Описание содержимого</td>
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;">Сумма</td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                            Депосит на покупку товаров или оказание услуг
                        </td>
                        <td><?=$tModel->convert($model->total,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
            <div  class="terms" style="margin: 10px; 0px">
                <ul>
                    <li>Сумма указана в Японских йенах (JPY)</li>
                    <li>Оплата должна быть произведена в течение З-х дней с момента выставления счета</li>
                    <li>Все банковские расходы и комиссии банка несет плательщик.</li>
                </ul>
            </div>
            <div class="bank-info" style="margin-top: 20px;">
                <strong><u>Банковские реквизиты:</u></strong>
                <table>
                    <tr><td>Название банка:</td>	<td>The MUFG Bank,. Ltd.</td></tr>
                    <tr><td>Филиал:</td> <td>Kanazawa</td></tr>
                    <tr><td>Адрес банка:</td> <td>2-3-25 Korinbo, Kanazawa, Ishikawa, Japan</td></tr>
                    <tr><td>SWIFT Код:</td> <td>BOTKJPJT</td></tr>
                    <tr><td>Но.счета получателя:</td> <td>0217324</td></tr>
                    <tr><td>Получатель:</td> <td>ONTECO Co., Ltd.</td></tr>
                    <tr><td>Адрес получателя:</td> <td>3-5-22 Hachiman-machi, Imizu, Toyama, Japan</td></tr>
                    <tr><td>Телефон:</td> <td>0766-50-8767</td></tr>
                </table>
            </div>
    </section>
</div>