<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
$this->title = 'Tanuki - Order ID: '.$model->id;
?>
<div style="border:1px solid;width: 500px; padding: 12px;">
    Уважаемый <?=$model->user->first_name.' '.$model->user->last_name?>,<br/>
    <br/>
    Вы успешно пополнили свой счет на Tanuki Shop.<br/>
    <section>
        <div>
            <div style="margin-top: 22px;">
                <div class="order-summary-table col-lg-12">
                    <table width="100%" border="1" cellspacing="0" cellpadding="10">
                        <tr>
                            <td colspan="2" align="center"><strong><?=TBase::ShowLbl('order_details')?></strong></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('email_name'); ?> :<?=$model->user->first_name.' '.$model->user->last_name?></td>
                            <td><?=TBase::ShowLbl('ORDER_ID'); ?> :TNK0000<?= $model->id?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('TOTAL'); ?>:<?=$tModel->convert($model['total'],'JPY','JPY'); ?></td>
                            <td><?=TBase::ShowLbl('CURRENCY'); ?> :<?=$model->currency; ?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('email_method'); ?> :<?=$model->method; ?></td>
                            <td><?=TBase::ShowLbl('ORDER_STATUS'); ?> :<?=$model->getStatus(); ?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('ORDER_DATE'); ?> :<?=$model->date; ?></td>
                            <td><?=TBase::ShowLbl('INVOICE_NUMBER'); ?> :<?=$model->invoice; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding: 0px;">
                <h3 style="text-align: center;"><?=TBase::ShowLbl('PAYMENT');?> (JPY)</h3>
                <table width="100%" border="1" cellspacing="0" cellpadding="10">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr class="notranslate">
                        <td>Описание содержимого</td>
                        <td><?=TBase::ShowLbl('TOTAL')?></td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                            Депосит на покупку товаров или оказание услуг
                        </td>
                        <td><?=$tModel->convert($model->total,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
    <div style="width: 100%">
        <br/>
		Если у Вас возникнут дополнительные вопросы, пожалуйста свяжитесь с нами.<br/><br/>
        
	    Команда Tanuki Shop <br/>
        Onteco Co., Ltd.<br/>
        Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	    Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	    Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	    WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
	    E-mail: sales@tanukishop.com<br/>
	    HP: www.tanukishop.com
    </div>
        </div>
    </section>
</div>