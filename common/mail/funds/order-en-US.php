<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
$this->title = 'Tanuki - Order ID: '.$model->id;
?>
<div style="border:1px solid;width: 500px; padding: 12px;">
    Hello <?=$model->user->first_name.' '.$model->user->last_name?>,<br/>
    <br/>
    Your add fund order at Tanuki was placed successfully.<br/>
    <section>
        <div>
            <div style="margin-top: 22px;">
                <div class="order-summary-table col-lg-12">
                    <table width="100%" border="1" cellspacing="0" cellpadding="10">
                        <tr>
                            <td colspan="2" align="center"><strong><?=TBase::ShowLbl('COMPLETE_ORDER')?></strong></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('NAME'); ?> :<?=$model->user->first_name.' '.$model->user->last_name?></td>
                            <td><?=TBase::ShowLbl('ORDER_ID'); ?> :TNK0000<?= $model->id?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('TOTAL'); ?>:<?=$tModel->convert($model['total'],'JPY','JPY'); ?></td>
                            <td><?=TBase::ShowLbl('CURRENCY'); ?> :<?=$model->currency; ?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('PAYMENT_METHOD'); ?> :<?=$model->method; ?></td>
                            <td><?=TBase::ShowLbl('ORDER_STATUS'); ?> :<?=$model->getStatus(); ?></td>
                        </tr>
                        <tr>
                            <td><?=TBase::ShowLbl('ORDER_DATE'); ?> :<?=$model->date; ?></td>
                            <td><?=TBase::ShowLbl('INVOICE_NUMBER'); ?> :<?=$model->invoice; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding: 0px;">
                <h3 style="text-align: center;"><?=TBase::ShowLbl('PAYMENT');?> (JPY)</h3>
                <table width="100%" border="1" cellspacing="0" cellpadding="10">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr class="notranslate">
                        <td><?=TBase::ShowLbl('SERVICE'); ?></td>
                        <td><?=TBase::ShowLbl('TOTAL')?></td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                            Adding funds to deposit to pay for goods or service
                        </td>
                        <td><?=$tModel->convert($model->total,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
            <div style="width: 100%;margin-top: 40px;font-size: 12px;">
                Thank you for shopping with us! Our manager will contact you later regarding shipping.<br/>
                If you have any questions regarding your order, please do not hesitate to contact us.<br/>
                <br/>
                Tanuki Shop<br/>
                Head Office: Third Street 5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
                Branch Office: Shiraishi 638, Kosugi, Imizu-shi, Toyama, 939-0304, Japan<br/>
                Tel: +81 766 50 8767<br/>
                Fax: + 81 766-50-8768<br/>
                E-mail: sales@tanukishop.com<br/>
                Website: www.tanukishop.com<br/>
                <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg" style="width:100px;">
            </div>
        </div>
    </section>
</div>