<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
?>
<div style="width: 500px; padding: 12px;">
    <section>
        <div>
            <div style="margin-top: 22px;">
                <div style="text-align: left;margin-bottom: 10px;font-size: 20px;"><strong>INVOICE</strong></div>
                <div class="order-summary-table col-lg-12">
                    <table width="50%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td>Invoice No:</td>
                            <td><?=$model->id?></td>
                        </tr>
                        <tr>
                            <td>Date:</td>
                            <td><?=date('Y-m-d',strtotime($model->date));?></td>
                        </tr>
                    </table>
                </div>
            </div><div style="margin-top: 22px;">
                <div class="order-summary-table col-lg-12">
                    <table style="width: 91%;" cellspacing="0" cellpadding="3">
                        <tr>
                            <td style="width:9%;">Buyer:</td>
                            <td style="border-bottom:1px solid;width:25%;"><?=Yii::$app->user->identity->first_name?> <?=Yii::$app->user->identity->last_name?></td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>City/Postal Code:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Tel/Fax:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td style="border-bottom:1px solid;width:25%;"><?=Yii::$app->user->identity->email?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding: 0px;margin: 20px 0px;">
                <table width="100%" cellspacing="0" cellpadding="10" border="1">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr class="notranslate">
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;">Description</td>
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;">Amount</td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                            Deposit for goods or services
                        </td>
                        <td><?=$tModel->convert($model->total,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
            <div  class="terms" style="margin: 10px; 0px">
                <ul>
                    <li>Amount is indicated in Japanese Yen (JPY).</li>
                    <li>Payment should be made within 3 working days from the invoice issue date.</li>
                    <li>All bank transfer fees should be covered by the buyer.</li>
                </ul>
            </div>
            <div class="bank-info" style="margin-top: 20px;">
                <strong><u>Bank details:</u></strong>
                <table>
                    <tr><td>Bank name:</td>	<td>The MUFG Bank,. Ltd.</td></tr>
                    <tr><td>Branch:</td> <td>Kanazawa</td></tr>
                    <tr><td>Bank Address:</td> <td>2-3-25 Korinbo, Kanazawa, Ishikawa, Japan</td></tr>
                    <tr><td>SWIFT Code:</td> <td>BOTKJPJT</td></tr>
                    <tr><td>Beneficiary A/C:</td> <td>0217324</td></tr>
                    <tr><td>Beneficiary:</td> <td>ONTECO Co., Ltd.</td></tr>
                    <tr><td>Address:</td> <td>3-5-22 Hachiman-machi, Imizu, Toyama, Japan</td></tr>
                    <tr><td>Tel:</td> <td>0766-50-8767</td></tr>
                </table>
            </div>
    </section>
</div>