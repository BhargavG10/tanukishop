<?php
    $tModel = new \common\components\TCurrencyConvertor;
    $tbase = new \common\components\TBase();
    $tanuki = new \common\helper\Tanuki();
?>
<div style="font-family: Sans-Serif;">
    <div style="width: 100%">
        <div style="font-weight: bold;margin: 28px 0 24px 24px;">
            <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg">
        </div>
        <div style="font-weight: bold;margin: 28px 0 24px 24px;color: #e69138;font-size: 17px;">
            Congratulations, you are the winner of the following Yahoo auction lot on Tanuki Shop:
        </div>

    </div>
    <div style="width: 100%">
        <div style=" border-radius: 5px;height: auto;padding: 20px;margin-left: 20px;">
            <div style="margin-right: -15px;margin-left: -15px;">
            <table >
                <tr>
                    <td>
                        <img src="<?=$model->getImage()?>" height="150" width="150">
                    </td>
                    <td>
                        <table style="border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Product name :
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <?=$model->title?>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Auction ID :
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <a href="https://www.tanukishop.com/yahoo-auctions/detail?id=<?= $model->auction_id?>"><?= $model->auction_id?></a>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">Price :</th>
                            <td style="border-left:1px solid #fff; color: #e69138;font-size: 30px; "><?=$tModel->convert($model->amount,'JPY','JPY'); ?></td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <div style="width: 100%">
        Our team will process the purchase and issue the invoice for the final amount.<br/>
        If you have any additional questions, please do not hesitate to contact us.<br/><br/>
		
		Tanuki Shop Team<br/>
		Onteco Co., Ltd.<br/>
		Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
		Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
		Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
		WhatsApp: +81 80-4254-6699<br/>
		E-mail: sales@tanukishop.com<br/>
		HP: www.tanukishop.com
    </div>
</div>