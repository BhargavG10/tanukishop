<?php
    $tModel = new \common\components\TCurrencyConvertor;
    $tbase = new \common\components\TBase();
    $tanuki = new \common\helper\Tanuki();
?>
    <div style="width: 100%">
        <div style="font-weight: bold;margin: 28px 0 24px 24px;">
            <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg">
        </div>
        <div style="font-weight: bold;margin: 28px 0 24px 24px;color: #e69138;font-size: 17px;">
            Уважаемый <?=$user; ?>,<br/><br/>
			Поздравляем, Вы выиграли аукционный лот на Tanuki Shop!<br/><br/>
        </div>

    </div>
    <div style="width: 100%">
        <div style=" border-radius: 5px;height: auto;padding: 20px;margin-left: 20px;">
            <div style="margin-right: -15px;margin-left: -15px;">
            <table >
                <tr>
                    <td>
                        <img src="<?=$model->getImage()?>" width="150">
                    </td>
                    <td>
                        <table style="border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Название:
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <?=$model->title?>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Номер лота:
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <a href="https://www.tanukishop.com/yahoo-auctions/detail?id=<?= $model->auction_id?>"><?= $model->auction_id?></a>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">Цена:</th>
                            <td style="border-left:1px solid #fff; color: #e69138;font-size: 30px; "><?=$tModel->convert($model->amount,'JPY','JPY'); ?></td>
                        </tr>
						<tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">Дата:</th>
                            <td style="border-left:1px solid #fff; "><?=$model->auction_ends;?></td>
                        </tr>					
                    </table>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>
        <br/>
    <div style="width: 100%">
        Мы обработаем лот в ближайщее время и сообщим подробности об отправке.<br/>
		Если у Вас возникнут дополнительные вопросы по данному лоту, пожалуйста свяжитесь с нами.<br/><br/>
        
	    Команда Tanuki Shop <br/>
        Onteco Co., Ltd.<br/>
        Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	    Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	    Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	    WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
	    E-mail: sales@tanukishop.com<br/>
	    HP: www.tanukishop.com
    </div>