<?php
    $tModel = new \common\components\TCurrencyConvertor;
    $tbase = new \common\components\TBase();
    $tanuki = new \common\helper\Tanuki();
?>
<div style="font-weight: bold;margin: 28px 0 24px 24px;">
    Hello <?=$user; ?>,<br/><br/>
    You have placed a bid on Tanuki Shop! <br/>
    Until the auction ends, others may over bid you, so please check frequently the auction status and increase your bid if necessary.<br/>
	If your bid wins, we will notify you by email after auction ends.<br/><br/>
</div>

    <div style="width: 100%">
        <div style=" border-radius: 5px;height: auto;padding: 20px;margin-left: 20px;">
            <div style="margin-right: -15px;margin-left: -15px;">
            <table >
                <tr>
                    <td>
                        <img src="<?=$model->getImage()?>" width="150">
                    </td>
                    <td>
                        <table style="border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Product Title:
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <?=$model->title?>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">
                                Auction ID:
                            </th>
                            <td style="border-left:1px solid #fff; ">
                                <a href="https://www.tanukishop.com/yahoo-auctions/detail?id=<?= $model->auction_id?>"><?= $model->auction_id?></a>
                            </td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">Your bid:</th>
                            <td style="border-left:1px solid #fff; color: #e69138;font-size: 30px; "><?=$tModel->convert($model->amount,'JPY','JPY'); ?></td>
                        </tr>
						<tr style="border-bottom: 1px solid #fff;">
                            <th style="padding: 7px;text-align: left;">Auction ends:</th>
                            <td style="border-left:1px solid #fff; "><?=$model->auction_ends;?></td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>

<br/>
    <div style="width: 100%">
        If you have any additional questions, please do not hesitate to contact us.<br/><br/>
		
		Tanuki Shop Team<br/>
		Onteco Co., Ltd.<br/>
		Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
		Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
		Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
		WhatsApp: +81 80-4254-6699<br/>
		E-mail: sales@tanukishop.com<br/>
		HP: www.tanukishop.com
    </div>