<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
$shipping  = $model->shipping;

$this->title = 'Tanuki - Order ID: '.$model->id;
?>
<div style="font-weight: bold;margin: 28px 0 24px 24px;">
    Hello <?=$model->user->first_name.' '.$model->user->last_name?>,<br/>
    Your order at Tanuki was placed successfully. Our manager will contact you later regarding shipping.<br/>
</div>
<section>
    <div>
        <div style=" background: #f2f2f2;border-radius: 5px;float: left;height: auto;min-height: 500px;padding: 10px;width: 81%;margin-left: 20px;">
            <div style="margin-right: -15px;margin-left: -15px;">
                <div class="order-summary-table col-lg-6" style="width: 50%;float: left;    position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;">
                    <h3 style="color: #df7b2b;font-size: 16px;font-weight: 700;padding-left: 0px;padding-right: 20px;margin-top: 10px;margin-bottom: 10px;">Order Summary</h3>
                    <table style="width:60%;border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Order ID </th><td style="border-left:1px solid #fff; ">TNK0000<?= $model->id?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Total </th><td style="border-left:1px solid #fff; "><?=$tModel->convert($model['total'],'JPY','JPY'); ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Currency </th><td style="border-left:1px solid #fff; "><?=$model->currency; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Payment Method </th><td style="border-left:1px solid #fff; "><?=$model->method; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Order Status </th><td style="border-left:1px solid #fff; "><?=$model->status; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Ordered On Date </th><td style="border-left:1px solid #fff; "><?=$model->date; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Invoice </th> <td style="border-left:1px solid #fff; "><?=$model->invoice; ?></td></tr>
                    </table>
                </div>
                <?php if (isset($shipping->first_name)) { ?>
                <div class="order-shipping-summary col-lg-6">
                    <h3 style="color: #df7b2b;font-size: 16px;font-weight: 700;padding-left: 10px;padding-right: 20px;margin-top: 10px;margin-bottom: 10px;">Shipping Summary</h3>
                    <table style="width:45%;border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Name </th> <td style="border-left:1px solid #fff; "><?=$shipping->first_name.' '.$shipping->last_name?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Company Name </th> <td style="border-left:1px solid #fff; "><?=$shipping->company; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Apartment Number </th> <td style="border-left:1px solid #fff; "><?=$shipping->appt_no; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Address </th> <td style="border-left:1px solid #fff; "><?=$shipping->address_1.' '.$shipping->address_2; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">City </th> <td style="border-left:1px solid #fff; "><?=$shipping->city; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">State </th> <td style="border-left:1px solid #fff; "><?=$shipping->state; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Country </th> <td style="border-left:1px solid #fff; "><?=$tbase->countryDetail($shipping->country); ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Phone </th> <td style="border-left:1px solid #fff; "><?=$shipping->phone; ?></td></tr>
                    </table>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-12 margin-top-20 clearfix">
                <h3 class="inner-head">Ordered Product(s) Summary</h3>
                <table width="100%" border="0" cellspacing="1" cellpadding="1" style="border: 1px solid #fff;text-align: center;">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr style="border-bottom: 1px solid #fff;">
                        <td width="10%" height="30" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Image </td>
                        <td width="30%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Product Name</td>
                        <td width="13%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Shop name </td>
                        <td width="12%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Quantity </td>
                        <td width="22%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Tanuki Charges</td>
                        <td width="22%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Domestic Shipping</td>
                        <td width="13%" bgcolor="#df7b2b" style="border-bottom:1px solid #fff;color:#fff">Price </td>

                    </tr>
                    <?php
                    $count = $price = $total = 0 ;
                    if (count($model->orderDetail)>0){
                        foreach($model->orderDetail as $key => $item) {
	                        $count = $price = $total = 0 ;
                            ?>
                            <tr>
                                <td width="20%"  style="border-bottom:1px solid #fff;border-right:1px solid #fff;">
                                    <?php
                                    if (file_exists(\Yii::getAlias('@webroot').'/order-images/'.$item->product_image)) {
                                        echo \yii\helpers\Html::img(Yii::$app->params['order-image-url'].$item->product_image,['width'=>'200']);
                                    } else {
                                        echo \yii\helpers\Html::img(Yii::$app->params['no-image']);
                                    }
                                    ?>
                                </td>
                                <td  style="border-bottom:1px solid #fff;border-right:1px solid #fff;" width="20%" class="translate" data-title="<?=ucfirst($item['product_name']); ?>" data-short="0"><?=ucfirst($item['product_name']); ?></td>
                                <td  style="border-bottom:1px solid #fff;border-right:1px solid #fff;"  width="13%"><?=ucfirst($item['shop']); ?></td>
                                <td  style="border-bottom:1px solid #fff;border-right:1px solid #fff;"  width="12%"><?=$item['quantity']; ?></td>
                                <td  style="border-bottom:1px solid #fff;border-right:1px solid #fff;"  width="22%"><?=$tModel->convert($model->tanuki_charges,'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #fff;border-right:1px solid #fff;"  width="22%"><?=$tModel->convert($item->domestic_shipping,'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #fff;text-align: center;"  width="13%"><?=$tModel->convert(($item->price*$item['quantity'])+$model->tanuki_charges,'JPY','JPY'); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th colspan="2" align="right"></th>
                            <th colspan="3" style="border-left:1px solid #fff;border-right:1px solid #fff;border-bottom:1px solid #fff; padding-right: 33px;" align="right">Sub Total</th>
                            <td style="padding:12px;border-bottom:1px solid #fff;" align="center"><?=$tModel->convert($model->subtotal,'JPY','JPY'); ?></td>
                        </tr>
<!--                        <tr style="border-bottom: 1px solid #fff;">-->
<!--                            <th colspan="2" align="right"></th>-->
<!--                            <th colspan="3" style="border-left:1px solid #fff;border-right:1px solid #fff;border-bottom:1px solid #fff;" align="right">Tanuki Charges</th>-->
<!--                            <td style="padding:12px" align="center">--><?//=$tModel->convert($model->tanuki_charges,'JPY','JPY'); ?><!--</td>-->
<!--                        </tr>-->
                        <tr style="border-bottom: 1px solid #fff;">
                            <th colspan="2" align="right"></th>
                            <th colspan="3" style="border-left:1px solid #fff;border-right:1px solid #fff;border-bottom:1px solid #fff;padding-right: 33px;" align="right">Shipping Charges</th>
                            <td style="padding:12px;border-bottom:1px solid #fff;" align="center"><?=($model->shipping_charges) ? $model->shipping_charges : 'Pending'; ?></td>
                        </tr>
                        <tr style="border-top: 1px solid #fff;">
                            <th colspan="2" align="right"></th>
                            <th colspan="3" style="background-color: rgb(204, 204, 204);padding-right: 33px;"   align="right">Total (before shipping)</th>
                            <td style="padding:12px;" align="center"><?=$tModel->convert($model->total,'JPY','JPY'); ?></td>
                        </tr>
                    <?php } else { ?>
                        <tr style="border-bottom: 1px solid #fff;">
                            <td  style="padding:12px" colspan="6" align="center"><?=TBase::ShowLbl('EMPTY_CART'); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <?php if ($model->method =='Bank Deposit') { ?>
            Bank Details<br/>
            <div style="width: 100%;clear: both;">
                <?php

                $data = \common\models\Page::findOne(['slug'=>['bank-transfer']]);
                echo $data['detail_en'];
                ?>

                <div style="font-size:10px;margin-top: 12px;"><strong>[*] All charges must borne by the remitter</strong><div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div style="width: 100%;clear: both;padding: 22px;">
            Thank you for shopping with us! Our manager will contact you later regarding shipping.<br/>
            If you have any questions regarinding your order, please do not hesitate to contact us.<br/>
            <br/>
            Tanuki Shop<br/>
            Head Office: Third Street 5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
            Branch Office: Shiraishi 638, Kosugi, Imizu-shi, Toyama, 939-0304, Japan<br/>
            Tel: +81 766 50 8767<br/>
            Fax: + 81 766-50-8768<br/>
            E-mail: sales@tanukishop.com<br/>
            Website: www.tanukishop.com<br/>
            <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg">
        </div>
    </div>
</section>