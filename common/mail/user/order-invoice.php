<?php
    $tModel = new \common\components\TCurrencyConvertor;
    $tbase = new \common\components\TBase();
    $tanuki = new \common\helper\Tanuki();
?>
<div style="font-family: Sans-Serif;">
    <div style="width: 100%">
        <div style="font-weight: bold;margin: 28px 0 24px 24px;">
            <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg">
        </div>
        <div style="font-weight: bold;margin: 28px 0 24px 24px;color: #e69138;font-size: 17px;">
            Please check your order invoice:
        </div>
    </div>
    <div style="width: 100%">
        <div style=" border-radius: 5px;height: auto;padding: 20px;margin-left: 20px;">
            <div style="margin-right: -15px;margin-left: -15px;">
            <table >
                <tr>
                    <td>
                        <img src="<?=$model->getImage()?>" height="300" width="300">
                    </td>
                    <td>
                        <table style="border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Product name :</th><td style="border-left:1px solid #fff; ">TNK0000<?= $model->id?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Order ID :</th><td style="border-left:1px solid #fff; "><?=$model->orderDetail[0]->product_name?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Auction ID :</th><td style="border-left:1px solid #fff; "><a href="https://www.tanukishop.com/yahoo-auctions/detail?id=<?= $model->orderDetail[0]->product_id?>"><?= $model->orderDetail[0]->product_id?></a></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Price :</th><td style="border-left:1px solid #fff; color: #e69138;font-size: 30px; "><?=$tModel->convert($model->total,'JPY','JPY'); ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Tanuki Fee:</th><td style="border-left:1px solid #fff; "><label style="font-size: 17px;color: #782f4a;"><?=$model->tanuki_charges;?></label></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Domestic Shipping Fee:</th><td style="border-left:1px solid #fff; "><label style="font-size: 17px;color: #782f4a;"><?=$model->shipping_charges;?></label></td></tr>
                    </table>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <div style="width: 100%">
        Our team will process the purchase and issue the invoice for the final amount.<br/><br/>
        If you have any additional questions, please contact us.<br/><br/>
        Tanuki Team<br/>
        Tel: +81 766 50 8767<br/>
        Fax: + 81 766-50-8768<br/>
        E-mail: sales@tanukishop.com<br/>
        HP: www.tanukishop.com<br/>
    </div>
</div>