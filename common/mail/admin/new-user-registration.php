<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="account-activate">
    <p>
	  Новый пользователь зарегистрировался на сайте.<br/>
	  Данные пользователя:
	</p>

    <p>
	  Клиент:     <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?><br/>
	  Email: <?=$user->email; ?>	
	</p>
    <p>
	  <strong><a href="http://admin.tanukishop.com/customer/view?id=<?=$user->id?>">Открыть страницу клиента в админе ></a></strong>
	</p>
	<p>
       --<br/>
	   Tanuki Admin
    </p>
</div>
