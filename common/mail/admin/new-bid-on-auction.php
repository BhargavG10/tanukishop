<?php
    $tModel = new \common\components\TCurrencyConvertor;
    $tbase = new \common\components\TBase();
    $tanuki = new \common\helper\Tanuki();
?>

<p>Клиент сделал ставку на аукционе.</p>
<div style="width: 100%">
    <div style=" background: #f2f2f2;border-radius: 5px;height: auto;padding: 20px;margin-left: 20px;">
        <div style="margin-right: -15px;margin-left: -15px;">
            <table width="100" style="width:100%;border-spacing: 0;border-collapse: collapse;background-color: transparent;border: 1px solid #fff;" cellpadding="8">
                <tr style="border-bottom: 1px solid #fff;">
                    <th style="padding: 7px;text-align: left;">
                        Номер лота:
                    </th>
                    <td style="border-left:1px solid #fff; ">
                        <a href="https://www.tanukishop.com/yahoo-auctions/detail?id=<?=$model->auction_id?>"><?=$model->auction_id?></a>
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid #fff;">
                    <th style="padding: 7px;text-align: left;">Ставка: </th>
                    <td style="border-left:1px solid #fff; "><?=$tModel->convert($model->amount,'JPY','JPY'); ?></td>
                </tr>
                <tr style="border-bottom: 1px solid #fff;">
                    <th style="padding: 7px;text-align: left;">Окончание торгов: </th>
                    <td style="border-left:1px solid #fff; "><?=$model->auction_ends; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br/>
<br/>

<p>
       --<br/>
	   Tanuki Admin
    </p>