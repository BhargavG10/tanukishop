<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="account-activate">
    <p>Hello <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?>,</p>

    <p>Thank you for your registration on <?=\Yii::$app->name?>. To activate your account, please confirm your email by clicking the link below:</p>
    <p><strong><?= Html::a('Confirm email >>', $link) ?></strong></p>
    <p>After confirmation you can use your email and password indicated below to login to www.tanukishop.com</p>
    <p>
        Email : <?=$user->email; ?><br/>
        Password : <?=$password; ?>
    </p>
    <p>
        If you experience any difficulties with the registration process, please do not hesitate to contact us at sales@tanukishop.com.
    </p>
    <p>Happy shopping!</p>
    <p>
		Tanuki Shop Team<br/>
		Onteco Co., Ltd.<br/>
		Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
		Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
		Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
		WhatsApp: +81 80-4254-6699<br/>
		E-mail: sales@tanukishop.com<br/>
		HP: www.tanukishop.com
    </p>
</div>
