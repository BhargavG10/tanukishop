<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="account-activate">
    <p>Уважаемый <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?>,</p>

    <p>Спасибо за регистрацию на Tanuki Shop. Чтобы активировать Ваш аккаунт, пожалуйста подтвердите Ваш емайл, перейдя по ссылке ниже:</p>
    <p><strong><?= Html::a('Подтвердить >>', $link) ?></strong></p>
    <p>После подтверждения, Вы сможете использовать Ваш емайл и пароль указанный ниже для входа на сайт www.tanukishop.com.</p>
    <p>
        Емайл:   <?=$user->email; ?><br/>
        Пароль: <?=$password; ?>
    </p>
    <p>
        Если у Вас возникли трудности с регистрацией, свяжитесь с нами по емайлу sales@tanukishop.com.
    </p>
    <p>
	   Удачных покупок!<br/><br/>
	   Команда Tanuki Shop <br/>
       Onteco Co., Ltd.<br/>
       Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	   Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	   Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	   WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
	   E-mail: sales@tanukishop.com<br/>
	   HP: www.tanukishop.com
    </p>
</div>
