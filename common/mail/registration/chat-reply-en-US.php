Hello <?=$user->first_name.' '.$user->last_name?>,<br/>
You have a new message from Tanuki Shop:<br/><br/>

<strong><?=nl2br($msg)?></strong><br/><br/>

Please go to <strong><a href="<?=Yii::$app->urlManagerFrontend->createAbsoluteUrl('user/inbox')?>">My Messages</a></strong> to reply.<br/><br/>

Best regards,<br/><br/>

Tanuki Shop Team<br/>
Onteco Co., Ltd.<br/>
Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
WhatsApp: +81 80-4254-6699<br/>
E-mail: sales@tanukishop.com<br/>
HP: www.tanukishop.com