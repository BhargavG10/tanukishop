<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>
	   Dear <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?>,
	</p>
    <p>
	   You have requested a password reset on Tanuki Shop. To continue, please click the link below:
	</p>   
    <p>
	   <strong><?= Html::a('Change password >', $resetLink) ?></strong>
	</p>   
    <p>
	   * If this is a mistake and you haven't requested a password change, please ignore this message - your password will not be changed.
    </p>
    <p>
        If you experience any difficulties with the password reset, please contact us at sales@tanukishop.com.
    </p>
    <p>
		Tanuki Shop Team<br/>
		Onteco Co., Ltd.<br/>
		Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
		Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
		Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
		WhatsApp: +81 80-4254-6699<br/>
		E-mail: sales@tanukishop.com<br/>
		HP: www.tanukishop.com
    </p>
	
	 
</div>
