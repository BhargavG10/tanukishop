Добрый день,<br/>
Вам пришло новое сообщение от Tanuki Shop:<br/><br/>

<strong><?=nl2br($msg)?></strong><br/><br/>

Чтобы ответить, перейдите в <strong><a href="<?=Yii::$app->urlManagerFrontend->createAbsoluteUrl('user/inbox')?>">Мои сообщения</a></strong>.<br/><br/>

Команда Tanuki Shop <br/>
Onteco Co., Ltd.<br/>
Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
E-mail: sales@tanukishop.com<br/>
HP: www.tanukishop.com