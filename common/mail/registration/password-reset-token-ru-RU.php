<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>
	   Уважаемый <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?>,
	</p>
    <p>
	   Вы выбрали функцию восстановления пароля на сайте Tanuki Shop. Чтобы продолжить, нажмите на ссылку ниже:
	</p>   
    <p>
	   <strong><?= Html::a('Изменить пароль', $resetLink) ?></strong>
	</p>   
    <p>
	   * Если Вы получили это письмо по ошибке, скорее всего, другой пользователь ошибочно указал Ваш адрес, пытаясь изменить пароль.<br/> 
	   Если Вы не отправляли запрос, ничего не делайте и не обращайте внимания на это сообщение.
    </p>
	<p>
        Если у Вас возникли трудности с восстановлением пароля, свяжитесь с нами по емайлу sales@tanukishop.com.
    </p>
    <p>
	   Команда Tanuki Shop <br/>
       Onteco Co., Ltd.<br/>
       Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	   Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	   Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	   WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
	   E-mail: sales@tanukishop.com<br/>
	   HP: www.tanukishop.com
    </p>
	
	 
</div>
