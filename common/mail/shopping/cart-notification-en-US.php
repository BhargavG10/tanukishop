Hello,<br/>
Your selected items have been checked for stock availabily.<br/><br/>

You can now proceed to checkout: <strong><a href="https://www.tanukishop.com/cart-products/index">Shopping cart ></a></strong><br/><br/>

    <p>
		Best regards,<br/>
		Tanuki Shop Team<br/>
		Onteco Co., Ltd.<br/>
		Address:  3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
		Branch office: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
		Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
		WhatsApp: +81 80-4254-6699<br/>
		E-mail: sales@tanukishop.com<br/>
		HP: www.tanukishop.com
    </p>