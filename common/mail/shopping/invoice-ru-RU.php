<?php
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
$shipping  = $model->shipping;

$this->title = 'Tanuki - Order ID: '.$model->id;
?>
<div style="font-weight: bold;margin: 28px 0 24px 24px;">
    Уважаемый <?=$model->user->first_name.' '.$model->user->last_name?>,<br/>
    <p>Ваш заказ был обработан.</p>

    <p>Дополнительные расходы для этого заказа составляют <span style="font-weight: bold;font-size: 15px;">¥<?=($model->total - $model->paid_amount)?></span>.<br/>
	   У вас в кошельке есть <span style="font-weight: bold;font-size: 15px;">¥<?=number_format(((isset($model->user->credit)) ? $model->user->credit : 0))?></span>.</p>
	<p>Чтобы покрыть все расходы по этому заказу, добавьте <span style="font-weight: bold;font-size: 15px;">¥<?=number_format($balance)?></span> в свой кошелек.</p>
	<p style="font-size: 16px; font-weight: bold;"><a href="<?=Yii::$app->params['website-url']?>user/add-fund">Пополнить счёт >> </a></p>
</div>

<section>
<div style=" background: #ffffff;border-radius: 7px;float: left;height: auto;padding: 10px;width: 81%;margin-left: 0px;">
    <div style="margin-right: -15px;margin-left: -15px;">
        <div class="order-summary-table col-lg-6" style="width:45%; float:left; position:relative; min-height:1px; padding-right:15px; padding-left:15px;">
                    <h3 style="color: #df7b2b;font-size: 16px;font-weight: 700;padding-left: 0px;padding-right: 20px;margin-top: 10px;margin-bottom: 10px;">Описание заказа</h3>
                    <table style="width:80%; border-spacing:0; border-collapse:collapse; background-color: #f7f7f7;border: 1px solid #bebebe;" cellpadding="8">
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Номер заказа </th><td style="border-left:1px solid #fff; ">TNK0000<?= $model->id?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Дата </th><td style="border-left:1px solid #fff; "><?=date('Y-m-d',strtotime($model->date)); ?></td></tr>						
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Итого </th><td style="border-left:1px solid #fff; "><?=$tModel->convert($model['total'],'JPY','JPY'); ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Валюта </th><td style="border-left:1px solid #fff; "><?=$model->currency; ?></td></tr>
                        <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Метод оплаты </th><td style="border-left:1px solid #fff; "><?=$model->method; ?></td></tr>
						<tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Статус оплаты </th><td style="border-left:1px solid #fff; "><?=$model->status; ?></td></tr>
                        <tr style="border-bottom: 1px solid #bebebe;"><th style="padding: 7px;text-align: left;">Статус заказа </th><td style="border-left:1px solid #fff; "><?=$model->getStatus(); ?></td></tr> 
                    </table>
        </div>
				<?php if (isset($shipping->first_name)) { ?>
                    <div style="width:49%;float:right">
                        <h3 style="color: #df7b2b;font-size: 16px;font-weight: 700;padding-left: 10px;padding-right: 20px;margin-top: 10px;margin-bottom: 10px;">Адрес отправки</h3>
                        <table style="width:80%; border-spacing:0; border-collapse:collapse; background-color: #f7f7f7; border: 1px solid #bebebe;" cellpadding="8">
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">ФИО </th> <td style="border-left:1px solid #fff; "><?=$shipping->first_name.' '.$shipping->last_name?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Компания </th> <td style="border-left:1px solid #fff; "><?=$shipping->company; ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Квартира / дом</th> <td style="border-left:1px solid #fff; "><?=$shipping->appt_no; ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Адрес </th> <td style="border-left:1px solid #fff; "><?=$shipping->address_1.' '.$shipping->address_2; ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Город </th> <td style="border-left:1px solid #fff; "><?=$shipping->city; ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Область / район </th> <td style="border-left:1px solid #fff; "><?=$shipping->state; ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Страна </th> <td style="border-left:1px solid #fff; "><?=$tbase->countryDetail($shipping->country); ?></td></tr>
                            <tr style="border-bottom: 1px solid #fff;"><th style="padding: 7px;text-align: left;">Телефон </th> <td style="border-left:1px solid #fff; "><?=$shipping->phone; ?></td></tr>
                            <tr style="border-bottom: 1px solid #bebebe;"><th style="padding: 7px;text-align: left;">Метод отправки </th> <td style="border-left:1px solid #fff; ">
                            <?php
                                if (isset($shipping->method->title)) {
                                    echo $shipping->method->title;
                                } else {
                                    echo '-';
                                }
                            ?>
                            </td></tr>
                        </table>
                    </div>
				<?php } ?>
    </div>
			
    <div style="clear:both;"></div>			
	<div class="col-lg-12 margin-top-20 clearfix">
        <h3 class="inner-head">Описание товара</h3>
        <table width="100%" border="0" cellspacing="1" cellpadding="1" style="border: 1px solid #bebebe;text-align: center;">
					<?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr style="border-bottom: 1px solid #bebebe;">
                        <td width="7%" height="30" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Фото </td>
                        <td width="29%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Название</td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Магазин </td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Цена</td>
						<td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Количество </td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Комиссия Тануки</td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Доставка</td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Комиссия банка</td>
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Другие комиссии</td>                  
                        <td width="8%" bgcolor="#df7b2b" style="border-bottom:1px solid #bebebe;color:#fff">Итого</td>
                    </tr>					
					<?php
						$count = $price = $total = 0 ;
						if (count($model->orderDetail)>0){
							foreach($model->orderDetail as $key => $item) {
							$price = ($item->price * $item['quantity']) + $item['tanuki_fee'] + $item['domestic_shipping'] + $item['bank_fees'] + $item['other_fees'];
							$total += $price;
					?>
                            <tr>
                                <td width="7%"  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;">
	                <?php
//	                                if (file_exists(\Yii::$app->params['order-image-path'].'/order-images/'.$item->product_image)) {
		                                echo \yii\helpers\Html::img(Yii::$app->params['order-image-url'].$item->product_image,['width'=>'100']);
//	                                } else {
//		                                echo \yii\helpers\Html::img(Yii::$app->params['no-image']);
//	                                }
	                ?>
                                </td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;" width="29%" class="translate" data-title="<?=ucfirst($item['product_name']); ?>" data-short="0"><?=ucfirst($item['product_name']); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=ucfirst($item['shop']); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$item['quantity']; ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($item['tanuki_fee'],'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($item->domestic_shipping,'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($item['bank_fees'],'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($item['other_fees'],'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($item->price,'JPY','JPY'); ?></td>
                                <td  style="border-bottom:1px solid #bebebe;border-right:1px solid #fff;"  width="8%"><?=$tModel->convert($price,'JPY','JPY'); ?></td>
                            </tr>
					<?php
						}
						?>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th colspan="9" style="text-align:right;border-right:1px solid #fff;border-bottom:1px solid #fff; padding-right: 33px;" align="right">Промежуточный итог</th>
                            <td style="padding:12px;border-bottom:1px solid #fff;" align="center"><?=$tModel->convert($model->subtotal,'JPY','JPY'); ?></td>
                        </tr>
                        <tr  class="notranslate" style="border-top: 1px solid #333;">
                            <th colspan="9" style="text-align:right;border-right:1px solid #fff;border-bottom:1px solid #fff; padding-right: 33px;" align="right">Консолидация</th>
                            <td style="padding:12px;border-bottom:1px solid #fff;"  align="center"><?=$tModel->convert($model->consolidation,'JPY','JPY'); ?></td>
                        </tr>
                        <tr style="border-bottom: 1px solid #fff;">
                            <th colspan="9" style="text-align:right;border-right:1px solid #fff;border-bottom:1px solid #fff; padding-right: 33px;" align="right">Стоимость отправки</th>
                            <td style="padding:12px;border-bottom:1px solid #fff;" align="center"><?=($model->shipping_charges) ? '¥'.number_format($model->shipping_charges) : 'Pending'; ?></td>
                        </tr>
                        <tr style="border-top: 1px solid #fff;">
                            <th colspan="9" style="text-align:right;background-color:#bebebe; padding-right: 33px;"   align="right">ИТОГО	</th>
                            <td style="padding:12px; background-color:#bebebe; font-weight:bold; "align="center";><?=$tModel->convert($model->total,'JPY','JPY'); ?></td>
                        </tr>
					<?php } else { ?>
                        <tr style="border-bottom: 1px solid #fff;">
                            <td  style= colspan="6" align="center"><?=TBase::ShowLbl('EMPTY_CART'); ?></td>
                        </tr>
						<?php
					}
					?>
        </table>
    </div>
    <div style="width:100%">&nbsp;</div>
    <div style="width: 100%">
		Если у Вас возникнут дополнительные вопросы по данному лоту, пожалуйста свяжитесь с нами.<br/><br/>
        
	    Команда Tanuki Shop <br/>
        Onteco Co., Ltd.<br/>
        Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	    Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	    Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	    WhatsApp: +81 80-4254-6699 / +7 (908) 459-8352<br/>
	    E-mail: sales@tanukishop.com<br/>
	    HP: www.tanukishop.com
    </div>
</div>	
</section>