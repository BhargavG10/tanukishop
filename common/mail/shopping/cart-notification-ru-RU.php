Добрый день,<br/>
Ваши товары на Tanuki Shop прошли проверку на наличие на складе у поставщика.<br/><br/>

Вы можете преступить к оплате: <strong><a href="https://www.tanukishop.com/cart-products/index">Моя карзина ></a></strong><br/><br/>

    <p>
	   Удачных покупок!<br/><br/>
	   Команда Tanuki Shop <br/>
       Onteco Co., Ltd.<br/>
       Адрес: 3-5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
	   Филиал: Shiraishi 638, Kosugi, Imizu, Toyama, 939-0304, Japan<br/>
	   Tel: +81 766 50 8767  /  Fax: + 81 766-50-8768<br/>
	   WhatsApp: +81 90-3766-6717 / +7 (908) 459-8352<br/>
	   E-mail: sales@tanukishop.com<br/>
	   HP: www.tanukishop.com
    </p>