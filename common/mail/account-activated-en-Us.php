<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="account-activate">
    <p>Hello <?= Html::encode($user->first_name).' '.Html::encode($user->last_name) ?>,</p>

    <p>Your account is activated successfully</p>

    <p>Thanks for your precious time</p>
    <p>
        Regards<br/>
        Tanuki.
    </p>
</div>
