<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        '//cdn.jsdelivr.net/bootstrap/3.3.0/css/bootstrap.min.css',
        'tnk/css/weloveiconfonts.css',
        'tnk/css/easy-responsive-tabs.css',
        'tnk/css/jquery.bxslider.css',
        'tnk/css/style.css',
        'tnk/css/responsive.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',


    ];

    public $js = [
        'tnk/js/bootstrap.min.js',
        '//cdn.jsdelivr.net/jquery.cookie/1.4.1/jquery.cookie.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}