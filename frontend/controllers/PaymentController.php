<?php
namespace frontend\controllers;

use common\models\Order;
use common\models\PaymentMethodStripe;
use common\models\User;
use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \common\components\Paypal;
use common\components\TBase;
use common\components\Alipay;
/**
 * Site controller
 */
class PaymentController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['paypal-payment','create-order','bank-transfer-payment','alipay-payment','stripe-payment','alipay-return','pay-balance-transfer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['alipay-notify'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ('alipay-notify' == $action->id) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * create order
     */
    public function actionCreateOrder() {
        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::savePurchaseOrder()) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']){
            $orderID = \Yii::$app->session['orderID'];
        }

        $OrderModel = Order::findOne($orderID);

	    $total = $OrderModel->total;
//	    $extraCharges = $total*0.002; // extra as per client request
//	    $charges = round(($total * 3.9/100) + 40);
//	    $total_changes = ($charges + $extraCharges + $total);

//	    $coefficient = (40+($total*0.002));
//	    $coefficient = (40+($total*0.0039));
//	    $percentage = 0.039;

	    $cardCharges =  (($total * 0.039 + 40 ) * 1.0402);
	    $total_changes = ($cardCharges + $total);

//	    $cardCharges = round(($total * $percentage) + $coefficient);
//	    $total_changes = ($cardCharges + $total);


	    // paypal tax calculation
	    $OrderModel->total = $total_changes;
	    $OrderModel->save();

        $requestParams = array(
            'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
            'PAYMENTACTION' => 'Sale'
        );

        $creditCardDetails = array(
            'CREDITCARDTYPE' => $_REQUEST['OrderPayment']['type'],
            'ACCT' => $_REQUEST['OrderPayment']['number'],
            'EXPDATE' => $_REQUEST['OrderPayment']['expire_month'].$_REQUEST['OrderPayment']['expire_year'],
            'CVV2' => $_REQUEST['OrderPayment']['security_code']
        );

        $payerDetails = array(
            'FIRSTNAME' => $_REQUEST['OrderPayment']['first_name'],
            'LASTNAME' => $_REQUEST['OrderPayment']['last_name'],
            'COUNTRYCODE' => $_REQUEST['OrderPayment']['COUNTRYCODE'],
            'STATE' => $_REQUEST['OrderPayment']['STATE'],
            'CITY' => $_REQUEST['OrderPayment']['STATE'],
            'STREET' => $_REQUEST['OrderPayment']['STREET'],
            'ZIP' => $_REQUEST['OrderPayment']['ZIP']
        );

        $orderParams = array(
            'AMT' => $OrderModel->total,
            'ITEMAMT' => $OrderModel->total,
            'SHIPPINGAMT' => '0',
            'CURRENCYCODE' => $OrderModel->currency
        );

        $paypal = new \common\components\Paypal();
        $response = $paypal->request('DoDirectPayment',
            $requestParams + $creditCardDetails + $payerDetails + $orderParams
        );

        if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
	        $OrderModel = Order::findOne($orderID);
            $OrderModel->status = 'Success';
            $OrderModel->order_status = Order::DOMESTIC_SHIPPING;
            $OrderModel->payment_id = $response['TRANSACTIONID'];
            $OrderModel->response = json_encode($response);
            $OrderModel->save(false);
	        Order::orderInvoiceMail($orderID);

            unset(\Yii::$app->session['orderID']);
            unset(\Yii::$app->session['carts']);
            echo '1';
            exit;
        } else if( is_array($response) && $response['ACK'] == 'Failure') {
            echo $response['L_LONGMESSAGE0'];
            exit;
        }
    }

    /**
     * paypal payment processing
     */
    private function paypalPaymentProcessing() {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::savePurchaseOrder()) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']){
            $orderID = \Yii::$app->session['orderID'];
        }

        $orderDetail = Order::findOne($orderID);
        $total = $orderDetail->total;

//      old method
//	    $extraCharges = $total*0.002; // extra as per client request
//	    $charges = round(($total * 3.9/100) + 40);
//	    $total_changes = ($charges + $extraCharges + $total);

//	    $coefficient = (40+($total*0.002));
//	    $coefficient = (40+($total*0.0039));
//	    $percentage = 0.039;

	    $cardCharges =  (($total * 0.039 + 40 ) * 1.0402);
	    $total_changes = ($cardCharges + $total);

        // paypal tax calculation
        $orderDetail->total = round($total_changes);
        $orderDetail->save();


        $requestParams = array(
            'RETURNURL' => Url::to(['order/return','success'=>'1'],true),
            'CANCELURL' => Url::to(['order/return','success'=>'0'],true)
        );

        $orderParams = array(
            'PAYMENTREQUEST_0_AMT' => $orderDetail->total,
            'PAYMENTREQUEST_0_SHIPPINGAMT' => '0',
            'PAYMENTREQUEST_0_CURRENCYCODE' => $orderDetail->currency,
            'PAYMENTREQUEST_0_ITEMAMT' => $orderDetail->total
        );

        $paypal = new Paypal();
        $response = $paypal->request('SetExpressCheckout', $requestParams + $orderParams);
        if (is_array($response) && $response['ACK'] == 'Success') {
            $token = $response['TOKEN'];
            $PAYPAL_WEBSCR_URL = TBase::TanukiSetting('PAYPAL_WEBSCR_URL');
            header('Location: '.$PAYPAL_WEBSCR_URL. urlencode($token));
            exit;
        } else if (is_array($response) && $response['ACK'] == 'Failure') {
			die($response['L_SHORTMESSAGE0']);
        }
    }

    /**
     * @return \yii\web\Response
     */
    private function processBankDeposit() {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::savePurchaseOrder('Bank Deposit','JPY','Pending Payment')) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']) {
            $orderID = \Yii::$app->session['orderID'];
        }
        $orderDetail = Order::findOne($orderID);
        unset(\Yii::$app->session['orderID']);
        unset(\Yii::$app->session['carts']);

	    Order::orderInvoiceMail($orderID);

        return $this->redirect(['order/payment-completed', '_ls' => $orderDetail->id]);
        exit;
    }

    /**
     * @return \yii\web\Response
     */
    private function payByBalance() {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::savePurchaseOrder('Pay By Balance','JPY','Success',Order::DOMESTIC_SHIPPING)) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']) {
            $orderID = \Yii::$app->session['orderID'];
        }
        $orderDetail = Order::findOne($orderID);

        User::deductFund($orderDetail->total); // deduct amount

        unset(\Yii::$app->session['orderID']);
        unset(\Yii::$app->session['carts']);

	    Order::orderInvoiceMail($orderID);

        return $this->redirect(['order/payment-completed', '_ls' => $orderDetail->id]);
        exit;
    }

    public function actionPaypalPayment() {
        $this->paypalPaymentProcessing();
    }

    public function actionBankTransferPayment() {
        $this->processBankDeposit();
    }

    public function actionPayBalanceTransfer() {
        $this->payByBalance();
    }
}
