<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use \common\helper\Tanuki;
use \common\models\Category;
use \common\models\Rakuten;

/**
 * Site controller
 */
class RakutenController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        \Yii::$app->view->registerMetaTag([
            'name' => 'google',
            'content' => 'notranslate'
        ]);

        $this->layout = 'main';
	    $categories = Category::find()->where(['status'=>1,'shop'=>'rakuten'])->orderBy('serial_no')->all();
        return $this->render('index',['categories'=>$categories]);
    }

    public function actionList()
    {
	    $cid = Yii::$app->request->get('cid');
	    if ($cid) {
		    if ($model = Category::findOne( $cid )) {
			    \Yii::$app->view->title = $model->title_en;
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'title',
				    'content' => $model->meta_title,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'description',
				    'content' => $model->meta_description,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'keywords',
				    'content' => $model->meta_keywords,
			    ] );
		    }
	    }
        $page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] :  1;
        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['page'=>$page]);
        $searchBar = [];
        $category  = '';
        $model = new Rakuten;

        $cid = (isset($_REQUEST['cid']) && $_REQUEST['cid'] != 'All') ? $_REQUEST['cid'] : false;

        if ($cid) {
            $category = Category::findOne(['ref_id'=>$cid]);
            $searchBar = array_merge($searchBar,['genreId'=>$cid]);
            $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['cid'=>$cid]);
        }

        if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'lth') {

            $searchBar = array_merge($searchBar,['sort'=>'+itemPrice']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=> 'lth']);
        } else if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'htl') {
            $searchBar = array_merge($searchBar,['sort'=>'-itemPrice']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=> 'htl']);
        }

        if (isset($_REQUEST['pmin']) && $_REQUEST['pmin'] != '') {
            $searchBar = array_merge($searchBar,['minPrice'=>$_REQUEST['pmin']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmin'=> $_REQUEST['pmin']]);
        }

        if (isset($_REQUEST['pmax']) && $_REQUEST['pmax'] != '') {
            $searchBar = array_merge($searchBar,['maxPrice'=>$_REQUEST['pmax']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmax'=> $_REQUEST['pmax']]);
        }

        if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') {
            $searchBar = array_merge($searchBar, ['keyword' => $_REQUEST['s']]);
            $params = array_merge($params,['s'=> $_REQUEST['s']]);
        }
        $data = $model->RakutenAPI($searchBar,$page);
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_listing',['data'=>$data,'category'=>$category,'param'=>$params]);
        }
        $pages = new Pagination(['totalCount' => $data['data']['totalRecords']]);
        return $this->render('listing',['data'=>$data,'category'=>$category,'pages'=>$pages,'param'=>$params]);
    }

    public function actionSearch($title=null, $page=1)
    {
        $model = new Rakuten();
        $category = Category::findOne(['slug'=>$title]);
        $searchBar = ['genreId'=>$category->ref_id];
//       $searchBar = array_merge($searchBar,['minPrice'=>50,'maxPrice'=>100]); # min max price
       //$searchBar = array_merge($searchBar,['sort'=>'+itemPrice']); # min max price +itemPrice -itemPrice +reviewCount

        $data = $model->RakutenAPI($searchBar,$page);
        $pages = new Pagination(['totalCount' => $data['data']['totalRecords']]);
        return $this->render('p-list',['data'=>$data,'category'=>$category,'pages'=>$pages]);

        //https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=json&keyword=addidas&genreId=559887&shopCode=rakuten24&minPrice=10&maxPrice=200&applicationId=1078337665598272286
        /*$searchBar = [];
        $model = new Tanuki;
        $category = Category::findOne(['slug'=>$title]);
        $searchBar = ['genreId'=>$category->ref_id];
        if (isset($_GET['search'])) {
            $cat = (isset($_REQUEST['title']) && $_REQUEST['title'] != '') ? $_REQUEST['title'] : null;
            $x = (isset($_REQUEST['title']) && $_REQUEST['x'] != '') ? $_REQUEST['x'] : null;
            if ($cat != null || $cat != 'all') {
                $searchBar = array_merge($searchBar, ['genreId' => $cat]);
            }
            if ($x != null) {
                $searchBar = array_merge($searchBar, ['keyword' => $x]);
            }
            echo "<pre>";
            print_r($searchBar);
            exit;
        }
        $data = $model->RakutenAPI($searchBar);
        $pages = new Pagination(['totalCount' => $data['data']['totalRecords']]);
        return $this->render('p-list',['data'=>$data,'category'=>$category,'pages'=>$pages]);*/
    }

    public function actionDetail($id)
    {
        $category = '';
        if (strpos($id,'__')) {
            $data = explode('__',$id);
            $id  = (isset($data[1])) ? $data[1] : 0;
            $category = Category::findOne($id);
            $itemCode = $data[0];
        } else {
            $itemCode = $id;
        }

        $model = new Rakuten();
        $data = $model->RakutenAPI(['itemCode'=>$itemCode]);
        return $this->render('detail',['data'=>$data,'rModel'=>$model,'category'=>$category]);
    }

}
