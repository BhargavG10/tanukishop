<?php
namespace frontend\controllers;

use Yii;
use \common\models\Yahoo;
use \common\models\Category;

/**
 * Site controller
 */
class YahooController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'google',
            'content' => 'notranslate'
        ]);
        $this->layout = 'main';
        $categories = Category::findAll(['status'=>1,'shop'=>'yahoo']);
        return $this->render('categories',['categories'=>$categories]);
    }

    public function actionList()
    {
        $model  = new Yahoo();
        $per_page = 20;
        $title  = (isset($_REQUEST['title'])) ? $_REQUEST['title'] : false;
        $page   = (int)(isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $paramsVar = ['offset'=>$page];
        $category = false;

        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['title'=>$title,'page'=>1]);

        if ($title != null) {
            $category = Category::findOne(['slug'=>$title]);
            $paramsVar = array_merge($paramsVar,['category_id'=>$category->ref_id]);
        }

        if (isset($_REQUEST['s'])) {
            $paramsVar = array_merge($paramsVar, ['query' => $_REQUEST['s']]);
            $params = array_merge($params, ['s' => $_REQUEST['s']]);

        }

        if (isset($_REQUEST['pmin']) && $_REQUEST['pmin'] != '') {
            $paramsVar = array_merge($paramsVar,['price_from'=>$_REQUEST['pmin']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmin'=>$_REQUEST['pmin']]); # min max price +itemPrice -itemPrice +reviewCount
        }

        if (isset($_REQUEST['pmax']) && $_REQUEST['pmax'] != '') {
            $paramsVar = array_merge($paramsVar,['price_to'=>$_REQUEST['pmax']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmax'=>$_REQUEST['pmax']]); # min max price +itemPrice -itemPrice +reviewCount
        }


        if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'lth') {
            $paramsVar = array_merge($paramsVar,['sort'=>'+price']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=>$_REQUEST['sort']]);
        } else if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'htl') {
            $paramsVar = array_merge($paramsVar,['sort'=>'-price']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=>$_REQUEST['sort']]);
        }

        $data = $model->productsListing($paramsVar);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial(
                '_listing',
                ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
            );
        }

        return $this->render(
            'listing',
            ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
        );
    }

    public function actionDetail()
    {
        $id = $_REQUEST['id'];

        $category = '';
        if (strpos($id,'__')) {
            $data = explode('__',$id);
            $id  = (isset($data[1])) ? $data[1] : 0;
            $category = Category::findOne($id);
            $itemCode = $data[0];
        } else {
            $itemCode = $id;
        }

        $model = new Yahoo();
        $data = $model->productsDetail($itemCode);
        return $this->render('detail',['data'=>$data,'category'=>$category]);
    }
}