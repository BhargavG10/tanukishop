<?php

namespace frontend\controllers;

use common\components\MailCamp;
use common\components\TBase;
use common\models\Order;
use common\models\OrderStatus;
use common\models\PackagesOrder;
use common\models\UserShippingAddress;
use Yii;
use common\models\Packages;
use common\models\PackagesSearch;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackagesController implements the CRUD actions for Packages model.
 */
class PackagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
	    return [
		    'access' => [
			    'class' => AccessControl::className(),
			    'rules' => [
				    [
					    'actions' => ['index','view','create','shipping','order','update','delete'],
					    'allow' => true,
					    'roles' => ['@'],
				    ],
			    ],
		    ],
		    'verbs' => [
			    'class' => VerbFilter::className(),
			    'actions' => [
				    'logout' => ['post'],
			    ],
		    ],
	    ];
    }

    /**
     * Lists all Packages models.
     * @return mixed
     */
    public function actionIndex()
    {
		Packages::deleteAll(['status'=>'incomplete','created_by' => Yii::$app->user->id]);
        $searchModel = new PackagesSearch();
        $searchModel->created_by = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Packages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Packages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		
        $model = new Packages();
        $model->name = 'TNK'.strtotime('now');
        $model->created_on = new \yii\db\Expression('NOW()');
        $model->modified_on = new \yii\db\Expression('NOW()');
        $model->created_by  =   Yii::$app->user->id;
        $model->status =   'incomplete';
        $model->save();
		
        return $this->redirect(['shipping', 'id' => $model->id]);
    }

    /**
     * Creates a new Packages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionShipping($id)
    {
		
        $model = $this->findModel($id);
        if (isset($_REQUEST['save'])) {
            if (!isset($_REQUEST['shipping']) || !isset($_REQUEST['shipping_methods_id'] )) {
                Yii::$app->session->setFlash('danger','Please select both shipping address and shipping method');
                return $this->refresh();

            }
            $address = UserShippingAddress::findOne($_REQUEST['shipping']);
            $model->first_name = $address->first_name;
            $model->last_name = $address->last_name;
            $model->company = $address->company;
            $model->address_1 = $address->address_1;
            $model->address_2 = $address->address_2;
            $model->appt_no = $address->appt_no;
            $model->city = $address->city;
            $model->state = $address->state;
            $model->country = $address->country;
            $model->zipcode = $address->zipcode;
            $model->phone = $address->phone;
            $model->email = (string)$address->email;
            $model->shipping_methods_id = $_REQUEST['shipping_methods_id'];
            $model->save(false);
            Yii::$app->session->setFlash('success','Package Address created successfully. Please select orders for package');
            return $this->redirect(['order', 'id' => $model->id]);

        }
        return $this->render('shipping', [
            'packageModel' => $model,
            'shippingList' => UserShippingAddress::find()->where(['user_id'=>Yii::$app->user->id])->all(),
            'shippingMethods' => \common\models\ShippingMethods::findAll(['status'=>1]),
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionOrder($id)
    {
        $packageOrderList = ArrayHelper::map(PackagesOrder::find()->asArray()->all(),'order_id','order_id');
        $packageModel = $this->findModel($id);
        $orderList = Order::find()
            ->where(['order_status'=>OrderStatus::PACKAGE_ORDER_PENDING])
            ->andwhere(['user_id'=>Yii::$app->user->id])
            ->andWhere(['not in','id',[implode(',',array_keys($packageOrderList))]])
            ->all();


        if (isset($_REQUEST['order_id'])) {

            if (!isset($_REQUEST['order_id'])) {
                Yii::$app->session->setFlash('danger','Please select order id(s)');
                return $this->refresh();
            }
            foreach ($_REQUEST['order_id'] as $order_id) {
                $model = new PackagesOrder();
                $model->order_id = $order_id;
                $model->package_id = $id;
                $model->save();
            }
            $packageModel->status = 'pending';
            $packageModel->save(false);
	        MailCamp::adminPackageNotification($model->package_id);

            Yii::$app->session->setFlash('success','Package created successfully. Waiting for admin approval. ');
            return $this->redirect(['index']);
        }
        return $this->render('order', [
            'orderModel' => $orderList,
            'packageModel' => $packageModel,
            'shippingList' => UserShippingAddress::find()->all(),
        ]);
    }

    /**
     * Updates an existing Packages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Packages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Packages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Packages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Packages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
