<?php
namespace frontend\controllers;

use common\models\User;
use common\models\UserAuctionBlockedAmount;
use common\models\UserAuctions;
use common\components\TBase;
use Yii;
use \common\models\YahooAuctions;
use \common\models\Category;
use \common\components\MailCamp;
use yii\helpers\Html;

/**
 * Site controller
 */
class YahooAuctionsController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'google',
            'content' => 'notranslate'
        ]);
        $this->layout = 'main';
        $categories = Category::find()->where(['status'=>1,'shop'=>'yahoo_auctions'])->orderBy('serial_no')->all();
        return $this->render('categories',['categories'=>$categories]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
	    $cid = Yii::$app->request->get('cid');
	    if ($cid) {
		    if ($model = Category::findOne( $cid )) {
			    \Yii::$app->view->title = $model->title_en;
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'title',
				    'content' => $model->meta_title,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'description',
				    'content' => $model->meta_description,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'keywords',
				    'content' => $model->meta_keywords,
			    ] );
		    }
	    }
        $model  = new YahooAuctions();
        $id = (
                Yii::$app->request->get('cid') &&
                (
                    Yii::$app->request->get('cid') != 'All' ||
                    Yii::$app->request->get('cid') != 0
                )
            ) ? Yii::$app->request->get('cid') : false;
        $page   = (int)Yii::$app->request->get('page') ? Yii::$app->request->get('page') : 1;
        $paramsVar = ['page'=>$page];
        $category = false;

        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['page'=>1]);

        if ($id) {
            $category = Category::findOne(['ref_id'=>$id]);
            $paramsVar = array_merge($paramsVar,['category'=>$id]);
            $params = array_merge($params,['cid'=>$id]);
        }

        if (Yii::$app->request->get('s')) {
            $paramsVar = array_merge($paramsVar, ['query' => Yii::$app->request->get('s')]);
            $params = array_merge($params,['cid'=>$id,'s' => Yii::$app->request->get('s')]);

        }

        if (Yii::$app->request->get('aucminprice')) {
            $paramsVar['aucminprice'] = Yii::$app->request->get('aucminprice');
            $params = array_merge($params, ['aucminprice' => $paramsVar['aucminprice']]);
        }

        if (Yii::$app->request->get('aucmaxprice')) {
            $paramsVar['aucmaxprice'] = Yii::$app->request->get('aucmaxprice');
            $params = array_merge($params, ['aucmaxprice' => $paramsVar['aucmaxprice']]);
        }

        if (Yii::$app->request->get('aucmin_bidorbuy_price')) {
            $paramsVar['aucmin_bidorbuy_price'] = Yii::$app->request->get('aucmin_bidorbuy_price');
            $params = array_merge($params, ['aucmin_bidorbuy_price' => $paramsVar['aucmin_bidorbuy_price']]);
        }

        if (Yii::$app->request->get('aucmax_bidorbuy_price')) {
            $paramsVar['aucmax_bidorbuy_price'] = Yii::$app->request->get('aucmax_bidorbuy_price');
            $params = array_merge($params, ['aucmax_bidorbuy_price' => $paramsVar['aucmax_bidorbuy_price']]);
        }

        $paramsVar['sort'] = 'end'; //current price
        $paramsVar['order'] = 'a'; //current price
        $paramsVar['ranking'] = 'popular'; // order ascending order

        if (Yii::$app->request->get('sort')){
            $paramsVar['sort'] = Yii::$app->request->get('sort');
            $params = array_merge($params, ['sort' => $paramsVar['sort']]);
        }

        if (Yii::$app->request->get('order')){
            $paramsVar['order'] = Yii::$app->request->get('order');
            $params = array_merge($params, ['order' => $paramsVar['order']]);
        }

        if (Yii::$app->request->get('ranking')){
            $paramsVar['ranking'] = Yii::$app->request->get('ranking');
            $params = array_merge($params, ['ranking' => $paramsVar['ranking']]);
        }

        if (Yii::$app->request->get('store') && Yii::$app->request->get('store') != '0'){
            $paramsVar['store'] = Yii::$app->request->get('store');
            $params = array_merge($params, ['store' => $paramsVar['store']]);
        }

        if (Yii::$app->request->get('item_status') && Yii::$app->request->get('item_status') != '0'){
            $paramsVar['item_status'] = Yii::$app->request->get('item_status');
            $params = array_merge($params, ['item_status' => $paramsVar['item_status']]);
        }

        if (Yii::$app->request->get('new')){
            $paramsVar['new'] = 1;
            $params = array_merge($params, ['new' => $paramsVar['new']]);
        }

        if (Yii::$app->request->get('freeshipping')){
            $paramsVar['freeshipping'] = 1;
            $params = array_merge($params, ['freeshipping' => $paramsVar['freeshipping']]);
        }

        if (Yii::$app->request->get('buynow')){
            $paramsVar['buynow'] = 1;
            $params = array_merge($params, ['buynow' => $paramsVar['buynow']]);
        }

        if (Yii::$app->request->get('thumbnail')){
            $paramsVar['thumbnail'] = 1;
            $params = array_merge($params, ['thumbnail' => $paramsVar['thumbnail']]);
        }

        if (Yii::$app->request->get('goodSellers')){
            $paramsVar['goodSellers'] = 1;
            $params = array_merge($params, ['goodSellers' => $paramsVar['goodSellers']]);
        }


        if (Yii::$app->request->get('search') || Yii::$app->request->get('s')) {
            $data = $model->auctionSearch($paramsVar);
        } else {
            $data = $model->productsListing($paramsVar);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial(
                '_listing',
                ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
            );
        }
        return $this->render(
            'listing',
            ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
        );
    }

    /**
     * @return string
     */
    public function actionSellerList()
    {
        $model  = new YahooAuctions();
        $page   = (int)(isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $paramsVar = ['page'=>$page];
        $category = false;

        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['page'=>1]);

        if (Yii::$app->request->get('aucminprice')) {
            $paramsVar['aucminprice'] = Yii::$app->request->get('aucminprice');
            $params = array_merge($params, ['aucminprice' => $paramsVar['aucminprice']]);
        }

        if (Yii::$app->request->get('aucmaxprice')) {
            $paramsVar['aucmaxprice'] = Yii::$app->request->get('aucmaxprice');
            $params = array_merge($params, ['aucmaxprice' => $paramsVar['aucmaxprice']]);
        }

        if (Yii::$app->request->get('aucmin_bidorbuy_price')) {
            $paramsVar['aucmin_bidorbuy_price'] = Yii::$app->request->get('aucmin_bidorbuy_price');
            $params = array_merge($params, ['aucmin_bidorbuy_price' => $paramsVar['aucmin_bidorbuy_price']]);
        }

        if (Yii::$app->request->get('aucmax_bidorbuy_price')) {
            $paramsVar['aucmax_bidorbuy_price'] = Yii::$app->request->get('aucmax_bidorbuy_price');
            $params = array_merge($params, ['aucmax_bidorbuy_price' => $paramsVar['aucmax_bidorbuy_price']]);
        }

        $paramsVar['sort'] = 'score'; //current price
        $paramsVar['ranking'] = 'popular'; // order ascending order

        if (Yii::$app->request->get('sellerID')){
            $paramsVar['sellerID'] = Yii::$app->request->get('sellerID');
            $params = array_merge($params, ['sellerID' => $paramsVar['sellerID']]);
        }

        if (Yii::$app->request->get('sort')){
            $paramsVar['sort'] = Yii::$app->request->get('sort');
            $params = array_merge($params, ['sort' => $paramsVar['sort']]);
        }

        if (Yii::$app->request->get('order')){
            $paramsVar['order'] = Yii::$app->request->get('order');
            $params = array_merge($params, ['order' => $paramsVar['order']]);
        }

        if (Yii::$app->request->get('ranking')){
            $paramsVar['ranking'] = Yii::$app->request->get('ranking');
            $params = array_merge($params, ['ranking' => $paramsVar['ranking']]);
        }

        if (Yii::$app->request->get('store') && Yii::$app->request->get('store') != '0'){
            $paramsVar['store'] = Yii::$app->request->get('store');
            $params = array_merge($params, ['store' => $paramsVar['store']]);
        }

        if (Yii::$app->request->get('item_status') && Yii::$app->request->get('item_status') != '0'){
            $paramsVar['item_status'] = Yii::$app->request->get('item_status');
            $params = array_merge($params, ['item_status' => $paramsVar['item_status']]);
        }

        if (Yii::$app->request->get('new')){
            $paramsVar['new'] = 1;
            $params = array_merge($params, ['new' => $paramsVar['new']]);
        }

        if (Yii::$app->request->get('freeshipping')){
            $paramsVar['freeshipping'] = 1;
            $params = array_merge($params, ['freeshipping' => $paramsVar['freeshipping']]);
        }

        if (Yii::$app->request->get('buynow')){
            $paramsVar['buynow'] = 1;
            $params = array_merge($params, ['buynow' => $paramsVar['buynow']]);
        }

        if (Yii::$app->request->get('thumbnail')){
            $paramsVar['thumbnail'] = 1;
            $params = array_merge($params, ['thumbnail' => $paramsVar['thumbnail']]);
        }

        if (Yii::$app->request->get('goodSellers')){
            $paramsVar['goodSellers'] = 1;
            $params = array_merge($params, ['goodSellers' => $paramsVar['goodSellers']]);
        }

        $data = $model->productsSellerListing($paramsVar);
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial(
                '_listing',
                ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
            );
        }

        return $this->render(
            'seller',
            ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
        );
    }

    /**
     * @param $id
     * @param string $cid
     * @return string
     */
    public function actionDetail($id,$cid = '')
    {
        $category = '';
        if ($cid) {
            $category = Category::findOne($cid);
        }
        $itemCode = $id;

        $model = new YahooAuctions();
        $model->productsDetail($itemCode);

        return $this->render('detail',['model'=>$model,'category'=>$category]);
    }

    public function actionAuctionBidRequest($bid, $auction_id) {
	    if (Yii::$app->user->isGuest) {
		    return $this->redirect(['/site/login']);
	    } else {
		    return $this->redirect( [ 'yahoo-auctions/place-auction', 'bid' => $bid,'auction_id'=>$auction_id ,'ref'=>Yii::$app->request->referrer] );
	    }

	    return $this->render('wait-page');
    }
	/**
	 * @param $bid
	 * @param $auction_id
	 *
	 * @return \yii\web\Response
	 */
    public function actionPlaceAuction($bid, $auction_id, $ref)
    {
    	if (Yii::$app->user->isGuest) {
		    return $this->redirect(['/site/login']);
	    }

        $regular_bid_block = TBase::TanukiSetting('regular_bid_block');

        $model = new YahooAuctions();
        $model->productsDetail($auction_id);

        if ($model->YahooAction['ResultSet']['Result']['Status'] != 'open') {
            Yii::$app->session->setFlash('warning',TBase::_x('BID_IS_CLOSED_FOR_THIS_AUCTION'));
            return $this->redirect(['yahoo-auctions/detail','id'=>$auction_id]);
            exit;
        }

        if ((int)$model->YahooAction['ResultSet']['Result']['Bids'] && $model->YahooAction['ResultSet']['Result']['Price'] >= $bid) {
            Yii::$app->session->setFlash('warning',TBase::_x('BID_AMOUNT_SHOULD_BE_GREATER_THEN') .$model->YahooAction['ResultSet']['Result']['Price'].' ');
            return $this->redirect(['yahoo-auctions/detail','id'=>$auction_id]);
            exit;
        } else if ((int)$model->YahooAction['ResultSet']['Result']['Bids'] == 0 && $model->YahooAction['ResultSet']['Result']['Price'] > $bid) {
	        Yii::$app->session->setFlash('warning',TBase::_x('BID_AMOUNT_SHOULD_BE_GREATER_THEN') .$model->YahooAction['ResultSet']['Result']['Price'].' ');
	        return $this->redirect(['yahoo-auctions/detail','id'=>$auction_id]);
	        exit;
        }

        $UserBidCost = (int)$bid + (int)$regular_bid_block;

        $link = Html::a(TBase::ShowLbl('MY_BALANCE'),['user/add-fund']);

//        if ($UserBidCost > Yii::$app->user->identity->credit) {
//            Yii::$app->session->setFlash('warning',TBase::ShowLbl('INSUFFICIENT_FUNDS_FOR_BID').' '.$link);
//            return $this->redirect($ref);
//        }

        if ((int)Yii::$app->request->get('bid') <= 0) {
            Yii::$app->session->setFlash('warning',TBase::_x('AUCTION_AMOUNT_SHOULD_BE_GREATER_THEN_0'));
            return $this->redirect($ref);
        }

		$response  = UserAuctions::bidOnAuction(
			$bid,
			$auction_id,
			$model,
			$regular_bid_block
		);
//	    $response['operation'] = 'success';//todo need to remove

        if ($response['operation'] == 'error') {
	        if ($response['message'] == 'INSUFFICIENT_AMOUNT') {
        	    Yii::$app->session->setFlash('warning',TBase::ShowLbl('INSUFFICIENT_FUNDS_FOR_BID').' '.$link);
	            return $this->redirect($ref);
            } else {
	            Yii::$app->session->setFlash('warning',$response['message']);
	            return $this->redirect($ref);
	        }
        } else if ($response['operation'] == 'success') {
	        Yii::$app->session->setFlash('success',TBase::ShowLbl('BID_SUCCESS'));
	        return $this->redirect(['yahoo-auctions/detail','id'=>Yii::$app->request->get('auction_id')]);
	        exit;
        }

	    return $this->render('wait-page');
    }

    /**
     * @param $auction_id
     * @return \yii\web\Response
     */

    public function actionBuyOutMail($auction_id) {

        $buyout_bid_block = TBase::TanukiSetting('buyout_bid_block');

        $model = new YahooAuctions();
        $model->productsDetail($auction_id);

        if ($model->YahooAction['ResultSet']['Result']['Status'] != 'open') {
            Yii::$app->session->setFlash('warning',TBase::_x('BID_IS_CLOSED_FOR_THIS_AUCTION'));
            return $this->redirect(['yahoo-auctions/detail','id'=>$auction_id]);
            exit;
        }

		$bidAmount = (isset($model->YahooAction['ResultSet']['Result']['TaxinBidorbuy'])) ? $model->YahooAction['ResultSet']['Result']['TaxinBidorbuy'] : $model->YahooAction['ResultSet']['Result']['Bidorbuy'];
        $UserBidCost = (int)$bidAmount+ $buyout_bid_block;

        $link = Html::a(TBase::ShowLbl('MY_BALANCE'),['user/add-fund']);

        if ($UserBidCost > TBase::currentUserBalance()) {
            Yii::$app->session->setFlash('warning',TBase::ShowLbl('INSUFFICIENT_FUNDS_FOR_BID').' '.$link);
            return $this->redirect(Yii::$app->request->referrer);
            exit;
        }
	    $YahooModel = $model;

        $response = UserAuctions::buyoutOrder($model,Yii::$app->request->get('auction_id'),$UserBidCost);

	    if ($response['operation'] == 'success') {
		    Yii::$app->session->setFlash('success',TBase::ShowLbl('BUYOUT_SUCCESS'));
		    return $this->redirect(['yahoo-auctions/detail','id'=>Yii::$app->request->get('auction_id')]);
		    exit;
	    } else {
		    Yii::$app->session->setFlash('warning',$response['msg']);
		    return $this->redirect(['yahoo-auctions/detail','id'=>Yii::$app->request->get('auction_id')]);
		    exit;
	    }
    }
}