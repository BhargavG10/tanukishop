<?php
namespace frontend\controllers;

use common\components\MailCamp;
use common\components\TBase;
use common\models\CurrencyTable;
use common\models\Labels;
use common\models\LabelsDetail;
use common\models\Page;
use common\models\ShippingMethodCalculationFinal;
use common\models\YahooAuctions;
use common\models\YahooConnect;
use common\models\YConnect;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\YahooOAuth2;
use yii\authclient\OAuth2;
use \common\models\ShippingCategory;
/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'user-validate', 'add-to-cart', 'lang-switch', 'product-shipping-cost', 'request-password-reset', 'paypal-testing', 'connect','category-shipping-cost','get-box','moneta-payment','moneta-payment-return'],
                'rules' => [
                    [
                        'actions' => ['signup', 'user-validate', 'lang-switch', 'captcha', 'product-shipping-cost', 'request-password-reset', 'paypal-testing', 'connect','category-shipping-cost','get-box','moneta-payment','moneta-payment-return'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'add-to-cart', 'lang-switch', 'product-shipping-cost', 'paypal-testing', 'stripe-testing', 'yahoo','won-list','category-shipping-cost','get-box','moneta-payment','moneta-payment-return'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => null,
                'minLength' => 4,
                'maxLength' => 4,
                'offset' => 12,
                'transparent' => true,
                'foreColor' => 0x269abc,
            ],
        ];
    }

    public function actionLangSwitch($lang = 'en-US')
    {
        $return = \common\components\Language::SetLanguage($lang);
        if ($return) {
            $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $pageData = Page::findOne(2);
        return $this->render('page/index', [
            'pageData' => $pageData,
        ]);
    }

    public function actionFee()
    {
        $pageData = Page::findOne(5);
        return $this->render('page/page', ['pageData' => $pageData]);
    }

    public function actionShipping()
    {
        $pageData = Page::findOne(4);
        return $this->render('page/page', ['pageData' => $pageData]);
    }

    public function actionHelp()
    {
        $pageData = Page::findOne(7);
        return $this->render('page/page', ['pageData' => $pageData]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (isset($_REQUEST['returnurl'])) {
            Yii::$app->getSession()->set('returnUrl', $_REQUEST['returnurl']);
        } else if (Yii::$app->getUser()->getReturnUrl()) {
            Yii::$app->getSession()->set('returnUrl', Yii::$app->getUser()->getReturnUrl());
        }

        $this->layout = 'main_container';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $SignUpModel = new SignupForm();
        $model = new LoginForm();

        # Registration
        if ($SignUpModel->load(Yii::$app->request->post())) {
            $password = $SignUpModel->password;
            if ($user = $SignUpModel->signup()) {
                $SignUpModel->sendEmail($password);
                Yii::$app->session->setFlash('success', TBase::ShowLbl('REGISTRATION_SUCCESSFULLY'));
	            return $this->redirect(['/registration-complete']);
            }
        }

        # Login
        $model->type = 'user';
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

        	$userModel = Yii::$app->user->identity;
	        $userModel->last_login = date('Y-m-d H:i:s');
	        $userModel->save(false);

            if (Yii::$app->getSession()->has('returnUrl')) {
                return $this->redirect(Yii::$app->getSession()->get('returnUrl'));
            } else {
                return $this->redirect(['user/my-page']);
            }
        } else {
            return $this->render('login', [
                'model' => $model,
                'signUpModel' => $SignUpModel,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $pageData = Page::findOne(6);
        $model = new ContactForm();
        $model->subject = TBase::ShowLbl('contact_us');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', TBase::ShowLbl('CONTACT_US_MAIL_SENT'));
            } else {
                Yii::$app->session->setFlash('error', TBase::ShowLbl('CONTACT_MAIL_ERROR'));
            }

            return $this->refresh();
        } else {
            return $this->render('page/contact', [
                'model' => $model,
                'pageData' => $pageData
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $pageData = Page::findOne(3);
        return $this->render('page/page', ['pageData' => $pageData]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionPage($slug)
    {
        $pageData = Page::findOne(['slug'=>$slug]);
        return $this->render('page/page', ['pageData' => $pageData]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'main_container';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $password = $model->password;
            if ($user = $model->signup()) {

                $model->sendEmail($password);
                Yii::$app->session->setFlash('success', TBase::ShowLbl('REGISTRATION_SUCCESSFULLY'));
                return $this->redirect(['login']);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'main_container';
        $model = new PasswordResetRequestForm();
        $model->type = 'user';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', TBase::ShowLbl('CHECK_EMAIL_INSTRUCTION'));

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', TBase::ShowLbl('SORRY_PASSWORD_RESET_ERROR'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', TBase::ShowLbl('NEW_PASSWORD_SAVED'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionUserValidate($l, $t)
    {
        $user = User::findOne([
            'email' => base64_decode($l),
            'auth_key' => base64_decode($t),
            'status' => 9,
        ]);

        if ($user) {
            $user->status = 10;
            $user->auth_key = '';
            $user->save();
            Yii::$app->session->setFlash('success', TBase::ShowLbl('ACCOUNT_ACTIVATED_SUCCESS'));
            return $this->redirect(['login']);
        } else {
            Yii::$app->session->setFlash('danger', TBase::ShowLbl('INVALID_CREDENTIALS'));
            return $this->redirect(['login']);
        }
    }

    public function actionSearch($shop = null, $x = null)
    {
        if ($shop == 'auction') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo-auctions/list']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo-auctions/list', 'cid' => 'All', 'search' => '', 's' => $x]));
        } else if ($shop == 'amazon') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['amazon/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['amazon/list', 's' => $x]));
        } else if ($shop == 'rakuten') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['rakuten/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['rakuten/list', 's' => $x, 'page' => 1]));
        } else if ($shop == 'yahoo') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo/list','cid'=>'All','s' => $x]));
        }
        exit;
    }

    public function actionProductShippingCost()
    {
        if (Yii::$app->request->isAjax) {
            if (trim($_POST['package_weight']) == "") {
                echo "<span style='color:#df7b2b;'>" . TBase::ShowLbl('PLEASE_ENTER_PACKAGE_WEIGHT') . '</span>';
                exit;
            } else if ($_POST['country_id'] == '') {
                echo "<span style='color:#df7b2b;'>" . TBase::ShowLbl('PLEASE_SELECT_SHIPPING_COUNTRY') . '</span>';
                exit;
            }
            $data = Yii::$app->request->post();
            $q = "SELECT m.*,f.* FROM `tnk_shipping_method_zone_calculation_final` as f left join tnk_shipping_methods as m on f.shipping_method_id = m.id";
            $q .= " where country_id = '{$data['country_id']}' and '{$data['package_weight']}' BETWEEN weight_from and weight_to ORDER by cost";

            $result = ShippingMethodCalculationFinal::findBySql($q)->all();
            if (!$result) {
                echo "<span style='color:#df7b2b;'>" . TBase::ShowLbl('SELECTED_SHIPPING_METHOD_IS_NOT_AVAILABLE_FOR_YOUR_COUNTRY') . '</span>';
                exit;
            }
            return $this->renderPartial('cost', [
                'result' => $result,
                'data' => $data
            ]);
        }
    }

    public function actionSendMail()
    {
        if (Yii::$app->user->id && Yii::$app->request->isAjax) {
            $data = \common\models\User::findOne(Yii::$app->user->id);
	        echo MailCamp::adminProductQuery($_POST['body'],$_POST['url']);
        }
    }

    public function actionFeaturedProduct()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('page/_featured-product');
        }
    }

    public function actionCopy($from, $to)
    {
        $data = Labels::find()->where(['between', 'id', $from, $to])->all();

        foreach ($data as $label) {
            $model = new LabelsDetail();
            $model->title = $label->title;
            $model->lang_code = 'en-US';
            $model->labels_id = $label->id;
            $model->save();

            $model = new LabelsDetail();
            $model->title = $label->title;
            $model->lang_code = 'ru-RU';
            $model->labels_id = $label->id;
            $model->save();
        }
        echo "done copy : " . count($data);
    }

    public function actionUpdateCurrencyTable()
    {
        $model = new \common\components\TCurrencyConvertor;
        echo $model->update_rates();
    }

    public function actionCateList($id)
    {
        $model = new YahooAuctions();
        $data = $model->getCategories($id);
        echo 'Main Category:' . $data['Result']['CategoryName'];
        if ($data['Result']['ChildCategory']) {
            echo "<ul>";

            foreach ($data['Result']['ChildCategory'] as $key => $category) {
                echo "<li>" . $category['CategoryName'] . "</li>";
                $subData = $model->getCategories($category['CategoryId']);
                if (isset($subData['Result']['ChildCategory'])) {
                    echo "<ul class='subcat'>";

                    foreach ($subData['Result']['ChildCategory'] as $key => $subcategory) {
                        echo "<li>" . $subcategory['CategoryName'] . "</li>";
                    }
                    echo "</ul>";
                }
            }
            echo "</ul>";
        }
    }


    public function actionYahoo()
    {
        $this->layout = 'blank';
        $KEY = 'dj0zaiZpPTRubUFueW41a3VZWiZzPWNvbnN1bWVyc2VjcmV0Jng9ZTI-';
        $SECRET = '591f65a65e0f571fcbaf95b8a181bd2d04afb2fc';
        $model = new YConnect($KEY,$SECRET);
        $url = $model->authorization('https://tanukishop.com/site/response','code','best_1001_best@yahoo.co.jp');
        return $this->redirect($url);
//    $api = 'https://auth.login.yahoo.co.jp/yconnect/v1/authorization';
//    $appid = 'dj0zaiZpPW9JbEQ2dXhEOUVndyZzPWNvbnN1bWVyc2VjcmV0Jng9MDY-';
//    $params = array (
//            'response_type' => 'token',
//            'client_id' => $appid,
//            'redirect_uri' => 'https://tanukishop.com/site/response',
//    );
//    $header = array (
//            'content-type: application/x-www-form-urlencoded;charset=utf-8'
//        );
//    $ch = curl_init ($api. '?'. Http_build_query ($params));
//    curl_setopt_array ($ch, array (
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_HTTPHEADER => $header,
//            CURLOPT_HEADER => true
//    ));
//    $result = curl_exec ($ch);
//    curl_close ($ch);
//    print $result;
//    exit;

//        define ('CONSUMER_KEY', 'dj0zaiZpPW9JbEQ2dXhEOUVndyZzPWNvbnN1bWVyc2VjcmV0Jng9MDY-') ;
//        define ('CALLBACK_URL', 'https://tanukishop.com/site/response');
//        define('AUTH_URL', 'https://auth.login.yahoo.co.jp/yconnect/v1/authorization');
//
//        $params  =  array (
//                'client_id ' => CONSUMER_KEY,
//                'scope' => 'best_1001_best@yahoo.co.jp',
//                'response_type' => 'code',
//                'redirect_uri' => 'https://tanukishop.com/site/response',
//            );
//
//        return header("Location: " . AUTH_URL . '?' . http_build_query($params));
    }

    public function actionResponse()
    {
        $KEY = 'dj0zaiZpPTRubUFueW41a3VZWiZzPWNvbnN1bWVyc2VjcmV0Jng9ZTI-';
        $SECRET = '591f65a65e0f571fcbaf95b8a181bd2d04afb2fc';
        $connect = new YConnect($KEY,$SECRET);
        $code = Yii::$app->request->get('code');
        $response = $connect->token($code,'https://tanukishop.com/site/response');
        YahooConnect::deleteAll(['token_type'=>'bearer']);
        $model = new YahooConnect;
        $model->access_token = $response['access_token'];
        $model->token_type = $response['token_type'];
        $model->time_started = date("Y-m-d H:i:s");
        $model->time_expired = date("Y-m-d H:i:s", time()+3600);
        $model->refresh_token = $response['refresh_token'];
        if ($model->save(false)) {
            $this->redirect(['won-list']);
        }
    }

    public function actionWonList() {
//        echo "this will be your won list";
//        echo "<pre>";
//        $yahoo = YahooConnect::find()->one();
//        $KEY = 'dj0zaiZpPTRubUFueW41a3VZWiZzPWNvbnN1bWVyc2VjcmV0Jng9ZTI-';
//        $SECRET = '591f65a65e0f571fcbaf95b8a181bd2d04afb2fc';
//        $connect = new YConnect($KEY,$SECRET);
//        $user = $connect->userinfo($yahoo->access_token);
////        $wonList = $connect->wonList($yahoo->access_token);
//        print_r($user);
////        print_r($wonList);
//        echo "</pre>";
    }

    public function actionAdminLogin($email)
	{
		$user = User::findOne(['email'=>$email]);
		if ($user) {
			Yii::$app->user->login( $user, 3600 * 24 * 30 );
			return $this->redirect( [ 'user/my-page' ] );
		}
		return false;
	}

	public function actionCategoryShippingCost()
    {
        if (Yii::$app->request->isAjax) {
            if (trim($_POST['shipping_category']) != "") {
                $data = Yii::$app->request->post();
                $result = ShippingCategory::findOne($data['shipping_category']);
            }
            if (trim($_POST['shipping_category']) == "") {
                echo "<span style='color:#df7b2b;font-size: 20px;'>" . TBase::ShowLbl('PLEASE_SELECT_SHIPPING_CATEGORY') . '</span>';
                exit;
            }else if (trim($_POST['package_weight']) == "" && $result->weight_base == 1) {
                echo "<span style='color:#df7b2b;font-size: 20px;'>" . TBase::ShowLbl('PLEASE_ENTER_PACKAGE_WEIGHT') . '</span>';
                exit;
            } else if ($_POST['country_id'] == '') {
                echo "<span style='color:#df7b2b;font-size: 20px;'>" . TBase::ShowLbl('PLEASE_SELECT_SHIPPING_COUNTRY') . '</span>';
                exit;
            }else if (!isset($_POST['option'])) {
                echo "<span style='color:#df7b2b;font-size: 20px;'>" . TBase::ShowLbl('PLEASE_SELECT_OPTION') . '</span>';
                exit;
            }
            $data = Yii::$app->request->post();
            $result = ShippingCategory::findOne($data['shipping_category']);
            if (!$result) {
                echo "<span style='color:#df7b2b;font-size: 20px;'>" . TBase::ShowLbl('SELECTED_SHIPPING_METHOD_IS_NOT_AVAILABLE_FOR_YOUR_COUNTRY') . '</span>';
                exit;
            }
            return $this->renderPartial('cost_category', [
                'result' => $result,
                'data' => $data
            ]);
        }
        return false;
    }

    public function actionGetBox($id)
    {
    	$result = ShippingCategory::findOne($id);

    	if($result->weight_base ==0){
			echo "";
        } else{
			echo '
				<div class="col-md-4">
					<label class="lbbl">'.Tbase::ShowLbl('ESTIMATED_PACKAGE_WEIGHT').':</label>
				</div>
				<div class="col-md-8">
					<input type="text" class="form-control form-control1 form-control2" name="package_weight" id="package_weight" placeholder="'.Tbase::ShowLbl('package-weight-kg').'" />
				</div>
			';
       }
    }

	public function actionMonetaPayment() {
    	echo "test";
    	exit;
//		return $this->render('moneta');
	}

	public function actionMonetaPaymentReturn() {
		echo "<pre>";
		print_r($_REQUEST);
		exit;
	}
}

