<?php
namespace frontend\controllers;

use common\models\Order;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class MonetaPaymentController extends BaseController
{

	public $enableCsrfValidation = false;

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','return','callback'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
	                [
                        'actions' => ['callback'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    public function actionIndex() {
	    $orderID = 0;
	    $orderDetail = false;
	    if (!isset(\Yii::$app->session['orderID'])) {
		    if (Order::savePurchaseOrder('moneta')) {
			    $orderID = \Yii::$app->session['orderID'];
		    } else{
			    echo "Error While Saving your order. Please try again";
			    exit;
		    }
		    $orderDetail = Order::findOne($orderID);
		    if ($orderDetail->method != 'moneta') {
			    $orderDetail->method == 'moneta';
			    $orderDetail->save(false);
		    }
	    } elseif (\Yii::$app->session['orderID']){
		    $orderID = \Yii::$app->session['orderID'];
		    $orderDetail = Order::findOne($orderID);
	    }
	    if ($orderDetail) {
			$FinalAmount = $orderDetail->total;
            $FinalAmount = \common\models\CurrencyTable::convertMoneta((float)$orderDetail->total,'JPY','RUB');
		    $sid              = '56817999';
		    $cart_order_id    = $orderID;
		    $amount           = number_format((float)$FinalAmount, 2, '.', '');
		    $currency_code    = 'RUB';
		    $payanyway_test   = 0;
		    $payanyway_secret = 'QWERTY';

		    $params = [
			    'sid'              => $sid,
			    'cart_order_id'    => $orderID,
			    'amount'           => $amount,
			    'currency_code'    => $currency_code,
			    'payanyway_test'   => $payanyway_test,
			    'payanyway_secret' => $payanyway_secret,
			    'MNT_SIGNATURE'    => md5( $sid . $cart_order_id . $amount . $currency_code . $payanyway_test . $payanyway_secret )
		    ];

		    return $this->render( 'moneta', $params );
	    }
	    return false;
    }

	/**
	 * @return \yii\web\Response
	 */
    public function actionReturn() {
    	$order = Order::findOne($_REQUEST['MNT_TRANSACTION_ID']);
    	if ($order) {
		    $order->status = 'Order confirmed';
		    $order->order_status = Order::PAYMENT_PENDING;
		    if ($order->save(false)) {
		    	return $this->redirect(['order/payment-completed', '_ls' => $_REQUEST['MNT_TRANSACTION_ID']]);
		    }
	    }
    }

	/**
	 * call back
	 */
	public function actionCallback() {

		// get Pay URL data
		$MNT_ID = $this->getVar('MNT_ID');
		$MNT_TRANSACTION_ID = $this->getVar('MNT_TRANSACTION_ID');
		$MNT_OPERATION_ID = $this->getVar('MNT_OPERATION_ID');
		$MNT_AMOUNT = $this->getVar('MNT_AMOUNT');
		$MNT_CURRENCY_CODE = $this->getVar('MNT_CURRENCY_CODE');
		$MNT_TEST_MODE = $this->getVar('MNT_TEST_MODE');
		$MNT_SIGNATURE = $this->getVar('MNT_SIGNATURE');

		$showResult = 'FAIL';
		if ($MNT_TRANSACTION_ID && $MNT_SIGNATURE) {
			$mnt_dataintegrity_code = 'QWERTY';
			$check_signature = md5($MNT_ID . $MNT_TRANSACTION_ID . $MNT_OPERATION_ID . $MNT_AMOUNT . $MNT_CURRENCY_CODE . $MNT_TEST_MODE . $mnt_dataintegrity_code);

			if ($MNT_SIGNATURE == $check_signature) {
				$order_id = $MNT_TRANSACTION_ID;
				$order_info = Order::findOne($order_id);
				if ($order_info) {
					$order_info->status = "success";
					$order_info->order_status = Order::DOMESTIC_SHIPPING;
					$order_info->save(false);

					// begin of kassa.payanyway.ru code
					$inventoryPositions = array();
					/** @var ModelAccountOrder $modelAccountOrder */

					$products = $order_info->orderDetail;
					$orderTotals = $order_info->total;
					foreach ($products as $product)
					{
						$inventoryPositions[] = array(
							'name' => trim(preg_replace("/&?[a-z0-9]+;/i", "", htmlspecialchars($product->product_name))),
							'price' => $product->price,
							'quantity' => $product->quantity,
							'vatTag' => self::paw_kassa_VATNOVAT,
						);
					}

					$kassa_delivery = $orderTotals;
					$kassa_inventory = json_encode($inventoryPositions);
					self::monetaPayURLResponse($_REQUEST['MNT_ID'], $_REQUEST['MNT_TRANSACTION_ID'],
						$mnt_dataintegrity_code, true, false, true, true, $kassa_inventory, $order_info->user->email,
						$kassa_delivery);

					exit;
				}
			}
		}

		echo $showResult;
	}

	private function getVar($name) {
		$value = false;
		if (isset($_POST[$name])) {
			$value = $_POST[$name];
		}
		else if (isset($_GET[$name])) {
			$value = $_GET[$name];
		}

		return $value;
	}

	const paw_kassa_VAT0     = 1104;  // НДС 0%
	const paw_kassa_VAT10    = 1103;  // НДС 10%
	const paw_kassa_VAT18    = 1102;  // НДС 18%
	const paw_kassa_VATNOVAT = 1105;  // НДС не облагается
	const paw_kassa_VATWR10  = 1107;  // НДС с рассч. ставкой 10%
	const paw_kassa_VATWR18  = 1106;  // НДС с рассч. ставкой 18%

	private static function monetaPayURLResponse($mnt_id, $mnt_transaction_id, $mnt_data_integrity_code, $success = false,
		$repeatRequest = false, $echo = true, $die = true, $kassa_inventory = null, $kassa_customer = null, $kassa_delivery = null)
	{
		if ($success === true)
			$resultCode = '200';
		elseif ($repeatRequest === true)
			$resultCode = '402';
		else
			$resultCode = '500';
		$mnt_signature = md5($resultCode.$mnt_id.$mnt_transaction_id.$mnt_data_integrity_code);
		$response = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
		$response .= "<MNT_RESPONSE>\n";
		$response .= "<MNT_ID>{$mnt_id}</MNT_ID>\n";
		$response .= "<MNT_TRANSACTION_ID>{$mnt_transaction_id}</MNT_TRANSACTION_ID>\n";
		$response .= "<MNT_RESULT_CODE>{$resultCode}</MNT_RESULT_CODE>\n";
		$response .= "<MNT_SIGNATURE>{$mnt_signature}</MNT_SIGNATURE>\n";
		if (!empty($kassa_inventory) || !empty($kassa_customer) || !empty($kassa_delivery))
		{
			$response .= "<MNT_ATTRIBUTES>\n";
			foreach (array('INVENTORY' => $kassa_inventory, 'CUSTOMER' => $kassa_customer, 'DELIVERY' => $kassa_delivery) as $k => $v)
				if (!empty($v))
					$response .= "<ATTRIBUTE><KEY>{$k}</KEY><VALUE>{$v}</VALUE></ATTRIBUTE>\n";
			$response .= "</MNT_ATTRIBUTES>\n";
		}
		$response .= "</MNT_RESPONSE>\n";
		if ($echo === true)
		{
			header("Content-type: application/xml");
			echo $response;
		}
		else
			return $response;
		if ($die === true)
			die;
		return '';
	}
}
