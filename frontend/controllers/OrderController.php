<?php
namespace frontend\controllers;

use common\components\MailCamp;
use common\components\TBase;
use common\models\CartProductAttributes;
use common\models\CartProducts;
use common\models\Order;
use common\models\OrderProductAttributes;
use common\models\User;
use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Session;
use \common\helper\Tanuki;
use \common\components\Paypal;
use \common\models\OrderSearch;
/**
 * Site controller
 */
class OrderController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['order-partial-view','add-to-cart','index','view','payment-processing','create-order','payment-completed','remove-cart','order-mail','report','return','paypal-payment-processing','paypal','credit-cart-process','bank-deposit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main_container';
        $searchModel = new OrderSearch();
        $searchModel->user_id = Yii::$app->user->identity->id;
        $searchModel->order_status = ['1','2','3','4','5','6','7','8'];
        $searchModel->type = ['product_purchase','auction','buyout'];

        $dataProvider = $searchModel->frontSearch(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionAddToCart()
    {
        $sh  = (isset($_REQUEST['sh'])) ? $_REQUEST['sh'] : false;
        $quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : 1;
        $code = (isset($_REQUEST['code'])) ? $_REQUEST['code'] : false;
        $code = (is_array($code)) ? $code[0] : $code;

        if ($sh && $quantity && $code) {

            $exist = CartProducts::checkIfExist($code,$sh);
            if ($exist) {
                $exist->quantity += $quantity;
                $exist->save(false);
                Yii::$app->session->setFlash('success', TBase::ShowLbl('ITEM_ADDED_TO_CART_FOR_REVIEW'));
            } else {
                $model = new CartProducts();
                $model->shop = $sh;
                $model->buyer_id = Yii::$app->user->id;
                $model->product_id = $code;
                $model->quantity = $quantity;
                $model->status = 'pending';
                $model->notify = 0;
                $model->created_on = date('Y-m-d');
                if ($model->save(false)) {
                    if (isset($_REQUEST['params'])) {
                        foreach ($_REQUEST['params'] as $key => $value) {
                            $CartAttribute = new CartProductAttributes();
                            $CartAttribute->cart_id = $model->id;
                            $CartAttribute->attribute_name = $key;
                            $CartAttribute->attribute_value = $value;
                            $CartAttribute->save(false);
                        }
                    }
                    Yii::$app->session->setFlash('success', TBase::ShowLbl('ITEM_ADDED_TO_CART_FOR_REVIEW'));
                } else {
                    print_r($model->errors);
                    exit;
                }
            }
	        MailCamp::adminAddToCartNotification();
        } else {
            Yii::$app->session->setFlash('error','Invalid token.');
        }

        if (Yii::$app->request->referrer == 'https://www.tanukishop.com/site/login') {
	        $shop = Yii::$app->request->get('sh');
	        $code = Yii::$app->request->get('code');
	        return $this->redirect([$shop.'/detail','id'=>$code]);
	        exit;
        }
        $parsed = parse_url(Yii::$app->request->referrer);
        $query = $parsed['query'];

        parse_str($query, $params);

        if (isset($params['cart_id'])) {
            unset($params['cart_id']);
        }

        $string = http_build_query($params);
        return $this->redirect(Url::to([$parsed['path']]).'?'.$string);
    }

    public function actionRemoveCart($cart_id)
    {
        $cart = \Yii::$app->session['carts'];
        if($cart[$cart_id]) {
            unset($cart[$cart_id]);
        }
        \Yii::$app->session['carts'] = $cart;
        Yii::$app->session->setFlash('success',TBase::ShowLbl('ITEM_DELETED_FROM_CART_SUCCESSFULLY'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCreateOrder()
    {
        if (Yii::$app->request->isAjax) {
            if (!isset(\Yii::$app->session['orderID']) && isset(\Yii::$app->session['carts'])) {
                $TanukiCharges = TBase::TanukiCharges();
                $total = 0;
                if (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts']) > 0) {
                    foreach (\Yii::$app->session['carts'] as $key => $cartItem) {
                        $api = new Tanuki();
                        $api->singleProductDetail($cartItem['product_code'], $cartItem['shop']);
                        $amount = $api->shopProductDetail($cartItem['shop'], 'price');
                        $total += ($amount * $cartItem['quantity']) + $TanukiCharges;
                    }
                }

                // paypal tax calculation
                $paypalTax = (($total * 0.039 + 40 ) * 1.0402);
                $finalCost = $total+$paypalTax;



                $OrderModel = new \common\models\Order();
                $OrderModel->subtotal = $total;
                $OrderModel->total = $finalCost;
                $OrderModel->tanuki_charges = $TanukiCharges;
                $OrderModel->user_id = Yii::$app->user->id;
                $OrderModel->date = date('Y-m-d H:i:s');
                $OrderModel->response = '';
                $OrderModel->failure_reason = '';
                $OrderModel->shipping_charges = 0;
                $OrderModel->currency = 'JPY';
                $OrderModel->method = $_REQUEST['method'];
                $OrderModel->status = 'Pending Payment';
                $OrderModel->invoice = uniqid();
                if ($OrderModel->save(false)) {
                    \Yii::$app->session['orderID'] = $OrderModel->id;

                    if (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts']) > 0) {
                        foreach (\Yii::$app->session['carts'] as $key => $cartItem) {
                            $api->singleProductDetail($cartItem['product_code'], $cartItem['shop']);
                            $amount = $api->shopProductDetail($cartItem['shop'], 'price');
                            $OrderDetailModel = new \common\models\OrderDetail();
                            $OrderDetailModel->order_id = $OrderModel->id;
                            $OrderDetailModel->shop = $cartItem['shop'];
                            $OrderDetailModel->product_id = $cartItem['product_code'];
                            $OrderDetailModel->product_name = $api->shopProductDetail($cartItem['shop'], 'name');
                            $OrderDetailModel->product_image = TBase::saveOrderImage($api->shopProductDetail($cartItem['shop'], 'image'),$OrderModel->id,$cartItem['shop']);
                            $OrderDetailModel->quantity = $cartItem['quantity'];
                            $OrderDetailModel->price = $amount;
                            $OrderDetailModel->currency = 'JPY';
                            if ($OrderDetailModel->save()) {
                                if (isset($cartItem['params'])) {  // saving attributes in case if any product have any attributes
                                    foreach($cartItem['params'] as $attribute => $value) {
                                        $modelAttribute = new OrderProductAttributes();
                                        $modelAttribute->attribute_name = $attribute;
                                        $modelAttribute->attribute_value = $value;
                                        $modelAttribute->order_id = $OrderModel->id;
                                        $modelAttribute->product_id = $cartItem['product_code'];
                                        $modelAttribute->save();
                                    }
                                }
                            }

                        }
                    }

                    if (\Yii::$app->session['shipping']) {
                        $shipping = \Yii::$app->session['shipping'];
                        $OrderShipping = new \common\models\ShippingAddress();

                        $OrderShipping->first_name = $shipping['first_name'];
                        $OrderShipping->last_name = $shipping['last_name'];
                        $OrderShipping->company = $shipping['company'];
                        $OrderShipping->address_1 = $shipping['address_1'];
                        $OrderShipping->address_2 = $shipping['address_2'];
                        $OrderShipping->appt_no = $shipping['appt_no'];
                        $OrderShipping->city = $shipping['city'];
                        $OrderShipping->state = $shipping['state'];
                        $OrderShipping->country = $shipping['country'];
                        $OrderShipping->zipcode = $shipping['zipcode'];
                        $OrderShipping->phone = $shipping['appt_no'];
                        $OrderShipping->email = TBase::userEmail();
                        $OrderShipping->shipping_method = $shipping['shipping_method'];
                        $OrderShipping->order_id = $OrderModel->id;
                        $OrderShipping->save(false);
                    }
                    echo $OrderModel->id;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            } else {
                echo \Yii::$app->session['orderID'];
                exit;
            }
        }
    }

    public function actionCreditCartProcess() {

        $OrderModel = \common\models\Order::findOne($_REQUEST['orderID']);

        $requestParams = array(
            'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
            'PAYMENTACTION' => 'Sale'
        );

        $creditCardDetails = array(
            'CREDITCARDTYPE' => $_REQUEST['OrderPayment']['type'],
            'ACCT' => $_REQUEST['OrderPayment']['number'],
            'EXPDATE' => $_REQUEST['OrderPayment']['expire_month'].$_REQUEST['OrderPayment']['expire_year'],
            'CVV2' => $_REQUEST['OrderPayment']['security_code']
        );

        $payerDetails = array(
            'FIRSTNAME' => $_REQUEST['OrderPayment']['first_name'],
            'LASTNAME' => $_REQUEST['OrderPayment']['last_name'],
            'COUNTRYCODE' => $_REQUEST['OrderPayment']['COUNTRYCODE'],
            'STATE' => $_REQUEST['OrderPayment']['STATE'],
            'CITY' => $_REQUEST['OrderPayment']['STATE'],
            'STREET' => $_REQUEST['OrderPayment']['STREET'],
            'ZIP' => $_REQUEST['OrderPayment']['ZIP']
        );

        $orderParams = array(
            'AMT' => $OrderModel->total,
            'ITEMAMT' => $OrderModel->total,
            'SHIPPINGAMT' => '0',
            'CURRENCYCODE' => $OrderModel->currency
        );

        $paypal = new \common\components\Paypal();
        $response = $paypal->request('DoDirectPayment',
            $requestParams + $creditCardDetails + $payerDetails + $orderParams
        );

        if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
            $OrderModel->paypal_created_time = $response['TIMESTAMP'];
            $OrderModel->status = 'Success';
            $OrderModel->transaction_id = $response['TRANSACTIONID'];
            $OrderModel->response = json_encode($response);
            $OrderModel->save(false);
            Order::orderInvoiceMail($OrderModel);

            unset(\Yii::$app->session['orderID']);
            unset(\Yii::$app->session['carts']);
            echo '1';
            exit;
        } else {
            echo 'Invalid Credit Card Credentials';
            exit;
        }
    }

    public function actionPaymentCompleted($_ls = '',$er = '')
    {
        if ($_ls) {
            unset(\Yii::$app->session['orderID']);
            unset(\Yii::$app->session['carts']);
            $model = \common\models\Order::findOne(['id'=>$_ls,'user_id'=>Yii::$app->user->id]);
            CartProducts::deleteCartAfterOrder();
            return $this->render('order-completed',['model'=>$model]);
        } else {
            return $this->redirect(['site/index']);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $this->layout = 'main_container';
        $model = \common\models\Order::findOne(['id'=>$id]);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionReturn()
    {
        $OrderModel = \common\models\Order::findOne(\Yii::$app->session['orderID']);

        $mailView = 'order';
        if ($OrderModel->type == Order::ADD_FUND) {
            $mailView = ($OrderModel->method == 'Paypal' || $OrderModel->method == 'Credit Card') ? 'funds/order-'.Yii::$app->user->identity->language: 'funds/invoice-'.Yii::$app->user->identity->language;
        }

        if (isset($_REQUEST['success']) && $_REQUEST['success'] ==1) {
            $paypal = new \common\components\Paypal();

            $requestParams = array(
                'TOKEN' => $_GET['token'],
                'PAYMENTACTION' => 'Sale',
                'PAYERID' => $_GET['PayerID'],
                'PAYMENTREQUEST_0_AMT' => $OrderModel->total, // Same amount as in the original request
                'PAYMENTREQUEST_0_CURRENCYCODE' => $OrderModel->currency // Same currency as the original request
            );

            $response1 = $paypal->request('DoExpressCheckoutPayment', $requestParams);

            if (is_array($response1) && ($response1['ACK'] == 'Success' || $response1['ACK'] == 'SuccessWithWarning')) { // Payment successful

                $OrderModel->status = 'Success';
                $OrderModel->order_status = Order::DOMESTIC_SHIPPING;
                $OrderModel->token = $response1['TOKEN'];
                $OrderModel->response = json_encode($response1);
                $OrderModel->save(false);
                $to = TBase::userEmail();

                if ($OrderModel->type == 'add_fund') {
                    User::updateFund(Yii::$app->user->id,$OrderModel->subtotal,'paypal'); // update user credit;
                }

	            Order::orderInvoiceMail($OrderModel->id,$mailView);

                unset(\Yii::$app->session['orderID']);
                unset(\Yii::$app->session['carts']);
                return $this->redirect(['order/payment-completed', '_ls' => $OrderModel->id]);
                exit;
            }
        } else {
            $OrderModel->status = 'Failed';
            $OrderModel->invoice = '0';
            $OrderModel->order_status = 9;
            $OrderModel->response = '0';
            $OrderModel->failure_reason = 'Transaction cancelled by user';
            $OrderModel->save(false);

            unset(\Yii::$app->session['orderID']);
            unset(\Yii::$app->session['carts']);
            return $this->redirect(['order/payment-completed', '_ls' => $OrderModel->id, 'er' => 1]);
        }
    }

    public function actionOrderPartialView(){
        if (Yii::$app->request->isAjax && $_REQUEST['id']) {
            $this->layout = 'blank';
            $model = \common\models\Order::findOne(['id' => $_REQUEST['id']]);
            return $this->renderPartial('_view_partial', [
                'model' => $model,
            ]);
        }
    }


}
