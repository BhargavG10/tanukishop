<?php
namespace frontend\controllers;

use common\models\Amazon;
use Yii;
use \common\models\Category;

/**
 * Site controller
 */
class AmazonController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $categories = Category::find()->where(['status'=>1,'shop'=>'amazon'])->orderBy('serial_no')->all();
        return $this->render('index',['categories'=>$categories]);

    }

    public function actionList()
    {
	    $cid = Yii::$app->request->get('cid');
	    if ($cid) {
		    if ($model = Category::findOne( $cid )) {
			    \Yii::$app->view->title = $model->title_en;
		    	\Yii::$app->view->registerMetaTag( [
				    'name'    => 'title',
				    'content' => $model->meta_title,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'description',
				    'content' => $model->meta_description,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'keywords',
				    'content' => $model->meta_keywords,
			    ] );
		    }
	    }
        sleep(2);
        $category = '';
        $model = new Amazon();
        $page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] :  1;
        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['page'=>$page]);
        $searchBar = [];
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != ' ' && $_REQUEST['id'] != 'All') {
            $categoryData = Category::findOne(['ref_id' => $_REQUEST['id']]);
            $searchBar = array_merge($searchBar,['genreId' => $categoryData->ref_id]);
            $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['id'=>$_REQUEST['id']]);
            $category = $categoryData->ref_id;
        }

        if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'lth') {
            $model->amazonPrice = 'price';
        } else if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'htl') {
            $model->amazonPrice = '-price';
        }
        if (isset($_REQUEST['pmin']) && $_REQUEST['pmin'] != '') {
            $model->amazonMin = $_REQUEST['pmin']; # min max price +itemPrice -itemPrice +reviewCount
        }

        if (isset($_REQUEST['pmax']) && $_REQUEST['pmax'] != '') {
            $model->amazonMax = $_REQUEST['pmax'];
        }

        try {
            if (isset($_REQUEST['s'])) {
                $response = $model->search('All', $_REQUEST['s'], null, $page);
            } else {
                $response = $model->productList($categoryData->ref_id, $categoryData->amazon_search_index, $page);
            }

            $data = simplexml_load_string($response);
        } catch(Exception $e){
            echo $e->getMessage();
        }

        if (!isset($_REQUEST['s'])) {
            $category = $categoryData;
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_listing',['data'=>$data,'category'=>$category,'param'=>$params]);
        }

        return $this->render('listing',['data'=>$data,'category'=>$category,'param'=>$params]);
    }

    public function actionDetail($id)
    {
        sleep(2);
        $model = new Amazon;
        $data = $model->productDetail(['id'=>$id]);
        $xml = simplexml_load_string($data);
        $json = json_encode($xml);
        $array = json_decode($json, true);
        return $this->render('detail',['data'=>$array,'id'=>$id]);
    }


}
