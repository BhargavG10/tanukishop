<?php
namespace frontend\controllers;

use common\models\Page;
use Yii;
use yii\web\Controller;
/**
 * Site controller
 */
class PageController extends BaseController
{
    /**
     * @inheritdoc
     */


    public function actionIndex($slug)
    {
        \Yii::$app->view->registerMetaTag([
        'name' => 'google',
            'content' => 'notranslate'
        ]);
        $pageData = Page::findOne(['slug'=>$slug]);
        return $this->render('/site/page/page',['pageData'=>$pageData]);
    }
}
