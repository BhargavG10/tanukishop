<?php
namespace frontend\controllers;

use common\components\TBase;
use common\models\Order;
use common\models\OrderSearch;
use Yii;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use common\models\User;
use common\models\Messages;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class UserController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['user-validate','lang-switch','captcha','product-shipping-cost'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['my-page','logout','add-to-cart','product-shipping-cost','inbox','fav-products','yahoo-auction-fav-products','add-fund','funds','update','delete-fav','auction-delete-fav'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => null,
            ],
        ];
    }

    public function actionMyPage()
    {
	    return $this->render('my-page');
    }
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $lang = ($session->has('zlang')) ? $session->get('zlang') : 'ru-RU';
        Yii::$app->user->logout();
        $session->set('zlang', $lang);
        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'main_container';
        $model = new PasswordResetRequestForm();
        $model->type = 'user';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', TBase::ShowLbl('CHECK_EMAIL_INSTRUCTION'));

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', TBase::ShowLbl('SORRY_PASSWORD_RESET_ERROR'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', TBase::ShowLbl('NEW_PASSWORD_SAVED'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionUserValidate($l,$t)
    {
        $user = User::findOne([
            'email' => base64_decode($l),
            'auth_key' => base64_decode($t),
            'status' => 9,
        ]);

        if ($user) {
            $user->status = 10;
            $user->auth_key = '';
            $user->save();

            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'RegSuccess-html', 'text' => 'RegSuccess-text'],
                    ['user' => $user]
                )
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($user->email)
                ->setSubject('New Activated Successfully : ' . \Yii::$app->name)
                ->send();


            Yii::$app->session->setFlash('success', TBase::ShowLbl('ACCOUNT_ACTIVATED_SUCCESS'));
            return $this->redirect(['login']);
        } else {
            Yii::$app->session->setFlash('danger', TBase::ShowLbl('INVALID_CREDENTIALS'));
            return $this->redirect(['login']);
        }
    }

    public function actionSearch($shop = null, $x = null)
    {
        if ($shop == 'amazon') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['amazon/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['amazon/products','x'=>$x]));
        } else if ($shop == 'rakuten') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['rakuten/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['rakuten/products','x'=>$x]));
        } else if ($shop == 'yahoo') {
            if ($x == '') {
                return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo/index']));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(['yahoo/products','x'=>$x]));
        }
        exit;
    }

    public function actionProfile()
    {
        $user = \common\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        return $this->render('profile', [
            'user' => $user,
        ]);
    }

    public function actionUpdate()
    {
        $model = new \common\models\PasswordForm;
        $user = \common\models\User::findOne(['id' => Yii::$app->user->identity->id]);

        if (isset($_POST['User']) && $user->load(Yii::$app->request->post())) {
            $mydata = $_POST['User'];
            $user->first_name = $mydata['first_name'];
            $user->last_name = $mydata['last_name'];
            $user->email = $mydata['email'];
            $user->language = $mydata['language'];
            $user->save();

            Yii::$app->session->setFlash('success', TBase::ShowLbl('PROFILE_UPDATED'));
            return $this->redirect(['update']);
        }


        if($model->load(Yii::$app->request->post()) && $model->validate() && isset($_POST['PasswordForm'])){
            $user->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
            if($user->save()){
                Yii::$app->getSession()->setFlash('success',TBase::ShowLbl('PASSWORD_CHANGED_SUCCESS'));
            }else{
                Yii::$app->getSession()->setFlash('danger',TBase::ShowLbl('PASSWORD_NOT_CHANGED'));
            }
            return $this->redirect(['update']);
        }

        return $this->render('update', [
            'user' => $user,
            'model'=>$model
        ]);
    }

    public function actionChangePassword(){
        $model = new \common\models\PasswordForm;
        $modeluser = User::findOne(Yii::$app->user->getId());

        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash('success',TBase::ShowLbl('PASSWORD_CHANGED_SUCCESS'));
                    }else{
                        Yii::$app->getSession()->setFlash('danger',TBase::ShowLbl('PASSWORD_NOT_CHANGED'));
                    }
                    return $this->redirect(['change-password']);
                }catch(\Exception $e){
                    Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
                    return $this->render('change-password',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('change-password',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('change-password',[
                'model'=>$model
            ]);
        }
    }
    public function actionFavProducts(){

        $searchModel = new \app\models\FavoriteProductSearch();
        $searchModel->user_id = Yii::$app->user->getId();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/favorite/favorite-product', [

            'listDataProvider' => $dataProvider,
        ]);
    }

    public function actionYahooAuctionFavProducts(){

        $searchModel = new \app\models\FavoriteProductSearch();
        $searchModel->user_id = Yii::$app->user->getId();
        $searchModel->shop = ['yahoo_auction'];
        $dataProvider = $searchModel->YSearch(Yii::$app->request->queryParams);

        return $this->render('/favorite/yahoo-favorite-product', [

            'listDataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteFav($id)
    {
        $product = \app\models\FavoriteProduct::findOne(['id'=>$id]);
        if ($product->delete()) {
            Yii::$app->session->setFlash('success', TBase::ShowLbl('PRODUCT_DELETED_SUCCESS'));
        } else {
            Yii::$app->session->setFlash('error', TBase::ShowLbl('PRODUCT_NOT_DELETED'));
        }
        return $this->redirect(['/user/fav-products']);
    }

    public function actionAuctionDeleteFav($id)
    {
        $product = \app\models\FavoriteProduct::findOne(['id'=>$id]);
        if ($product->delete()) {
            Yii::$app->session->setFlash('success', TBase::ShowLbl('PRODUCT_DELETED_SUCCESS'));
        } else {
            Yii::$app->session->setFlash('error', TBase::ShowLbl('PRODUCT_NOT_DELETED'));
        }
        return $this->redirect(['/user/yahoo-auction-fav-products']);
    }

    public function actionInbox(){
        \Yii::$app->view->registerMetaTag([
            'name' => 'google',
            'content' => 'notranslate'
        ]);
        $model = new \frontend\models\Chat;
        $messageAll = Messages::findAll(['user_id'=>Yii::$app->user->getId()]);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $message = new Messages();
                $message->user_id = Yii::$app->user->getId();
                $message->message_from = 'user';
                $message->date = new \yii\db\Expression('NOW()');
                $message->msg = $model->msg;
                $message->is_read_by_user = 1;
                $message->is_read_by_admin = 0;
                    if ($message->save(false)) {
                        if ($model->sendEmail()) {
                            Yii::$app->session->setFlash('success', TBase::ShowLbl('QUERY_SUBMIT'));
                            $this->refresh();
                        }
                    }
            }
        }

        return $this->render('/message/index',['model'=>$model,'message'=>$messageAll]);
    }


    /**
     * @return string
     */
    public function actionFunds()
    {
        $this->layout = 'main_container';
        $searchModel = new OrderSearch();
        $searchModel->user_id = Yii::$app->user->identity->id;
        $searchModel->type = 'add_fund';
        $dataProvider = $searchModel->frontSearch(Yii::$app->request->queryParams);
        return $this->render('/funds/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionAddFund()
    {
	    $model = new \frontend\models\OrderPayment();
		$userModel = new \common\models\User();
		$userModel->scenario = 'add-fund';
		$fundAmount = 0;
        if ($userModel->load(Yii::$app->request->get()) && $userModel->validate()) {
			if($userModel->credit > 0){
				$fundAmount = $userModel->credit;
			}
        }
        return $this->render('/funds/add-fund',['model'=>$model,'userModel'=>$userModel,'fundAmount'=>$fundAmount]);
    }
}
