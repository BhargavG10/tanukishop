<?php

namespace frontend\controllers;

use common\models\CartProductAttributes;
use Yii;
use common\models\CartProducts;
use common\models\CartProductsSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * CartProductsController implements the CRUD actions for CartProducts model.
 */
class CartProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all CartProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDelete($id) {

        if ($this->findModel($id)->delete()) {
            CartProductAttributes::deleteAll(['cart_id'=>$id]);
            Yii::$app->session->setFlash('success','cart data deleted successfully');
            return $this->redirect(['cart-products/index']);
        }
    }
    /**
     * Finds the CartProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CartProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CartProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
