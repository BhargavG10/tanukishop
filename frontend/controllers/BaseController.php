<?php
/**
 * Created by PhpStorm.
 * User: anilkumar
 * Date: 11/22/16
 * Time: 10:14 AM
 */

namespace frontend\controllers;
use yii\web\Controller;
use yii;

class BaseController extends Controller
{
    public function init() {

        $url = str_ireplace('www.', '', $_SERVER['SERVER_NAME']);
        if (!isset($_COOKIE['googtrans'])) {
            setcookie("googtrans","/en/ru", strtotime( '+1 year' ), "/",'.'.$url);
        }

        if (!isset($_COOKIE['zLang'])) {

            $session = Yii::$app->session;
            $session->set('zLang', 'ru-RU');
            setcookie("zLang", "1", strtotime( '+1 year' ), "/", '.' . $url);
        }
    }
}