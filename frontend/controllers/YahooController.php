<?php
namespace frontend\controllers;

use Yii;
use \common\models\Yahoo;
use \common\models\Category;

/**
 * Site controller
 */
class YahooController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'google',
            'content' => 'notranslate'
        ]);
        $this->layout = 'main';
        $categories = Category::find()->where(['status'=>1,'shop'=>'yahoo'])->orderBy('serial_no')->all();
        return $this->render('categories',['categories'=>$categories]);
    }

    public function actionList()
    {
    	$cid = Yii::$app->request->get('cid');
    	if ($cid) {
		    if ($model = Category::findOne( $cid )) {
			    \Yii::$app->view->title = $model->title_en;
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'title',
				    'content' => $model->meta_title,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'description',
				    'content' => $model->meta_description,
			    ] );
			    \Yii::$app->view->registerMetaTag( [
				    'name'    => 'keywords',
				    'content' => $model->meta_keywords,
			    ] );
		    }
    	}

	    $model  = new Yahoo();
        $per_page = 20;
	    $id = false;
        if (Yii::$app->request->get('cid')) {
	        $id = Yii::$app->request->get( 'cid' );
        }

        $page   = (int)(isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $paramsVar = [];
        if (Yii::$app->request->get('page')) {
            $paramsVar = ['offset'=>$page];
        }
        $category = false;

        $params = array_merge([Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],['cid'=>$id,'page'=>$page]);

	    $cid = (
		    Yii::$app->request->get('cid') && Yii::$app->request->get('cid') != 0
	    ) ? Yii::$app->request->get('cid') : false;

	    if ($cid) {
            $category = Category::findOne(['ref_id'=>$cid]);
            $paramsVar = array_merge($paramsVar,['category_id'=>$cid]);
        }

        if (isset($_REQUEST['s'])) {
            $paramsVar = array_merge($paramsVar, ['query' => $_REQUEST['s']]);
            $params = array_merge($params, ['s' => $_REQUEST['s']]);
        }

        if (isset($_REQUEST['pmin']) && $_REQUEST['pmin'] != '') {
            $paramsVar = array_merge($paramsVar,['price_from'=>$_REQUEST['pmin']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmin'=>$_REQUEST['pmin']]); # min max price +itemPrice -itemPrice +reviewCount
        }

        if (isset($_REQUEST['pmax']) && $_REQUEST['pmax'] != '') {
            $paramsVar = array_merge($paramsVar,['price_to'=>$_REQUEST['pmax']]); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['pmax'=>$_REQUEST['pmax']]); # min max price +itemPrice -itemPrice +reviewCount
        }


        if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'lth') {
            $paramsVar = array_merge($paramsVar,['sort'=>'+price']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=>$_REQUEST['sort']]);
        } else if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'htl') {
            $paramsVar = array_merge($paramsVar,['sort'=>'-price']); # min max price +itemPrice -itemPrice +reviewCount
            $params = array_merge($params,['sort'=>$_REQUEST['sort']]);
        }

        $data = $model->productsListing($paramsVar);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial(
                '_listing',
                ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
            );
        }

        return $this->render(
            'listing',
            ['data'=>$data,'category'=>$category,'param'=>$params,'TModel'=>$model]
        );
    }

    public function actionDetail($id,$cid = false)
    {
        $itemCode = $id;
        $category = ($cid != false) ? Category::findOne(['ref_id'=>$cid]) : '';

        $model = new Yahoo();
        $data = $model->productsDetail($itemCode);
        return $this->render('detail',['data'=>$data,'category'=>$category]);
    }
}