<?php

namespace frontend\controllers;

use common\components\TBase;
use Yii;
use app\models\FavoriteProduct;
use app\models\FavoriteProductSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Expression;
/**
 * FavoriteProductController implements the CRUD actions for FavoriteProduct model.
 */
class FavoriteProductController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','delete'],
                'rules' => [
                    [
                        'actions' => ['index','create','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FavoriteProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FavoriteProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new FavoriteProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $shop  = (isset($_REQUEST['shop'])) ? $_REQUEST['shop'] : false;
        $code = (isset($_REQUEST['code'])) ? $_REQUEST['code'] : false;
        $code = (is_array($code)) ? $code[0] : $code;

        $model = new FavoriteProduct();
        $data = $model->findOne(['shop'=>$shop,'code'=>$code,'user_id'=>Yii::$app->user->id]);
        if (!$data) {
            $model->code = $code;
            $model->shop = $shop;
            $model->user_id = Yii::$app->user->id;
            $model->created_on = new Expression('NOW()');
            if ($model->save(false)) {
	            return TBase::_x('ITEM_ADDED_TO_FAVORITE_LIST_SUCCESSFULLY');
            }
        } else {
	        return TBase::_x('ITEM_ALREADY_ADDED_TO_YOUR_FAVORITE_LIST');
        }
//            return $this->redirect(Yii::$app->request->referrer);
    }

   /**
     * Deletes an existing FavoriteProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FavoriteProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FavoriteProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FavoriteProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
