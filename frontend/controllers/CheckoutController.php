<?php
namespace frontend\controllers;

use common\components\TBase;
use common\models\User;
use frontend\models\OrderPayment;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Session;
use frontend\models\ShippingDetail;
/**
 * Site controller
 */
class CheckoutController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['add-to-cart','shipping'],
                'rules' => [
                    [
                        'actions' => ['add-to-cart','index','shipping','confirm','pay'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * @return \yii\web\Response
     */
    public function actionAddToCart()
    {
        $sh  = (isset($_REQUEST['sh'])) ? $_REQUEST['sh'] : false;
        $quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : 1;
        $code = (isset($_REQUEST['code'])) ? $_REQUEST['code'] : false;
        $code = (is_array($code)) ? $code[0] : $code;

        if ($sh && $quantity && $code) {

            $f = false;
            $session = new Session;
            $session->open();
            $cart = \Yii::$app->session['carts'];

            if (count($cart) > 0) {
                foreach ($cart as $key => $item) {
                    if ($item['product_code'] == $code) {
                        $f = true;
                    }
                }
            }

            if ($f) {
                Yii::$app->session->setFlash('error', TBase::ShowLbl('ALREADY_IN_CART'));
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                $cart[] = [
                    "product_code" => $code,
                    "quantity" => $quantity,
                    "shop" => $sh
                ];
                \Yii::$app->session['carts'] = $cart;
                Yii::$app->session->setFlash('success', TBase::ShowLbl('ADDED_TO_CART'));
            }
        } else {
            Yii::$app->session->setFlash('error',TBase::ShowLbl('INVALID_TOKEN'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionShipping()
    {
	    $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();

	    if (!$approveCartProducts) {
	    	return $this->goHome();
	    }

        $model = new ShippingDetail();
        if (\Yii::$app->user->id) {
            $data = User::findOne(\Yii::$app->user->id);
            $model->first_name = $data->first_name;
            $model->last_name = $data->last_name;
            $model->email = $data->email;
        }

        if (\Yii::$app->session['shipping']) {
            $ship =   \Yii::$app->session['shipping'];
            $model->first_name = $ship['first_name'];
            $model->last_name = $ship['last_name'];
            $model->company = $ship['company'];
            $model->address_1 = $ship['address_1'];
            $model->address_2 = $ship['address_2'];
            $model->appt_no = $ship['appt_no'];
            $model->city = $ship['city'];
            $model->state = $ship['state'];
            $model->country = $ship['country'];
            $model->zipcode = $ship['zipcode'];
            $model->phone = $ship['phone'];
            $model->email = $ship['email'];
            $model->shipping_method = $ship['shipping_method'];
        }
//        $carts = (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts'])>0) ? \Yii::$app->session['carts'] : [];
	    $carts = ($approveCartProducts) ? $approveCartProducts : [];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            \Yii::$app->session['shipping'] = $_REQUEST['ShippingDetail'];

            return $this->redirect(['confirm']);
        }
        return $this->render('shipping',['model'=>$model,'carts'=>$carts]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
//        $carts = (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts'])>0) ? \Yii::$app->session['carts'] : [];
        $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();

	    if (!$approveCartProducts) {
		    return $this->goHome();
	    }

	    return $this->render('index',['carts'=>$approveCartProducts]);
    }

    /**
     * @return string
     */
    public function actionConfirm()
    {
        $shipping = \Yii::$app->session['shipping'];
        if(!$shipping) {
            Yii::$app->session->setFlash('error','Please fill shipping detail first');
            return $this->redirect(['/checkout/shipping']);
        }

	    $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();

	    if (!$approveCartProducts) {
		    return $this->goHome();
	    }
        //$carts = (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts'])>0) ? \Yii::$app->session['carts'] : [];
        return $this->render('confirm',['carts'=>$approveCartProducts]);
    }

    /**
     * @param $code
     * @return \yii\web\Response
     */
    public function actionDelete($code)
    {
        $cart = \Yii::$app->session['carts'];
        $key = array_search($code, array_column($cart, 'product_code'));
        if ($key >= 0) {
            unset($cart[$key]);
            $cart = array_merge($cart);
            \Yii::$app->session['carts'] = $cart;
            Yii::$app->session->setFlash('success',TBase::ShowLbl('CART_DELETED'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string
     */
    public function actionPay()
    {
        $shipping = \Yii::$app->session['shipping'];
        if(!$shipping) {
            Yii::$app->session->setFlash('error','Please fill shipping detail first');
            return $this->redirect(['/checkout/shipping']);
        }

        //$strip = new \common\models\PaymentMethodStripe();
        $model = new OrderPayment();
        $tanukiModel = new \common\helper\Tanuki();
        $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();
        $total = 0;
        foreach($approveCartProducts as $key => $item) {
            $tanukiCharges = TBase::TanukiCharges();
            $tanukiModel->singleProductDetail($item['product_id'],$item['shop']);
            $amount = $tanukiModel->shopProductDetail($item['shop'],'price');
            $total += ($amount * $item['quantity'])+$tanukiCharges + $item['domestic_shipping'];
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            #Ajax call will be used further
        }

        return $this->render('pay',['model' => $model,'carts'=>$approveCartProducts,'total'=>$total]);
    }

    /**
     * @return string
     */
    public function actionOrderIndex()
    {
        $this->layout = 'main_container';
        $searchModel = new \common\models\OrderSearch();
        $searchModel->user_id == Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('orderIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $this->layout = 'main_container';
        $model = \common\models\Order::findOne(['id'=>$id]);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * method redirect on appropriate payment method
     */
    public function actionPaymentMethodSelection() {

        if ($_POST['_payment_method']) {
            switch (Yii::$app->request->post('payment_method')) {
                case 'paypal':
                    return $this->redirect(['payment/paypal-payment']);
                    break;
                case 'credit_card':
                    return $this->redirect(['payment/paypal-credit-card-payment']);
                    break;
                case 'bank_transfer':
                    return $this->redirect(['payment/bank-transfer-payment']);
                    break;
                case 'alipay':
                    return $this->redirect(['payment/alipay-payment']);
                    break;
                case 'stripe' :
                    return $this->redirect(['payment/stripe-payment']);
                    break;
                default:
                    Yii::$app->session->setFlash('error','Please select valid payment method');
                    return $this->redirect(['checkout/payment-method-selection']);
                    break;
            }
        } else {
            return $this->redirect(['index']);
        }
    }

}
