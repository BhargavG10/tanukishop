<?php

namespace frontend\controllers;

use Yii;
use common\models\UserAuctions;
use common\models\UserAuctionsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserAuctionsController implements the CRUD actions for UserAuctions model.
 */
class UserAuctionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
	    return [
		    'access' => [
			    'class' => AccessControl::className(),
			    'rules' => [
				    [
					    'actions' => ['index','view','delete'],
					    'allow' => true,
					    'roles' => ['@'],
				    ],
			    ],
		    ],
		    'verbs' => [
			    'class' => VerbFilter::className(),
			    'actions' => [
				    'logout' => ['post'],
			    ],
		    ],
	    ];
    }

    /**
     * Lists all UserAuctions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserAuctionsSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->is_deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserAuctions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing UserAuctions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserAuctions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAuctions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAuctions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
