<?php
namespace frontend\controllers;

use common\components\MailCamp;
use common\models\Order;
use common\models\PaymentMethodStripe;
use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \common\components\Paypal;
use common\components\TBase;
use common\components\Alipay;
/**
 * Site controller
 */
class FundPaymentController extends BaseController
{
    public function beforeAction($action)
    {
        if ('alipay-notify' == $action->id) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    private function paypalPaymentProcessing($amount) {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::saveFundOrder('Paypal','JPY','6',Order::PAYMENT_PENDING, $amount)) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']){
            $orderID = \Yii::$app->session['orderID'];
        }
        $orderDetail = Order::findOne($orderID);

	    $total = $orderDetail->total;
//	    $extraCharges = $total*0.002; // extra as per client request
//	    $charges = round(($total * 3.9/100) + 40);
//	    $total_changes = ($charges + $extraCharges + $total);
//	    $coefficient = (40+($total*0.002));
//	    $coefficient = (40+($total*0.0039));
//	    $percentage = 0.039;
//
//	    $cardCharges = round(($total * $percentage) + $coefficient);
//	    $total_changes = ($cardCharges + $total);

	    $cardCharges =  round(($total * 0.039 + 40 ) * 1.0402);
	    $total_changes = round($cardCharges + $total);


	    // paypal tax calculation
	    $orderDetail->subtotal = $total;
	    $orderDetail->total = $total_changes;
	    $orderDetail->paid_amount = $total_changes;
	    $orderDetail->tanuki_charges = $cardCharges;

	    $orderDetail->save();


        $requestParams = array(
            'RETURNURL' => Url::to(['order/return','success'=>'1'],true),
            'CANCELURL' => Url::to(['order/return','success'=>'0'],true)
        );

        $orderParams = array(
            'PAYMENTREQUEST_0_AMT' => $orderDetail->total,
            'PAYMENTREQUEST_0_SHIPPINGAMT' => '0',
            'PAYMENTREQUEST_0_CURRENCYCODE' => $orderDetail->currency,
            'PAYMENTREQUEST_0_ITEMAMT' => $orderDetail->paid_amount
        );
        $paypal = new Paypal();
        $response = $paypal->request('SetExpressCheckout', $requestParams + $orderParams);
        if (is_array($response) && $response['ACK'] == 'Success') {
            $token = $response['TOKEN'];
            $PAYPAL_WEBSCR_URL = TBase::TanukiSetting('PAYPAL_WEBSCR_URL');
            header('Location: '.$PAYPAL_WEBSCR_URL. urlencode($token));
            exit;
        } else if (is_array($response) && $response['ACK'] == 'Failure') {
	        die($response['L_SHORTMESSAGE0']);
        }
    }


    private function monetaPaymentProcessing($amount) {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::saveFundOrder('moneta','JPY','6',Order::PAYMENT_PENDING, $amount)) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']){
            $orderID = \Yii::$app->session['orderID'];
            $orderUpdate = Order::findOne(['id'=>$orderID,'user_id' => Yii::$app->user->id]);
            if($orderUpdate && $orderUpdate->total != $amount){
                $orderUpdate->total =$amount;
                $orderUpdate->save();
            }

        }
        $orderDetail = Order::findOne($orderID);

	    $total = $orderDetail->total;
//	    $coefficient = (40+($total*0.0039));
//	    $percentage = 0.039;
//
//	    $cardCharges = round(($total * $percentage) + $coefficient);
//	    $total_changes = ($cardCharges + $total);
//

	    // paypal tax calculation
	    $orderDetail->subtotal = $total;
	    $orderDetail->total = $total;
	    $orderDetail->paid_amount = $total;
	    $orderDetail->tanuki_charges = $total;
	    $orderDetail->save();
	    return $this->redirect(['moneta-payment/index', '_ls' => $orderDetail->id]);
	    exit;

    }

    private function processFundBankDeposit($amount) {

        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::saveFundOrder('Bank Deposit','JPY',$status = ' ',Order::PAYMENT_PENDING, $amount)) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']) {
            $orderID = \Yii::$app->session['orderID'];
        }
        $orderDetail = Order::findOne($orderID);
        unset(\Yii::$app->session['orderID']);

        $subject = 'Add Funds - Invoice ' . $orderDetail->id;
        $to = Yii::$app->user->identity->email;
        if (Yii::$app->user->identity->language == 'ru-RU') {
	        $subject = 'Инвойс на пополнение счета';
        }
        Yii::$app->mailer->compose('funds/invoice-'.Yii::$app->user->identity->language, [
            'model' => $orderDetail,
        ])
            ->setTo($to)
            ->setFrom([TBase::TanukiSetting('sales-email') => 'Tanukishop.com'])
            ->setSubject($subject)
            ->send();
	    MailCamp::adminFundNotification($orderDetail->id);
        return $this->redirect(['order/payment-completed', '_ls' => $orderDetail->id]);
        exit;
    }

    public function actionCreditCart($amount) {
        if (!isset(\Yii::$app->session['orderID'])) {
            if (Order::saveFundOrder('Credit Card','JPY',$status = ' ',Order::PAYMENT_PENDING, $amount)) {
                $orderID = \Yii::$app->session['orderID'];
            } else{
                echo "Error While Saving your order. Please try again";
                exit;
            }
        } elseif (\Yii::$app->session['orderID']){
            $orderID = \Yii::$app->session['orderID'];
        }

        $OrderModel = Order::findOne($orderID);

	    $total = $OrderModel->total;
//	    $extraCharges = $total*0.002; // extra as per client request
//	    $charges = round(($total * 3.9/100) + 40);
//	    $total_changes = ($charges + $extraCharges + $total);
//	    $coefficient = (40+($total*0.0039));
//	    $percentage = 0.039;

	    $cardCharges = 0;
	    $total_changes = ($cardCharges + $total);


	    // credit card tax calculation
	    $OrderModel->subtotal = $total;
	    $OrderModel->total = $total_changes;
	    $OrderModel->paid_amount = $total_changes;
	    $OrderModel->tanuki_charges = $cardCharges;
	    $OrderModel->save();

        $requestParams = array(
            'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
            'PAYMENTACTION' => 'Sale'
        );

        $creditCardDetails = array(
            'CREDITCARDTYPE' => $_REQUEST['OrderPayment']['type'],
            'ACCT' => $_REQUEST['OrderPayment']['number'],
            'EXPDATE' => $_REQUEST['OrderPayment']['expire_month'].$_REQUEST['OrderPayment']['expire_year'],
            'CVV2' => $_REQUEST['OrderPayment']['security_code']
        );

        $payerDetails = array(
            'FIRSTNAME' => $_REQUEST['OrderPayment']['first_name'],
            'LASTNAME' => $_REQUEST['OrderPayment']['last_name'],
            'COUNTRYCODE' => $_REQUEST['OrderPayment']['COUNTRYCODE'],
            'STATE' => $_REQUEST['OrderPayment']['STATE'],
            'CITY' => $_REQUEST['OrderPayment']['STATE'],
            'STREET' => $_REQUEST['OrderPayment']['STREET'],
            'ZIP' => $_REQUEST['OrderPayment']['ZIP']
        );

        $orderParams = array(
            'AMT' => $OrderModel->total,
            'ITEMAMT' => $OrderModel->total,
            'SHIPPINGAMT' => '0',
            'CURRENCYCODE' => $OrderModel->currency
        );

        $paypal = new \common\components\Paypal();
        $response = $paypal->request('DoDirectPayment',
            $requestParams + $creditCardDetails + $payerDetails + $orderParams
        );

        if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
            $OrderModel->status = 'Success';
            $OrderModel->order_status = Order::DOMESTIC_SHIPPING;
            $OrderModel->payment_id = $response['TRANSACTIONID'];
            $OrderModel->response = json_encode($response);
            $OrderModel->save(false);
            unset(\Yii::$app->session['orderID']);
            unset(\Yii::$app->session['carts']);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'code' => 1,
                'order_id' => $OrderModel->id
            ];
            exit;
        } else if( is_array($response) && $response['ACK'] == 'Failure') {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'code' => 0,
                'msg' => $response['L_LONGMESSAGE0']
            ];
            exit;
        }
    }

    public function actionPaymentMethodSelection(){
        if (Yii::$app->request->get('_payment_method') && Yii::$app->request->get('fund_amount')) {
            $_payment_method = Yii::$app->request->get('_payment_method');
            $fund_amount = Yii::$app->request->get('fund_amount');
            if (!empty($fund_amount) && !empty($_payment_method)) {
                switch (Yii::$app->request->get('_payment_method')) {
                    case 'bank_transfer' :
                        $this->processFundBankDeposit(Yii::$app->request->get('fund_amount'));
                        break;
                    case 'paypal':
                        $this->paypalPaymentProcessing(Yii::$app->request->get('fund_amount'));
                        break;
                    case 'moneta-payment':
                        $this->monetaPaymentProcessing(Yii::$app->request->get('fund_amount'));
                        break;
                    default:
                        Yii::$app->session->setFlash('warning', 'Invalid access');
                        return $this->goBack();
                        break;
                }
            } else {
                Yii::$app->session->setFlash('warning', 'Invalid access');
                return $this->goBack();
            }
        }
    }
}
