
jQuery(document).ready(function($) {

   if (($('.product-load-more').length) > 0){
      $('<input>').attr({
         type: 'hidden',
         id: 'lang',
         name: 'lang',
         value: 'jpy'
      }).appendTo('#w0');
   }

   var cleaned_host;
   if(location.host.indexOf('www.') === 0){
      cleaned_host = location.host.replace('www.','');
   }

   var eCamerasArray = [];

   // product currency change js used in list page

   $('ul.currency-filters li a').click(function () {
      var currency = $(this).data('currency');
      $('.amount').each(function ()
          {
             $(this).text($(this).data(currency));
          }
      );
      $('#lang').val(currency);
      $('ul.currency-filters li a').removeClass('current');
      if (currency == 'jpy') $('#currency-jpy').addClass('current');
      if (currency == 'usd') $('#currency-usd').addClass('current');
      if (currency == 'rub') $('#currency-rub').addClass('current');
      //$('#lblCurrentCurency').text($(this).text()); $('.filter-currency').text($(this).data('symbol')); $.cookie('currency', currency, { expires: 100 });
   });



   // shipping calculator js used in detail page

   jQuery(document).ready(function($){
      jQuery('#calculate-shipping').click(function(){
         jQuery('#item-shipping-calculation').addClass('hidden');
         jQuery('.absolute').show();
         jQuery.ajax({
            type : 'POST',
            url :  shipping_url, // initialized in product detail page
            dataType : 'json',
            data: jQuery('#shipping_calculator').serialize(),
            success: function( data ) {
               if (data['error'] == '0') {
                  jQuery('#item-shipping-calculation').removeClass('hidden');
                  jQuery('.absolute').hide();
                  jQuery('.shipping_charge').html(data['shipping_cost']);
                  jQuery('.total_cost').html(data['total_cost']);
               } else {
                  alert(data['error']);
                  jQuery('.absolute').hide();
               }
               return false;
            }
         });
         return false;
      });
      return false;
   });

    jQuery(document).ready(function($){
        jQuery('body').on('click','.wish-i',function(){
            $('.loading-extend-text span').empty();
            $('.loading-extend,.loading-image').show();
            $.get( jQuery(this).data('href') )
                .done(function(data) {
                    $('.loading-extend,.loading-image').hide();
                    $('.loading-extend-text,.loading-text').show();
                    $('#loading-text-id').html(data);
                })
                .fail(function(data) {
                    $('.loading-extend,.loading-image').hide();
                    return true;
                });
            return false;
        });

        jQuery('body').on('click','#close-loading-text',function(){
            $('.loading-extend-text span').empty();
            $('.loading-extend-text,.loading-text').hide();
        });
        return false;
    });


   // slider and tab on product detai page
   if ($('#bxslider-de').length>0) {
   //$("#horizontalTab").easyResponsiveTabs({
   //   type: "default",
   //   width: "auto",
   //   fit: true,
   //   closed: "accordion"
   //});


      var $j = jQuery.noConflict();
      var realSlider = $j("ul#bxslider-de").bxSlider({
         //speed: 1000,
         pager: false,
         auto: true,
         nextText: '',
         prevText: '',
         infiniteLoop: true,
         hideControlOnEnd: true,
         onSlideBefore: function ($slideElement, oldIndex, newIndex) {
            changeRealThumb(realThumbSlider, newIndex);
         }
      });

      var realThumbSlider = $j("ul#bxslider-pager").bxSlider({
         minSlides: 3,
         maxSlides: 3,
         mode: 'horizontal',
         slideWidth: 110,
         slideMargin: 14,
         moveSlides: 1,
         pager: false,
         auto: true,
         //speed: 1000,
         infiniteLoop: true,
         hideControlOnEnd: false,
         nextText: '<span></span>',
         prevText: '<span></span>',
         onSlideBefore: function ($slideElement, oldIndex, newIndex) {
            /*$j("#sliderThumbReal ul .active").removeClass("active");
             $slideElement.addClass("active"); */
         }
      });

      linkRealSliders(realSlider, realThumbSlider);

      if ($j("#bxslider-pager li").length < 5) {
         $j("#bxslider-pager .bx-next").hide();
      }

      // sincronizza sliders realizzazioni
      function linkRealSliders(bigS, thumbS) {

         $j("ul#bxslider-pager").on("click", "a", function (event) {
            event.preventDefault();
            var newIndex = $j(this).parent().attr("data-slideIndex");
            bigS.goToSlide(newIndex);
         });
      }

      //slider!=$thumbSlider. slider is the realslider
      function changeRealThumb(slider, newIndex) {
         var $thumbS = $j("#bxslider-pager");
         $thumbS.find('.active').removeClass("active");
         $thumbS.find('li[data-slideIndex="' + newIndex + '"]').addClass("active");

         if (slider.getSlideCount() - newIndex >= 4)slider.goToSlide(newIndex);
         else slider.goToSlide(slider.getSlideCount() - 4);
      }
   }

   // $(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");


   // Credit card payment page
   // if ($('form#payment-form').length>0) {
   //    $('form#payment-form').on('beforeSubmit', function (e) {
   //       $('#myModal').show();
   //       $('#myModal .modal-content p').html(CREATING_YOUR_ORDER_PLEASE_WAIT);
   //
   //       var path = '';
   //       var data = '';
   //       path = $('form#payment-form').attr('action');
   //       data = $('form#payment-form').serialize();
   //
   //       jQuery.ajax({
   //          type: 'POST',
   //          url: $('form#payment-form').attr('action'),
   //          data: $('form#payment-form').serialize(),
   //          success: function (data) {
   //
   //             console.log(data);
   //              if (jQuery.trim(data) == 0) {
   //                  console.log('Payment Successfully');
   //                  window.location = $("#successAction").val()+'?_ls='+jQuery('#orderID').val();
   //                  return false;
   //              } else {
   //                  console.log('Payment UnSuccessfully');
   //                  $('#order-error').html(data);
   //                  return false;
   //                  //alert(ERROR_WHILE_ORDER_CREATION_PLEASE_TRY_AGAIN);
   //                  console.log('Error while order creation');
   //                  return false;
   //              }
   //             return false;
   //
   //          },
   //          beforeSend: function () {
   //             console.log('Please wait. Creating Order....');
   //          }
   //       });
   //       return false;
   //    }).on('submit', function (e) {    // can be omitted
   //       e.preventDefault();         // can be omitted
   //       return false;
   //    });
   // }


    // add fund - Credit card payment page
    // if ($('form#add-fund-payment-form-cc').length>0) {
    //     $('form#add-fund-payment-form-cc').on('beforeSubmit', function (e) {
    //         $('#myModal').show();
    //         $('#myModal .modal-content p').html(CREATING_YOUR_ORDER_PLEASE_WAIT);
    //
    //         var path = '';
    //         var data = '';
    //         path = $('form#add-fund-payment-form-cc').attr('action')+'?amount='+$('#fund_amount').val();
    //         data = $('form#add-fund-payment-form-cc').serialize();
    //
    //         jQuery.ajax({
    //             type: 'POST',
    //             url: path,
    //             data: data,
    //             success: function (data) {
    //                 $('#myModal').hide();
    //                 console.log(data);
    //                 if (jQuery.trim(data['code']) == 1) {
    //                     console.log('Payment Successfully');
    //                     window.location = $("#successAction").val()+'?_ls='+data['order_id'];
    //                     return false;
    //                 } else {
    //                     console.log('Payment UnSuccessfully');
    //                     $('#order-error').html(data['msg']);
    //                     console.log('Error while order creation');
    //                     return false;
    //                 }
    //                 return false;
    //
    //             },
    //             beforeSend: function () {
    //                 console.log('Please wait. Creating Order....');
    //             }
    //         });
    //         return false;
    //     }).on('submit', function (e) {    // can be omitted
    //         e.preventDefault();         // can be omitted
    //         return false;
    //     });
    // }


   // bank-trasnfer-payment-form
   if ($('form#bank-trasnfer-payment-form').length>0) {
      $('.bank-deposit').on('click', function (e) {
         if (!confirm($('#bank-confirmation').val())) {
            return false;
         }
         $('#myModal').show();
         $('#myModal .modal-content p').html(CREATING_YOUR_ORDER_PLEASE_WAIT);

         jQuery.ajax({
            type: 'POST',
            url: $('form#bank-trasnfer-payment-form').attr('action'),
            data: $('form#bank-trasnfer-payment-form').serialize(),
            success: function (data) {

               if (jQuery.trim(data) != 0) {
                  jQuery('#bank-orderID').val(data);
                  window.location = $("#bank-deposit-success-action").val()+'?_ls='+jQuery('#bank-orderID').val();
                  console.log(data);
               } else {
                  alert(ERROR_WHILE_ORDER_CREATION_PLEASE_TRY_AGAIN);
                  console.log('Error while order creation');
                  return false;
               }
               return false;

            },
            beforeSend: function () {
               console.log('Please wait. Creating Order....');
            }
         });

         return false;
      }).on('submit', function (e) {    // can be omitted
         e.preventDefault();         // can be omitted
         return false;
      });
   }

   $('#send-mail').click(function(){
      if ($.trim($('#msg').val()) == '') {
         alert('Please enter msg');
         return false;
      }

      jQuery.ajax({
         type: 'POST',
         url: $('#mail-path').val(),
         data: {body:$('#msg').val(),url:window.location.href},
         success: function (data) {
            $('#send-mail').text('Send').removeAttr('disabled');

            if (jQuery.trim(data) == 1) {
               alert(MESSAGE_SEND_SUCCESSFULLY);
               $('#msg').val('')
               return false;
            } else {
               alert(ERROR_WHILE_SENDING_MAIL_PLEASE_TRY_AGAIN_LATER);
               $('#msg').val('')
               return false;
            }
            return false;
         },
         beforeSend: function () {
            $('#send-mail').text('Please Wait...').attr('disabled','disabled');
            console.log('Please wait. Sending Mail');
         }
      });
      return false;
   });

   // product listing page
   $('.product-load-more').click(function(){
      if ($(this).data('shop') == 'yahoo' ){
         $('#w0 input[name="page"]').val(parseInt($('#w0 input[name="page"]').val())+20);
      } else {
         $('#w0 input[name="page"]').val(parseInt($('#w0 input[name="page"]').val())+1);
      }
      $('.loading-extend,.loading-image').show();

      jQuery.ajax({
         type: 'GET',
         url: ajax_url,
         data: $('#w0').serialize(),
         success: function (data) {
            $('.loading-extend,.loading-image').hide();
            if (jQuery.trim(data)==0) {
               $(".top_p.list-brand").append('No more products.');
               $('.product-load-more').hide();
            } else {
               $(".top_p.list-brand").append(data);
               //translate();
            }
            return false;
         },
         beforeSend: function () {
            //$('.loading-extend,.loading-image').show();
         },
         error : function(data, textStatus, jqXHR) {
            $('.loading-extend,.loading-image').hide();
            $(".top_p.list-brand").append('Server error.');
         }
      });
      return false;
   });

   if ($('.home-featured-product').length>0) {
      jQuery.ajax({
         type: 'POST',
         url: featureURl,
         success: function (data) {
            jQuery('.home-featured-product').html(data);
         },
         beforeSend: function () {
         },
         error: function (data, textStatus, jqXHR) {
            jQuery('.lb-loader').html(ERROR_WHILE_LOADING_SERVER_DATA);
         }
      });
   }

   $('.creditcard').on('click',function(e){
      $('.payment-div').slideUp('slow');
      $('.payment-div.'+$(this).data('div')).slideDown('slow');
   });


   // order detail view in package system

    $('.order-detail-view').click(function(){
    $(this).html('Please wait..');
        $('.modal-body').empty();
        jQuery.ajax({
            type: 'POST',
            url: order_detail_url,
            data: {id:$(this).data('id')},
            success: function (data) {
                $('#order-detail').modal('show');
                $('#order-detail .modal-dialog').html(data);
                $('.order-detail-view').html('View Detail');
            },
            beforeSend: function () {
                $('.loading-extend,.loading-image').show();
            },
            error : function(data, textStatus, jqXHR) {
                $('.order-detail-view').html('View Detail');
            }
        });
        return false;
    });


    $('#auction-sort-list').on('change',function(){
       window.location.href = $(this).val();
    });

    $('.add-fund-paypal-btn,.add-fund-bank-deposit').on('click',function() {
       var payment_method = ($(this).data('payment-type') == 'paypal') ? 'Paypal' : 'Bank Transfer';
       if ( confirm('Are you sure you want to pay with '+payment_method) ) {
           $('#myModal').show();
            window.location.href = add_fund_payment_url+'?_payment_method='+$(this).data('payment-type')+'&fund_amount='+$('#fund_amount').val();
       }
    });
});
