jQuery(document).ready(function($){

   var cleaned_host;
   if(location.host.indexOf('www.') === 0){
      cleaned_host = location.host.replace('www.','');
   }

   $('.product-bx').show();
   var eCamerasArray = [];
   // product currency change js used in list page

   $('ul.currency-filters li a').click(function () {
      var currency = $(this).data('currency');
      $('.amount').each(function ()
          {
             $(this).text($(this).data(currency));
          }
      );
      $('ul.currency-filters li a').removeClass('current');
      if (currency == 'jpy') $('#currency-jpy').addClass('current');
      if (currency == 'usd') $('#currency-usd').addClass('current');
      if (currency == 'rub') $('#currency-rub').addClass('current');
      //$('#lblCurrentCurency').text($(this).text()); $('.filter-currency').text($(this).data('symbol')); $.cookie('currency', currency, { expires: 100 });
   });



   // shipping calculator js used in detail page

   jQuery(document).ready(function($){
      jQuery('#calculate-shipping').click(function(){
         jQuery('.no-shipping-msg').hide();
         jQuery('#item-shipping-calculation').addClass('hidden');

         jQuery('.absolute').show();
         jQuery.ajax({
            type : 'POST',
            url :  shipping_url, // initialized in product detail page
            data: jQuery('#shipping_calculator').serialize()+'&quantity='+$('#quantity_class').val(),
            success: function( data ) {
               console.log(data);
               jQuery('#item-shipping-calculation').removeClass('hidden');
               jQuery('.absolute').hide();
               jQuery('#item-shipping-calculation').html(data);
               return false;
            }
         });
         return false;
      });
      return false;
   });

 jQuery(document).ready(function($){
      jQuery('#calculate-shipping-category').click(function(){
         jQuery('.no-shipping-msg').hide();
         jQuery('#item-shipping-calculation').addClass('hidden');

         jQuery('.absolute').show();
         jQuery.ajax({
            type : 'POST',
            url :  shipping_url_category, // initialized in product detail page
            data: jQuery('#calculate-shipping-form').serialize()+'&quantity='+$('#quantity_class').val(),
            success: function( data ) {
               //console.log(data);
               jQuery('#item-shipping-calculation_category').removeClass('hidden');
               jQuery('.absolute').hide();
               jQuery('#item-shipping-calculation_category').html(data);
               return false;
            }
         });
         return false;
      });
      return false;
   });


   // slider and tab on product detai page
    if ($('#shipping-horizontalTab').length>0) {
        $("#shipping-horizontalTab").easyResponsiveTabs({
            type: "default",
            width: "auto",
            fit: true,
            closed: "accordion"
        });
    }

   if ($('#bxslider-de').length>0) {


      var $j = jQuery.noConflict();
      var realSlider = $j("ul#bxslider-de").bxSlider({
         pager: false,
         auto: true,
         nextText: '',
         prevText: '',
         infiniteLoop: false,
         hideControlOnEnd: true,
         onSlideBefore: function ($slideElement, oldIndex, newIndex) {
            changeRealThumb(realThumbSlider, newIndex);
         }
      });

      var realThumbSlider = $j("ul#bxslider-pager").bxSlider({
         minSlides: 3,
         maxSlides: 3,
         mode: 'horizontal',
         slideWidth: 110,
         slideMargin: 14,
         moveSlides: 1,
         pager: false,
         auto: true,
         infiniteLoop: false,
         hideControlOnEnd: true,
         nextText: '<span></span>',
         prevText: '<span></span>',
         onSlideBefore: function ($slideElement, oldIndex, newIndex) {
            $j("#sliderThumbReal ul .active").removeClass("active");
             $slideElement.addClass("active");
         }
      });

      linkRealSliders(realSlider, realThumbSlider);

      if ($j("#bxslider-pager li").length < 5) {
         $j("#bxslider-pager .bx-next").hide();
      }

      // sincronizza sliders realizzazioni
      function linkRealSliders(bigS, thumbS) {

         $j("ul#bxslider-pager").on("click", "a", function (event) {
            event.preventDefault();
            var newIndex = $j(this).parent().attr("data-slideIndex");
            bigS.goToSlide(newIndex);
         });
      }

      //slider!=$thumbSlider. slider is the realslider
      function changeRealThumb(slider, newIndex) {
         var $thumbS = $j("#bxslider-pager");
         $thumbS.find('.active').removeClass("active");
         $thumbS.find('li[data-slideIndex="' + newIndex + '"]').addClass("active");

         if (slider.getSlideCount() - newIndex >= 4)slider.goToSlide(newIndex);
         else slider.goToSlide(slider.getSlideCount() - 4);
      }
   }

   // $(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");


   // payment page
   if ($('form#payment-form').length>0) {
      $('form#payment-form').on('beforeSubmit', function (e) {
         $('#myModal').show();
         $('#myModal .modal-content p').html(CREATING_YOUR_ORDER_PLEASE_WAIT);

         var path = '';
         var data = '';
         path = $('form#payment-form').attr('action');
         data = $('form#payment-form').serialize();

         jQuery.ajax({
            type: 'POST',
            url: $('form#payment-form').attr('action'),
            data: $('form#payment-form').serialize(),
            success: function (data) {

               console.log(data);
               if (jQuery.trim(data) != 0) {
                  jQuery('#orderID').val(data);
                  $('#myModal .modal-content p').html(ORDER_CREATED_PAYMENT_PROCESSING_PLEASE_WAIT);
                  path = $('#paymentAction').val();
                  data = $('form#payment-form').serialize();
                  jQuery.ajax({
                     type: 'POST',
                     url: path,
                     data: data,
                     success: function (data) {
                        $('#myModal').hide();
                        if (jQuery.trim(data) == 1) {
                           console.log('Payment Successfully');
                           window.location = $("#successAction").val();
                           return false;
                        } else {
                           console.log('Payment UnSuccessfully');
                           $('#order-error').show().html(data);
                           return false;
                        }
                        return false;
                     },
                     beforeSend: function () {
                        console.log('Please wait. Payment Processing');
                     }
                  });

               } else {
                  alert(ERROR_WHILE_ORDER_CREATION_PLEASE_TRY_AGAIN);
                  console.log('Error while order creation');
                  return false;
               }
               return false;

            },
            beforeSend: function () {
               console.log('Please wait. Creating Order....');
            }
         });

         return false;
      }).on('submit', function (e) {    // can be omitted
         e.preventDefault();         // can be omitted
         return false;
      });
   }

   $('#send-mail').click(function(){
      if ($.trim($('#msg').val()) == '') {
         alert('Please enter msg');
         return false;
      }

      jQuery.ajax({
         type: 'POST',
         url: $('#mail-path').val(),
         data: {body:$('#msg').val(),url:window.location.href},
         success: function (data) {
            $('#send-mail').text('Send').removeAttr('disabled');

            if (jQuery.trim(data) == 1) {
               alert('Message Send Successfully');
               $('#msg').val('')
               return false;
            } else {
               alert('Error while sending mail. Please try again later');
               $('#msg').val('')
               return false;
            }
            return false;
         },
         beforeSend: function () {
            $('#send-mail').text('Sending..').attr('disabled','disabled');
            console.log('Please wait. Sending Mail');
         }
      });
      return false;
   });
});
