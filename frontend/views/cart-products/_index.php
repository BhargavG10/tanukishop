<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;
use common\components\TBase as LBL;
$this->title = LBL::_x('CART_PRODUCTS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <div class="col-md-4">
                    <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                </div>
                <div class="col-md-8 msg">

                </div>
            </div>
            <div class="ibox-content" >

                <div class="cart-products-index" style="position: relative;">
                    <div class="_load" style="display: none;">
                        <span>Please wait</span>
                    </div>
                    <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute'=>'buyer_id',
                                    'value' => function($data) {
                                        return $data->buyer->fullName;
                                    },
                                ],
                                'product_id',
                                'shop',
                                'quantity',
                                [
                                    'attribute'=>'status',
                                    'format'=>'raw',
                                    'value'=>function($model) {
                                        $status = ['pending'=>'pending','approve'=>'approve','disapprove'=>'disapprove'];
                                        $html = '';
                                        return $html .= Html::dropDownList('status',$model->status,$status,['class'=>'status','id'=>$model->id]);
                                    }
                                ],
                                [
                                    'attribute'=>'notify',
                                    'value'=>function($data) {
                                        return ($data->notify) ? 'Sent' : 'Pending';
                                    }
                                ],
                                [
                                     'attribute'=>'created_on',
                                     'filter'=>false,
                                     'format'=>'date'
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}',
                                    'buttons' => [
                                        'view' => function ($url,$model) {
                                            $url = Yii::$app->params['website-url'].$model->shop.'/'.$model->product_id;
                                            return Html::a('View Product',Yii::$app->params['website-url'].$model->shop.'/detail?id='.$model->product_id,['class'=>'btn btn-primary','target'=>'_blank']);
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(
    '$(".status").on("change", function() {
        $("._load").show();
        $.ajax({
            url: "'.\yii\helpers\Url::to(["change-status"]).'",
            data: {id:this.id,status:$(this).val(),buyer_id:'.Yii::$app->request->get('buyer_id').'},
        })
        .done(function( data ) {
            $("._load").hide();
            $(".msg").html(data.message);
        });
    });',
    \yii\web\View::POS_READY,
    'my-status-change'
);
?>

<?php
$this->registerCss("
.cart-products-index{position:relative;}
._load{position: absolute;width: 100%;height: 100%;background: #000;opacity: .5;text-align: center;}
._load span{color: #fff;top: 50%;display: block;position: absolute;left: 45%;}

");
?>