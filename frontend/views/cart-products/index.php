<?php
$CurrencyModel = new \common\components\TCurrencyConvertor;
$model = new \common\helper\Tanuki();
use common\components\TBase as LBL;
$this->title = LBL::_x('CART_PRODUCTS');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    td.translate{width:40%;}
    .btn{border-radius: 0px;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=LBL::_x('home')?></a></li>
        <li class="active"><?=LBL::_x('CART_PRODUCTS')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <h3 class="inner-head"><?=LBL::_x('APPROVED_CART_PRODUCT')?></h3>
            <div class="table-responsive text-center">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?=LBL::_x('IMAGE')?></th>
                            <th><?=LBL::_x('PRODUCT_NAME')?></th>
                            <th class="text-center"><?=LBL::_x('QUANTITY')?></th>
                            <th class="text-center"><?=LBL::_x('SHOP')?></th>
                            <th class="text-center"><?=LBL::_x('PRICE')?></th>
                            <th class="text-center"><?=LBL::_x('DOMESTIC_SHIPPING')?></th>
                            <th class="text-center">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (Yii::$app->user->identity->approveCartProducts) {
                            $approveCartProducts = Yii::$app->user->identity->getApproveCartProducts()->with('params')->asArray()->all();
                            foreach($approveCartProducts as $key => $items) {
                                $model->singleProductDetail($items['product_id'],$items['shop']);
                                $name = $model->shopProductDetail($items['shop'],'name');
                                $price = $model->shopProductDetail($items['shop'],'price');
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                        $img =\yii\helpers\Html::img($model->getProductImage($items['shop']),['width'=>70,'height'=>70]);
                                        echo \yii\helpers\Html::a($img,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?>
                                    </td>
                                    <td  style="width: 48%!important;text-decoration: none;" class="translate  text-left">
                                        <?=\yii\helpers\Html::a($name,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?><br/>
                                        <?php if (isset($items['params'])) {
                                            foreach ($items['params'] as $params) {
                                                echo '<strong>'.$params['attribute_name'].'</strong>:'.$params['attribute_value'].'<br/>';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td style="width: 8%;"><?=$items['quantity']; ?></td>
                                    <td><?=$items['shop']; ?></td>
                                    <td><?=$CurrencyModel->convert($price,'JPY','JPY'); ?></td>
                                    <td><?=$CurrencyModel->convert($items['domestic_shipping'],'JPY','JPY'); ?></td>
                                    <td style="width: 11%! important;">
                                        <?=\yii\helpers\Html::a('<i class="fa fa-eye"></i>',[$items['shop'].'/detail','id'=>$items['product_id']],['class'=>'btn btn-primary'])?>
                                        <?=\yii\helpers\Html::a('<i class="fa fa-trash"></i>',
                                            ['cart-products/delete','id'=>$items['id']],
                                            [
                                                'class'=>'btn btn-primary',
                                                'method'=>'post',
                                                'data' => [
	                                                'confirm' => LBL::_x('CART_DELETE_CONFIRMATION'),
                                                ],
                                            ]
                                        )?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <div class="">
                    <?php if (Yii::$app->user->identity->approveCartProducts) { ?>
                        <?=\yii\helpers\Html::a(LBL::_x('PROCESS_TO_CHECKOUT'),['checkout/index'],['class'=>'btn btn-success'])?>
                    <?php } ?>
                </div>
            </div>

            <h3 style="margin-top: 130px;" class="inner-head"><?=LBL::_x('PENDING_CART_PRODUCT')?></h3>
            <div class="table-responsive text-center">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><?=LBL::_x('IMAGE')?></th>
                        <th><?=LBL::_x('PRODUCT_NAME')?></th>
                        <th class="text-center"><?=LBL::_x('QUANTITY')?></th>
                        <th class="text-center"><?=LBL::_x('SHOP')?></th>
                        <th class="text-center"><?=LBL::_x('PRICE')?></th>
                        <th class="text-center">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if (Yii::$app->user->identity->pendingCartProducts) {
                        $pendingCartProducts = Yii::$app->user->identity->getPendingCartProducts()->with('params')->asArray()->all();
                        foreach(Yii::$app->user->identity->pendingCartProducts as $key => $items) {

                            $model->singleProductDetail($items['product_id'],$items['shop']);
                            $name = $model->shopProductDetail($items['shop'],'name');
                            $price = $model->shopProductDetail($items['shop'],'price');
                            ?>
                            <tr>
                                <td>
                                    <?php
                                    $img =\yii\helpers\Html::img($model->getProductImage($items['shop']),['width'=>70,'height'=>70]);
                                    echo \yii\helpers\Html::a($img,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?>
                                </td>
                                <td  style="width: 57%!important;text-decoration: none;" class="translate  text-left">
                                    <?=\yii\helpers\Html::a($name,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?><br/>
                                    <?php if (isset($items['params'])) {
                                        foreach ($items['params'] as $params) {
                                            echo '<strong>'.$params['attribute_name'].'</strong>:'.$params['attribute_value'].'<br/>';
                                        }
                                    }
                                    ?>
                                </td>
                                <td style="width: 8%;"><?=$items['quantity']; ?></td>
                                <td><?=$items['shop']; ?></td>
                                <td><?=$CurrencyModel->convert($price,'JPY','JPY'); ?></td>
                                <td style="width: 11%! important;">
                                    <?=\yii\helpers\Html::a('<i class="fa fa-eye"></i>',[$items['shop'].'/detail','id'=>$items['product_id']],['class'=>'btn btn-primary'])?>
                                    <?=\yii\helpers\Html::a('<i class="fa fa-trash"></i>',
                                        ['cart-products/delete','id'=>$items['id']],
                                        [
                                            'class'=>'btn btn-primary',
                                            'method'=>'post',
                                            'data' => [
                                                'confirm' => LBL::_x('CART_DELETE_CONFIRMATION'),
                                            ],
                                        ]
                                    )?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    <?php

                    if (Yii::$app->user->identity->disApproveCartProducts) {
	                    $pendingCartProducts = Yii::$app->user->identity->getDisApproveCartProducts()->with('params')->asArray()->all();
	                    foreach(Yii::$app->user->identity->disApproveCartProducts as $key => $items) {

		                    $model->singleProductDetail($items['product_id'],$items['shop']);
		                    $name = $model->shopProductDetail($items['shop'],'name');
		                    $price = $model->shopProductDetail($items['shop'],'price');
		                    ?>
                            <tr>
                                <td>
				                    <?php
				                    $img =\yii\helpers\Html::img($model->getProductImage($items['shop']),['width'=>70,'height'=>70]);
				                    echo \yii\helpers\Html::a($img,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?>
                                </td>
                                <td  style="width: 57%!important;text-decoration: none;" class="translate  text-left">
				                    <?=\yii\helpers\Html::a($name,[$items['shop'].'/detail','id'=>$items['product_id']],['target'=>'_blank','style'=>'color:inherit'])?><br/>
				                    <?php if (isset($items['params'])) {
					                    foreach ($items['params'] as $params) {
						                    echo '<strong>'.$params['attribute_name'].'</strong>:'.$params['attribute_value'].'<br/>';
					                    }
				                    }
				                    ?>
                                </td>
                                <td style="width: 8%;"><?=$items['quantity']; ?></td>
                                <td><?=$items['shop']; ?></td>
                                <td><?=LBL::_x('NOT_AVAILABLE')?></td>
                                <td style="width: 11%! important;">
				                    <?=\yii\helpers\Html::a('<i class="fa fa-eye"></i>',[$items['shop'].'/detail','id'=>$items['product_id']],['class'=>'btn btn-primary'])?>
				                    <?=\yii\helpers\Html::a('<i class="fa fa-trash"></i>',
					                    ['cart-products/delete','id'=>$items['id']],
					                    [
						                    'class'=>'btn btn-primary',
						                    'method'=>'post',
						                    'data' => [
							                    'confirm' => LBL::_x('CART_DELETE_CONFIRMATION'),
						                    ],
					                    ]
				                    )?>
                                </td>
                            </tr>
		                    <?php
	                    }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>