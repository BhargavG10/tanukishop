<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserShippingAddress */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Shipping Address',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Shipping Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">

            <div class="col-lg-12">
                <h3 style="margin-bottom: 22px;" class="row inner-head"><?= Html::encode($this->title) ?></h3>
                <div class="packages-create">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>