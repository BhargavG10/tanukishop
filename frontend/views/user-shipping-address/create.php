<?php

use yii\helpers\Html;
use common\components\TBase as LBL;

/* @var $this yii\web\View */
/* @var $model common\models\UserShippingAddress */

$this->title = Yii::t('app', 'Create User Shipping Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Shipping Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=LBL::_x('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">

            <div class="col-lg-12">
                <h3 style="margin-bottom: 22px;" class="row inner-head"><?=LBL::_x('CREATE_NEW_SHIPPING_ADDRESS');?></h3>
                    <div class="user-shipping-address-create">
                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
</section>
