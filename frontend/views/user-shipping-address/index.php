<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\TBase;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserShippingAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Shipping Addresses');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .table tbody td:first-child {width: 10%;}
    .add-new-btn{margin-right: 15px;  margin-top: 10px;  }
    .add-new-btn a{background: #7ac0c8; border: none;}
    .table thead tr th{text-align: center;}
    .grid-view .fa{background: none;padding: 0;}
    .btn-primary{border-radius: 0;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=\common\components\TBase::ShowLbl('NAVIGATION_SHIPPING_ADDRESS')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class="dashboard">
            <div class="clearfix">
                <h3 class="inner-head "><?=TBase::ShowLbl('NAVIGATION_SHIPPING_ADDRESS')?></h3>
            </div>
            <div class="col-md-12"><?= Html::a(Yii::t('app', TBase::_x('CREATE_USER_SHIPPING_ADDRESS')), ['create'], ['class' => 'btn btn-primary pull-right', 'style'=>"margin-bottom: 10px;"]) ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive text-center">
                        <?php Pjax::begin(); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'emptyText' => TBase::_x('NO_DATA_FOUND'),
                                'summary' => '',
                                'columns' => [
        //                            ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute'=>'address_1',
                                        'label'=>TBase::_x('CREATE_SHIPPING_ADDRESS'),
                                    ],
                                    [
                                        'content' => function ($model, $key, $index, $column) {
                                                return Html::a('<i class="fa fa-pencil" ></i>', ['update', 'id' => $model->id],['class'=>'btn btn-primary','target'=>'_blank'])
                                                .' '.  Html::a('<i class="fa fa-trash" ></i>', ['delete', 'id' => $model->id], ['data-method'=> 'post','data-confirm'=>'Are you sure you want to delete this address?','class'=>'btn btn-primary','target'=>'_blank']);

                                        }
                                    ]
                                ],
                            ]); ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>