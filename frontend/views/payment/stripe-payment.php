<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;
use \common\helper\Tanuki;

$CurrencyModel = new \common\components\TCurrencyConvertor;
$TModel = new Tanuki();
$tanukiCharges = \common\components\TBase::TanukiCharges();
$this->title = 'Select Payment Method';

?>
<style>
    .tab-content a{display: none;}
    a.accordion-link{color:#fff;}
    .table tbody td:first-child{width:50%!important;font-weight: bold;}
    .table tbody tr:last-child{background: gainsboro;}

    .payment-errors {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
        padding: 15px;
        display: block;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>
<section>
    <div class="container notranslate">
        <ol class="breadcrumb"></ol>
        <div class="left-menu">
            <h3 class="inner-head notranslate" style="font-size: 15px;"><?=TBase::ShowLbl('CHECKOUT_PROCESS')?></h3>
            <?=TBase::checkoutSideBarMenu(true)?>
        </div>

        <div class=" dashboard">
            <h3 class="inner-head border-b">Payment</h3>
            <div class="col-sm-8 col-xs-7 mt20">
                <p>
                    <?php
                    $lang = (TBase::CLang() == 'ru-RU') ? 'payment_page_text_russian' : 'payment_page_text_english';
                    echo TBase::TanukiSetting($lang);
                    ?>
                </p>
            </div>

            <div class="col-sm-4 col-xs-5"><?=Html::img(yii\helpers\Url::to('@web/tnk/images/ssl.png', true),['alt'=>'ssl img','class'=>"pull-right"]);?></div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <div class="stripe-payment payment-div">
                    <div class="dropin-page" class="notranslate">
                        <form action="<?=\yii\helpers\Url::to(['payment/stripe-payment']); ?>" method="POST" id="payment-form" class="form-horizontal" role="form">
                        <fieldset>
                            <legend>Payment</legend>
                            <span class="payment-errors" style="display: none;"></span>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="card-number">Card Number</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" data-stripe="number" size="20" placeholder="Debit/Credit Card Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <select class="form-control col-sm-2" data-stripe="exp_month">
                                                <option>Month</option>
                                                <option value="01">Jan (01)</option>
                                                <option value="02">Feb (02)</option>
                                                <option value="03">Mar (03)</option>
                                                <option value="04">Apr (04)</option>
                                                <option value="05">May (05)</option>
                                                <option value="06">June (06)</option>
                                                <option value="07">July (07)</option>
                                                <option value="08">Aug (08)</option>
                                                <option value="09">Sep (09)</option>
                                                <option value="10">Oct (10)</option>
                                                <option value="11">Nov (11)</option>
                                                <option value="12">Dec (12)</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <select class="form-control" data-stripe="exp_year">
                                                <option value="13">2013</option>
                                                <option value="14">2014</option>
                                                <option value="15">2015</option>
                                                <option value="16">2016</option>
                                                <option value="17">2017</option>
                                                <option value="18">2018</option>
                                                <option value="19">2019</option>
                                                <option value="20">2020</option>
                                                <option value="21">2021</option>
                                                <option value="22">2022</option>
                                                <option value="23">2023</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" size="4" maxlength="4" data-stripe="cvc" placeholder="Security Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <input type="submit" class="btn btn-success" value="Pay Now" id="submit_btn">
                                </div>
                            </div>
                        </fieldset>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                    </form>
                </div>
            </div>
        </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<!-- TO DO : Place below JS code in js file and include that JS file -->
<script type="text/javascript">
    Stripe.setPublishableKey('<?=Yii::$app->params['public_test_key']?>');

    $(function() {
        var $form = $('#payment-form');
        $form.submit(function(event) {
            // Disable the submit button to prevent repeated clicks:
            $form.find('#submit_btn').prop('disabled', true);
            $form.find('#submit_btn').val('Please Wait...');

            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from being submitted:
            return false;
        });
    });

    function stripeResponseHandler(status, response) {
        // Grab the form:
        var $form = $('#payment-form');

        if (response.error) { // Problem!

            // Show the errors on the form:
            $form.find('.payment-errors').text(response.error.message).show();
            $form.find('#submit_btn').prop('disabled', false); // Re-enable submission
            $form.find('#submit_btn').val('Pay Now');
        } else { // Token was created!

            // Get the token ID:
            var token = response.id;

            // Insert the token ID into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" name="stripeToken">').val(token));

            // Submit the form:
            $form.get(0).submit();
        }
    };
</script>