
<!--<form action="https://demo.moneta.ru/assistant.htm" method="post" id="pay-moneta-form">-->
<form action="https://www.payanyway.ru/assistant.htm" method="post" id="pay-moneta-form">
	<input type="hidden" name="MNT_ID" value="<?=$sid?>" />
	<input type="hidden" name="MNT_TRANSACTION_ID" value="<?=$cart_order_id?>" />
	<input type="hidden" name="MNT_AMOUNT" value="<?=$amount?>" />
	<input type="hidden" name="MNT_CURRENCY_CODE" value="RUB" />
	<input type="hidden" name="MNT_TEST_MODE" value="0" />
	<input type="hidden" name="MNT_SIGNATURE" value="<?=$MNT_SIGNATURE?>" />
	<input type="hidden" name="MNT_SUCCESS_URL" value="<?=\yii\helpers\Url::toRoute(['moneta-payment/return'],true)?>" />
	<input type="hidden" name="MNT_FAIL_URL" value="<?=\yii\helpers\Url::toRoute(['site/index'],true)?>" />
	<input type="hidden" name="MNT_RETURN_URL" value="<?=\yii\helpers\Url::toRoute(['site/index'],true)?>" />
	<div class="buttons" style="display: none;">
		<div class="pull-right" style="">
			<input type="submit" value="pay now" class="btn btn-primary" />
		</div>
	</div>
</form>
<div class="container" style="padding:44px 35px; text-align: center;">
    <?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?>
</div>

<script>
    document.getElementById("pay-moneta-form").submit();// Form submission
</script>