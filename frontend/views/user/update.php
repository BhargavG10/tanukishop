<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = \common\components\TBase::ShowLbl('UPDATE_PROFILE')
?>
<style>
    .btn-cart1 {
        width: 20% !important;
    }
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <div class="col-lg-6">
                <h3 class="inner-head "> <?= \common\components\TBase::ShowLbl('UPDATE_PROFILE') ?>	</h3>
                <div class="">
                    <?php $form = ActiveForm::begin(['id' => 'form-update',
                        'options'=>['class'=>'form-horizontal'],
                        'fieldConfig'=>[
                            'template'=>"{label}\n<div class=\"col-lg-6\">
                                    {input}</div>\n<div class=\"col-lg-12\">
                                    {error}</div>",
                            'labelOptions'=>['class'=>'col-lg-4 control-label'],
                        ],]); ?>
                    <?= $form->field($user, 'first_name')->textInput() ?>
                    <?= $form->field($user, 'last_name')->textInput()?>
                    <?= $form->field($user, 'email')->textInput();?>
	                <?= $form->field($user, 'language')->dropDownList(['en-US'=>'English','ru-RU'=>'Russian']) ?>
                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
                            <?= Html::submitButton(\common\components\TBase::ShowLbl('UPDATE_PROFILE'), ['class'=>'btn width-204 color-782f4a']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-lg-6">
                        <h3 class="inner-head "> <?= \common\components\TBase::ShowLbl('CHANGE_PASSWORD') ?>	</h3>
                        <div class="">
                            <?php $form = ActiveForm::begin([
                                'id'=>'changepassword-form',
                                'options'=>['class'=>'form-horizontal'],
                                'fieldConfig'=>[
                                    'template'=>"{label}\n<div class=\"col-lg-6\">
                                {input}</div>\n<div class=\"col-lg-12\">
                                {error}</div>",
                                    'labelOptions'=>['class'=>'col-lg-4 control-label'],
                                ],
                            ]); ?>
                            <?= $form->field($model,'oldpass')->passwordInput() ?>
                            <?= $form->field($model,'newpass')->passwordInput() ?>
                            <?= $form->field($model,'repeatnewpass')->passwordInput() ?>

                            <div class="form-group">
                                <div class="col-lg-offset-4 col-lg-8">
                                    <?= Html::submitButton(\common\components\TBase::ShowLbl('CHANGE_PASSWORD'),[
                                        'class'=>'btn width-204 color-782f4a'
                                    ]) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
            </div>
            <div class="col-lg-12" style="margin-top:76px">
                <img src="http://www.tanukishop.com/uploads/profile_page.jpg"></div>
            </div>
        </div>
    </div>
</section>