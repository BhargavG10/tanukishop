<?php

use yii\helpers\Html;
use \common\models\User;
use common\components\TBase as LBL;
$tanuki = new \common\helper\Tanuki;
$acModel = new \common\models\YahooAuctions();
$currencyModel = new \common\components\TCurrencyConvertor;
$acModel->productsDetail($model->code);

if (isset($acModel->YahooAction['ResultSet']['Result']['Title'])) {
?>
<style>
    .icon-eye-open,.icon-trash {  font-size: 16px;  color: #000;}
    table td {
        padding: 3px !important;
        vertical-align: middle ! important;
    }

</style>
<?php  if($acModel->YahooAction['ResultSet']['Result']['Status'] != 'closed'){?>
<tr class="txt-c">
    <td class="text-left translate">
        
		<?php
		
        if (isset($acModel->YahooAction['ResultSet']['Result']['Img']['Image1']) &&	!empty($acModel->YahooAction['ResultSet']['Result']['Img']['Image1'])) 
		{
			echo Yii::$app->formatter->asImage($acModel->YahooAction['ResultSet']['Result']['Img']['Image1'],['class'=>"img-thumbnail"]);
			
        }
                                ?>
    </td>
	<td class="text-left translate">
        <?=$acModel->YahooAction['ResultSet']['Result']['AuctionID']?>
    </td>
	<td class="text-left translate">
        <?=$acModel->YahooAction['ResultSet']['Result']['Title']?>
    </td>
    <td>
        <?php
        if (isset($acModel->YahooAction['ResultSet']['Result']['Price'])) { ?>
            <?=(isset($acModel->YahooAction['ResultSet']['Result']['TaxinPrice'])) ? $currencyModel->convert($acModel->YahooAction['ResultSet']['Result']['TaxinPrice'],'JPY','JPY') : $currencyModel->convert($acModel->YahooAction['ResultSet']['Result']['Price'],'JPY','JPY') ?>
            <?php
        }
        ?>
    </td>
    <td>
        <?=$acModel->YahooAction['ResultSet']['Result']['Bids']?>
    </td>
    <td>
        <?=Html::a($acModel->YahooAction['ResultSet']['Result']['Seller']['Id'],['yahoo-auctions/seller-list','sellerID'=>$acModel->YahooAction['ResultSet']['Result']['Seller']['Id']])?>
    </td>
    <td>
        <?php
        $now = new DateTime();
        $future_date = new DateTime($acModel->YahooAction['ResultSet']['Result']['EndTime']);
        $interval = $future_date->diff($now);
        echo $interval->format("%a days %h hours %i mins %s ses");?>
    </td>
    <td>
         <?php
			$msg = '';
			if (
					isset($acModel->YahooAction['ResultSet']['Result']['HighestBidders'])  &&
					isset($acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'])
			) {
				if (!Yii::$app->user->isGuest) {
					if (User::_isUserBidOnAuction($acModel->YahooAction['ResultSet']['Result']['AuctionID'])) {
						if ($acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'] == 'J*v*A***'  && User::_isUserTopBidder($acModel->YahooAction['ResultSet']['Result']['AuctionID']) ) {
							$msg = '<span style="color:green">'.LBL::_x('YOU_ARE_THE_HIGHEST_BIDDER').'</span>';
						} else {
							$msg = '<span style="color:red">'.LBL::_x('YOU_ARE_THE_OUT_BIDDER').'</span>';
						}
					}
				} else {
					echo $acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'];
				}
			}
			?>
		<?=$msg;?>
    </td>
    <td width="">
        <?=Html::a('<i class="fa fa-eye"></i>', ['yahoo-auctions/detail', 'id' => $model->code],['class'=>'btn btn-primary','target'=>'_blank']);?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?=Html::a('<i class="fa fa-trash"></i>', ['user/delete-fav', 'id' => $model->id],['onclick'=>'return confirm("'.\common\components\TBase::ShowLbl('DELETE_CONFIRMATION').'")','class'=>'btn btn-primary']);?>
    </td>
</tr>
<?php }else{
	$model->delete();
}


 } ?>