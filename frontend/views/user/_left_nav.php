<?php
use yii\helpers\Url;
use common\components\TBase as LBL;
?>
<style>
    .left-menu a { padding: 14px 0 10px 4px!important; } .left-menu{width: 133px!important;} .user-menu.nav > li > a:hover, .user-menu.nav > li > a:focus {color: inherit;text-decoration: none;font-weight: inherit!important;} .left-menu li.active a{font-weight: inherit!important;} ul.user-menu li a {font-size: 14px!important;}
</style>

<div class="left-menu">
    <?php $cntrl = Yii::$app->controller->id; $action = Yii::$app->controller->action->id; ?>
    <ul class="notranslate nav user-menu">
        <li class="<?=($cntrl == 'user' && $action == 'my-page') ? 'active' : ''; ?>" ><a href="<?=Url::to(['user/my-page'])?>"><i class="fa fa-user-circle-o"></i><?=LBL::_x('MY_PAGE');?></a></li>
        <li class="<?=($cntrl == 'user-auctions') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user-auctions/index'])?>"><i class="fa fa-gavel"></i><?=LBL::_x('USER_AUCTIONS');?></a></li>
        <li class="<?=($cntrl == 'user' && $action == 'yahoo-auction-fav-products') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user/yahoo-auction-fav-products'])?>"><i class="fa fa-heart"></i><?=LBL::_x('AUCTION_FAVORITE_PRODUCTS')?></a></li>
        <li class="<?=($cntrl == 'cart-products') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/cart-products/index'])?>"><i class="fa fa-shopping-bag"></i><?=LBL::_x('CART_PRODUCTS')?></a></li>
        <li class="<?=($cntrl == 'user' && $action == 'fav-products') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user/fav-products'])?>"><i class="fa fa-heart"></i><?=LBL::_x('FAVORITE_PRODUCT')?></a></li>
        <li class="<?=($cntrl == 'order') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/order/index'])?>"><i class="fa fa-bars"></i><?=LBL::_x('MY_ORDERS')?></a></li>
        <li class="<?=($cntrl == 'user' && ($action == 'funds'||$action == 'add-fund')) ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user/funds'])?>"><i class="fa fa-money"></i><?=LBL::_x('ADD_FUND')?></a></li>
        <li class="<?=($cntrl == 'packages' && ($action == 'index' || $action == 'order' || $action == 'shipping')) ? 'active' : ''; ?>" ><a href="<?=Url::to(['/packages/index'])?>"><i class="fa fa-suitcase"></i><?=LBL::_x('PACKAGES')?></a></li>
        <li class="<?=($cntrl == 'user-shipping-address' && $action == 'index') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user-shipping-address/index'])?>"><i class="fa fa-map-marker"></i><?=LBL::_x('NAVIGATION_SHIPPING_ADDRESS')?></a></li>
        <li class="<?=($cntrl == 'user' && $action == 'update') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user/update'])?>"><i class="fa fa-user-o"></i><?=LBL::_x('EDIT_PROFILE')?></a></li>
        <li class="<?=($cntrl == 'user' && $action == 'inbox') ? 'active' : ''; ?>" ><a href="<?=Url::to(['/user/inbox'])?>"><i class="fa fa-comments-o"></i><?=LBL::_x('MESSAGES')?></a></li>
    </ul>
</div>