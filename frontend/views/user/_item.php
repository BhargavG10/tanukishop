<?php
/**
 * Created by PhpStorm.
 * User: Narmail
 * Date: 7/29/2016
 * Time: 2:38 PM
 */

use yii\helpers\Html;
$tModel = new \common\components\TCurrencyConvertor;
$tanuki = new \common\helper\Tanuki;
$tanuki->singleProductDetail($model->code,$model->shop);
$price = $tanuki->getProductPrice($model->shop);
$img = $tanuki->getProductImage($model->shop);
$name =  $tanuki->getProductName($model->shop);
?>
<style>
    .icon-eye-open,.icon-trash {  font-size: 16px;  color: #000;}
    table td {
        padding: 3px !important;
        vertical-align: middle ! important;
    }

</style>
<tr class="txt-c">
    <td class="" style="width:13%!important;">
        <a style="color:#000;" href="<?=\yii\helpers\Url::to([$model->shop.'/detail', 'id' => $model->code],true)?>"><img src="<?=$img; ?>" height="60" width="60"></a>
    </td>
    <td style="width: 54%!important;" width="" class="translate" data-title="<?=$name; ?>" data-limit="50"><strong>
        <a style="color:#000;" href="<?=\yii\helpers\Url::to([$model->shop.'/detail', 'id' => $model->code],true)?>"><?=$name; ?></a></strong><br/>
    </td>
    <td width=""><?= ucfirst(str_replace('_',' ',$model->shop));?></td>
    <td width="">
        <?=Html::a('<i class="fa fa-eye"></i>', [$model->shop.'/detail', 'id' => $model->code],['class'=>'btn btn-primary','target'=>'_blank']);?>&nbsp;&nbsp;|&nbsp;&nbsp;
        <?=Html::a('<i class="fa fa-trash"></i>', ['user/delete-fav', 'id' => $model->id],['onclick'=>'return confirm("'.\common\components\TBase::ShowLbl('DELETE_CONFIRMATION').'")','class'=>'btn btn-primary']);?>
    </td>
</tr>