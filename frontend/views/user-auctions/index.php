<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\widgets\ListView;
use yii\widgets\Pjax;

use common\components\TBase as LBL;

$this->title = Yii::t('app', 'User Auctions');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table tr th{text-align: center;} table tr td:nth-child(2){    width: 9%;text-align: center;} table tr td:nth-child(3){width: 30%;} table tr td:nth-child(4){width: 80px;} table tr td:nth-child(5){width: 5%;}
    table tr td:nth-child(6){width: 11%;} table tr td:nth-child(7){width: 12%;}
    .table tbody td:first-child {
        width: 8%! important;
       padding: 0px! important;
       margin: 0px! important;}
    .table tbody td:last-child{width: 5%;  vertical-align: inherit;} .grid-view td{white-space: inherit; } .summary{    text-align: right;}
    .grid-view .fa {font-size: 14px;padding: 0px!important;background: none!important;} .btn-primary{border-radius: 0px!important;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=LBL::_x('MY_AUCTION');?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <h3 class="inner-head"><?=LBL::_x('MY_AUCTION')?></h3>
            <div class="table-responsive text-center">

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table min-w700 table-bordered table-striped sort-numerical">
                    <tr>
                        <th class="txt-c" style="text-align: center;" ><?=LBL::_x('IMAGE');?></th>
                        <th class="txt-c" style="text-align: center;" ><?=LBL::_x('AUCTION_ID');?></th>
                        <th class="txt-c text-left" style="width: 20%;text-align: left;" ><?=LBL::_x('AUCTION_TITLE');?></th>
                        <th class="txt-c"><?=LBL::_x('CURRENT_PRICE');?></th>
                        <th class="txt-c"><?=LBL::_x('NUMBER_OF_BIDS')?></th>
                        <th class="txt-c"><?=LBL::_x('SELLER');?></th>
                        <th class="txt-c" style="text-align: center;" >
                            <?php if (isset($_REQUEST['sort']) && $_REQUEST['sort'] == '-auction_ends') { ?>
                                <?=Html::a(LBL::_x('TIME_LEFT'),['user-auctions/index?sort=auction_ends'],['class'=>'asc'])?>
                            <?php } else { ?>
	                            <?=Html::a(LBL::_x('TIME_LEFT'),['user-auctions/index?sort=-auction_ends'],['class'=>'desc'])?>
                            <?php }?>
                        </th>
                        <th class="txt-c"><?=LBL::_x('STATUS');?></th>
                        <th class="txt-c" style="text-align: left;" ><?=LBL::_x('VIEW')?></th>
                    </tr>
                    <?php
                    if ($dataProvider->getCount()) {
                        echo ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => '_item',
                        ]);
                    } else {
                        ?>
                        <tr>
                            <td align="center" colspan="9">
                                <?=LBL::_x('NO_PRODUCT_FOUND')?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</section>
