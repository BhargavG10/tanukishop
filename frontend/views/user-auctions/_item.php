<?php

use yii\helpers\Html;
use \common\models\User;
use \common\components\TBase;
$acModel = new \common\models\YahooAuctions();
$acModel->productsDetail($model->auction_id,'small');

if (
        isset($acModel->YahooAction['ResultSet']['Result']['Title']) &&
        $acModel->YahooAction['ResultSet']['Result']['Status'] != 'closed'
) {
?>
    <style> .icon-eye-open,.icon-trash {  font-size: 16px;  color: #000;} table td { padding: 3px !important; vertical-align: middle ! important; } </style>
    <tr class="txt-c">
        <td class="text-center translate">
            <?=Html::a($acModel->getImage(), ['yahoo-auctions/detail', 'id' => $model->auction_id]);?>
        </td>
        <td class="text-left translate">
            <?=Html::a($acModel->YahooAction['ResultSet']['Result']['AuctionID'], ['yahoo-auctions/detail', 'id' => $model->auction_id]);?>
        </td>
        <td class="text-left translate"><?=$acModel->YahooAction['ResultSet']['Result']['Title']?></td>
        <td><?=$acModel->getPrice(); ?></td>
        <td><?=$acModel->YahooAction['ResultSet']['Result']['Bids']?></td>
        <td><?=Html::a($acModel->YahooAction['ResultSet']['Result']['Seller']['Id'],['yahoo-auctions/seller-list','sellerID'=>$acModel->YahooAction['ResultSet']['Result']['Seller']['Id']],['target'=>'_blank'])?></td>
        <td class="id_<?=$model->auction_id?>"><?=$model->getTimeLeft(); ?></td>
        <td><b>
                <span id="currentWinner" class="notranslate">
                    <?php
                    $msg = '';
                    if (
                        isset($acModel->YahooAction['ResultSet']['Result']['HighestBidders'])  &&
                        isset($acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'])
                    ) {
                        if (!Yii::$app->user->isGuest) {
                            if (User::_isUserBidOnAuction($acModel->YahooAction['ResultSet']['Result']['AuctionID'])) {
                                if ($acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'] == 'J*v*A***'  && User::_isUserTopBidder($acModel->YahooAction['ResultSet']['Result']['AuctionID']) ) {
                                    $msg = '<span style="color:green">'.TBase::_x('YOU_ARE_THE_HIGHEST_BIDDER').'</span>';
                                } else {
                                    $msg = '<span style="color:red">'.TBase::_x('YOU_ARE_THE_OUT_BIDDER').'</span>';
                                }
                            }
                        } else {
                            echo $acModel->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'];
                        }
                    }
                    ?>
                </span>
		        <?=$msg;?>
                <br/></td>
        <td width="">
            <?=Html::a('<i class="fa fa-eye"></i>', ['yahoo-auctions/detail', 'id' => $model->auction_id],['class'=>'btn btn-primary','target'=>'_blank']);?>
        </td>
    </tr>
    <?php
} else {
    $model->softDelete();
}

//$date = new \DateTime($model->auction_ends);
//
//$js = '
//jQuery(".id_'.$model->auction_id.'").countdown("'.$date->format("Y/m/d H:i:s").'")
// .on("update.countdown", function(event) {
//    jQuery(this).text( event.strftime("%Dd: %Hh: %Mm: %Ss") );
//  })
//.on("finish.countdown", function(event) {
//  location.reload();
//});';
//$this->registerJs($js);

?>

