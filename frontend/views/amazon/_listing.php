<?php
use common\components\TBase;
$model = new \common\components\TCurrencyConvertor;
$this->title = \common\components\TBase::ShowLbl('RAKUTEN_PRODUCT_LIST');

$cnt = 0;
if (isset($data->Items->Item) && count($data->Items->Item)>0) {
    $j = 1;
    foreach ($data->Items->Item as $key => $item) {
            //echo ($j == 1) ? '<div class="row '.$j.'">' : '';

            $json = json_encode($item);
            $detail = json_decode($json,TRUE);

            if (isset($detail['VariationSummary']['LowestPrice']['Amount']))
            {
                $price = $detail['VariationSummary']['LowestPrice']['Amount'];
            } else if (isset($detail['OfferSummary']['LowestNewPrice']['Amount'])) {
                $price = $detail['OfferSummary']['LowestNewPrice']['Amount'];
            }
            $code = $detail['ASIN'];
            $title = $detail['ItemAttributes']['Title'];
            ?>

            <div class=" row col-sm-3">
                <div class="product-lst position-relative">
                    <?php if ($category) { ?>
                        <a target="_blank" href="<?= \yii\helpers\Url::to(['amazon/detail','id'=>$code])?>">
                        <?php } else { ?>
                        <a target="_blank" href="<?= \yii\helpers\Url::to(['amazon/detail','id'=>$code])?>">
                            <?php } ?>
                            <div style="height: 128px;overflow: hidden;">
                                <?php if ((isset($detail['MediumImage']['URL']))){ ?>
                                    <img src="<?=$detail['MediumImage']['URL']?>" alt="product">
                                <?php } else { ?>
                                    <img src="<?='/tnk/images/no_image.gif'; ?>" alt="product" style="    width: 70%;">
                                <?php } ?>
                            </div>
                            <div class="product-list-product translate" id="product_title_<?=$cnt;?>" data-title="<?=$title; ?>"><?=$title; ?></div>
                        </a>

                        <h6 class="notranslate">
                            <a class="amount" href="javascript:void(0)"
                               data-usd="<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>"
                               data-rub="<?=\common\models\CurrencyTable::convert($price,'JPY','RUB'); ?>"
                               data-jpy="<?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?>"
                               data-cny="<?=\common\models\CurrencyTable::convert($price,'JPY','CNY'); ?>"
                               data-eur="<?=\common\models\CurrencyTable::convert($price,'JPY','EUR'); ?>"
                            >
                                <?php
                                if (isset($_REQUEST['lang'])) {
                                    if ($_REQUEST['lang'] == 'jpy') {
                                        echo \common\models\CurrencyTable::convert($price,'JPY','JPY');
                                    } else if ($_REQUEST['lang'] == 'usd') {
                                        echo \common\models\CurrencyTable::convert($price,'JPY','USD');
                                    } else if ($_REQUEST['lang'] == 'cny') {
                                        echo \common\models\CurrencyTable::convert($price,'JPY','CNY');
                                    } else if ($_REQUEST['lang'] == 'eur') {
                                        echo \common\models\CurrencyTable::convert($price,'JPY','EUR');
                                    } else {
                                        echo \common\models\CurrencyTable::convert($price,'JPY','RUB');
                                    }
                                }
                                ?>
                            </a>
                        </h6>

                        <div class="cate-list product-icon-listing">
                            <div class="clearfix">
                                <?php
                                if ($category) { ?>
                                    <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['amazon/detail','id'=>$code])?>"></a></div>
                                <?php } else {
                                    ?>
                                    <div class="pull-left" ><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['amazon/detail','id'=>$code])?>"></a></li>
                                <?php } ?>
                                    <div class="pull-left"><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'amazon','code'=>$code])?>"></a></li>
                                    <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'amazon','code'=>$code],true)?>"></a></li>
                            </div>
                        </div>
                </div>
            </div>
        <?php
        //echo (($j % 4 == 0)) ? '</div><div class="row ' . $j . '">' : '';
        $j++;
        $cnt++;
    }
} else {
    echo "0";
    exit;
}
?>

