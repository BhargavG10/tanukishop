<?php
use yii\widgets\LinkPager;
use common\components\TBase;
use common\components\TBase as LBL;
$model = new \common\components\TCurrencyConvertor;
?>


<div class="m_container rakuten margin-bottom-20 amazon notranslate">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a href="<?=\yii\helpers\Url::to(['amazion/index'])?>"><?=TBase::ShowLbl('amazon')?></a></li>
            <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                <li><a href="#"><?=$_REQUEST['s']; ?> </a></li>
            <?php } else { ?>
                <li><a href="#"><?=$category->title_en; ?> </a></li>
            <?php } ?>
        </ol>
    </div>
</div>

<div class="container amazon">
    <!-- listing page -->
    <section class="listing-panel">
        <div class="container" style="position: relative;">
            <div class="row">
                <div class="col-md-2 col-sm-4 left-bar notranslate">
                    <div class="well">
                        <?php echo $this->render('_left',['category'=>$category,'param'=>$param]); ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-8 product-list-page">
                    <div class="well">
                        <div class="row notranslate">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <ul class="filters">
                                        <li><?=TBase::ShowLbl('SORT_BY');?>:</li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='lth') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'lth'])); ?>"><?=TBase::ShowLbl('LOW_TO_HIGH')?></a></li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='htl') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'htl'])); ?>"><?=TBase::ShowLbl('HIGH_TO_LOW')?></a></li>
                                    </ul>
                                </div>
                                <div class="pull-left text-enter">
                                    <ul class="filters currency-filters">
                                        <li class="text-center"><?=TBase::ShowLbl('TOTAL_RESULT');?>&nbsp;&nbsp;<label style="color: #782f4a;"><?=(isset($data->Items->TotalResults)) ? $data->Items->TotalResults : 0; ?></label></li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <ul class="filters currency-filters">
                                        <li><?=TBase::ShowLbl('CURRENCY');?>:</li>
                                        <li><a class="current" id="currency-jpy" data-currency="jpy"><?=TBase::ShowLbl('JAPANESE_YEN');?></a></li>
                                        <li><a id="currency-usd"  data-currency = "usd"><?=TBase::ShowLbl('US_DOLLAR');?></a></li>
                                        <li><a id="currency-rub"  data-currency = "rub"><?=TBase::ShowLbl('RUSSIAN_RUBLE');?></a></li>
                                        <li><a id="currency-cny"  data-currency = "cny"><?=TBase::ShowLbl('CHINESE_YUAN');?></a></li>
                                        <li><a id="currency-eur"  data-currency = "eur"><?=TBase::ShowLbl('EURO');?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="top_p list-brand">
                        <?php
                        if (!$data->Error) {
                            $cnt = 0;
                            if (isset($data->Items->Item) && count($data->Items->Item) > 0) {
                            $j = 1;
                            foreach ($data->Items->Item as $key => $item) {
                                //echo ($j == 1) ? '<div class="row '.$j.'">' : '';

                                $json = json_encode($item);
                                $detail = json_decode($json, TRUE);

                                if (isset($detail['VariationSummary']['LowestPrice']['Amount'])) {
                                    $price = $detail['VariationSummary']['LowestPrice']['Amount'];
                                } else if (isset($detail['OfferSummary']['LowestNewPrice']['Amount'])) {
                                    $price = $detail['OfferSummary']['LowestNewPrice']['Amount'];
                                } else {
                                    $price = 0;
                                }
                                $code = $detail['ASIN'];
                                $title = $detail['ItemAttributes']['Title'];
                                ?>

                                <div class=" row col-sm-3">
                                    <div class="product-lst position-relative">
                                        <?php if ($category) { ?>
                                            <a target="_blank" href="<?= \yii\helpers\Url::to(['amazon/detail', 'id' => $code]) ?>">
                                            <?php } else { ?>
                                            <a target="_blank" href="<?= \yii\helpers\Url::to(['amazon/detail', 'id' => $code]) ?>">
                                                <?php } ?>
                                                <div style="height: 128px;overflow: hidden;">
                                                    <?php if ((isset($detail['MediumImage']['URL']))) { ?>
                                                        <img src="<?= $detail['MediumImage']['URL'] ?>" alt="product">
                                                    <?php } else { ?>
                                                        <img src="<?= '/tnk/images/no_image.gif'; ?>" alt="product"
                                                             style="    width: 70%;">
                                                    <?php } ?>
                                                </div>
                                                <div class="product-list-product translate" id="product_title_<?= $cnt; ?>"
                                                     data-title="<?= $title; ?>"><?= $title; ?></div>
                                            </a>

                                            <h6 class="notranslate">
                                                <a class="amount" href="javascript:void(0)"
                                                   data-usd="<?= \common\models\CurrencyTable::convert($price, 'JPY', 'USD'); ?>"
                                                   data-rub="<?= \common\models\CurrencyTable::convert($price, 'JPY', 'RUB'); ?>"
                                                   data-jpy="<?= \common\models\CurrencyTable::convert($price, 'JPY', 'JPY'); ?>"
                                                   data-cny="<?= \common\models\CurrencyTable::convert($price, 'JPY', 'CNY'); ?>"
                                                   data-eur="<?= \common\models\CurrencyTable::convert($price, 'JPY', 'EUR'); ?>">
                                                    <?= \common\models\CurrencyTable::convert($price, 'JPY', 'JPY'); ?>
                                                </a>
                                            </h6>

                                            <div class="cate-list product-icon-listing">
                                                <div class="clearfix">
                                                    <?php
                                                    if ($category) { ?>
                                                        <div class=" pull-left"><a target="_blank" class="detail-i"
                                                                                   href="<?= \yii\helpers\Url::to(['amazon/detail', 'id' => $code]) ?>"></a>
                                                        </div>
                                                    <?php } else {
                                                        ?>
                                                        <div class=" pull-left"><a target="_blank" class="detail-i"
                                                                                   href="<?= \yii\helpers\Url::to(['amazon/detail', 'id' => $code]) ?>"></a>
                                                        </div>
                                                    <?php } ?>
                                                    <div class=" pull-left"><a class="cart-i"
                                                                               href="<?= \yii\helpers\Url::to(['order/add-to-cart', 'sh' => 'amazon', 'code' => $code]) ?>"></a>
                                                    </div>
                                                    <div class="pull-left no-border"><a class="wish-i"
                                                                                        data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create', 'shop' => 'amazon', 'code' => $code],true) ?>"></a>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <?php
                                //echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
                                $j++;
                                $cnt++;
                            } ?>
                            </div>
                            <?php
                            } else { ?>
                                <div class='text-center'><?=TBase::ShowLbl('NO_PRODUCT_FOUND')?></div>
                            <?php }
                        } else { ?>
                                <span class="notranslate"><?=TBase::ShowLbl('AMAZON_SERVER_OVERLOAD')?></span>
                                <!--Array ( [Code] => RequestThrottled [Message] => AWS Access Key ID: AKIAJRSMQKJ6EYZAXPPA. You are submitting requests too quickly. Please retry your requests at a slower rate. )-->
                                <?php
                            }
                        ?>
                    </div>
                    <hr>
                    <?php /*if (isset($data->Items->TotalResults) && (int)json_decode($data->Items->TotalResults)>15) { ?>
                        <div class="row">
                            <div class="col-md-12 notranslate">
                                <div class="text-center">
                                    <a href="#" class="product-load-more" data-shop="amazon"> << open more products >> </a>
                                </div>
                            </div>
                        </div>
                    <?php } */ ?>
                    <div class="row">
                        <div class="col-md-12 notranslate">
                            <div class="text-center">
                    <?php
                    $pagination = new yii\data\Pagination(['totalCount' => $data->Items->TotalResults, 'pageSize'=>10]);

                    echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pagination,
                    ]);
                    ?>

                            </div>
                        </div>
                    </div>

	                <?php if ($category) { ?>
                        <div class="seo-txt">
			                <?=$category->seo_text; ?>
                        </div>
	                <?php } ?>
                </div>
            </div>
            <!-- slide left side bar div -->
            <div class="col-md-2 scroll-left-side-bar" style="position: absolute; display: none;">
                <div class="well">
                    <a id="back-to-top" style="margin-top: 10px;width: 90%;" href="" class="btn filter-btn"><?=LBL::_x('GO_TO_FILTER_AREA'); ?></a></div>
            </div>
            <!-- slide left side bar div -->
        </div>
</div>
</section>
</div>

<?=$this->registerJS('
var ajax_url = "'.\yii\helpers\Url::to(['amazon/list']).'";
',yii\web\View::POS_BEGIN);


$this->registerCss("
div.cate-list > ul li{    width: 63px!important;}
");
?>
<div class="loading-extend" style="display: none;"></div>
<div class="loading-image" style="display: none;"><?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?></div>

