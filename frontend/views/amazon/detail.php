<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;

?>
<style>
    .tab_link{color: #fff !important;text-decoration: none;}
</style>
<div class='row'>
</div>
<?php
$language = ['JPY'=>'JPY','USD'=>'USD','RUB'=>'RUB'];
$category = '';
$model = new \common\components\TCurrencyConvertor;
if (!isset($data['Error']['Code'])) {
    $name = $data['Items']['Item']['ItemAttributes']['Title'];
	$this->title = $name;
    $code = $data['Items']['Item']['ASIN'];
    $review = 0;
    if (isset($data['Items']['Item']['VariationSummary']['LowestPrice']['Amount']))
    {
        $price = $data['Items']['Item']['VariationSummary']['LowestPrice']['Amount'];
    } else if (isset($data['Items']['Item']['OfferSummary']['LowestNewPrice']['Amount'])) {
        $price = $data['Items']['Item']['OfferSummary']['LowestNewPrice']['Amount'];
    } else {
        $price = 0;
    }
    $shopCode = $data['Items']['Item']['ASIN'];
    $affiliateUrl = $shopCode;
    $itemCaption = '';
    if (isset($data['Items']['Item']['ItemAttributes']['Feature'])) {

        if (is_array($data['Items']['Item']['ItemAttributes']['Feature'])) {
            $itemCaption = strip_tags(implode(',', $data['Items']['Item']['ItemAttributes']['Feature']));
        } else {
            $itemCaption = $data['Items']['Item']['ItemAttributes']['Feature'];
        }
    }
    if (isset($data['Items']['Item']['EditorialReviews']['EditorialReview']['Content'])) {
        $itemCaption .= $data['Items']['Item']['EditorialReviews']['EditorialReview']['Content'];
    }
    $this->title = \common\components\TBase::ShowLbl('RAKUTEN_PRODUCT_DETAIL');
    $countries = \yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(), 'id', 'title');

    ?>
    <div class="m_container rakuten margin-bottom-20 amazon notranslate">
        <div class="container ">
            <ol class="breadcrumb">
                <li><a href="<?=\yii\helpers\Url::to(['amazon/index']) ?>"><?=\common\components\TBase::ShowLbl('amazon')?></a></li>
                <?php if ($category) { ?>
                    <li> <a href="<?=\yii\helpers\Url::to(['amazon/list','title'=>$category->slug]) ?>"><?php
                            if (\common\components\Language::CLang() == 'en-US') {
                                echo $category->title_en;
                            } else {
                                echo $category->title_ru;
                            }
                            ?></a></li>
                <?php } ?>
            </ol>
        </div>
    </div>
    <section class="product-page amazon">
        <div class="container">
            <div class="product-plug">
                <div class="row">
                    <div class="col-md-5 notranslate">
                        <div class="row product-bx">
                            <div class="col-md-12 col-sm-12 bx-slid  wow fadeInUp white-bg bxsliderWrapper">
                                <ul id="bxslider-de">
                                    <?php

                                    if (isset($data['Items']['Item']['ImageSets']['ImageSet'])) {
                                        foreach ($data['Items']['Item']['ImageSets']['ImageSet'] as $img) {
                                            if (isset($img['LargeImage'])) {
                                                ?>
                                                <li>
                                                    <a href="<?= $img['LargeImage']['URL']?>" data-lightbox="example-set"><img src="<?= $img['LargeImage']['URL']; ?>"></a>
                                                </li>
                                            <?php }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-12  wow fadeInUp">
                                <div class="bxpager-slid">
                                    <ul id="bxslider-pager">
                                        <?php
                                        $i = 0;
                                        foreach ($data['Items']['Item']['ImageSets']['ImageSet'] as $item) {
                                            if (isset($item['LargeImage'])) {
                                                ?>
                                                <li data-slideIndex="<?= $i ?>">
                                                    <a><img src="<?= $item['LargeImage']['URL'] ?>" height="110"></a>
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <h2 data-title="<?=Yii::t('app',$name);?>" class="product_title translate" data-short="0"><?=Yii::t('app',$name);?></h2>
                        <ul class="smartpay notranslate  margin-top-25">
                            <li><a href="javascript:void(0)"><?=Yii::t('app',$code);?></a></li>
                            <li>
                                <div id="rate1_rateblock" title="Rated 4.29 out of 5" class="star-rating notranslate">
                                    <span id="rate1_stars" style="width:<?=(80*($review/5))?>px;" class="notranslate">
                                </div>
                            </li>
                        </ul>
                        <hr>
                        <form id="add-to-cart" action="/order/add-to-cart" method="GET">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
                            <ul class="select-box notranslate margin-top-25">
                                <?php if ($price) { ?>
                                <li style="width:41%;">
                                    <p style="font-size: 24px;">
                                        <?=\common\components\TBase::ShowLbl('PRICE')?>: <a style="margin-top: 7px;" class="amount" href="javascript:void(0)" data-usd="<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>" data-rub="<?=\common\models\CurrencyTable::convert($price,'JPY','RUB'); ?>" data-jpy="<?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?>"><?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?></a>
                                        <span style="font-size:15px; ">(~<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>)</span>
                                    </p>
                                </li>
                                <li style="width:10%;">
                                    <select class="form-control" name="quantity" id="quantity_class">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    <input type="hidden" name="code" value="<?=$code?>">
                                    <input type="hidden" name="sh" value="amazon">
                                </li>
                                    <li style="width:25%;">
                                        <?php if (Yii::$app->request->get('cart_id')) { ?>
                                            <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                                <?=Tbase::ShowLbl('UPDATE_TO_CART'); ?>
                                            </button>
                                            <input type="hidden" name="cart_id" value="<?=Yii::$app->request->get('cart_id')?>">
                                        <?php } else { ?>
                                            <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                                <?=Tbase::ShowLbl('ADD_TO_CART'); ?>
                                            </button>
                                        <?php } ?>
                                    </li>
                                <?php } else { ?>
                                    <li><?=\common\components\TBase::ShowLbl('OUT_OF_STOCK'); ?></li>
                                <?php } ?>
                                <li><h5><a href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'amazon','code'=>$shopCode])?>"><img src="/tnk/images/wishlist.png" alt="wishlist" /><?=\common\components\TBase::ShowLbl('ADD_TO_WISHLIST')?></a></h5></li>
                            </ul>
                        </form>
                        <hr>
                        <ul class="seller-ul notranslate">
                            <li class="width-22-percent">
                                <h5><?=\common\components\TBase::ShowLbl('SELLER_ITEMS'); ?></h5>
                                <h4><?=Yii::t('app',$shopCode);?></h4>
                            </li>
                            <li class="width-25-percent"><h6><a target="_blank" href="https://www.amazon.co.jp/dp/<?=$affiliateUrl?>"><?=\common\components\TBase::ShowLbl('THIS_ITEM_PAGE_ON_AMAZON'); ?></a></h6></li>
                            <?php if (Yii::$app->user->isGuest) { ?>
                                <li class="width-32-percent"><textarea class="form-control" placeholder="<?=TBase::ShowLbl('PLEASE_LOGIN_TO_SEND_MESSAGE')?>"></textarea></li>
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>">
                                        <button class="btn btn-post">
                                            <?=TBase::ShowLbl('LOGIN')?>
                                        </button>
                                    </a>
                                </li>
                            <?php } else { ?>
                                <?=Html::hiddenInput('mail-path',\yii\helpers\Url::to(['site/send-mail']),['id'=>'mail-path'])?>
                                <li class="width-32-percent"><textarea class="form-control" id="msg" placeholder="<?=\common\components\TBase::ShowLbl('COMMENTS'); ?>"></textarea></li>
                                <li><button id="send-mail" class="btn btn-post"><?=\common\components\TBase::ShowLbl('POST');?></button></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-7 how-much1 position-relative notranslate">
                        <div class="how-much">
                            <div class="absolute"><?=Tbase::ShowLbl('PLEASE_WAIT');?></div>
                            <h4><?=Tbase::ShowLbl('HOW_MUCH_WILL_IT_COST'); ?></h4>
                            <input type="hidden" name="quantity" class="quantity_class" id="quantity_class" value="1">
                            <form class="shipping_calculator" id="shipping_calculator">
                                <ul class="select-box">
                                    <li><input type="text" class="form-control form-control1" name="package_weight" id="package_weight" placeholder="<?=Tbase::ShowLbl('PACKAGE_WEIGHT'); ?>" /></li>
                                    <li><?php echo Html::dropDownList('country_id', null, $countries,['class'=>'form-control','prompt'=>Tbase::ShowLbl('SHIP_TO')]) ; ?></li>
                                    <li><?php echo Html::dropDownList('language_id', 'jpy', $language,['class'=>'form-control','prompt'=>Tbase::ShowLbl('CURRENCY')]) ; ?></li>
                                    <li> <button class="btn btn-cart1" id="calculate-shipping"><i class="fa fa-search" aria-hidden="true"></i></button></li>
                                </ul>
                                <input type="hidden" name="price" id="price" value="<?=$price?>">
                                <input type="hidden" name="tanuki_charge" id="tanuki_charge" value="300">
                            </form>
                            <p><?=Tbase::ShowLbl('THE_LIMIT_OF_WEIGHT_OF_PACKAGE_IS_30KG');?></p>
                            <div class="no-shipping-msg" style="display: none;color:#df7b2b;">
                            </div>
                            <ul class="item-price hidden" id="item-shipping-calculation">
                                <li><strong><?=Tbase::ShowLbl('PRICE'); ?>:</strong></span></li>
                                <li><strong><?=Tbase::ShowLbl('TANUKI_FEE'); ?>: </strong>¥ 300</li>
                                <li><strong><?=Tbase::ShowLbl('INTERNATIONAL_SHIPPING'); ?>: </strong> ¥<label class="shipping_charge"></label></li>
                                <li><a href="javascript:void(0)"><strong><?=Tbase::ShowLbl('TOTAL'); ?>: </strong>¥<label class="total_cost"></label> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-tabs">
                <div id="horizontalTab ">
                    <ul class="resp-tabs-list notranslate">
                        <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><?=\common\components\TBase::ShowLbl('DESCRIPTION');?></li>
                        <li class="resp-tab-item" aria-controls="tab_item-1" role="tab">
                            <?=Html::a(TBase::ShowLbl('APPROXIMATE_WEIGHT_OF_DIFFERENT_GOODS'),['site/page','slug'=>\common\models\Page::getSlug(8)],['class'=>'tab_link'])?>
                        </li>
                        <li class="resp-tab-item" aria-controls="tab_item-2" role="tab">
                            <?=Html::a(TBase::ShowLbl('JAPANESE_CLOTHING_AND_SHOE_SIZES'),['site/page','slug'=>\common\models\Page::getSlug(9)],['class'=>'tab_link'])?>
                        </li>
                    </ul>
                    <div class="resp-tabs-container">
                        <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><?=\common\components\TBase::ShowLbl('DESCRIPTION');?></h2>
                        <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0">
                            <p data-short="0" data-title="<?=Yii::t('app',$itemCaption);?>" class="product_description translate"><?=Yii::t('app',$itemCaption);?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="other-similer">
                <h3 class="heading_bg"><span><?=\common\components\TBase::ShowLbl('OTHER_SIMILAR_PRODUCTS');?></span></h3>
                <div class="row">
                    <?php
                    /*
                     * $data['Items']['Item']['BrowseNodes']['BrowseNode'][0]['BrowseNodeId']
                    $similar_products = $rModel->RakutenAPI(['genreId'=>$categoryID], 1, 4);
                    foreach ($similar_products['data']['data'] as $sproduct) {
                    if ($category) { ?>
                    <a class="no-hover" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$sproduct['Item']['itemCode'].'__'.$category->id])?>">
                        <?php } else { ?>
                        <a class="no-hover" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$sproduct['Item']['itemCode']])?>">
                            <?php } ?>

                            <div class="col-md-3 col-sm-6">
                                <div class="other-image">
                                    <?php $img = (isset($sproduct['Item']['mediumImageUrls'][0])) ? $sproduct['Item']['mediumImageUrls'][0]['imageUrl'] : (isset($sproduct['Item']['smallImageUrls'][0])) ? $sproduct['Item']['smallImageUrls'][0]['imageUrl'] : ''; ?>
                                    <img src="<?=substr($img,0,strpos($img,'?')); ?>" alt="product" height="122"/>
                                </div>
                                <div class="other-para"><p style="display: block;height: 83px;overflow: hidden;" class="translate" data-title="<?=$sproduct['Item']['itemName']; ?>"><?=$sproduct['Item']['itemName']; ?></p>
                                    <h6><a class="no-hover">¥<?=$sproduct['Item']['itemPrice']; ?></a></h6>
                                </div>
                            </div>
                        </a>
                        <?php
                        }*/
                        ?>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
<div class="m_container rakuten margin-bottom-20 amazon notranslate">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a href="<?=\yii\helpers\Url::to(['rakuten/index']) ?>"><?=\common\components\TBase::ShowLbl('rakuten')?></a></li>
            <?php if ($category) { ?>
                <li> <a href="<?=\yii\helpers\Url::to(['rakuten/list','title'=>$category->slug]) ?>"><?php
                        if (\common\components\Language::CLang() == 'en-US') {
                            echo $category->title_en;
                        } else {
                            echo $category->title_ru;
                        }
                        ?></a></li>
            <?php } ?>
        </ol>

    </div>
</div>
<section class="product-page">
    <div class="container">
        <div class="product-plug">
            <div class="row">
                <span class="notranslate"><?=TBase::ShowLbl('AMAZON_SERVER_OVERLOAD')?></span>
                <!--Array ( [Code] => RequestThrottled [Message] => AWS Access Key ID: AKIAJRSMQKJ6EYZAXPPA. You are submitting requests too quickly. Please retry your requests at a slower rate. )-->
            </div>
        </div>
    </div>
</section>
<?php }?>
<?=$this->registerCss('
.form-control1{
margin-left:0px!important;
margin-bottom:0px!important;
}
a.amount{text-decoration:none;}

#quantity_class{
    color: #000;font-size: 17px;text-align: center;padding-left: 17px;
}
');?>
<?=$this->registerCssFile('@web/tnk/css/lightbox.min.css');?>
<?=$this->registerJsFile('@web/tnk/js/lightbox-plus-jquery.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/tnk/js/modernizr-2.6.2.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
