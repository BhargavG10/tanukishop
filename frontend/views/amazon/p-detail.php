<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class='row'>
<?php
foreach ($data['Items']['Item']['ItemAttributes'] as $key => $item){
    ?>
    <div class="row">
        <div class="col-lg-2"><strong><?php echo $key; ?></strong></div>
        <div class="col-lg-10"><?php if(is_array($item)) { print_r($item); } else { echo $item; } ?></div>
    </div>
<?php } ?>
</div>

<?php
$form = ActiveForm::begin([
    'id' => 'add-to-cart-form',
    'options' => [
        'class' => 'form-horizontal',
    ],
    'method' => 'GET',
    'action' => \yii\helpers\Url::to(['order/add-to-cart']),
]) ?>
    <input type="hidden" name="sh" value="amazon">
    <input type="text" name="quantity" value="1">
    <input type="hidden" name="name" value="<?php echo $data['Items']['Item']['ItemAttributes']['Title'] ?>">
    <input type="hidden" name="code" value="<?php echo $id; ?>">
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Add to cart', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end();

?>