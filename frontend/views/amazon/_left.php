<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<style>
    button.accordion {
        background: none;
        color: #444;
        cursor: pointer;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
        padding:7px 4px;
    }

    button.accordion.active, button.accordion:hover {
        background-color: #ddd;
    }

    button.accordion:after {
        content: '\002B';
        color: #777;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    button.accordion.active:after {
        content: "\2212";
    }

    div.panel {
        padding: 0 6px 0px 10px;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
</style>

<h4 class="notranslate">
    <?=\common\components\TBase::ShowLbl('CATEGORIES');?>
</h4>
<div class="panel-group left-side-bar notranslate" id="accordion">
    <label>
        <?php
        if ($category) {
            echo (\common\components\TBase::CLang() == 'ru-RU') ? $category->title_ru : $category->title_en;
        }
        ?>
    </label>
<hr>
    <?php
    if ($category) {
        $data = \common\models\Category::findAll(['parent_id' => $category->id,'shop'=>'yahoo']);
        if ($data) {
            foreach ($data as $cat) { ?>
                <button class="accordion"><?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?></button>
                <div class="panel">
                    <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'id' => $cat->ref_id]) ?>">
                        <?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?> (All)
                    </a>
                    <?php
                    $child = \common\models\Category::findAll(['parent_id' => $cat->id,'shop'=>'yahoo']); ?>
                    <?php foreach ($child as $childCat) { ?>

                        <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'id' => $childCat->ref_id]) ?>">
                            <?= (\common\components\TBase::CLang() == 'ru-RU') ? $childCat->title_ru : $childCat->title_en; ?>
                        </a>

                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        } else { ?>
            <a>
                <?= (\common\components\TBase::CLang() == 'ru-RU') ? $category->title_ru : $category->title_en; ?> (All)
            </a>
        <?php }
    } else {
        $data = \common\models\Category::findAll(['shop'=>'amazon','parent_id'=>0]);
        if ($data) {
            foreach ($data as $cat) { ?>
                <button class="accordion"><?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?></button>
                <div class="panel">
                    <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'id' => $cat->ref_id]) ?>">
                        <?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?> (All)
                    </a>
                    <?php
                    $child = \common\models\Category::findAll(['parent_id' => $cat->id,'shop'=>'amazon']); ?>
                    <?php foreach ($child as $childCat) { ?>
                        <div>
                            <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'id' => $childCat->ref_id]) ?>">
                                <?= (\common\components\TBase::CLang() == 'ru-RU') ? $childCat->title_ru : $childCat->title_en; ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
    }
    ?>
</div>

<h4 class="notranslate"><?=\common\components\TBase::ShowLbl('FILTER_BY_PRICE')?></h4>
<hr>
<div class="row min-max notranslate">
    <?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl($param),'method' => 'GET']) ?>
    <input type="hidden" name="page" id="page" value="1">
    <div class="col-lg-12 right-pad-zero margin-bottom-7">
        <div class="col-xs-2 right-pad-zero ">
            <?=\common\components\TBase::ShowLbl('FROM')?>:
        </div>
        <div class="col-xs-10 right-pad-zero ">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmin']) && $_REQUEST['pmin']!= '') ? $_REQUEST['pmin'] : ''; ?>" name="pmin" placeholder="Min">
        </div>
    </div>
    <div class="col-lg-12 right-pad-zero margin-bottom-7">
        <div class="col-xs-2 right-pad-zero ">
            <?=\common\components\TBase::ShowLbl('TO')?>:
        </div>
        <div class="col-xs-10 right-pad-zero ">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmax']) && $_REQUEST['pmax']!= '') ? $_REQUEST['pmax'] : ''; ?>" name="pmax" placeholder="Max">
        </div>
    </div>
    <input type="submit" name="search" value="<?=\common\components\TBase::ShowLbl('FILTER')?>" class="btn filter-btn">
    <?php ActiveForm::end() ?>
</div>
<div class="clearfix"></div>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
</script>