<?php
use yii\helpers\Html;
$lang = \common\components\TBase::CLang();
$this->title = \common\components\TBase::ShowLbl('amazon');
?>

<!-- listing page -->
<div class="m_container rakuten amazon notranslate">
    <div class="container"><?=\common\components\TBase::ShowLbl('amazon'); ?></div>
</div>
<div class="container amazon-cat notranslate">
    <div class="row marg-l0">

        <?php
        $j = 1;
        foreach ($categories as $cat) {
            if ($cat->parent_id == 0) {
                echo ($j == 1 || ($j % 4 == 0)) ? '<div class="row margin-bottom-20">' : '';
                ?>

                <div class="col-md-3 col-sm-3 cat-list <?php echo $j.'_'.($j % 4); ?>">
                    <p>
                        <?php $title = ($lang == 'en-US') ? $cat->title_en : $cat->title_ru;
                        echo Html::a($title,['amazon/list','id'=>$cat->ref_id],['style'=>'color:#fff;']);?>
                    </p>
                    <ul>
                        <?php
                        foreach ($categories as $catSub) {
                            if ($cat->id == $catSub->parent_id) {

                                if ($lang == 'en-US') {
                                    $title = $catSub->title_en;
                                } else {
                                    $title = $catSub->title_ru;
                                }
                                echo '<li>'.Html::a($title,['amazon/list','id'=>$catSub->ref_id]).'</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <?php
                echo ($j %4 == 0 ) ? '</div>' : '';
                $j++;
            }
            }
        ?>

        <div class="clearfix"></div>
    </div>
</div>
<!-- listing page -->
