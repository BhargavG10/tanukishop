<?php
use yii\widgets\LinkPager;
//$model = new \common\components\TCurrencyConvertor;
?>
<?php //echo \common\models\CurrencyTable::convert((1490,'JPY','USD'); ?>
<?php //echo \common\models\CurrencyTable::convert((1490,'JPY','CNY'); ?>


<!--<div class="container">
    <ol class="breadcrumb">
        <li><a href="#">Rakuten</a></li>
        <li><a href="#"><?/*=$category->title_en; */?> </a></li>
    </ol>
</div>-->
<div class="m_container rakuten margin-bottom-20 amazon">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a href="#">Amazon</a></li>
            <li><a href="#"><?=$category->title_en; ?> </a></li>
            <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                <li><a href="#"><?=$_REQUEST['s']; ?> </a></li>
            <?php } ?>
        </ol>

    </div>
</div>
<div class="container">
<!-- listing page -->
    <section class="listing-panel">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-4">
                <div class="well">
                    <?php echo $this->render('_left-bar',['category'=>$category]); ?>
                </div>
            </div>
            <div class="col-md-10 col-sm-8 product-list-page">
                <div class="well">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <ul class="filters">
                                    <li>Sort by:</li>

                                    <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='lth') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'lth'])); ?>">Low to high</a></li>
                                    <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='htl') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'htl'])); ?>">high to low</a></li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <ul class="filters">
                                    <li>Currency:</li>
                                    <li><a href="javascript:void(0)">Japanese yen</a></li>
                                    <li><a href="javascript:void(0)">US Dollar</a></li>
                                    <li><a href="javascript:void(0)">Chinese yuan</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <ul class="top_p list-brand">
                        <?php
                        if (isset($data->Items->Item) && count($data->Items->Item)>0) {
                            foreach ($data->Items->Item as $key => $item){
                                ?>
                                <li>
                                    <div class="product-lst">
                                        <a href="<?= \yii\helpers\Url::to(['product-detail','id'=>$item->ASIN])?>">
                                            <img width= "93" height="" src="<?=$item->MediumImage->URL; ?>" alt="product">
                                            <p><?=substr($item->ItemAttributes->Title,0,50); ?></p>
                                        </a>
                                        <h6><a href="javascript:void(0)"><?=$item->OfferSummary->LowestNewPrice->FormattedPrice ?></a></h6>
                                        <div class="cate-list">
                                            <ul>
                                                <li><a href="<?= \yii\helpers\Url::to(['product-detail','id'=>$item->ASIN])?>"><img src="tnk/images/list-1-icon.png" alt="" /></a></li>
                                                <li><a href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'amazon','quantity'=>1,'name'=>$item->ItemAttributes->Title,'code'=>$item->ASIN])?>"><img src="tnk/images/list-2-icon.png" alt="" /></a></li>
                                                <li><a href="javascript:void(0)"><img src="tnk/images/list-3-icon.png" alt="" /></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }


                        }
                        ?>
                        <!--product list-->
                    </ul>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                           <?=LinkPager::widget([
                               'pagination' => $pages,
                           ]);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
