<?php
use \common\components\TBase;
if (isset($data['ResultSet'][0]['Result'])) {

    $model = new \common\components\TCurrencyConvertor;
    $cnt = 0;

    $j = 1;
    foreach($data['ResultSet'][0]['Result'] as $current){
        if (!isset($current['Name'])) {
            continue;
        }
        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
        ?>
        <div class="col-sm-3 <?=$j?>">
            <div class="product-lst position-relative">
                <div class="loading hidden">
                    <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                </div>
                <?php if ($category) { ?>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code'],'cid'.$category->ref_id])?>">
                    <?php } else { ?>
                    <a target="_blank" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code']])?>">
                        <?php } ?>
                        <div style="height: 150px;overflow: hidden;">
                            <img src="<?=$current['Image']['Medium']; ?>" alt="product">
                        </div>
                        <div class="product-list-product translate" id="product_title_<?=$cnt;?>" data-title="<?=$current['Name']; ?>"><?=$current['Name']; ?></div>
                    </a>
                    <h6>
                        <a class="amount" href="javascript:void(0)"
                           data-usd="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','USD'); ?>"
                           data-rub="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','RUB'); ?>"
                           data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','JPY'); ?>"
                           data-cny="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','CNY'); ?>"
                           data-eur="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','EUR'); ?>">
                            <?php
                            if (isset($_REQUEST['lang'])) {
                                if ($_REQUEST['lang'] == 'jpy') {
                                    echo \common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','JPY');
                                } else if ($_REQUEST['lang'] == 'usd') {
                                    echo \common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','USD');
                                } else if ($_REQUEST['lang'] == 'cny') {
                                    echo \common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','CNY');
                                } else if ($_REQUEST['lang'] == 'eur') {
                                    echo \common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','EUR');
                                } else {
                                    echo \common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','RUB');
                                }
                            }
                            ?>
                        </a>
                    </h6>
                    <div class="cate-list product-icon-listing">
                        <div class="clearfix">
                            <?php if ($category) { ?>
                                <div class=" pull-left" ><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code'],'cid'.$category->ref_id])?>"></a></div>
                            <?php } else { ?>
                                <div class=" pull-left" ><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code']])?>"></a></div>
                            <?php } ?>

                            <div class=" pull-left" ><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'yahoo','code'=>$current['Code']])?>"></a></div>
                            <div class=" pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'yahoo','code'=>$current['Code']],true)?>"></a></div>
                        </div>
                    </div>
            </div>
        </div>
        <?php
        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
        $j++;
        $cnt++;
    } ?>
    </div>
<?php } else {
    echo "0";
    exit;
}
?>