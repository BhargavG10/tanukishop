<style>
    #myModal{
        z-index: 9999999;
</style>
<?php
use yii\helpers\Html;
use common\components\TBase;
$model = new \common\components\TCurrencyConvertor;

$name = $data['ResultSet'][0]['Result'][0]['Name'];
$this->title = $name;
$code = $data['ResultSet'][0]['Result'][0]['Code'];
$price = $data['ResultSet'][0]['Result'][0]['Price']['_value'];
$shopCode = $data['ResultSet'][0]['Result'][0]['Store']['Id'];
$affiliateUrl = $data['ResultSet'][0]['Result'][0]['Url'];

$description = (isset($data['ResultSet'][0]['Result'][0]['Caption'])) ? $data['ResultSet'][0]['Result'][0]['Caption'] : $data['ResultSet'][0]['Result'][0]['Description'];
$this->title = Tbase::ShowLbl('YAHOO_PRODUCT_DETAIL');

$imageSetMedium = [];
$imageSetSmall = [];


if (isset($data['ResultSet'][0]['Result'][0]['Image']) && count($data['ResultSet'][0]['Result'][0]['Image']) > 0) {
    if (isset($data['ResultSet'][0]['Result'][0]['Image']['Medium'])) {
        $imageSetMedium[] = $data['ResultSet'][0]['Result'][0]['Image']['Medium'];
    }

    if (isset($data['ResultSet'][0]['Result'][0]['Image']['Small'])) {
        $imageSetSmall[] = $data['ResultSet'][0]['Result'][0]['Image']['Small'];
    }
}
if (isset($data['ResultSet'][0]['Result'][0]['RelatedImages']) && count($data['ResultSet'][0]['Result'][0]['RelatedImages']) > 0 && $data['ResultSet'][0]['Result'][0]['RelatedImages'] != '') {
    foreach ($data['ResultSet'][0]['Result'][0]['RelatedImages'] as $imgs) {
        if (isset($imgs['Medium'])) {
            $imageSetMedium[] = $imgs['Medium'];
            $imageSetSmall[] = $imgs['Small'];
        }
    }
}
$countries = \yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(), 'id', 'title');
$language = ['JPY'=>'JPY','USD'=>'USD','RUB'=>'RUB'];
?>
<style>
    .tab_link{color: #fff !important;text-decoration: none;}
</style>
<div class="m_container rakuten margin-bottom-20 yahoo notranslate">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a class="color-back" href="<?=\yii\helpers\Url::to(['yahoo/index']) ?>"><?=Tbase::ShowLbl('yahoo_shopping')?></a></li>
            <?php if ($category) { ?>
                <li><a class="color-back" href="<?=\yii\helpers\Url::to(['yahoo/list','cid'=>$category->ref_id]) ?>"><?php
                    if (\common\components\Language::CLang() == 'en-US') {
                        echo $category->title_en;
                    } else {
                        echo $category->title_ru;
                    }
                    ?></a></li>
            <?php } ?>
        </ol>
    </div>
</div>
<section class="product-page">
    <div class="container">
        <div class="product-plug">
            <div class="row">
                <div class="col-md-5 notranslate">
                    <div class="row product-bx">
                        <div class="col-md-12 col-sm-12 bx-slid  wow fadeInUp white-bg bxsliderWrapper">
                            <ul id="bxslider-de">
                                <?php
                                if ($imageSetMedium) {
                                    $j=0;
                                    foreach($imageSetMedium as $bitem) {
                                        if (isset($bitem)) {
                                            $url = str_replace('/g/','/l/',$bitem);
                                            ?>
                                            <li><a href="<?=$url; ?>" data-lightbox="example-set"><img src="<?=$url; ?>"></a></li>
                                            <?php
                                        }
                                        $j++;
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-12 col-sm-12  wow fadeInUp">
                            <div class="bxpager-slid">
                                <ul id="bxslider-pager">
                                    <?php if ($imageSetSmall) {
                                        $i =0;
                                        foreach($imageSetSmall as $item) {
                                            if (isset($item)) { ?>
                                                <li data-slideIndex="<?= $i ?>">
                                                    <a><img src="<?= (isset($item)) ? str_replace('/c/','/l/',$item) : ''; ?>"></a>
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 content-area">
                    <div class="col-md-12 content-area">
                        <h2 class="translate" data-short="0"><?=Yii::t('app',$name);?></h2>
                        <h3 class="heading_bg notranslate"><span><?=Tbase::ShowLbl('ITEM_INFO') ?></span></h3>
                            <div class="clearfix info notranslate">
                                <div class="clearfix col-lg-8 left-info">
                                    <table class="info table table-bordered table-striped">
                                    <tr>
                                        <th><?=Tbase::ShowLbl('SHIPPING_WITHIN_JAPAN') ?></th>
                                        <td><?php
                                        if (isset($data['ResultSet'][0]['Result'][0]['Shipping']['Code']) && $data['ResultSet'][0]['Result'][0]['Shipping']['Code'] == '2') {
                                            echo Tbase::ShowLbl('FREE');
                                        } else {
                                            echo Tbase::ShowLbl('NOT_FREE');
                                        } ?>
                                        </td>
                                    </tr>
                                    <tr><th><?=Tbase::ShowLbl('ITEM_CONDITION')?>:</th><td><?=(isset($data['ResultSet'][0]['Result'][0]['Condition'])) ? $data['ResultSet'][0]['Result'][0]['Condition'] : TBase::ShowLbl('USED_ITEM')?></td></tr>
                                    <tr><th><?=Tbase::ShowLbl('AVAILABILITY')?>:</th><td><?=(isset($data['ResultSet'][0]['Result'][0]['Availability']) && $data['ResultSet'][0]['Result'][0]['Availability'] == 'instock') ? TBase::ShowLbl('YES') : TBase::ShowLbl('NOT_AVAILABLE')?></td></tr>
                                    </table>
                                </div>
                                <div class="col-lg-4 right-info">
                                    <ul  class="smartpay ">
                                        <li><a href="javascript:void(0)"><?=Yii::t('app',$code);?></a></li>
                                        <li>
                                            <div id="rate1_rateblock" title="Rated 4.29 out of 5" class="star-rating notranslate">
                                            <span id="rate1_stars" style="width:<?=(80*($data['ResultSet'][0]['Result'][0]['Review']['Rate']/5))?>px;" class="notranslate">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <hr>

                        <form id="add-to-cart" action="/order/add-to-cart" method="GET">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
                            <?php if (isset($data['ResultSet'][0]['Result'][0]['Order'])) {?>
                                <ul class="inventory-options" style="padding-bottom: 25px;border-bottom: 1px solid #dedede;margin-bottom: 19px;}">
                                    <?php
                                        for ($i = 0; $i < count($data['ResultSet'][0]['Result'][0]['Order']) - 1; $i++) {
                                            if ($data['ResultSet'][0]['Result'][0]['Order'][$i]['_attributes']['type'] == 'select') { ?>
                                                <li><?=$data['ResultSet'][0]['Result'][0]['Order'][$i]['Name']?>
                                                    <br/>
                                                    <select name=params["<?=$data['ResultSet'][0]['Result'][0]['Order'][$i]['Name']?>]" style="width: 100%;" class="form-control">
                                                        <?php for ($j=0; $j < count($data['ResultSet'][0]['Result'][0]['Order'][$i]['Values'])-1; $j++) { ?>
                                                            <option><?=$data['ResultSet'][0]['Result'][0]['Order'][$i]['Values'][$j]['Value'];?> </option>
                                                        <?php } ?>
                                                    </select>
                                                </li>
                                            <?php }
                                        }
                                    ?>
                                    <li>
	                                    <?php if(
		                                    isset($data['ResultSet'][0]['Result'][0]['Inventories']) &&
		                                    $data['ResultSet'][0]['Result'][0]['Inventories'][0]['Order'] &&
		                                    $data['ResultSet'][0]['Result'][0]['Inventories'][0]['Order'] != ""
	                                    ) { ?>
                                            <button style="background:#7ac0c8 none repeat scroll 0 0" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><?=TBase::ShowLbl('CHECK_INVENTORY_INFORMATION')?></button>
	                                    <?php } ?>
                                    </li>
                                </ul>
                            <?php } ?>
                            <ul class="select-box notranslate">
                            <li style="width:41%;">
                                <p style="font-size: 24px;">
                                    <?=Tbase::ShowLbl('PRICE')?>: <a style="margin-top: 7px;" class="amount" href="javascript:void(0)" data-usd="<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>" data-rub="<?=\common\models\CurrencyTable::convert($price,'JPY','RUB'); ?>" data-jpy="<?=\common\models\CurrencyTable::convert($price ,'JPY','JPY'); ?>"><?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?></a>
                                    <span style="font-size:15px; ">(~<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>)</span>
                                </p>
                            </li>
                            <li style="width: 10%;">
                            <select class="form-control" name="quantity" id="quantity_class">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            <input type="hidden" name="code" value="<?=$code ?>">
                            <input type="hidden" name="sh" value="yahoo">
                            </li>
                            <li style="width:25%;">
                                <?php if (Yii::$app->request->get('cart_id')) { ?>
                                    <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                        <?=Tbase::ShowLbl('UPDATE_TO_CART'); ?>
                                    </button>
                                    <input type="hidden" name="cart_id" value="<?=Yii::$app->request->get('cart_id')?>">
                                <?php } else { ?>
                                    <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                        <?=Tbase::ShowLbl('ADD_TO_CART'); ?>
                                    </button>
                                <?php } ?>
                            </li>
                            <li><h5><a href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'yahoo','code'=>$code])?>"><img src="/tnk/images/wishlist.png" alt="wishlist" /> <?=Tbase::ShowLbl('ADD_TO_WISHLIST'); ?></a></h5></li>
                            </ul>
                        </form>
                        <hr>
                        <ul class="seller-ul notranslate">
                            <li style="word-wrap: break-word;width:29%!important">
                                <h5><?=TBase::ShowLbl('SELLER_ITEMS'); ?></h5>
                                <h4><?=Yii::t('app',$shopCode);?></h4>
                            </li>
                            <li class="width-25-percent"><h6><a target="_blank" href="<?=$affiliateUrl?>"><?=Tbase::ShowLbl('THIS_ITEM_PAGE_ON_YAHOO'); ?></a></h6></li>
                            <?php if (Yii::$app->user->isGuest) { ?>
                                <li class="width-32-percent"><textarea class="form-control" placeholder="<?=TBase::ShowLbl('PLEASE_LOGIN_TO_SEND_MESSAGE')?>"></textarea></li>
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>">
                                        <button class="btn btn-post">
                                            <?=TBase::ShowLbl('LOGIN')?>
                                        </button>
                                    </a>
                                </li>
                            <?php } else { ?>
                                <?=Html::hiddenInput('mail-path',\yii\helpers\Url::to(['site/send-mail']),['id'=>'mail-path'])?>
                                <li class="width-32-percent"><textarea class="form-control" id="msg" placeholder="<?=Tbase::ShowLbl('COMMENTS'); ?>"></textarea></li>
                                <li><button id="send-mail" class="btn btn-post"><?=Tbase::ShowLbl('POST');?></button></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-12 how-much1 position-relative notranslate">
                        <div class="how-much">
                            <div class="absolute"><?=Tbase::ShowLbl('PLEASE_WAIT');?></div>
                            <h4><?=Tbase::ShowLbl('HOW_MUCH_WILL_IT_COST'); ?></h4>
                            <form class="shipping_calculator" id="shipping_calculator">
                                <ul class="select-box">
                                    <li><input type="text" class="form-control form-control1" name="package_weight" id="package_weight" placeholder="<?=Tbase::ShowLbl('PACKAGE_WEIGHT'); ?>" /></li>
                                    <li><?php echo Html::dropDownList('country_id', null, $countries,['class'=>'form-control','prompt'=>Tbase::ShowLbl('SHIP_TO')]) ; ?></li>
                                    <li><?php echo Html::dropDownList('language_id', 'jpy', $language,['class'=>'form-control','prompt'=>Tbase::ShowLbl('CURRENCY')]) ; ?></li>
                                    <li> <button class="btn btn-cart1" id="calculate-shipping"><i class="fa fa-search" aria-hidden="true"></i></button></li>
                                </ul>
                                <input type="hidden" name="price" id="price" value="<?=$price?>">
                                <input type="hidden" name="tanuki_charge" id="tanuki_charge" value="300">
                            </form>
                            <p><?=Tbase::ShowLbl('THE_LIMIT_OF_WEIGHT_OF_PACKAGE_IS_30KG');?></p>
                            <div class="no-shipping-msg" style="display: none;color:#df7b2b;">
                            </div>
                            <ul class="item-price hidden" id="item-shipping-calculation">
                                <li><strong><?=Tbase::ShowLbl('PRICE'); ?>:</strong><?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?></span></li>
                                <li><strong><?=Tbase::ShowLbl('TANUKI_FEE'); ?>: </strong>¥ 300</li>
                                <li><strong><?=Tbase::ShowLbl('INTERNATIONAL_SHIPPING'); ?>: </strong> ¥<label class="shipping_charge"></label></li>
                                <li><a href="javascript:void(0)"><strong><?=Tbase::ShowLbl('TOTAL'); ?>: </strong>¥<label class="total_cost"></label> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="product-tabs">
            <div id="horizontalTab">
                <ul class="resp-tabs-list notranslate">
                    <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><?=Tbase::ShowLbl('DESCRIPTION');?></li>
                    <li class="resp-tab-item" aria-controls="tab_item-1" role="tab">
                        <?=Html::a(TBase::ShowLbl('APPROXIMATE_WEIGHT_OF_DIFFERENT_GOODS'),['site/page','slug'=>\common\models\Page::getSlug(8)],['class'=>'tab_link'])?>
                    </li>
                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab">
                        <?=Html::a(TBase::ShowLbl('JAPANESE_CLOTHING_AND_SHOE_SIZES'),['site/page','slug'=>\common\models\Page::getSlug(9)],['class'=>'tab_link'])?>
                    </li>
                </ul>

                <div class="resp-tabs-container">
                    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><?=Tbase::ShowLbl('DESCRIPTION');?></h2>
                    <div data-short="0" id="product_description" class="resp-tab-content resp-tab-content-active " aria-labelledby="tab_item-0">
                        <?=$description?>
                        <?php
                        if (isset($data['ResultSet'][0]['Result'][0]['SpAdditional']) && $data['ResultSet'][0]['Result'][0]['SpAdditional'] != '') {
                            echo $data['ResultSet'][0]['Result'][0]['SpAdditional'];
                        }
                        echo (isset($data['ResultSet'][0]['Result'][0]['Description'])) ? $data['ResultSet'][0]['Result'][0]['Description'] : '';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-similer">
            <h3 class="heading_bg"><span><?=Tbase::ShowLbl('OTHER_SIMILAR_PRODUCTS');?></span></h3>
            <div class="row">
                <?php
                $category = false;
                if (isset($_REQUEST['cid'])) {
                    $category = $_REQUEST['cid'];
                } else if (isset($data['ResultSet'][0]['Result'][0]['ProductCategory']['ID'])) {
                    $category = $data['ResultSet'][0]['Result'][0]['ProductCategory']['ID'];
                }
                if ($category) {

                    $TModel  = new \common\models\Yahoo;
                    $similar_result = $TModel->productsListing(['category_id'=>$category,'hits'=>4]);
                    if (
                        count($similar_result) >0 &&
                        isset($similar_result['ResultSet']) &&
                        isset($similar_result['ResultSet'][0]) &&
                        isset($similar_result['ResultSet'][0]['Result'])
                ) {
                        foreach ($similar_result['ResultSet'][0]['Result'] as $sproduct) {

                            if (!isset($sproduct['Name'])) {
                                continue;
                            }
                            if (isset($sproduct['Image']['Medium'])) {
                                $url = str_replace('/g/', '/l/', $sproduct['Image']['Medium']);
                            }
                            ?>
                            <a class="no-hover" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$sproduct['Code']])?>">
                                <div class="col-md-3 col-sm-6">
                                    <div class="other-image"><img src="<?=$url; ?>"></div>
                                    <div class="other-para"><p style="display: block;height: 83px;overflow: hidden;" class="translate"><?=$sproduct['Name'] ?></p>
                                        <h6><a class="no-hover">¥<?=number_format($sproduct['Price']['_value']);?></a></h6>
                                    </div>
                                </div>
                            </a>
                        <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</section>

<?=$this->registerCss('
.form-control1{
margin-left:0px!important;
margin-bottom:0px!important;
}
#quantity_class{
    color: #000;font-size: 17px;text-align: center;padding-left: 17px;
}
table.info th{width:50%;}table.info td{text-align:center;text-transform: capitalize;}
a.amount{text-decoration:none;}
.info{margin-top:7px;}
div.left-info{padding-left:0px;}
div.left-info table{    margin-top: 0px;}
div.right-info{text-align: center;padding: 28px;background:#f9f9f9;}
div.right-info ul.smartpay li {margin-right:0px;}
header .badge { z-index: 2222;}
');?>
<?=$this->registerCssFile('@web/tnk/css/lightbox.min.css');?>
<?=$this->registerJsFile('@web/tnk/js/lightbox-plus-jquery.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/tnk/js/modernizr-2.6.2.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>

<?php $this->registerJs(
        "
var x = $('*').filter(function () { 
return this.style.position == 'fixed' 
}).css('position', 'static');
        "
)?>
<?php

if (
        isset($data['ResultSet'][0]['Result'][0]['Inventories']) &&
        $data['ResultSet'][0]['Result'][0]['Inventories'][0]['Order'] &&
        $data['ResultSet'][0]['Result'][0]['Inventories'][0]['Order'] != ""
) {
?>
    <div id="myModal" class="modal fade" role="dialog" data-keyboard="true" data-backdrop="true">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?=Tbase::ShowLbl('INVENTORY_INFORMATION');?></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped text-center">
                        <tr align="center">
                            <th class="text-center"><?=Tbase::ShowLbl('COLOR');?></th>
                            <th class="text-center"><?=Tbase::ShowLbl('SIZE');?></th>
                            <th class="text-center"><?=Tbase::ShowLbl('INVENTORY_STATUS');?></th>
                        </tr>

		                <?php
		                foreach($data['ResultSet'][0]['Result'][0]['Inventories'] as $item) {
			                if (isset($item['Order'])) { ?>
                                <tr>
                                    <td><?=(isset($item['Order'][0]['Value']))?$item['Order'][0]['Value']:''?></td>
                                    <td><?=(isset($item['Order'][1]['Value']))?$item['Order'][1]['Value']:''?></td>
                                    <td><i class="fa fa-<?=(isset($item['Quantity']) && $item['Quantity'] > 0) ? 'check' : 'times' ?>" aria-hidden="true"></i></td>
                                </tr>
				                <?php
			                }
		                }
		                ?>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
<?php } ?>

