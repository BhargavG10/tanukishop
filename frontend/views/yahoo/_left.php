<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use common\components\TBase;
?>
<style>
    button.accordion{background:0 0;color:#444;cursor:pointer;width:100%;border:none;text-align:left;outline:0;font-size:15px;transition:.4s;padding:7px 4px}button.accordion.active,button.accordion:hover{background-color:#ddd}button.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}button.accordion.active:after{content:"\2212"}div.panel{padding:0 6px 0 10px;max-height:0;overflow:hidden;transition:max-height .2s ease-out}
</style>

<h4 class="notranslate"><?=\common\components\TBase::ShowLbl('CATEGORIES');?></h4>
<div class="panel-group left-side-bar translate" id="accordion">
    <label>
	    <?php
	    if (Yii::$app->request->get('cid') != 'All') {
	    $model = new \common\models\Yahoo();
	    $categoryData = $model->getCategories( Yii::$app->request->get( 'cid' ) );
	    if ( isset( $categoryData['Result']['Categories']['Current']['Title']['Medium'] ) ) {
		    echo $categoryData['Result']['Categories']['Current']['Title']['Medium'];
	    }
	    ?>
    </label>
    <hr>
	<?php
	if ( isset( $categoryData['Result']['Categories'] ) ) {
		$data = $categoryData;
		if ( isset( $data['Result']['Categories']['Children']['Child'] ) ) {
			foreach ( $data['Result']['Categories']['Children']['Child'] as $key => $category ) { ?>
                <button class="accordion"><?= $category['Title']['Medium'] ?></button>
                <div class="panel">
                    <a href="<?= \yii\helpers\Url::to( [
						Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
						'cid' => $category['Id']
					] ) ?>">
						<?= $category['Title']['Medium'] ?> (All)<br/>
                    </a>
					<?php
					$subData = $model->getCategories( $category['Id'] );
					if ( isset( $subData['Result']['Categories']['Children']['Child'] ) ) {
						foreach ( $subData['Result']['Categories']['Children']['Child'] as $key => $subcategory ) {
						    if (isset($subcategory['Id'])) {
							    ?>
                                <a href="<?= \yii\helpers\Url::to( [
								    Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
								    'cid' => $subcategory['Id']
							    ] ) ?>">
								    <?= $subcategory['Title']['Medium'] ?>
                                </a><br/>
							    <?php
						    }
						}
					}
					?>
                </div>
				<?php
			}
		} else if ( isset( $data['Result'] ) ) { ?>
            <a>
				<?php
				if ( isset( $categoryData['Result']['Categories']['Current']['Title']['Medium'] ) ) {
					echo $categoryData['Result']['Categories']['Current']['Title']['Medium'] . ' (All)';
				}
				?>
            </a>
		<?php }
	    }
	} else {
        $data = \common\models\Category::findAll(['shop'=>'yahoo','parent_id'=>0]);
        if ($data) {
            foreach ($data as $cat) { ?>
                <button class="accordion"><?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?></button>
                <div class="panel">
                    <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $cat->ref_id]) ?>">
                        <?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?> (All)
                    </a>
                    <?php
                    $child = \common\models\Category::findAll(['parent_id' => $cat->id,'shop'=>'yahoo']); ?>
                    <?php foreach ($child as $childCat) { ?>
                        <div>
                            <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $childCat->ref_id]) ?>">
                                <?= (\common\components\TBase::CLang() == 'ru-RU') ? $childCat->title_ru : $childCat->title_en; ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
    }
    ?>
</div>

<h4 class="notranslate"><?=\common\components\TBase::ShowLbl('FILTER_BY_PRICE')?></h4>
<hr>
<div class="row min-max notranslate">
    <?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createAbsoluteUrl($param),'method' => 'GET']) ?>
    <div class="col-lg-12 right-pad-zero margin-bottom-7">
		<?=TBase::_x('CURRENT_PRICE')?><br/><hr/>
        <div class="col-xs-5 right-pad-zero " style="padding-left: 0px">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmin']) && $_REQUEST['pmin']!= '') ? $_REQUEST['pmin'] : ''; ?>" name="pmin" placeholder="<?=TBase::ShowLbl('MIN')?>">
        </div>
        <div class="col-xs-5 right-pad-zero " style="padding-left: 0px;margin-left: 27px;">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmax']) && $_REQUEST['pmax']!= '') ? $_REQUEST['pmax'] : ''; ?>" name="pmax" placeholder="<?=TBase::ShowLbl('MAX')?>">
        </div>
    </div>
    <input type="submit" name="search" value="<?=\common\components\TBase::ShowLbl('FILTER')?>" class="btn filter-btn">
    <?php ActiveForm::end() ?>
</div>
<div class="clearfix"></div>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
</script>
