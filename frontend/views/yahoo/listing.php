<?php
    use \common\components\TBase;
    use common\components\TBase as LBL;
    $model = new \common\components\TCurrencyConvertor;

?>
<div class="m_container margin-bottom-20 yahoo">
    <div class="container">
        <ol class="breadcrumb">
            <li><a class="color-back" href="<?=\yii\helpers\Url::to(['/yahoo']); ?>"><?=TBase::ShowLbl
                    ('yahoo_shopping'); ?></a></li>
            <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                <li><a class="color-back" href="#"><?=$_REQUEST['s']; ?> </a></li>
            <?php } else if ($category){ ?>
                    <li><a class="color-back" href="#"><?=$category->title_en; ?> </a></li>
            <?php } ?>
        </ol>
    </div>
</div>
<div class="container notranslate">
    <section class="listing-panel">
        <div class="container" style="position: relative;">
            <div class="row">
                <div class="col-md-2 col-sm-4 left-bar">
                    <div class="well">
                        <?php echo $this->render('_left',['category'=>$category,'param'=>$param]); ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-8 product-list-page">
                    <div class="well">
                        <div class="row notranslate">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <ul class="filters">
                                        <li><?=TBase::ShowLbl('SORT_BY');?>:</li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='lth') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'lth'])); ?>"><?=TBase::ShowLbl('LOW_TO_HIGH')?></a></li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='htl') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'htl'])); ?>"><?=TBase::ShowLbl('HIGH_TO_LOW')?></a></li>
                                    </ul>
                                </div>
                                <div class="pull-left text-enter">
                                    <ul class="filters filter-result-count">
                                        <li class="text-center"><?=TBase::ShowLbl('TOTAL_RESULT');?>&nbsp;&nbsp;<label style="color: #782f4a;"><?=(isset($data['ResultSet']['totalResultsAvailable'])) ? number_format($data['ResultSet']['totalResultsAvailable']) : 0; ?></label></li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <ul class="filters currency-filters">
                                        <li><?=TBase::ShowLbl('CURRENCY');?>:</li>
                                        <li><a class="current" id="currency-jpy" data-currency="jpy"><?=TBase::ShowLbl('JAPANESE_YEN');?></a></li>
                                        <li><a id="currency-usd"  data-currency = "usd"><?=TBase::ShowLbl('US_DOLLAR');?></a></li>
                                        <li><a id="currency-rub"  data-currency = "rub"><?=TBase::ShowLbl('RUSSIAN_RUBLE');?></a></li>
                                        <li><a id="currency-cny"  data-currency = "cny"><?=TBase::ShowLbl('CHINESE_YUAN');?></a></li>
                                        <li><a id="currency-eur"  data-currency = "eur"><?=TBase::ShowLbl('EURO');?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="top_p list-brand">
                                <?php
                                $cnt = 0;
                                $j = 1;
                                if (isset($data['ResultSet'][0]['Result']) && count($data['ResultSet'][0]['Result'])>2) {


                                    foreach($data['ResultSet'][0]['Result'] as $current){
                                        if (!isset($current['Name'])) {
                                            continue;
                                        }
                                        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
                                        ?>
                                        <div class="col-sm-3 <?=$j?>">
                                            <div class="product-lst position-relative">
                                                <div class="loading hidden">
                                                    <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                                                </div>
                                                <?php if ($category) { ?>
                                                    <a target="_blank" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code'],'cid'=>$category->ref_id])?>">
                                                <?php } else { ?>
                                                    <a target="_blank" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code']])?>">
                                                <?php } ?>
                                                        <div style="height: 150px;overflow: hidden;">
                                                            <img src="<?=$current['Image']['Medium']; ?>" alt="product">
                                                        </div>
                                                    <div class="product-list-product translate" id="product_title_<?=$cnt;?>" data-title="<?=$current['Name']; ?>"><?=$current['Name']; ?></div>
                                                </a>
                                                <h6>
                                                    <a class="amount" href="javascript:void(0)"
                                                       data-usd="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','USD'); ?>"
                                                       data-rub="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','RUB'); ?>"
                                                       data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','JPY'); ?>"
                                                       data-cny="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','CNY'); ?>"
                                                       data-eur="<?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','EUR'); ?>">
                                                        <?=\common\models\CurrencyTable::convert((float)$current['Price']['_value'],'JPY','JPY'); ?>
                                                    </a>
                                                </h6>
                                                <div class="cate-list product-icon-listing">
                                                    <div class="clearfix">
                                                        <?php if ($category) { ?>
                                                            <div class="pull-left"><a  target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code'],'cid'=>$category->ref_id])?>"></a></div>
                                                        <?php } else { ?>
                                                            <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['yahoo/detail','id'=>$current['Code']])?>"></a></div>
                                                        <?php } ?>
                                                        <div class="pull-left"><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'yahoo','code'=>$current['Code']])?>"></a></div>
                                                        <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'yahoo','code'=>$current['Code']],true)?>"></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
                                        $j++;
                                        $cnt++;
                                    } ?>
                                    </div>
                                <?php }
                                if ($j <= 2) {
                                    echo "<div class='text-center'>".TBase::ShowLbl('NO_PRODUCT_FOUND')."</div>";
                                }
                                ?>
                        </div>
                        <hr>
                    <?php if (isset($data['ResultSet'][0]['Result']) && count($data['ResultSet'][0]['Result'])> 15) { ?>
                        <div class="row">
                            <div class="col-md-12 notranslate">
                                <div class="text-center">
                                    <a href="#" class="product-load-more" data-shop="yahoo"> <?=TBase::ShowLbl('OPEN_MORE_PRODUCTS')?> </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

	                <?php
	                if ($category) { ?>
                        <div class="seo-txt">
			                <?=$category->seo_text; ?>
                        </div>
	                <?php } ?>
                    </div>
                </div>
                <!-- slide left side bar div -->
                <div class="col-md-2 scroll-left-side-bar" style="position: absolute; display: none;">
                    <div class="well">
                        <a id="back-to-top" style="margin-top: 10px;width: 90%;" href="" class="btn filter-btn"><?=LBL::_x('GO_TO_FILTER_AREA'); ?></a></div>
                </div>
                <!-- slide left side bar div -->
            </div>
        </div>
    </section>
</div>
<?=$this->registerJS('
var ajax_url = "'.\yii\helpers\Url::to(['yahoo/list']).'";
',yii\web\View::POS_BEGIN)?>
<div class="loading-extend" style="display: none;"></div>
<div class="loading-image" style="display: none;">
    <?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?>
</div>