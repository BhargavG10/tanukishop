<style>
    .text-center{
        font-size: 19px;
    }
    img {
        width: 58px;
    }
</style>
<section class="product-page">
    <div class="container">
        <div class="product-plug">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <?=\yii\helpers\Html::img('@web/tnk/images/bx_loader.gif')?><br/>
                        <?=\common\components\TBase::_x('PLEASE_WAIT')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>