<?php
    use yii\helpers\Html;
    use \common\components\TBase;
    $this->title = TBase::ShowLbl('YAHOO_CATEGORIES');
    $lang = TBase::CLang();
?>
<style>
     p a{color:#fff;}
     p a:hover{text-decoration: none;color:#fff;}
    .main-image{
        height:90px;
        overflow: hidden;
        margin-top: 15px;
        background: #888888;
    }
</style>
<div class="m_container rakuten yahoo-auction">
    <div class="container color-back">
        <div class="col-md-7 notranslate" style="margin-top: 4px;">
            <?=TBase::ShowLbl('YAHOO_AUCTION'); ?>
        </div>
        <div class="col-md-5 right-side-search">
            <form id="w1" action="<?=\yii\helpers\Url::to(['yahoo-auctions/detail'],true) ?>" method="GET">
                <!--                <div class="col-md-6 col-xs-10">-->
                <!--                    <label style="font-size: 14px;color:#333">--><?//=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID') ?><!-- :</label>-->
                <!--                </div>-->
                <div class="col-md-offset-6 col-md-5 col-xs-10">
                    <input class="form-control" name="id" value="" type="text" placeholder="<?=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID')?>">
                </div>
                <div class="col-md-1 col-xs-10 padding-left-0">
                    <input type="submit" class="yahoo_auction_search_btn search_btn" name="search" value="">
                </div>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            </form>
        </div>
    </div>
</div>
<div class="container yahoo-auction notranslate main-image">
    <div class="row">
        <?=Html::a(Html::img('/uploads/yahooparts.jpg'),['/parts'],['target'=>'_blank'])?>
    </div>
</div>
<div class="container yahoo-auction notranslate">
    <div class="row marg-l0">
        <?php
        $j = 1;
        foreach ($categories as $cat) {
            if ($cat->parent_id == 0) {
                echo ($j == 1 || ($j % 4 == 0)) ? '<div class="row margin-bottom-20">' : ''; ?>
                <div class="col-md-3 col-sm-3 cat-list <?php echo $j.'_'.($j % 4); ?>">
                    <p>
                        <?=Html::a(($lang == 'en-US') ? $cat->title_en : $cat->title_ru,['yahoo-auctions/list','cid'=>$cat->ref_id]); ?>
                    </p>
                    <ul>
                        <?php
                        foreach ($categories as $catSub) {
                            if ($cat->id == $catSub->parent_id) {
                                $title = ($lang == 'en-US') ? $catSub->title_en : $catSub->title_ru;
                                echo '<li>'.Html::a($title,['yahoo-auctions/list','cid'=>$catSub->ref_id]).'</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <?php
                echo ($j %4 == 0 ) ? '</div>' : '';
                $j++;
            }
        }
        ?>
        <div class="clearfix"></div>
    </div>
</div>