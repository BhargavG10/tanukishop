<?php
use yii\helpers\Html;
use common\components\TBase;
use common\models\User;
use yii\helpers\Url;
$currencyModel = new \common\components\TCurrencyConvertor;

$this->title = $model->YahooAction['ResultSet']['Result']['Title'];
$tanukiCharges = TBase::TanukiCharges('tanuki-yahoo-auction-shipping-charges');
$countries = \yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(), 'id', 'title');
$language = ['JPY'=>'JPY','USD'=>'USD','RUB'=>'RUB'];
$ShippingCountries = \yii\helpers\ArrayHelper::map(\common\models\ShippingCountry::find()->where(['is_active'=>1])->orderBy('id')->all(), 'id', 'name');
$ShippingCategory = \yii\helpers\ArrayHelper::map(\common\models\ShippingCategory::find()->where(['is_active'=>1])->orderBy('id')->all(), 'id', 'name');
?>
<style>
.tab_link{color: #fff !important;text-decoration: none;}
.form-radio{
        box-shadow: 0 0px 0px rgba(0, 0, 0, 0.075) inset!important;
        height: 17px !important;
        width: 17px !important;
		    vertical-align: middle;
    }
    .form-control2{
        margin-bottom: 12px!important;
    }
	.lbbl{    margin-top: 7px;
			font-size: 14px;
	}
	.btn-cart2 {
    background: #782f4a !important;
    border: 1px solid #782f4a !important;
    height: 34px !important;
	color: #fff !important;
}
.btn-post{
    padding: 8px!important;
    font-size: 11px;
}

.shipping-tabs .resp-tabs-list li{
    margin: 0 24px;
}
.btn.no-bg .fa{
    display:none;
}
.btn.no-bg img{
    width:20px;
}
</style>
<div class="m_container rakuten margin-bottom-20 yahoo-auction">
    <div class="container ">
        <div class="col-md-9 translate" style="margin-top: 4px;">
            <ol class="breadcrumb">
                <li><a class="color-back" href="<?=\yii\helpers\Url::to(['yahoo-auctions/index']) ?>"><?=Tbase::ShowLbl('YAHOO_AUCTION')?></a></li>
                <?php
                if (isset($model->YahooAction['ResultSet']) && isset($model->YahooAction['ResultSet']['Result']['CategoryPath']) && !empty($model->YahooAction['ResultSet']['Result']['CategoryPath']) ) {

	                $catID = explode(',',$model->YahooAction['ResultSet']['Result']['CategoryIdPath']);

                    foreach (explode('>',$model->YahooAction['ResultSet']['Result']['CategoryPath']) as $key => $cat) {
	                    if ($key == 0) { continue; }
                    ?>
                        <li><a class="color-back" href="<?=\yii\helpers\Url::to(['yahoo-auctions/list','cid'=>$catID[$key]]) ?>"><?=$cat?></a></li>
                    <?php
                    }
                }
                ?>
            </ol>
        </div>
        <div class="col-md-3 right-side-search">
            <form id="w1" action="<?=\yii\helpers\Url::to(['yahoo-auctions/detail'],true) ?>" method="GET">
                <div class="col-md-offset-1 col-md-10 col-xs-10 notranslate">
                    <input class="form-control" name="id" value="" type="text" placeholder="<?=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID')?>">
                </div>
                <div class="col-md-1 col-xs-10 padding-left-0">
                    <input type="submit" class="yahoo_auction_search_btn search_btn" name="search" value="">
                </div>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            </form>
        </div>
    </div>
</div>
<section class="product-page">
    <div class="container">
        <div class="product-plug">
            <div class="row">
                <?php
//                if ($model->YahooAction['ResultSet']['Result']['Status'] == 'closed') {
//                    echo "<h3 class='text-center'>".Tbase::ShowLbl('AUCTION_IS_CLOSED')."</h3>";
//                } else if (isset($model->YahooAction['Error'])) {
//                    echo '<div class="text-center">'.$model->YahooAction['Error']['Message'].'</div>';
//                } else {
                if (isset($model->YahooAction['Error'])) {
                    echo '<div class="text-center">'.$model->YahooAction['Error']['Message'].'</div>';
                } else {
                ?>
                <div class="col-md-5 notranslate">
                    <div class="row product-bx">
                        <div class="col-md-12 col-sm-12 bx-slid  wow fadeInUp white-bg bxsliderWrapper">
                            <ul id="bxslider-de">
                                <?php
                                    if(
                                            isset($model->YahooAction['ResultSet']['Result']['Img']['Image1']) &&
                                            !empty($model->YahooAction['ResultSet']['Result']['Img']['Image1']))
                                    {
                                    foreach ($model->YahooAction['ResultSet']['Result']['Img'] as $key => $value) {
                                        if (strpos($value, "https://") !== false) {
                                            ?>
                                            <li><a href="<?=$value; ?>" data-lightbox="example-set"><img src="<?=$value; ?>"></a></li>
                                        <?php
                                        }
                                        // You can access $value or create a new array based off these values
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-12 col-sm-12  wow fadeInUp">
                            <div class="bxpager-slid">
                                <ul id="bxslider-pager">
                                    <?php
                                    $i =0;
                                    if(
                                        isset($model->YahooAction['ResultSet']['Result']['Img']['Image1']) &&
                                        !empty($model->YahooAction['ResultSet']['Result']['Img']['Image1']))
									{
                                        foreach ($model->YahooAction['ResultSet']['Result']['Img'] as $key => $value) {
                                            if (strpos($value, "https://") !== false) { ?>
                                                <li data-slideIndex="<?= $i ?>">
                                                    <a><img src="<?= $value; ?>"></a>
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 padding-right-0">
                    <div class="col-md-12">
                    <h2 class="translate" data-short="0"><?=Yii::t('app',$model->YahooAction['ResultSet']['Result']['Title']);?></h2>
                        <div class="clearfix ">
                            <div class="col-lg-8 left-info">
                                <h3 class="heading_bg notranslate"><span><?=Tbase::ShowLbl('ITEM_INFO') ?></span></h3>
                                <div class="">
                                    <table class="table table-striped" >
                                        <tbody><tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('ENDS_IN') ?>:</td>
                                            <td><b><span id="endTime" class="notranslate">
                                                <?php
                                                    $format = new DateTime($model->YahooAction['ResultSet']['Result']['EndTime']);
                                                    echo $format->format('m/d/Y H:i:s A');
                                                ?>(Tokyo)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('CURRENT_TIME') ?>:</td>
                                            <td><b><span id="currentTime" class="notranslate">
                                                <?php
                                                    $format = new DateTime($model->YahooAction['ResultSet']['Result']['StartTime']);
                                                    echo $format->format('m/d/Y H:i:s A');
                                                ?>(Tokyo)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('NUMBER_OF_BIDS') ?>:</td>
                                            <td><b><span id="bidNum" class="notranslate"><?=$model->YahooAction['ResultSet']['Result']['Bids']?></span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('AVAILABLE') ?>:</td>
                                            <td><b><span id="bidQty" class="notranslate"><?=$model->YahooAction['ResultSet']['Result']['AvailableQuantity']?></span></b> pcs</td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('LEADING_BIDDER') ?>:</td>
                                            <td><b>
                                                    <span id="currentWinner" class="notranslate">
                                                        <?php
                                                        $msg = '';
                                                        if (
                                                                isset($model->YahooAction['ResultSet']['Result']['HighestBidders'])  &&
                                                                isset($model->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'])
                                                        ) {
                                                            if (!Yii::$app->user->isGuest) {
                                                                if (User::_isUserBidOnAuction($model->YahooAction['ResultSet']['Result']['AuctionID'])) {
                                                                    if ($model->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'] == 'J*v*A***'  && User::_isUserTopBidder($model->YahooAction['ResultSet']['Result']['AuctionID']) ) {
                                                                        $msg = '<span style="color:green">'.TBase::_x('YOU_ARE_THE_HIGHEST_BIDDER').'</span>';
                                                                    } else {
                                                                        $msg = '<span style="color:red">'.TBase::_x('YOU_ARE_THE_OUT_BIDDER').'</span>';
                                                                    }
                                                                }
                                                            } else {
                                                                echo $model->YahooAction['ResultSet']['Result']['HighestBidders']['Bidder']['Id'];
                                                            }
                                                        }
                                                        ?>
                                                    </span>
                                                    <?=$msg;?>
                                                <br/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('VAT') ?>:</td>
                                            <td>
                                                <b>
                                                    <span id="lblTax" class="notranslate">
                                                        <?=(isset($model->YahooAction['ResultSet']['Result']['TaxRate'])) ?
                                                            $model->YahooAction['ResultSet']['Result']['TaxRate'] : 0;
                                                        ?>%
                                                    </span>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('RETURN_GOODS') ?>:</td>
                                            <td><b>
                                                    <span id="lblReturnable" class="notranslate"><?php
                                                        if (isset($model->YahooAction['ResultSet']['Result']['ItemReturnable']['Allowed'])) {
                                                            echo TBase::ShowLbl('POSSIBLE');
                                                        } else {
                                                            echo TBase::ShowLbl('IMPOSSIBLE');
                                                        }
                                                        ?></span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('ITEM_CONDITION') ?>:</td>
                                            <td>
                                                <b>
                                                    <span id="lblItemStatus">
                                                        <?=(isset($model->YahooAction['ResultSet']['Result']['ItemStatus']['Condition'])) ?
                                                            $model->YahooAction['ResultSet']['Result']['ItemStatus']['Condition'] :
                                                            '-';
                                                        ?>
                                                    </span>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('SHIPPING_WITHIN_JAPAN') ?>:</td>
                                            <td><b><span id="lblChargeForShipping" class="notranslate">
                                                <?php
                                                if (isset($model->YahooAction['ResultSet']['Result']['Option']['FreeshippingIcon'])) {
                                                    echo TBase::ShowLbl('FREE_SHIPPING');
                                                } else {
                                                    echo TBase::ShowLbl('NO_FREE_SHIPPING');
                                                }
                                            ?></span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('AUCTION_ID') ?>:</td>
                                            <td><b><span id="txtitemCode" itemprop="productID" class="notranslate">
                                                    <a target="_blank" href="<?=$model->YahooAction['ResultSet']['Result']['AuctionItemUrl']?>"><?=$model->YahooAction['ResultSet']['Result']['AuctionID']?></a>
                                                </span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('AUTOMATIC_EXTENSION') ?></td>
                                            <td><b><span id="lblIsAutomaticExtension" class="notranslate"><?=($model->YahooAction['ResultSet']['Result']['IsAutomaticExtension']) ? TBase::ShowLbl('YES') : TBase::ShowLbl('NO')?></span></b></td>
                                        </tr>
                                        <tr>
                                            <td class="notranslate"><?=TBase::ShowLbl('SALES_AREA') ?>:</td>
                                            <td><b><span id="lblIsAutomaticExtension">
                                                    <a target="_blank" href="https://maps.google.com/maps?q=<?=$model->YahooAction['ResultSet']['Result']['Location']?>"><?=$model->YahooAction['ResultSet']['Result']['Location']?></a>
                                            </span></b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix col-lg-4 left-info padding-right-0 notranslate">
	                            <?php if (isset($model->YahooAction['ResultSet']['Result']['Price']) ) {

	                                if ((isset($model->YahooAction['ResultSet']['Result']['Bidorbuy']) &&
	                                     ($model->YahooAction['ResultSet']['Result']['Bidorbuy'] == $model->YahooAction['ResultSet']['Result']['Price'])
	                                )) {
		                                echo "";
	                                } else {
	                                ?>
                                <h3 style="background: none; text-align: center;" class=" heading_bg texbidNowt-center notranslate">
                                    <span style="padding: 0;">
                                        <?=TBase::ShowLbl('CURRENT_PRICE') ?>
                                    </span>
                                </h3>

                                <div class="col-md-12 text-center clearfix " style="margin-top: 11px;">
                                    <div class="col-xs-12">
                                        <?php
                                        if (
                                                isset($model->YahooAction['ResultSet']['Result']['Price'])
                                        ) { ?>
                                            <label style="color:#2a6496;font-size: 19px;font-weight: normal;"><?=$currencyModel->convert($model->YahooAction['ResultSet']['Result']['Price'],'JPY','JPY');?></label>
                                            &nbsp;(~<?=$currencyModel->convert($model->YahooAction['ResultSet']['Result']['Price'],'JPY','USD');?>)<br/>
                                            (<?=(isset($model->YahooAction['ResultSet']['Result']['TaxinPrice'])) ? $currencyModel->convert($model->YahooAction['ResultSet']['Result']['TaxinPrice'],'JPY','JPY') : $currencyModel->convert($model->YahooAction['ResultSet']['Result']['Price'],'JPY','JPY') ?> <?=Tbase::ShowLbl('INCLUDING_TAX')?>)
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <?php if (
                                            !Yii::$app->user->isGuest &&
                                            isset($model->YahooAction['ResultSet']['Result']['Status']) &&
                                            $model->YahooAction['ResultSet']['Result']['Status'] == 'open'
                                    ) { ?>
                                    <form action="<?=\yii\helpers\Url::to(['yahoo-auctions/auction-bid-request'],true) ?>" method="GET">
                                    <?php } ?>
                                    <div class="col-xs-12">
                                    <?php if (
                                        isset($model->YahooAction['ResultSet']['Result']['Status']) &&
                                        $model->YahooAction['ResultSet']['Result']['Status'] == 'open'

                                    ) { ?>
                                        <input type="number" min="0" name="bid" id="bid" value="" placeholder="<?=TBase::ShowLbl('ADD_YOUR_BID') ?>" style="font-size:18px;width: 100%;padding: 6px;text-align: center;border-radius: 5px;border: 2px solid #df7b2b;margin-top: 11px;" <?=(Yii::$app->user->isGuest) ? 'disabled=disabled' : '';?>>
                                    <?php } ?>
                                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                        <input type="hidden" name="auction_id" value="<?=$model->YahooAction['ResultSet']['Result']['AuctionID']?>" />
                                        <input type="hidden" name="auction_end" value="<?=$model->YahooAction['ResultSet']['Result']['EndTime']?>" />
                                    </div>
                                        <?php if (
                                            isset($model->YahooAction['ResultSet']['Result']['Status']) &&
                                            $model->YahooAction['ResultSet']['Result']['Status'] == 'open'
                                        ) { ?>
                                            <div class="col-xs-12">
                                                <?php if (
                                                    !Yii::$app->user->isGuest
                                                ) { ?>
                                                    <button id="bidNow" style="width: 142px;margin-top: 12px;background: #df7b2b;color: #fff;" class="bid-now-btn btn btn_shadow"><i class="fa fa-gavel"></i>&nbsp;<span class="txt"><?=TBase::ShowLbl('PLACE_BID') ?></span></button>
                                                <?php } else { ?>
                                                    <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>">
                                                        <button id="bidNow" style="width: 142px;margin-top: 12px;background: #df7b2b;color: #fff;" class="btn btn_shadow"><i class="fa fa-gavel"></i>
                                                            <?=TBase::ShowLbl('LOGIN') ?>
                                                        </button>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php if (!Yii::$app->user->isGuest) { ?>
                                        </form>
                                    <?php } ?>
                                </div>
                                    <?php }
	                            } ?>

                                <?php if (isset($model->YahooAction['ResultSet']['Result']['Bidorbuy'])) { ?>

                                    <div class="clearfix col-md-12 text-center" style="margin-top: 11px;">
                                        <h3 style="    padding-right: 0px;color: #2a6496;background: none;font-size: 14px;" class="text-center notranslate"><?=TBase::ShowLbl('BUY_NOW_PRICE') ?></h3>
                                        <div class="col-lg-12 text-center">
                                            <label style="color:#2a6496;font-size: 19px;font-weight: normal;">
                                                <?=$currencyModel->convert($model->YahooAction['ResultSet']['Result']['Bidorbuy'],'JPY','JPY')?></label>
                                            (~<?=$currencyModel->convert($model->YahooAction['ResultSet']['Result']['Bidorbuy'],'JPY','USD');?>)
                                            (<?=(isset($model->YahooAction['ResultSet']['Result']['TaxinBidorbuy'])) ? $currencyModel->convert($model->YahooAction['ResultSet']['Result']['TaxinBidorbuy'],'JPY','JPY') : $currencyModel->convert($model->YahooAction['ResultSet']['Result']['Bidorbuy'],'JPY','JPY') ?> <?=Tbase::ShowLbl('INCLUDING_TAX')?>)
                                        </div>
                                    <?php
                                    if (
                                        isset($model->YahooAction['ResultSet']['Result']['Status']) &&
                                        $model->YahooAction['ResultSet']['Result']['Status'] == 'open') { ?>

                                        <div class="col-xs-12">
                                            <?php if (!Yii::$app->user->isGuest) { ?>
                                                <a href="<?=\yii\helpers\Url::to(['yahoo-auctions/buy-out-mail','auction_id'=>$model->YahooAction['ResultSet']['Result']['AuctionID']])?>" id="bidNow" style="width: 142px;margin-top: 12px;background: #2a6496;color: #fff;" class="btn btn_shadow buy-now-btn"><i class="fa fa-gavel"></i>&nbsp;&nbsp;<span class="txt"><?=TBase::ShowLbl('BUY_NOW') ?></span></a>
                                            <?php } else { ?>
                                                <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>" id="bidNow" style="width: 142px;margin-top: 12px;background: #2a6496;color: #fff;" class="btn btn_shadow">
                                                    <?=TBase::ShowLbl('LOGIN')?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>

                                <div class="col-lg-12 text-center remain-time">
                                    <h3 style="padding-right: 0px;color: #782f4a;font-size:15px;margin-top:33px;background: none;" class="text-center notranslate"><?=TBase::ShowLbl('REMAINING_TIME') ?></h3>

                                    <?php
                                    if ($model->YahooAction['ResultSet']['Result']['Status'] != 'closed') {
                                        $now = new DateTime();
                                        $future_date = new DateTime($model->YahooAction['ResultSet']['Result']['EndTime']);
                                        $interval = $future_date->diff($now); ?>
                                        <label style="font-size: 16px;color: #782f4a;" class="class-clock-result" id="class-clock-result">
                                            <?=$interval->format("%a d: %h h: %i m: %s s");?>
                                        </label>
                                    <?php } else {
	                                    echo "<h3 class='text-center'>".Tbase::ShowLbl('AUCTION_IS_CLOSED')."</h3>";
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <hr>
                    <ul class="seller-ul notranslate">
                        <li style="word-wrap: break-word;width:29%!important">
                            <h5><?=TBase::ShowLbl('SELLER_ITEMS'); ?></h5>
                            <h4><?=Html::a($model->YahooAction['ResultSet']['Result']['Seller']['Id'],['yahoo-auctions/seller-list','sellerID'=>$model->YahooAction['ResultSet']['Result']['Seller']['Id']])?></h4>
                        </li>
                        <li class="width-25-percent"><h6><a style="color:#782f4a;" href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'yahoo_auction','code'=>$model->YahooAction['ResultSet']['Result']['AuctionID']])?>"><img src="/tnk/images/wishlist.png" alt="wishlist" /> <?=Tbase::ShowLbl('ADD_TO_WISHLIST'); ?></a></h6></li>
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <li class="width-32-percent"><textarea class="form-control" placeholder="<?=TBase::ShowLbl('PLEASE_LOGIN_TO_SEND_MESSAGE')?>"></textarea></li>
                            <li>
                                <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>">
                                    <button class="btn btn-post">
                                        <?=TBase::ShowLbl('LOGIN')?>
                                    </button>
                                </a>
                            </li>
                        <?php } else { ?>
                            <?=Html::hiddenInput('mail-path',\yii\helpers\Url::to(['site/send-mail']),['id'=>'mail-path'])?>
                            <li style="width: 27%!important;"><textarea class="form-control" id="msg" placeholder="<?=Tbase::ShowLbl('COMMENTS'); ?>"></textarea></li>
                            <li><button id="send-mail" class="btn btn-post"><?=Tbase::ShowLbl('POST');?></button></li>
                        <?php }?>
                    </ul>
                </div>
                </div>
                <div class="col-md-12 how-much1 position-relative notranslate shipping-tabs">
                    <div class="how-much position-relative" id="shipping-horizontalTab">
                        <div class="absolute"><?=Tbase::ShowLbl('PLEASE_WAIT');?></div>
                        <h4><?=Tbase::ShowLbl('HOW_MUCH_WILL_IT_COST'); ?></h4>
                        <div class="tabs clearfix position-relative">
                            <div class="pull-left first-tab"><?=Tbase::ShowLbl('SHIPPING_TAB_1'); ?></div>
                            <div class="pull-left">
                                <ul class="resp-tabs-list">
                                    <li><?=Tbase::ShowLbl('SHIPPING_TAB_2'); ?></li>
                                    <li><?=Tbase::ShowLbl('SHIPPING_TAB_3'); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="position-absolute border"></div>
                        <div class="resp-tabs-container">
                            <div class="clearfix">
                                <input type="hidden" name="quantity" class="quantity_class" id="quantity_class" value="1">
                                <form class="shipping_calculator" id="shipping_calculator">
                                    <ul class="select-box">
                                        <li><input type="text" class="form-control form-control1" name="package_weight" id="package_weight" placeholder="<?=Tbase::ShowLbl('PACKAGE_WEIGHT'); ?>" /></li>
                                        <li><?php echo Html::dropDownList('country_id', null, $countries,['class'=>'form-control','prompt'=>Tbase::ShowLbl('SHIP_TO')]) ; ?></li>
                                        <li><?php echo Html::dropDownList('language_id', 'jpy', $language,['class'=>'form-control','prompt'=>Tbase::ShowLbl('CURRENCY')]) ; ?></li>
                                        <li> <button class="btn btn-cart1" id="calculate-shipping"><i class="fa fa-search" aria-hidden="true"></i></button></li>
                                    </ul>
                                    <input type="hidden" name="price" id="price" value="<?=$model->YahooAction['ResultSet']['Result']['Price']?>">
                                    <input type="hidden" name="tanuki_charge" id="tanuki_charge" value="<?=$tanukiCharges?>">
                                </form>
                                <p><?=Tbase::ShowLbl('THE_LIMIT_OF_WEIGHT_OF_PACKAGE_IS_30KG');?></p>
                                <div class="no-shipping-msg" style="display: none;color:#df7b2b;">
                                </div>
                                <ul class="item-price hidden" id="item-shipping-calculation">
                                    <li><strong><?=Tbase::ShowLbl('PRICE'); ?>:</strong></span></li>
                                    <li><strong><?=Tbase::ShowLbl('TANUKI_FEE'); ?>: </strong>¥ 300</li>
                                    <li><strong><?=Tbase::ShowLbl('INTERNATIONAL_SHIPPING'); ?>: </strong> ¥<label class="shipping_charge"></label></li>
                                    <li><a href="javascript:void(0)"><strong><?=Tbase::ShowLbl('TOTAL'); ?>: </strong>¥<label class="total_cost"></label> </a></li>
                                </ul>
                            </div>

                            <div class="clearfix" style="display: none;">
                                <?php
                                if (TBase::CLang() == 'en-US') {?>
                                    <?=Tbase::ShowLbl('Calculator-Description')?>
                                <?php } else { ?>
                                    <form class="shipping_calculator-category" id="calculate-shipping-form">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-4"><label class="lbbl"><?=Tbase::ShowLbl('PRICE')?>:</label></div>
                                            <div class="col-md-8">    <input type="text" class="form-control form-control1 form-control2" name="package_weight" id="package_weight" value="<?=number_format($model->YahooAction['ResultSet']['Result']['Price'],0)?>" readonly="readonly"/>
                                                &nbsp; <span style= "top: 7px !important;position: absolute;">JPY</span></div>
                                            <div class="col-md-4"><label class="lbbl"><?=Tbase::ShowLbl('Shipping_Category')?>:</label></div>
                                            <div class="col-md-8">
                                                <?php echo Html::dropDownList('shipping_category', 'jpy', $ShippingCategory,['class'=>'form-control form-control1 form-control2','prompt'=>Tbase::ShowLbl('shipping_category'), 'onchange'=>'
                                        $.get( "'.Url::toRoute('site/get-box').'", { id: $(this).val()  } )
                                        .done(function( data ) {
                                            $( "#weightBase" ).html( data );
                                        }
                                    );']) ; ?>
                                            </div>
                                            <div id="weightBase"></div>

                                            <div class="col-md-4">
                                                <label class="lbbl">
                                                    <?=Tbase::ShowLbl('SHIPPING_COUNTRY')?>:
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <?php echo Html::dropDownList('country_id', null, $ShippingCountries,['class'=>'form-control form-control1 form-control2','prompt'=>Tbase::ShowLbl('SHIP_TO')]) ; ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="lbbl"><?=Tbase::ShowLbl('select_options')?>:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="radio" class=" form-radio"  name="option" id="oprion1" value="1" />
                                                <label for="oprion1"><?=Tbase::ShowLbl('Pickup_from_warehouse')?></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4">&nbsp;</div>
                                            <div class="col-md-8">
                                                <input type="radio" class="form-radio" name="option" id="oprion2" value="2" />
                                                <label for="oprion2"><?=Tbase::ShowLbl('Delivery_to_TC')?></label>
                                            </div>
                                            <div class="col-md-4">&nbsp;</div>
                                            <div class="col-md-8"><br />
                                                <button class="btn btn-cart2 col-md-8" id="calculate-shipping-category">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="price" id="price" value="<?=$model->YahooAction['ResultSet']['Result']['Price']?>">
                                        <input type="hidden" name="tanuki_charge" id="tanuki_charge" value="300">
                                    </form>
                                    <div class="clearfix">
                                        <div class="no-shipping-msg" style="display: none;color:#df7b2b;"></div>
                                        <ul class="item-price hidden" id="item-shipping-calculation_category"></ul>
                                    </div>
                                <?php  }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <?php if (!isset($model->YahooAction['Error']) && $model->YahooAction['ResultSet']['Result']['Status'] != 'closed') { ?>
        <div class="product-tabs">
            <div id="horizontalTab">
                <ul class="resp-tabs-list notranslate">
                    <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><?=Tbase::ShowLbl('DESCRIPTION');?></li>
                    <li class="resp-tab-item" aria-controls="tab_item-1" role="tab">
                        <?=Html::a(TBase::ShowLbl('APPROXIMATE_WEIGHT_OF_DIFFERENT_GOODS'),['site/page','slug'=>\common\models\Page::getSlug(8)],['class'=>'tab_link'])?>
                    </li>
                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab">
                        <?=Html::a(TBase::ShowLbl('JAPANESE_CLOTHING_AND_SHOE_SIZES'),['site/page','slug'=>\common\models\Page::getSlug(9)],['class'=>'tab_link'])?>
                    </li>
                </ul>

                <div class="resp-tabs-container">
                    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><?=Tbase::ShowLbl('DESCRIPTION');?></h2>
                    <div data-short="0" id="product_description" class="resp-tab-content resp-tab-content-active " aria-labelledby="tab_item-0">
                        <?=$model->YahooAction['ResultSet']['Result']['Description']?>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-similer">
            <h3 class="heading_bg"><span><?=Tbase::ShowLbl('OTHER_SIMILAR_PRODUCTS');?></span></h3>
            <div class="row">
                <?php
                $category = false;
                if (isset($_REQUEST['cid'])) {
                    $category = $_REQUEST['cid'];
                }
                if ($category) {

                    $SModel  = new \common\models\YahooAuctions();
	                $rand = rand(1,10);
                    $similar_result = $SModel->productsListing(['category'=>$category,'page'=>$rand]);

                    if (
                        count($similar_result) >0 &&
                        isset($similar_result['Result']) &&
                        isset($similar_result['Result']['Item'])
                    ) {
                        $i=1;
                        foreach ($similar_result['Result']['Item'] as $sproduct) {


                            if ($i > 4) {
                                continue;
                            }

                            if (!isset($sproduct['Title'])) {
                                continue;
                            }
                            if (isset($sproduct['Image'])) {
                                $url = $sproduct['Image'];
                            }
                            ?>
                            <a class="no-hover" href="<?= \yii\helpers\Url::to(['yahoo-auctions/detail','id'=>$sproduct['AuctionID'],'cid'=>$category])?>">
                                <div class="col-md-3 col-sm-6">
                                    <div class="other-image"><img src="<?=$url; ?>"></div>
                                    <div class="other-para"><p style="display: block;height: 83px;overflow: hidden;" class="translate"><?=$sproduct['Title'] ?></p>
                                        <?php if (isset($sproduct['CurrentPrice'])) { ?>
                                            <h6><a class="no-hover">¥<?=number_format($sproduct['CurrentPrice']);?></a></h6>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>
                            <?php
	                        $i++;
                        }
                    }
                }
                ?>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<?=$this->registerCss('
.form-control1{
margin-left:0px!important;
margin-bottom:0px!important;
}
#quantity_class{
    color: #000;font-size: 17px;text-align: center;padding-left: 17px;
}
table.info th{width:50%;}table.info td{text-align:center;text-transform: capitalize;}
a.amount{text-decoration:none;}
.info{margin-top:7px;}
div.left-info{padding-left:0px;}
div.left-info table{    margin-top: 0px;}
div.right-info{text-align: center;padding: 28px;background:#f9f9f9;}
div.right-info ul.smartpay li {margin-right:0px;}
header .badge { z-index: 2222;}
');?>
<?=$this->registerCssFile('@web/tnk/css/lightbox.min.css');?>
<?=$this->registerJsFile('@web/tnk/js/lightbox-plus-jquery.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/tnk/js/modernizr-2.6.2.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/js/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/js/moment-with-locales.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/js/moment-timezone-with-data-2010-2020.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>

<?php
$js = "
var x = $('*').filter(function () { 
return this.style.position == 'fixed' 
}).css('position', 'static');

$('.bid-now-btn').on('click',function(){
    $(this).addClass('no-bg');
    $(this).children('span').html('".\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')."');
})

$('.buy-now-btn').on('click',function(){
  $(this).addClass('no-bg');
    $(this).children('span').html('".\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')."');
})";

if (isset($model->YahooAction['ResultSet']) && $model->YahooAction['ResultSet']['Result']['Status'] != 'closed') {
	$now = new DateTime();
	$data = new DateTime($model->YahooAction['ResultSet']['Result']['EndTime']);
	$js .= '
	var dateNow = moment.tz("' . $data->format( "Y-m-d H:i:s" ) . '", "Asia/Tokyo");
	jQuery("#class-clock-result")
  .countdown(dateNow.toDate(), function(event) {
    jQuery(this).text(
      event.strftime("%D d: %H h: %M m: %S s")
    );
  }).on("finish.countdown", function(event) {
  jQuery(this).text("Action Ended");
});
';
}
$this->registerJs($js);
?>

