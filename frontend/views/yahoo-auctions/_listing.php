<?php
$model = new \common\components\TCurrencyConvertor;
use \common\components\TBase as LBL;
$cnt = 0;
$j = 1;
if (isset($data['Result']['Item']) && count($data['Result']['Item'])>2) {
    foreach($data['Result']['Item'] as $current){
        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
        ?>

        <div class="col-sm-3 <?=$j?>">
            <div class="product-lst position-relative">
                <div class="loading hidden">
                    <?=LBL::_x('PLEASE_WAIT'); ?>
                </div>
                <?php if ($category) { ?>
                <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>">
                    <?php } else { ?>
                    <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>">
                        <?php } ?>
                        <div style="height: 150px;overflow: hidden;">
                            <?=Yii::$app->formatter->asImage($current['Image'],['class'=>"img-thumbnail"])?>
                        </div>
                        <div class="product-list-product translate"><?=$current['Title']; ?></div>
                    </a>
                    <h6>
                        <div class="clearfix">
                            <div class="col-md-6 text-center" style="border-right: 1px solid #dedede;">
	                            <?=LBL::_x('CURRENT_PRICE')?><br/>
                                <?php if (isset($current['CurrentPrice'])) { ?>
                                    <a class="amount" href="javascript:void(0)"
                                       data-usd="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','USD'); ?>"
                                       data-rub="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','RUB'); ?>"
                                       data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>"
                                       data-cny="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','CNY'); ?>"
                                       data-eur="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','EUR'); ?>">
		                                <?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>
                                    </a>
                                <?php } else {
                                    echo '-';
                                }?>
                            </div>
                            <div class="col-md-6 text-center">
	                            <?=LBL::_x('BAYOUT_PRICE')?><br/>
                                <?php if (isset($current['BidOrBuy'])) { ?>
                                    <a class="amount" href="javascript:void(0)"
                                       data-usd="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','USD'); ?>"
                                       data-rub="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','RUB'); ?>"
                                       data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>"
                                       data-cny="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','CNY'); ?>"
                                       data-eur="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','EUR'); ?>">
                                        <?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>
                                    </a>
                                <?php } else {
                                    echo '-';
                                }?>
                            </div>
                        </div>
                    </h6>
                    <div class="cate-list product-icon-listing">
                        <div class="clearfix">
                            <?php if ($category) { ?>
                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>"></a></div>
                            <?php } else { ?>
                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>"></a></div>
                            <?php } ?>
                            <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'yahoo_auction','code'=>$current['AuctionID']],true)?>"></a></div>
                        </div>
                    </div>
            </div>
        </div>
        <?php

        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
        $j++;
        $cnt++;

    } ?>
    </div>
<?php } else {
    echo "0";
    exit;
}
?>