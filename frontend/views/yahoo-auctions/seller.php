<?php
    use \common\components\TBase;
    $model = new \common\components\TCurrencyConvertor;
    $this->title = TBase::ShowLbl('YAHOO_PRODUCT_LISTING');
?>
<div class="m_container rakuten margin-bottom-20 yahoo-auction notranslate">
    <div class="container ">
        <div class="col-md-7 notranslate" style="margin-top: 4px;">
            <ol class="breadcrumb">
                <li><a class="color-back" href="<?=\yii\helpers\Url::to(['yahoo/list']); ?>"><?=TBase::ShowLbl('yahoo_shopping'); ?></a></li>
                <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                    <li><a class="color-back" href="#"><?=$_REQUEST['s']; ?> </a></li>
                <?php } else if ($category){ ?>
                    <li><a class="color-back" href="#"><?=$category->title_en; ?> </a></li>
                <?php } ?>
            </ol>
        </div>
        <div class="col-md-5 right-side-search">
            <form id="w1" action="<?=\yii\helpers\Url::to(['yahoo-auctions/detail'],true) ?>" method="GET">
                <div class="col-md-6 col-xs-10">
                    <label style="font-size: 14px;color:#333"><?=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID') ?> :</label>
                </div>
                <div class="col-md-4 col-xs-10">
                    <input class="form-control" name="id" value="" type="text" placeholder="<?=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID')?>">
                </div>
                <div class="col-md-1 col-xs-10 padding-left-0">
                    <input type="submit" class="yahoo_auction_search_btn search_btn" name="search" value="">
                </div>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            </form>
        </div>
    </div>
</div>
<div class="container notranslate">
    <section class="listing-panel">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-4">
                    <div class="well">
                        <?=$this->render('_left',['category'=>$category,'param'=>$param]); ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-8 product-list-page">
                    <div class="well">
                        <div class="row notranslate">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <ul class="filters" style="padding-left: 0px;margin-top: 7px;">
                                        <li style="padding-left: 0px;"><?=TBase::ShowLbl('SORT_BY');?>:</li>
                                        <li>
                                            <select class="auction-sort-list" id="auction-sort-list" name="auction-sort-list">
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '1')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'cbids','order'=>'a','cid'=>Yii::$app->request->get('cid'),'s'=>1],true);?>">Price: Low to High</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '2')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'cbids','order'=>'d','cid'=>Yii::$app->request->get('cid'),'s'=>2],true);?>">Price: High to Low</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '3')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'bids','order'=>'a','cid'=>Yii::$app->request->get('cid'),'s'=>3],true);?>">No. of Bids: High to Low</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '4')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'bids','order'=>'d','cid'=>Yii::$app->request->get('cid'),'s'=>4],true);?>">No. of Bids: Low to High</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '5')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'end','order'=>'a','cid'=>Yii::$app->request->get('cid'),'s'=>5],true);?>">Time: Ending Soon</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '6')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'end','order'=>'d','cid'=>Yii::$app->request->get('cid'),'s'=>6],true);?>">Time: Newly Listed</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '7')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'bidorbuy','order'=>'a','cid'=>Yii::$app->request->get('cid'),'s'=>7],true);?>">Buyout Price: Low to High</option>
                                                <option <?=(Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '8')) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'bidorbuy','order'=>'d','cid'=>Yii::$app->request->get('cid'),'s'=>8],true);?>">Buyout Price: High to Low</option>
                                                <option <?=((Yii::$app->request->get('s') && (Yii::$app->request->get('s') == '9')) || !isset($_REQUEST['s'])) ? 'selected="selected"' : '';?> value="<?=\yii\helpers\Url::to(['yahoo-auctions/seller-list','sellerID'=>Yii::$app->request->get('sellerID'),'sort'=>'score','ranking'=>'popular','cid'=>Yii::$app->request->get('cid'),'s'=>9],true);?>">Most Popular + Newly Listed</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <ul class="filters currency-filters">
                                        <li><?=TBase::ShowLbl('CURRENCY');?>:</li>
                                        <li><a class="current" id="currency-jpy" data-currency="jpy"><?=TBase::ShowLbl('JAPANESE_YEN');?></a></li>
                                        <li><a id="currency-usd"  data-currency = "usd"><?=TBase::ShowLbl('US_DOLLAR');?></a></li>
                                        <li><a id="currency-rub"  data-currency = "rub"><?=TBase::ShowLbl('RUSSIAN_RUBLE');?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="top_p list-brand">
                                <?php
                                if (
                                        (int)$data['@attributes']['totalResultsReturned'] == 1
                                ) {

                                    $current = $data['Result']['Item'];
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="product-lst position-relative">
                                            <div class="loading hidden">
                                                <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                                            </div>
                                            <?php if ($category) { ?>
                                            <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>">
                                                <?php } else if(isset($current['AuctionID'])){ ?>
                                                <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>">
                                                    <?php } ?>
                                                    <div style="height: 150px;overflow: hidden;">
                                                        <?=Yii::$app->formatter->asImage($current['Image'],['class'=>"img-thumbnail"])?>
                                                    </div>
                                                    <div class="product-list-product translate"><?=$current['Title']; ?></div>
                                                </a>
                                                <h6>
                                                    <div class="clearfix">
                                                        <div class="col-md-6 text-center" style="border-right: 1px solid #dedede;padding: 12px;">
                                                            Current Price<br/>
                                                            <?php if (isset($current['CurrentPrice'])) { ?>
                                                                <a class="amount" href="javascript:void(0)"
                                                                   data-usd="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','USD'); ?>"
                                                                   data-rub="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','RUB'); ?>"
                                                                   data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>">
                                                                    <?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>
                                                                </a>
                                                            <?php } else {
                                                                echo '-';
                                                            }?>
                                                        </div>
                                                        <div class="col-md-6 text-center" style="padding: 12px;">
                                                            Bayout Price<br/>
                                                            <?php if (isset($current['BidOrBuy'])) { ?>
                                                                <a class="amount" href="javascript:void(0)"
                                                                   data-usd="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','USD'); ?>"
                                                                   data-rub="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','RUB'); ?>"
                                                                   data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>">
                                                                    <?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>
                                                                </a>
                                                            <?php } else {
                                                                echo '-';
                                                            }?>
                                                        </div>
                                                    </div>
                                                </h6>
                                                <div class="cate-list product-icon-listing">
                                                    <div class="clearfix">
                                                        <?php if ($category) { ?>
                                                            <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>"></a></div>
                                                        <?php } else { ?>
                                                            <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>"></a></div>
                                                        <?php } ?>
                                                        <div class="pull-left"><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'yahoo_auction','id'=>$current['AuctionID']])?>"></a></div>
                                                        <div class="pull-left no-border"><a class="wish-i" href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'yahoo_auction','id'=>$current['AuctionID']])?>"></a></div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                <?php
                                } else {

                                $cnt = 0;
                                $j = 1;
                                if (isset($data['Result']['Item']) && count($data['Result']['Item'])>1) {
                                foreach($data['Result']['Item'] as $current){

                                        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
                                        ?>

                                        <div class="col-sm-3 <?=$j?>">
                                            <div class="product-lst position-relative">
                                                <div class="loading hidden">
                                                    <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                                                </div>
                                                    <?php if ($category) { ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>">
                                                    <?php } else if(isset($current['AuctionID'])){ ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>">
                                                    <?php } ?>
                                                        <div style="height: 150px;overflow: hidden;">
                                                            <?=Yii::$app->formatter->asImage($current['Image'],['class'=>"img-thumbnail"])?>
                                                        </div>
                                                        <div class="product-list-product translate"><?=$current['Title']; ?></div>
                                                    </a>
                                                    <h6>
                                                        <div class="clearfix">
                                                            <div class="col-md-6 text-center" style="border-right: 1px solid #dedede;padding: 12px;">
                                                                Current Price<br/>
                                                                <?php if (isset($current['CurrentPrice'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                                     data-usd="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','USD'); ?>"
                                                                                     data-rub="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','RUB'); ?>"
                                                                                     data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                            <div class="col-md-6 text-center" style="padding: 12px;">
                                                                Bayout Price<br/>
                                                                <?php if (isset($current['BidOrBuy'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                                    data-usd="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','USD'); ?>"
                                                                                    data-rub="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','RUB'); ?>"
                                                                                    data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                        </div>
                                                    </h6>
                                                    <div class="cate-list product-icon-listing">
                                                        <div class="clearfix">
                                                            <?php if ($category) { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>"></a></div>
                                                            <?php } else { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>"></a></div>
                                                            <?php } ?>
                                                            <div class="pull-left"><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'yahoo_auction','id'=>$current['AuctionID']])?>"></a></div>
                                                            <div class="pull-left no-border"><a class="wish-i" href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'yahoo_auction','id'=>$current['AuctionID']])?>"></a></div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <?php

                                        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
                                        $j++;
                                        $cnt++;

                                    } ?>
                                    </div>
                                <?php }
                                    if ($j <= 2) {
                                        echo "<div class='text-center'>".TBase::ShowLbl('NO_PRODUCT_FOUND')."</div>";
                                    }
                                }
                                ?>
                        </div>
                        <hr>
                    <?php if (isset($data['Result']['Item']) && count($data['Result']['Item'])> 15) { ?>
                        <div class="row">
                            <div class="col-md-12 notranslate">
                                <div class="text-center">
                                    <a href="#" class="product-load-more" data-shop="yahoo_auction"> <?=TBase::ShowLbl('OPEN_MORE_PRODUCTS')?> </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
<?=$this->registerJS('
var ajax_url = "'.\yii\helpers\Url::to(['yahoo-auctions/seller-list']).'";
',yii\web\View::POS_BEGIN)?>
<div class="loading-extend" style="display: none;"></div>
<div class="loading-image" style="display: none;"><?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?></div>