<?php
    use yii\widgets\ActiveForm;
    use common\components\TBase;
    use \common\components\TBase as LBL;
?>
<style>
    button.accordion{background:0 0;color:#444;cursor:pointer;width:100%;border:none;text-align:left;outline:0;font-size:15px;transition:.4s;padding:7px 4px}button.accordion.active,button.accordion:hover{background-color:#ddd}button.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}button.accordion.active:after{content:"\2212"}div.panel{padding:0 6px 0 10px;max-height:0;overflow:hidden;transition:max-height .2s ease-out}
</style>
    <h4 class="translate">
        <?=LBL::_x('CATEGORIES'); ?>
    </h4>
    <div class="panel-group left-side-bar translate" id="accordion">
        <label>
            <?php
            if (Yii::$app->request->get('cid')) {
            $model = new \common\models\YahooAuctions();
            $categoryData = $model->getCategories(Yii::$app->request->get('cid'));

            if ($categoryData['Result']) {
                echo $categoryData['Result']['CategoryName'];
            }
            ?>
        </label>
    <hr>
        <?php
        if (isset($categoryData['Result'])) {
            $data = $categoryData;
            if (isset($data['Result']['ChildCategory'])) {
                foreach ($data['Result']['ChildCategory'] as $key => $category) {
                    ?>
                    <button class="accordion"><?= $category['CategoryName'] ?></button>
                    <div class="panel">
                        <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $category['CategoryId']]) ?>">
                            <?= $category['CategoryName']; ?> (All)<br/>
                        </a>
                        <?php
                        $subData = $model->getCategories($category['CategoryId']);
                        if (isset($subData['Result']['ChildCategory'])) {
                            foreach ($subData['Result']['ChildCategory'] as $key => $subcategory) {
                                if (isset($subcategory['CategoryId'])) {
	                                ?>
                                    <a href="<?= \yii\helpers\Url::to( [
		                                Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
		                                'cid' => $subcategory['CategoryId']
	                                ] ) ?>">
		                                <?= $subcategory['CategoryName']; ?>
                                    </a><br/>
	                                <?php
                                }
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
            } else if (isset($data['Result'])) { ?>
                <a>
                    <?= $data['Result']['CategoryName']; ?> (All)
                </a>
            <?php }
        }
        } else {
            $data = \common\models\Category::findAll(['shop'=>'yahoo_auctions','parent_id'=>0]);
            if ($data) {
                foreach ($data as $cat) { ?>
                    <button class="accordion"><?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?></button>
                    <div class="panel">
                        <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $cat->ref_id]) ?>">
                            <?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?> (All)
                        </a>
                        <?php
                        $child = \common\models\Category::findAll(['parent_id' => $cat->id,'shop'=>'yahoo_auctions']); ?>
                        <?php foreach ($child as $childCat) { ?>
                            <div>
                                <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $childCat->ref_id]) ?>">
                                    <?= (\common\components\TBase::CLang() == 'ru-RU') ? $childCat->title_ru : $childCat->title_en; ?>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
            }
        }
        ?>
    </div>

    <h4 class=""><?=LBL::_x('FILTER_BY_PRICE')?></h4>
    <hr>
    <div class="row min-max ">
        <?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl($param),'method' => 'GET']) ?>
        <input type="hidden" name="page" id="page" value="1">
        <div class="col-lg-12 right-pad-zero margin-bottom-7">
	        <?=LBL::_x('CURRENT_PRICE')?><br/><hr/>
            <div class="col-xs-5 right-pad-zero " style="padding-left: 0px">
                <input type="number" class="form-control" value="<?=(isset($param['aucminprice']) && $param['aucminprice']!= '') ? $param['aucminprice'] : ''; ?>" name="aucminprice" placeholder="<?=TBase::ShowLbl('MIN')?>">
            </div>
            <div class="col-xs-5 right-pad-zero " style="padding-left: 0px;margin-left: 27px;">
                <input type="number" class="form-control" value="<?=(isset($param['aucmaxprice']) && $param['aucmaxprice']!= '') ? $param['aucmaxprice'] : ''; ?>" name="aucmaxprice" placeholder="<?=TBase::ShowLbl('MAX')?>">
            </div>
        </div>

        <div class="col-lg-12 right-pad-zero margin-bottom-7">
	        <?=LBL::_x('BAYOUT_PRICE')?><br/><hr/>
            <div class="col-xs-5 right-pad-zero " style="padding-left: 0px">
                <input type="number" class="form-control" value=" <?=(isset($param['aucmin_bidorbuy_price']) && $param['aucmin_bidorbuy_price']!= '') ? $param['aucmin_bidorbuy_price'] : ''; ?>" name="aucmin_bidorbuy_price" placeholder="<?=TBase::ShowLbl('MIN')?>">
            </div>
            <div class="col-xs-5 right-pad-zero " style="padding-left: 0px;margin-left: 27px;">
                <input type="number" class="form-control" value="<?=(isset($param['aucmax_bidorbuy_price']) && $param['aucmax_bidorbuy_price']!= '') ? $param['aucmax_bidorbuy_price'] : ''; ?>" name="aucmax_bidorbuy_price" placeholder="<?=TBase::ShowLbl('MAX')?>">
            </div>
        </div>

        <div class="col-lg-12 right-pad-zero margin-bottom-7" style="margin-top: 15px;">
            <?=LBL::_x('ITEM_CONDITION')?><br/><hr/>
            <div class="col-xs-12 right-pad-zero " style="padding-left: 0px;">
                <input type="radio" value="0" name="item_status" <?=(!isset($param['item_status'])) ? 'checked="checked"' : ''; ?>"> <?=LBL::_x('ALL_CATEGORIES'); ?><br/>
                <input type="radio" value="1" name="item_status" <?=(isset($param['item_status']) && $param['item_status'] == '1') ? 'checked="checked"' : ''; ?> ><?=LBL::_x('NEW_ITEM');?><br/>
                <input type="radio" value="2" name="item_status" <?=(isset($param['item_status']) && $param['item_status'] == '2') ? 'checked="checked"' : ''; ?>> <?=LBL::_x('USED_ITEM')?>
            </div>
        </div>

        <div class="col-lg-12 right-pad-zero margin-bottom-7" style="margin-top: 15px;">
            <?=LBL::_x('SELLER')?><br/><hr/>
            <div class="col-xs-12 right-pad-zero" style="padding-left: 0px;">
                <input type="radio" value="0" name="store" <?=(!isset($param['store'])) ? 'checked="checked"' : ''; ?>><?=LBL::_x('ALL_CATEGORIES'); ?><br/>
                <input type="radio" value="1" name="store" <?=(isset($param['store']) && $param['store'] == '1') ? 'checked="checked"' : ''; ?>><?=LBL::_x('STORE');?><br/>
                <input type="radio" value="2" name="store" <?=(isset($param['store']) && $param['store'] == '2') ? 'checked="checked"' : ''; ?>><?=LBL::_x('INDIVIDUAL_SELLER');?>
            </div>
        </div>

        <div class="col-lg-12 right-pad-zero margin-bottom-7" style="margin-top: 15px;">
            <?=LBL::_x('OTHER_OPTIONS');?><br/><hr/>
            <div class="col-xs-12 right-pad-zero" style="padding-left: 0px;">
                <input type="checkbox" name="freeshipping" value="1" <?=(isset($param['freeshipping']) && $param['freeshipping'] == '1') ? 'checked="checked"' : ''; ?>> <?=LBL::_x('FREE_DELIVERY_WITHIN_JAPAN'); ?><br/>
                <input type="checkbox" name="buynow" value="1" <?=(isset($param['buynow']) && $param['buynow'] == '1') ? 'checked="checked"' : ''; ?>> <?=LBL::_x('BAYOUT_PRICE')?><br/>
            </div>
        </div>

        <input type="submit" name="filter" value="<?=LBL::_x('FILTER')?>" class="btn filter-btn">
        <?php ActiveForm::end() ?>
    </div>
    <div class="clearfix"></div>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
</script>