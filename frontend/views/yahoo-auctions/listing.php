<?php
    use \common\components\TBase;
    use common\components\TBase as LBL;
    $model = new \common\components\TCurrencyConvertor;
?>
<style>
    .breadcrumb > li + li::before {
        color: #333;
        content: " > ";
        padding: 0 3px;
    }
    .product-icon-listing .pull-left {
        width: 50%!important;
    }
	.filter-result-count {
    width: 173px !important;}
</style>
<div class="m_container rakuten margin-bottom-20 yahoo-auction">
    <div class="container ">
        <div class="col-md-9 " style="margin-top: 4px;">
            <ol class="breadcrumb ">
                <li><a class="color-back notranslate" href="<?=\yii\helpers\Url::to(['yahoo-auctions/index']); ?>"><?=TBase::ShowLbl('YAHOO_AUCTION'); ?></a></li>
                <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                    <li><a class="color-back notranslate" href="#"><?=$_REQUEST['s']; ?> </a></li>
                <?php } else if ($category){ ?>
                    <li><a class="color-back" href="#"><?=$category->title_en; ?> </a></li>
                <?php } ?>
            <?php
            if (isset($data['Result']['CategoryPath'])) {
                $count = count(explode('>',$data['Result']['CategoryPath']));
                $catID = explode(',',$data['Result']['CategoryIdPath']);
                $cats = explode('>',$data['Result']['CategoryPath']);
                for($i=1; $i< $count; $i++){
                    echo "<li class='translate'>".\yii\helpers\Html::a($cats[$i],['yahoo-auctions/list','cid'=>$catID[$i]],['style'=>'color: #333!important']).'</li>';
                }
            }
            ?>

            </ol>

        </div>
        <div class="col-md-3 right-side-search">
            <form id="w1" action="<?=\yii\helpers\Url::to(['yahoo-auctions/detail'],true) ?>" method="GET">
<!--                <div class="col-md-6 col-xs-10">-->
<!--                    <label style="font-size: 14px;color:#333">--><?//=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID') ?><!-- :</label>-->
<!--                </div>-->
                <div class="col-md-10 col-xs-10">
                    <input class="form-control" name="id" value="" type="text" placeholder="<?=TBase::ShowLbl('SEARCH_AUCTION_LOT_ID')?>">
                </div>
                <div class="col-md-2 col-xs-10 padding-left-0">
                    <input type="submit" class="yahoo_auction_search_btn search_btn" name="search" value="">
                </div>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            </form>
        </div>
    </div>
</div>
<div class="container notranslate">
    <section class="listing-panel">
        <div class="container" style="position: relative;">
            <div class="row">
                <div class="col-md-2 col-sm-4 left-bar">
                    <div class="well">
                        <?php echo $this->render('_left',['category'=>$category,'param'=>$param]); ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-8 product-list-page">
                    <div class="well">
                        <div class="row notranslate">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <ul class="filters" style="padding-left: 0px;margin-top: 7px;">
                                        <li style="padding-left: 0px;"><?=TBase::ShowLbl('SORT_BY');?>:</li>
                                        <li style="padding: 0px!important;">

                                            <select class="auction-sort-list" id="auction-sort-list" name="auction-sort-list">
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '1')) ? 'selected="selected"' : '';?> value="<?=TBase::sortBy('cbids','a',1); ?>"><?=LBL::_x('LOW_TO_HIGH')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '2')) ? 'selected="selected"' : '';?> value="<?=TBase::sortBy('cbids','d',2);?>"><?=LBL::_x('HIGH_TO_LOW')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '3')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('bids','a',3);?>"><?=LBL::_x('NO_OF_BIDS_HIGH_TO_LOW')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '4')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('bids','d',4);?>"><?=LBL::_x('NO_OF_BIDS_LOW_TO_HIGH')?></option>
                                                <option <?=((Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '5')) || !isset($_REQUEST['srt'])) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('end','a',5);?>"><?=LBL::_x('TIME_ENDING_SOON')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '6')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('end','d',6);?>"><?=LBL::_x('TIME_NEWLY_LISTED')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '7')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('bidorbuy','a',7);?>"><?=LBL::_x('BUYOUT_PRICE_LOW_TO_HIGH')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '8')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('bidorbuy','d',8);?>"><?=LBL::_x('BUYOUT_PRICE_HIGH_TO_LOW')?></option>
                                                <option <?=(Yii::$app->request->get('srt') && (Yii::$app->request->get('srt') == '9')) ? 'selected="selected"' : '';?> value="<?=Tbase::sortBy('score','popular',9);?>"><?=LBL::_x('MOST_POPULAR_NEWLY_LISTED')?></option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pull-left text-center" style='max-width:100px;'>
                                    <ul class="filters filter-result-count">
                                        <li class="text-center"><?=TBase::ShowLbl('TOTAL_RESULT');?>&nbsp;&nbsp;<label style="color: #782f4a;"><?=(isset($data['@attributes']['totalResultsAvailable'])) ? number_format($data['@attributes']['totalResultsAvailable']) : 0; ?></label></li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <ul class="filters currency-filters">
                                        <li><?=TBase::ShowLbl('CURRENCY');?>:</li>
                                        <li><a class="current" id="currency-jpy" data-currency="jpy"><?=TBase::ShowLbl('JAPANESE_YEN');?></a></li>
                                        <li><a id="currency-usd"  data-currency = "usd"><?=TBase::ShowLbl('US_DOLLAR');?></a></li>
                                        <li><a id="currency-rub"  data-currency = "rub"><?=TBase::ShowLbl('RUSSIAN_RUBLE');?></a></li>
                                        <li><a id="currency-cny"  data-currency = "cny"><?=TBase::ShowLbl('CHINESE_YUAN');?></a></li>
                                        <li><a id="currency-eur"  data-currency = "eur"><?=TBase::ShowLbl('EURO');?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="top_p list-brand">
                                <?php

                                $cnt = 0;
                                $j = 1;

                                if ((isset($data['@attributes']['totalResultsAvailable']) && (int)$data['@attributes']['totalResultsAvailable'] > 0) && (isset($data['Result']['Item']) && count($data['Result']['Item'])>2  && !isset($data['Result']['Item']['AuctionID']))) {
                                    foreach($data['Result']['Item'] as $current){
                                        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
                                        ?>

                                        <div class="col-sm-3 <?=$j?>">
                                            <div class="product-lst position-relative">
                                                <div class="loading hidden">
                                                    <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                                                </div>
                                                    <?php if ($category) { ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>">
                                                    <?php } else { ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>">
                                                        <?php } ?>
                                                        <div style="height: 150px;overflow: hidden;">
                                                            <?=Yii::$app->formatter->asImage($current['Image'],['class'=>"img-thumbnail"])?>
                                                        </div>
                                                        <div class="product-list-product translate"><?=$current['Title']; ?></div>
                                                    </a>
                                                    <h6>
                                                        <div class="clearfix">
                                                            <div class="col-md-6 text-center" style="border-right: 1px solid #dedede;padding: 0px;">
                                                                <?=LBL::_x('CURRENT_PRICE')?><br/>
                                                                <?php if (isset($current['CurrentPrice'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                        data-usd="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','USD'); ?>"
                                                                        data-rub="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','RUB'); ?>"
                                                                        data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>"
                                                                        data-cny="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','CNY'); ?>"
                                                                        data-eur="<?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','EUR'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$current['CurrentPrice'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                            <div class="col-md-6 text-center" style="padding: 0px;">
	                                                            <?=LBL::_x('BAYOUT_PRICE')?><br/>
                                                                <?php if (isset($current['BidOrBuy'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                        data-usd="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','USD'); ?>"
                                                                        data-rub="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','RUB'); ?>"
                                                                        data-jpy="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>"
                                                                        data-cny="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','CNY'); ?>"
                                                                        data-eur="<?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','EUR'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$current['BidOrBuy'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                        </div>
                                                    </h6>
                                                    <div class="cate-list product-icon-listing">
                                                        <div class="clearfix">
                                                            <?php if ($category) { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID'],'cid'=>$category->ref_id])?>"></a></div>
                                                            <?php } else { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$current['AuctionID']])?>"></a></div>
                                                            <?php } ?>
                                                            <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'yahoo_auction','code'=>$current['AuctionID']],true)?>"></a></div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <?php

                                        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
                                        $j++;
                                        $cnt++;

                                    } ?>
                                    </div>
                                <?php }
								else if(isset($data['@attributes']['totalResultsAvailable']) && (int)$data['@attributes']['totalResultsAvailable'] > 0) { ?>
									 <div class="col-sm-3">
                                            <div class="product-lst position-relative">
                                                <div class="loading hidden">
                                                    <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
                                                </div>
                                                    <?php if ($category) { ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$data['Result']['Item']['AuctionID'],'cid'=>$category->ref_id])?>">
                                                    <?php } else { ?>
                                                        <a target="_blank" href="<?=\yii\helpers\Url::to(['detail','id'=>$data['Result']['Item']['AuctionID']])?>">
                                                        <?php } ?>
                                                        <div style="height: 150px;overflow: hidden;">
                                                            <?=Yii::$app->formatter->asImage($data['Result']['Item']['Image'],['class'=>"img-thumbnail"])?>
                                                        </div>
                                                        <div class="product-list-product translate"><?=$data['Result']['Item']['Title']; ?></div>
                                                    </a>
                                                    <h6>
                                                        <div class="clearfix">
                                                            <div class="col-md-6 text-center" style="border-right: 1px solid #dedede;padding: 0px;">
	                                                            <?=LBL::_x('CURRENT_PRICE')?><br/>
                                                                <?php if (isset($data['Result']['Item']['CurrentPrice'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                                     data-usd="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['CurrentPrice'],'JPY','USD'); ?>"
                                                                                     data-rub="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['CurrentPrice'],'JPY','RUB'); ?>"
                                                                                     data-jpy="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['CurrentPrice'],'JPY','JPY'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['CurrentPrice'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                            <div class="col-md-6 text-center" style="padding: 0px;">
                                                                <?=LBL::_x('BAYOUT_PRICE')?><br/>
                                                                <?php if (isset($data['Result']['Item']['BidOrBuy'])) { ?>
                                                                    <a class="amount" href="javascript:void(0)"
                                                                                    data-usd="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['BidOrBuy'],'JPY','USD'); ?>"
                                                                                    data-rub="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['BidOrBuy'],'JPY','RUB'); ?>"
                                                                                    data-jpy="<?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['BidOrBuy'],'JPY','JPY'); ?>">
                                                                        <?=\common\models\CurrencyTable::convert((float)$data['Result']['Item']['BidOrBuy'],'JPY','JPY'); ?>
                                                                    </a>
                                                                <?php } else {
                                                                    echo '-';
                                                                }?>
                                                            </div>
                                                        </div>
                                                    </h6>
                                                    <div class="cate-list product-icon-listing">
                                                        <div class="clearfix">
                                                            <?php if ($category) { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$data['Result']['Item']['AuctionID'],'cid'=>$category->ref_id])?>"></a></div>
                                                            <?php } else { ?>
                                                                <div class="pull-left"><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['detail','id'=>$data['Result']['Item']['AuctionID']])?>"></a></div>
                                                            <?php } ?>
                                                            <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'yahoo_auction','code'=>$data['Result']['Item']['AuctionID']],true)?>"></a></div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
								<?php }
                                if ($j <= 2) {
                                    echo "<div class='text-center'>".TBase::ShowLbl('NO_PRODUCT_FOUND')."</div>";
                                }
                                ?>

                        </div>
                        <hr>
                    <?php if (isset($data['Result']['Item']) && count($data['Result']['Item'])> 15) { ?>
                        <div class="row">
                            <div class="col-md-12 notranslate">
                                <div class="text-center">
                                    <a href="#" class="product-load-more" data-shop="yahoo_auction"> <?=TBase::ShowLbl('OPEN_MORE_PRODUCTS')?> </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


	                <?php if ($category) { ?>
                        <div class="seo-txt">
			                <?=$category->seo_text; ?>
                        </div>
	                <?php } ?>
                    </div>
                </div>
                <!-- slide left side bar div -->
                <div class="col-md-2 scroll-left-side-bar" style="position: absolute; display: none;">
                    <div class="well">
                        <a id="back-to-top" style="margin-top: 10px;width: 90%;" href="" class="btn filter-btn"><?=LBL::_x('GO_TO_FILTER_AREA'); ?></a></div>
                </div>
            </div>
        </div>
    </section>
</div>
<?=$this->registerJS('
var ajax_url = "'.\yii\helpers\Url::to(['yahoo-auctions/list']).'";
',yii\web\View::POS_BEGIN)?>
<div class="loading-extend" style="display: none;"></div>
<div class="loading-image" style="display: none;"><?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?></div>