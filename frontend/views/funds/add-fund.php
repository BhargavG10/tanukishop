<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;
use \common\helper\Tanuki;
use common\components\TBase as LBL;
$CurrencyModel = new \common\components\TCurrencyConvertor;
$TModel = new Tanuki();
$tanukiCharges = \common\components\TBase::TanukiCharges();

$this->title = 'Payment';
$month = ['01'=>'01 (Jan)','02'=>'02 (Feb)','03'=>'03 (March)','04'=>'04 (April)','05'=>'04 (May)','06'=>'06 (June)'];
$month += ['07'=>'07 (July)','08' =>'08 (Aug)','09'=>'09 (Sep)','10'=>'10 (Oct)','11'=>'11 (Nov)','12'=>'12 (Dec)'];
$year_range = range(date('Y'),date('Y')+15);
$year = array_combine($year_range,$year_range);

$this->title = LBL::_x('ADD_FUND');
?>
<style>
    .tab-content a.accordion-link{display: none;} a.accordion-link{color:#fff;} .table tbody td:first-child{width:50%!important;font-weight: bold;} .table tbody tr:last-child{background: gainsboro;} .btn-cart1 {  width: 20% !important;  } #pservices{  display: none;} .add-fund .responsive-tabs-container .nav > li > a {width: 314px!important;}
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
       <form action="" method="GET">
        <div class=" dashboard">
        <?php $form = ActiveForm::begin(); ?>
            <div class="">
                <h3 class="inner-head "><?=LBL::_x('ADD_MONEY_MSG');?></h3>
                <div class="">
                    <?=LBL::_x('INSERT_AMOUNT_MSG');?>
                </div>
				<div class="col-lg-3" >
                <div class="input-group " style="margin-top: 20px;">
                    <span class="input-group-addon">¥</span>
					<?= $form->field($userModel, 'credit',['template' => "{input}"])->textInput(['onkeypress'=>"return isNumber(event)",'style'=>"width: 200px;"])->label(false); ?>
                </div>
				<?= $form->field($userModel, 'credit',['template' => "{error}"]);?>
				</div>
				<div class="col-lg-3" style="margin-top: 20px;margin-left: 12px;">
					<input type='submit' class='btn btn-success' value='<?=LBL::_x('PROCEED');?>' />
				</div>
            </div>
        <?php ActiveForm::end(); ?>
    <?php
    if($fundAmount > 0 ) {
	  $total = $fundAmount;
    ?>
            <div class="dashboard checkout add-fund" >
                <h3 class="inner-head border-b"><?=LBL::_x('PAYMENT')?></h3>
                <div class="col-sm-8 col-xs-7 mt20">
                    <p><?=TBase::TanukiSetting((TBase::CLang() == 'ru-RU') ? 'payment_page_text_russian' : 'payment_page_text_english'); ?></p>
                </div>
                <div class="col-sm-4 col-xs-5"><?=Html::img(yii\helpers\Url::to('@web/tnk/images/ssl.png', true),['alt'=>'ssl img','class'=>"pull-right"]);?></div>
                <div class="clearfix"></div>
                <div class="col-sm-12 ">
                <ul class="nav nav-tabs responsive-tabs notranslate">
                    <li class="active" ><a href="#moneta"><?=LBL::_x('MONETA')?></a></li>
                    <li><a href="#paypal"><?=TBase::ShowLbl('PAYPAL')?></a></li>
<!--                    <li><a href="#Card">--><?//=TBase::ShowLbl('CREDIT_CARD')?><!--</a></li>-->
                    <li><a href="#Bank"><?=TBase::ShowLbl('BANK_TRANSFER')?></a></li>
        
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active clearfix" id="moneta">
		                <?php $data = \common\models\Page::findOne(['slug'=>'moneta']); ?>
                        <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
			                <?php echo (TBase::CLang() == 'ru-RU') ? $data->detail_ru : $data->detail_en ?>
                        </div>
		                <?php
		                $paypalCharges = 0;
		                $totalCharges = $total;

		                ?>
                        <div class="col-md-7">
                            <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL')?></div>
						                <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
						                <?=$CurrencyModel->convert($paypalCharges,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div> <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-5">
                            <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                            <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('MONETA_REDIRECTED_MSG');?></p>
                            <div class="panel-body" style="padding: 0px;">
                                <div class="paypal-payment payment-div">
                                    <div class="clearfix">
                                        <div class="col-md-offset-3 col-md-6">
							                <?=Html::a(TBase::ShowLbl('PAY'),['fund-payment/payment-method-selection','_payment_method'=>'moneta-payment','fund_amount'=>$total],['class'=>'btn btn-default paysubmit','style'=>'margin:0px!important;width:100%!important']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane clearfix" id="paypal">
	                    <?php

	                    $paypalCharges =  (($total * 0.039 + 40 ) * 1.0402);
	                    $totalCharges = ($paypalCharges + $total)

	                    ?>
                        <?php $data = \common\models\Page::findAll(['slug'=>['paypal','credit-card','bank-transfer']]); ?>
                        <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
                            <?=(TBase::CLang() == 'ru-RU') ? $data[0]->detail_ru : $data[0]->detail_en ?>
                        </div>
                        <div class="col-md-7">
                            <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                        <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
                                        <?=$CurrencyModel->convert($paypalCharges,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                        <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-5">
                            <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                            <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('PAYPAL_REDIRECTED_MSG');?></p>
                            <div class="panel-body" style="padding: 0px;">
                                <div class="paypal-payment payment-div">
                                    <div class="clearfix">
                                        <div class="col-md-offset-3 col-md-6">
                                            <?=Html::a(TBase::ShowLbl('PAY'),['fund-payment/payment-method-selection','_payment_method'=>'paypal','fund_amount'=>$total],['class'=>'btn btn-default paysubmit','style'=>'margin:0px!important;width:100%!important']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane clearfix" id="Card"  style="padding-bottom: 27px;">
                        <div class="col-md-12">
                            <?=(TBase::CLang() == 'ru-RU') ? $data[1]->detail_ru : $data[1]->detail_en ?>
                        </div>
                        <?php
                        $form = ActiveForm::begin([
                            'action' =>['fund-payment/credit-cart'],
                            'id' => 'payment-form',
                            'method' => 'post',
                        ]); ?>
                        <div class="clearfix col-md-12" >
                            <div class="credit-card-payment payment-div">
                                <div class="clearfix">

                                    <div class="col-md-6">
                                        <h4><?=LBL::_x('CARD_DETAIL');?></h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'number')->textInput(['placeholder'=>'Credit Card Number','maxlength'=>16])->label(false); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'security_code')->textInput(['placeholder'=>'Cvv Code','maxlength'=>4])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'expire_month')->dropDownList($month)->label(false); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'expire_year')->dropDownList($year)->label(false);?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'first_name')->textInput(['placeholder'=>'First Name'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'last_name')->textInput(['placeholder'=>'Last Name'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Billing Address</h4>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'STREET')->textInput(['placeholder'=>'Street'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'CITY')->textInput(['placeholder'=>'City'])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'STATE')->textInput(['placeholder'=>'State'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'COUNTRYCODE')->dropDownList(
                                                    TBase::countryList())->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'ZIP')->textInput(['placeholder'=>'Zip Code'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'type')->dropDownList(
                                                    [
                                                        'Visa' => 'Visa',
                                                        'MasterCard' => 'Master Card',
                                                        'Discover' => 'Discover',
                                                        'Amex' => 'Amex',
                                                        'JCB' => 'JCB',
                                                        'Maestro' => 'Maestro',
                                                    ])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    echo Html::hiddenInput('method','Credit Card');
                                    echo Html::hiddenInput('orderID','',['id'=>'orderID']);
                                    echo Html::hiddenInput('paymentAction',\yii\helpers\Url::to(['fund-payment/credit-cart']),['id'=>'paymentAction']);
                                    echo Html::hiddenInput('successAction',\yii\helpers\Url::to(['order/payment-completed']),['id'=>'successAction']);
                                    ?>

                                </div>
                                <div class="clearfix" id="order-error" style="color:red;padding: 17px;"></div>
                            </div>
                        </div>
	                    <?php
	                    $coefficient = (40+($total*0.002));
	                    $percentage = 0.039;

	                    $cardCharges = round(($total * $percentage) + $coefficient);
	                    $totalCharges = ($cardCharges + $total);
	                    ?>
                        <div class="col-md-7">
                            <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL');?></div>
                                        <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
                                        <?=$CurrencyModel->convert($cardCharges,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                        <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-lg-5">

                        </div>
                        <div class="col-md-5">
                            <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                            <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('CREDIT_CARD_PROCESS_MSG')?>.</p>
                            <div class="panel-body" style="padding: 0px;">
                                <div class="paypal-payment payment-div">
                                    <div class="clearfix">
                                        <div class="col-md-offset-3 col-md-6">
                                            <?= Html::submitButton(TBase::ShowLbl('PAY'),['class' => 'btn btn-default paysubmit','style'=>'margin:0px!important;width:100%!important']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="tab-pane" id="Bank">

                        <div class="tab-pane active clearfix" id="bank">
                            <?php $data = \common\models\Page::findAll(['slug'=>['paypal','credit-card','bank-transfer']]); ?>
                            <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
                                <?=(TBase::CLang() == 'ru-RU') ? $data[2]->detail_ru : $data[2]->detail_en ?>
                            </div>
                            <div class="col-md-7">
                                <h5><strong><?=LBL::_x('ORDER_SUMMARY');?></strong></h5>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('AMOUNT');?></div>
                                            <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
                                            <?=LBL::_x('YOUR_BANK_FEES');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                            <?=$CurrencyModel->convert(round($total),'JPY','JPY')?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                                <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('INVOICE_ISSUED_MSG');?></p>
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="paypal-payment payment-div">
                                        <div class="clearfix">
                                            <div class="col-md-offset-3 col-md-6">
                                                <?=Html::a(TBase::ShowLbl('COMPLETE_ORDER'),['fund-payment/payment-method-selection','_payment_method'=>'bank_transfer','fund_amount'=>$total],['class'=>'btn btn-default paysubmit','style'=>'margin:0px!important;width:100%!important']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
				
                    </div>
                   </div>      
            </div>
            </div>
  <?php } ?>
        </div>
    </div>
    </div>
</section>
    <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <p><?=TBase::ShowLbl('PAYMENT_PROCCESS')?></p>
        </div>
    </div>

<?php

$this->registerJs("
    var add_fund_payment_url = '".\yii\helpers\Url::to(['fund-payment/payment-method-selection'],true)."';
    
    
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

",\yii\web\View::POS_BEGIN);

$this->registerJs("
    jQuery('.responsive-tabs').responsiveTabs({
        accordionOn: ['xs'] // xs, sm, md, lg
    });
",\yii\web\View::POS_READY);

?>
<?php $this->registerJsFile("@web/tnk/js/jquery.bootstrap-responsive-tabs.min.js",['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>
