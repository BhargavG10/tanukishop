<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\TBase as LBL;
$tModel = new \common\components\TCurrencyConvertor;

$this->title = Yii::t('app', \common\components\TBase::ShowLbl('MY_ORDERS'));
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .table tbody td:first-child,.table tbody td:last-child {
        width: 10%;
    }

    .table th{
        text-align: center;
    }
    .grid-view .fa{background: none;padding: 0;}
    .btn-primary{border-radius: 0;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=\common\components\TBase::ShowLbl('MY_ORDERS')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <div class="clearfix">
                <h3 class="inner-head"><?=LBL::_x('FUNDS');?></h3>
            </div>
            <div class="col-md-12 margin-bottom-10" style="margin-bottom: 10px;">
		        <?=Html::a(LBL::_x('ADD_FUND'),['user/add-fund'],['class'=>'btn btn-primary pull-right'])?>
            </div>
            <div class="col-md-12">
            <div class="table-responsive text-center">
                <?php
                    Pjax::begin();
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'summary' => '',
                        'columns' => [
                            [
                                'attribute'=>'id',
                                'label'=>\common\components\TBase::ShowLbl('ORDER_ID'),
                                'value'=> function($data){
                                    return 'TNK0000'.$data->id;
                                },
                            ],
                            [
                                'attribute'=>'date',
                                'label'=>\common\components\TBase::ShowLbl('ORDER_DATE'),
                                'value'=> function($data){
                                    return date('d-m-Y',strtotime($data->date));
                                },
                            ],
                            [
                                'format' => 'html',
                                'attribute'=>\common\components\TBase::ShowLbl('TOTAL'),
                                'value'=> function($data){
                                    $tModel = new \common\components\TCurrencyConvertor;
                                    return $tModel->convert($data->subtotal,'JPY','JPY');
                                },
                            ],
                            [
                                'attribute'=>'status',
                                'label'=>\common\components\TBase::ShowLbl('STATUS'),
                                'value'=> function($data){
                                    return $data->getFundStatus();
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template'=>'{view}',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        $url = \yii\helpers\Url::toRoute(['order/view', 'id' => $model->id]);
                                        return  Html::a('<i class="fa fa-eye"></i>', $url,
                                            [ 'title' => Yii::t('app', \common\components\TBase::ShowLbl('VIEW')), 'class'=>'btn btn-primary', ]) ;
                                    },
                                ],
                            ],
                        ],
                    ]);
                    Pjax::end();
                ?>
            </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
