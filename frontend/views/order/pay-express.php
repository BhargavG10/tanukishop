<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;
use \common\helper\Tanuki;

$CurrencyModel = new \common\components\TCurrencyConvertor;
$TModel = new Tanuki();

/* @var $this yii\web\View */

$this->title = 'Tanukishop | Payment';
?>
<div class="site-index">
    <div class="jumbotron"></div>
    <div class="body-content">
        <div class="container">
            <h2><?=TBase::ShowLbl('ORDER_CONFIRMATION');?></h2>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th><?=TBase::ShowLbl('SERIAL_NO'); ?></th>
                    <!--                    <th>--><?//=TBase::ShowLbl('ITEM_NAME'); ?><!--</th>-->
                    <th><?=TBase::ShowLbl('ITEM_CODE'); ?></th>
                    <th><?=TBase::ShowLbl('SHOP');?></th>
                    <th><?=TBase::ShowLbl('PRICE'); ?></th>
                    <th><?=TBase::ShowLbl('QUANTITY'); ?></th>
                    <th><?=TBase::ShowLbl('TOTAL'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $count = 0;
                $total = 0;
                if (isset(\Yii::$app->session['carts']) && count(\Yii::$app->session['carts'])>0){
                    foreach(\Yii::$app->session['carts'] as $key => $item) {
                        ++$count;
                        $TModel->singleProductDetail($item['product_code'],$item['shop']);
                        $amount = $TModel->shopProductDetail($item['shop'],'price');
                        $total += ($amount * $item['quantity']);
                        ?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <td><?=$TModel->shopProductDetail($item['shop'],'name'); ?></td>
                            <!--                        <td>--><?php //echo $item['product_code']; ?><!--</td>-->
                            <td><?php echo $item['shop']; ?></td>
                            <td><?=$CurrencyModel->convert($amount,'JPY','JPY'); ?></td>
                            <td><?php echo $item['quantity']; ?></td>
                            <td><?=$CurrencyModel->convert(($item['quantity'] * $amount),'JPY','JPY'); ?></td>
                        </tr>
                        <?php
                    }
                } else { ?>
                    <tr>
                        <td colspan="6" align="center"><?=TBase::ShowLbl('EMPTY_CART'); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <table class="table">
                <tbody>
                <tr>
                    <td align="right">Sub Total Amount</td>
                    <td align="right"><?=$CurrencyModel->convert($total,'JPY','JPY'); ?></td>
                </tr>
                <tr>
                    <td align="right">Tanuki Charges</td>
                    <td align="right"><?=$CurrencyModel->convert(250,'JPY','JPY'); ?></td>
                </tr>
                <tr>
                    <td align="right"><strong>Total Amount</strong></td>
                    <td align="right"><strong><?=$CurrencyModel->convert($total+250,'JPY','JPY'); ?></strong></td>
                </tr>
                </tbody>
            </table>
            <hr/>
            <h3>Shipping Address</h3>
            <?php
            if (\Yii::$app->session['shipping']) {
                $shipping = \Yii::$app->session['shipping'];
                echo $shipping['first_name'].' '.$shipping['last_name'].'<br/>';
                echo $shipping['company'].'<br/> ';
                echo $shipping['address_1'].$shipping['address_2'].'<br/>';
                echo $shipping['appt_no'].'<br/> ';
                echo $shipping['city'].', '.$shipping['state'].'<br/>';
                echo $shipping['country'].'<br/>';
                echo $shipping['zipcode'].'<br/>';
                echo $shipping['shipping_method'].'<br/>';
            }
            ?>
            <?php echo Html::a('Change Shipping Address',['order/shipping']); ?>

            <hr/>
            <h3>Payment Method</h3>

        </div>
        <div class="container">
            <h3>Credit Card Detail</h3>
            <h3>Payment</h3>
            <?php $form = ActiveForm::begin([
                'action' =>['order/create-order'],
                'id' => 'payment-form',
                'method' => 'post',
            ]); ?>
            <div class="row">
                <div class="col-lg-3">
                    <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($model, 'last_name')->textInput() ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($model, 'number')->textInput() ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($model, 'security_code')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <?= $form->field($model, 'expire_month')->textInput() ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($model, 'expire_year')->textInput() ?>
                </div>
                <div class="col-lg-3">
                    <?php echo $form->field($model, 'type')->dropDownList(
                        [
                            'Visa' => 'Visa',
                            'MasterCard' => 'Master Card',
                            'Discover' => 'Discover',
                            'Amex' => 'Amex',
                            'JCB' => 'JCB',
                            'Maestro' => 'Maestro',
                        ]); ?>
                </div>
                <table class="table">
                    <tbody>
                    <tr>
                        <td><?php echo Html::a(TBase::ShowLbl('BACK'),['order/index'],['class'=>'btn btn-primary']); ?></td>
                        <td align="right">
                            <?= Html::submitButton('Done', ['class' => 'btn btn-primary']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php
                    echo Html::hiddenInput('orderID','',['id'=>'orderID']);
                    echo Html::hiddenInput('paymentAction',\yii\helpers\Url::to(['order/payment-processing']),['id'=>'paymentAction']);
                    echo Html::hiddenInput('successAction',\yii\helpers\Url::to(['order/payment-completed']),['id'=>'successAction']);
                ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="row alert-danger" id="order-error" style="display:none;padding: 17px;"></div>
        </div>
    </div>
</div>

<?php


$js = <<<JS
// get the form id and set the event
$('form#payment-form').on('beforeSubmit', function(e) {
    $('#myModal').show();
    $('#myModal .modal-content p').html('Creating your order please wait...');

    var path = '';
    var data = '';
    path = $('form#payment-form').attr('action');
    data = $('form#payment-form').serialize();

    jQuery.ajax({
        type:'POST',
        url:$('form#payment-form').attr('action'),
        data:$('form#payment-form').serialize(),
        success:function(data){

            console.log(data);
            if (jQuery.trim(data) != 0) {
                jQuery('#orderID').val(data);
                $('#myModal .modal-content p').html('Order Created. Payment Processing please wait...');
                path = $('#paymentAction').val();
                data = $('form#payment-form').serialize();
                jQuery.ajax({
                    type:'POST',
                    url:path,
                    data:data,
                    success:function(data){
                        $('#myModal').hide();
                        if (jQuery.trim(data) == 1) {
                            console.log('Payment Successfully');
                            window.location = $("#successAction").val();
                            return false;
                        } else {
                            console.log('Payment UnSuccessfully');
                            $('#order-error').show().html(data);
                            return false;
                        }
                        return false;
                    },
                    beforeSend:function(){
                        console.log('Please wait. Payment Processing');
                    }
                });

            } else {
                alert('Error while order creation. Please try again.');
                console.log('Error while order creation');
                return false;
            }
            return false;

        },
        beforeSend:function(){
            console.log('Please wait. Creating Order....');
        }
    });

    return false;
}).on('submit', function(e){    // can be omitted
    e.preventDefault();         // can be omitted
    return false;
});
JS;

$this->registerJs($js);

?>
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <p>Please waiting process your payment. Do not refresh your screen.</p>
    </div>

</div>