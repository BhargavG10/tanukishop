<?php
$tModel = new \common\components\TCurrencyConvertor;
?>
<tr class="txt-c">
    <td width="10%">TNK0000<?= $model['id']?></td>
    <td width="15%"><strong><?= date('d, M Y',strtotime($model['date']));?></strong></td>
    <td width="22%"><?= $model['invoice']?></td>
    <td width="13%"><?=$tModel->convert($model['total'],'JPY','JPY'); ?></td>
    <td width="13%"><?= ucfirst($model['status']);?></td>
    <td width="14%"><a href="<?php echo \Yii::$app->urlManager->createUrl(['order/view','id'=>$model['id']]);?>"><i class="icon-eye-open"></i></a></td>
</tr>

