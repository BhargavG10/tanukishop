<?php
    use \common\components\TBase as LBL;
    $this->title = LBL::_x('ORDER_COMPLETED');
?>
<section>
    <div class="container mt20 notranslate" style="font-size: 20px;">
        <div class=" text-center  dashboard width-98-percent">
            <h3 class="inner-head border-b">
                <?=($model->order_status == 9) ? LBL::_x('ORDER_CANCELLED') : LBL::_x('ORDER_COMPLETED'); ?>
            </h3>
            <?php if ($model->order_status != 9) { ?>
                <i class="font-133 fa fa-thumbs-up text-color-7ac0c8"></i>
                <br/>
                    <?=LBL::_x('ORDER_ID')?> : TNK0000<?=$model->id?><br/>
                    <?=\yii\helpers\Html::a(LBL::_x('PLEASE_CLICK_HERE_MSG'),['order/view','id'=>$model->id])?> <br/>
                    <br/>
                    <?=\yii\helpers\Html::a(LBL::_x('PRINT_INVOICE_MSG'),['order/view','id'=>$model->id,'_print'=>'1'])?>
                    <br/>
                    <br/>
                    <?=\yii\helpers\Html::a(LBL::_x('MY_ORDERS'),['/order/index'])?>
            <?php } else { ?>
                <i class="font-133  fa fa-exclamation" aria-hidden="true" style="color:#df7b2b;"></i><br/>
                <?=\yii\helpers\Html::a('<i class="fa-2x fa fa-arrow-left" aria-hidden="true"></i><br/>',['order/index'],['style'=>"color:#df7b2b;"])?>
            <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
</section>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>