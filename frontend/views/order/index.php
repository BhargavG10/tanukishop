<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\TBase as LBL;
$tModel = new \common\components\TCurrencyConvertor;

$this->title = Yii::t('app', \common\components\TBase::ShowLbl('MY_ORDERS'));
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .table tbody td:first-child,.table tbody td:last-child { width: 21px!important; } .table th{ text-align: center; } .view-detail-btn,.view-detail-btn:hover { background: grey; border: none; }
    .grid-view .fa{background: none;padding: 0;}
    .btn-primary{border-radius: 0;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=\common\components\TBase::ShowLbl('MY_ORDERS')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <h3 class="inner-head "><?=LBL::_x('MY_ORDERS')?></h3>
            <div class="table-responsive text-center">
                <?php
                    Pjax::begin();
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'summary' => '',
                        'columns' => [
	                        [
		                        'label'=>LBL::_x('IMAGE'),
		                        'format'=>'html',
		                        'value'=> function($data){
			                        return Html::a(Html::img($data->image,['width'=>50,'height'=>50]),['view','id'=>$data->id]);
		                        },
	                        ],
                            [
                                'attribute'=>'type',
                                'value'=> function($data){
                                    return $data->getOrderType();
                                },
                            ],
	                        [
		                        'attribute'=>'id',
		                        'label'=>LBL::_x('ORDER_ID'),
		                        'format'=>'html',
		                        'value'=> function($data){
			                        $html = 'TNK0000'.$data->id;
                                    if ($data->type == "buyout" || $data->type == "auction") {
	                                    $link = '<br/><span style="color: #428bca;">'.$data->productCode.'</span>';
	                                    $html .= Html::a($link,['yahoo-auctions/detail','id'=>$data->productCode],['style'=>'color:inherit']);
                                    }
			                        return $html;

		                        },
	                        ],
	                        [
                                'attribute'=>'date',
                                'label'=>LBL::_x('ORDER_DATE'),
                                'value'=> function($data){
                                    return date('d-m-Y',strtotime($data->date));
                                },
                            ],
                            [
                                'format' => 'html',
                                'attribute'=>LBL::_x('TOTAL'),
                                'value'=> function($data){
                                    $tModel = new \common\components\TCurrencyConvertor;
                                    return $tModel->convert($data->total,'JPY','JPY');
                                },
                            ],
                            [
                                'attribute'=>'status',
                                'label'=>LBL::_x('STATUS'),
                                'value'=> function($data){
                                    return $data->getStatus();
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template'=>'{view}',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return  Html::a('<i class="fa fa-eye"></i>', $url,
                                            [ 'title' => Yii::t('app', LBL::_x('VIEW')), 'class'=>'btn btn-primary', ]) ;
                                    },
                                ],
                            ],
                        ],
                    ]);
                    Pjax::end();
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
