<?php
use common\components\TBase;
$tbase = new \common\components\TBase();

/* @var $this yii\web\View */
/* @var $model common\models\Order */
$this->title = 'Tanuki - Order ID: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="container notranslate">
        <ol class="breadcrumb breadcrumb-dashboard-bar">
            <li><a href="#"><?=TBase::ShowLbl('home')?></a></li>
            <li class="active">"<?=TBase::ShowLbl('ORDER_DETAIL');?> : TNK0000<?= $model->id?>"</li>
        </ol>
    </div>
    <section>
        <div class="container">
            <?=$this->render('/user/_left_nav')?>
            <div class="dashboard print-invoice position-relative">
                <div class="top-header">
                    <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg" width="250">
                    <a onClick="window.print()" href="#"><i class="fa fa-print fa-3x" aria-hidden="true"></i></a>
                </div>
                <?php $view = '_view';

                if ($model->type == 'add_fund') {
                    $view = ($model->method == 'Paypal' || $model->method == 'Credit Card') ? '_fund' : '_invoice';
                }

                ?>
                <?=$this->render($view,['model'=>$model])?>

                <?php if ($model->method =='Bank Deposit') { ?>
                    Bank Details<br/>
                    <div style="border-radius: 5px;height: auto;width: 100%;">
                        <?php

                        $data = \common\models\Page::findOne(['slug'=>['bank-transfer']]);
                        echo $data['detail_en'];
                        ?>

                        <div style="font-size:10px;margin-top: 12px;"><strong>[*] All charges must borne by the remitter</strong><div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div style="width: 100%;clear: both;font-size:8px; " class="company-detail">
                    Thank you for shopping with us! Our manager will contact you later regarding shipping.<br/>
                    If you have any questions regarinding your order, please do not hesitate to contact us.<br/>
                    <br/>
                    Tanuki Shop<br/>
                    Head Office: Third Street 5-22, Hachiman-cho, Imizu, Toyama, 934-0025, Japan<br/>
                    Branch Office: Shiraishi 638, Kosugi, Imizu-shi, Toyama, 939-0304, Japan<br/>
                    Tel: +81 766 50 8767<br/>
                    Fax: + 81 766-50-8768<br/>
                    E-mail: sales@tanukishop.com<br/>
                    Website: www.tanukishop.com<br/>
                    <img src="https://www.tanukishop.com/uploads/newlogo_en_small.jpg" width="150">
                </div>

            </div>
        </div>
    </section>

    <script>
        function myFunction() {
            window.print();
        }
    </script>
<?php if (isset($_REQUEST['_print'])) { ?>
    <script>window.print();</script>
    <?php
}
?>