<?php
use yii\helpers\Html;
use common\components\TBase;
use common\components\TBase as LBL;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$shipping  = null;
$tanuki = new \common\helper\Tanuki();
?>
<style>
    table{background: #fff;}
    .margin-top-26{margin-top:26px;}
    .order-shipping-summary table tbody th,.order-summary-table table tbody th,
    .order-shipping-summary table tbody td,.order-summary-table table tbody td
    {width: 50%;border: 1px solid #333;}
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{vertical-align: middle!important;}
    .fa-print{position: absolute;  top: 2px;  right: 6px;}
    @media print {
        footer,header,.left-menu,.container.notranslate{display: none;}
        .print-invoice {background-color: white;height: 100%;width: 100%;position: fixed;top: 0;left: 0;margin: 0;padding: 15px;font-size: 14px;line-height: 18px;  }
        .order-summary-table{width:100%;}
        .table{border:1px solid #000;}
        td,th,tr {border: 1px solid;}
        .fa-print{display: none;}
        .jivo-iframe-container-left.jivo-state-widget.jivo-collapsed.jivo_shadow.jivo_rounded_corners.jivo_gradient,
        .jivo_shadow.jivo_rounded_corners.jivo_gradient.jivo-expanded.jivo-iframe-container-bottom{opacity: 0!important;}
    }

    table.less-padding tr td,table.less-padding tr th{padding: 2px;text-align: center!important;}
</style>
<div class="row clearfix notranslate margin-top-26">
<div class="order-summary-table col-lg-12">
    <table width="100%" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2" align="center"><strong><?=TBase::ShowLbl('COMPLETE_ORDER')?></strong></td>
        </tr>
        <tr>
            <td><?=LBL::_x('NAME'); ?> :<?=$model->user->first_name.' '.$model->user->last_name?></td>
            <td><?=LBL::_x('ORDER_ID'); ?> :TNK0000<?= $model->id?></td>
        </tr>
        <tr>
            <td><?=LBL::_x('TOTAL'); ?>:<?=$tModel->convert($model['subtotal'],'JPY','JPY'); ?></td>
            <td><?=LBL::_x('CURRENCY'); ?> :<?=$model->currency; ?></td>
        </tr>
        <tr>
            <td><?=LBL::_x('LBL_PAYMENT'); ?> :<?=$model->method; ?></td>
            <td><?=LBL::_x('ORDER_STATUS'); ?> :<?=$model->getFundStatus(); ?></td>
        </tr>
        <tr>
            <td><?=LBL::_x('ORDER_DATE'); ?> :<?=$model->date; ?></td>
            <td><?=LBL::_x('INVOICE_NUMBER'); ?> :<?=$model->invoice; ?></td>
        </tr>
    </table>
</div>
</div>
<div class="order-detail-table col-lg-12 margin-top-20" style="padding: 0px;">
    <h3 class="text-center inner-head notranslate"><?=LBL::_x('PAYMENT');?> (JPY)</h3>
    <table width="100%" border="1" cellspacing="0" cellpadding="0" class="table min-w700 table-order-view-detail" >
        <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
        <tr class="notranslate">
            <td><?=LBL::_x('SERVICE'); ?></td>
            <td><?=LBL::_x('TOTAL')?></td>
        </tr>
        <tr class="notranslate">
            <td>
            <?=LBL::_x('ADDING_FUNDS_TO_DEPOSIT_MSG');?>
            </td>
            <td><?=$tModel->convert($model->subtotal,'JPY','JPY')?></td>
        </tr>
    </table>
</div>