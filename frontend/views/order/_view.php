<?php
use yii\helpers\Html;
use common\components\TBase;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$shipping  = null;
$tanuki = new \common\helper\Tanuki();
if (isset($model->shipping)) {
    $shipping = $model->shipping;
}
?>
<style>
    table{background: #fff;}
    .margin-top-26{margin-top:26px;}
    .order-shipping-summary table tbody th,.order-summary-table table tbody th,
    .order-shipping-summary table tbody td,.order-summary-table table tbody td
    {width: 50%;border: 1px solid #333;}
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{vertical-align: middle!important;}
    .fa-print{position: absolute;  top: 2px;  right: 6px;}
    @media print {
        footer,header,.left-menu,.container.notranslate{display: none;}
        .print-invoice {background-color: white;height: 100%;width: 100%;position: fixed;top: 0;left: 0;margin: 0;padding: 15px;font-size: 14px;line-height: 18px;  }
        .order-summary-table{width:100%;}
        .table{border:1px solid #000;}
        td,th,tr {border: 1px solid;}
        .fa-print{display: none;}
        .jivo-iframe-container-left.jivo-state-widget.jivo-collapsed.jivo_shadow.jivo_rounded_corners.jivo_gradient,
        .jivo_shadow.jivo_rounded_corners.jivo_gradient.jivo-expanded.jivo-iframe-container-bottom{opacity: 0!important;}
    }

    table.less-padding tr td,table.less-padding tr th{padding: 2px;text-align: center!important;}
</style>
    <div class="order-summary-table"  style="padding: 0px;">
        <table width="100%" border="1" cellspacing="0" cellpadding="0" class="table min-w700 table-order-view-detail" >
            <tr>
                <td><strong><?=TBase::ShowLbl('COMPLETE_ORDER')?></strong></td>
                <td><strong><?=TBase::ShowLbl('LBL_SHIPPING_ADDRESS'); ?></strong></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('ORDER_ID'); ?> :TNK0000<?= $model->id?></td>
                <td><?=TBase::ShowLbl('NAME'); ?> :<?=(isset($shipping->fullName))?$shipping->fullName : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('TOTAL'); ?>:<?=$tModel->convert($model['total'],'JPY','JPY'); ?></td>
                <td><?=TBase::ShowLbl('COMPANY_NAME'); ?>:<?=(isset($shipping->company))?$shipping->company : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('CURRENCY'); ?> :<?=$model->currency; ?></td>
                <td><?=TBase::ShowLbl('APARTMENT_NUMBER'); ?> :<?=(isset($shipping->appt_no))?$shipping->appt_no : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('LBL_PAYMENT'); ?> :<?=$model->method; ?></td>
                <td><?=TBase::ShowLbl('ADDRESS'); ?> :<?=(isset($shipping->address_1))?$shipping->address_1.' '.$shipping->address_2 : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('ORDER_STATUS'); ?> :<?=$model->status; ?></td>
                <td><?=TBase::ShowLbl('CITY'); ?> :<?=(isset($shipping->city))?$shipping->city : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('ORDER_DATE'); ?> :<?=$model->date; ?></td>
                <td><?=TBase::ShowLbl('STATE'); ?> :<?=(isset($shipping->state))?$shipping->state : '';?>,
                    <?=TBase::ShowLbl('COUNTRY'); ?> :<?=(isset($shipping->country))?$tbase->countryDetail($shipping->country) : '';?></td>
            </tr>
            <tr>
                <td><?=TBase::ShowLbl('INVOICE_NUMBER'); ?> :<?=$model->invoice; ?></td>
                <td><?=TBase::ShowLbl('PHONE'); ?> :<?=(isset($shipping->phone))?$shipping->phone : '';?></td>
            </tr>
        </table>
    </div>
<div class="order-detail-table margin-top-20" style="padding: 0px;">
    <h3 class="inner-head notranslate"><?=TBase::ShowLbl('ORDERED_PRODUCT_SUMMARY');?></h3>
    <table width="100%" border="1" cellspacing="0" cellpadding="0" class="table min-w700 table-order-view-detail" >
        <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
        <tr class="notranslate">
            <td  style="width:10%"  height="30" bgcolor="#7ac0c8" class=" "><?=TBase::ShowLbl('IMAGE'); ?></td>
            <td width="30%" bgcolor="#7ac0c8" class=" "><?=TBase::ShowLbl('ITEM_NAME')?></td>
            <td width="7%" bgcolor="#7ac0c8" class=text-center "><?=TBase::ShowLbl('SHOP')?> </td>
            <td width="13%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('PRICE')?> </td>
            <td width="7%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('QUANTITY')?> </td>
            <td width="10%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('TANUKI_CHARGES')?></td>
            <td width="9%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('DOMESTIC_SHIPPING')?></td>
            <td width="9%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('WEIGHT')?> </td>
            <td width="9%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('BANK_FEES')?> </td>
            <td width="13%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('OTHER_FEES')?> </td>

            <td width="13%" bgcolor="#7ac0c8" class="text-center"><?=TBase::ShowLbl('TOTAL')?> </td>

        </tr>
        <?php
        $count = $price = $total = 0 ;
        if (count($model->orderDetail)>0){
            foreach($model->orderDetail as $key => $item) {
	            $price = ($item->price * $item['quantity']) + $model->tanuki_charges + $item['domestic_shipping'] + $item['bank_fees'] + $item['other_fees'];
	            $total += $price;
                ?>
                <tr class="">
                    <td style="width:10%;border-right:1px solid #333;" >
                        <?php
                        if (file_exists(\Yii::getAlias('@webroot').'/order-images/'.$item->product_image)) {
                            echo Html::img(Yii::$app->params['order-image-url'].$item->product_image,['width'=>'100','height'=>'140']);
                        } else {
                            echo Html::img(Yii::$app->params['no-image']);
                        }
                        ?>
                    </td>

                    <td style="width: 38%;border-bottom:1px solid #333;border-right:1px solid #333;"  class="translate" data-title="<?=ucfirst($item['product_name']); ?>" data-short="0">
                        <?php
                        if ($item['shop'] == "Yahoo Auction") {
	                        echo Html::a(ucfirst($item['product_name']),['yahoo-auctions/detail','id'=>$item['product_id']],['target'=>'_blank']);
                        } else {
	                        echo Html::a(ucfirst($item['product_name']),[$item['shop'].'/detail','id'=>$item['product_id']],['target'=>'_blank']);
                        }
                        ?>
                        <br/>
                        <?php
                        if (isset($item->productAttributes) && count($item->productAttributes)>0) { ?>
                            <table class="less-padding table table-bordered" style="margin-top: 30px">
                                <thead>
                                <tr>
                                    <th class="text-center">Attribute Name</th>
                                    <th class="text-center">Attribute Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($item->productAttributes as $attribute) { ?>
                                    <tr>
                                        <td><?=$attribute->attribute_name?></td>
                                        <td><?=$attribute->attribute_value?></td>
                                    </tr>
                                    <?php
                                } ?>
                                </tbody>
                            </table>
                            <?php
                        }
                        ?>
                    </td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="7%" class="text-center notranslate"><?=ucfirst($item['shop']); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="13%"  class="text-center notranslate"><?=$tModel->convert($item->price,'JPY','JPY'); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="7%" class=" text-center notranslate"><?=$item['quantity']; ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="10%" class="text-center notranslate"><?=$tModel->convert($item->tanuki_fee,'JPY','JPY'); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="9%" class="text-center notranslate"><?=$tModel->convert($item->domestic_shipping,'JPY','JPY'); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="9%" class="text-center notranslate"><?=$item['weight']; ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="9%" class="text-center notranslate"><?=$tModel->convert($item['bank_fees'],'JPY','JPY'); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="12%" class="text-center notranslate"><?=$tModel->convert($item['other_fees'],'JPY','JPY'); ?></td>
                    <td style="border-bottom:1px solid #333;border-right:1px solid #333;" width="13%"  class="text-center notranslate"><?=$tModel->convert($price,'JPY','JPY'); ?></td>
                </tr>
                <?php
            }
            ?>
            <tr  class="notranslate">
                <td colspan="10" align="right" style="border-left:1px solid #333;border-right:1px solid #333;"><?=TBase::ShowLbl('SUB_TOTAL')?></td>
                <td align="center"><?=$tModel->convert($model->subtotal,'JPY','JPY'); ?></td>
            </tr>
            <tr  class="notranslate" style="border-top: 1px solid #333;">
                <td colspan="10" align="right" style="border-left:1px solid #333;border-right:1px solid #333;"><?=TBase::ShowLbl('CONSOLIDATION')?></td>
                <td align="center"><?=$tModel->convert($model->consolidation,'JPY','JPY'); ?></td>
            </tr>
            <tr  class="notranslate" style="border-top: 1px solid #333;">
                <td colspan="10" align="right" style="border-left:1px solid #333;border-right:1px solid #333;"><?=TBase::ShowLbl('SHIPPING_CHARGES')?></td>
                <td align="center"><?=$tModel->convert($model->shipping_charges,'JPY','JPY'); ?></td>
            </tr>
            <tr  class="notranslate" style="border-top: 1px solid #333;">
                <td colspan="10" align="right" style="border-left:1px solid #333;border-right:1px solid #333;"><?=TBase::ShowLbl('TOTAL')?></td>
                <td align="center"><?=$tModel->convert(($model->total),'JPY','JPY'); ?></td>
            </tr>
        <?php } else { ?>
            <tr  class="notranslate">
                <td colspan="7" align="center"><?=TBase::ShowLbl('EMPTY_CART'); ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>