<?php
use common\components\TBase;
use common\components\TBase as LBL;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();

$tanuki = new \common\helper\Tanuki();
$this->title = 'Tanuki - Order ID: '.$model->id;
?>
<style>
    .fa-print{position: absolute;  top: 2px;  right: 6px;}
    table{border: none;}
    @media print {
        footer,header,.left-menu,.container.notranslate{display: none;}
        .print-invoice {background-color: white;height: 100%;width: 100%;position: fixed;top: 0;left: 0;margin: 0;padding: 15px;font-size: 14px;line-height: 18px;  }
        .top-header,.company-detail,.fa-print{display: none;}
        .jivo-iframe-container-left.jivo-state-widget.jivo-collapsed.jivo_shadow.jivo_rounded_corners.jivo_gradient,
        .jivo_shadow.jivo_rounded_corners.jivo_gradient.jivo-expanded.jivo-iframe-container-bottom{opacity: 0!important;}
    }
    table.less-padding tr td,table.less-padding tr th{padding: 2px;text-align: center!important;}
</style>
<div>
    <section>
        <div>
            <div style="margin-top: 22px;" class="clearfix">
                <div style="text-align: center;"><strong>Invoice</strong></div>
                <div class="col-lg-12">
                    <table class="table" border="0" width="100%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td colspan="2" align="left"><strong>SENT BY</strong></td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('COMPANY_NAME')?>:</td>
                            <td>Tanuki Shop</td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('ADDRESS_1')?>:</td>
                            <td>Third Street 5-22, Hachiman-cho, Imizu, Toyama</td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('CITY')?>/<?=LBL::_x('ZIPCODE')?>:</td>
                            <td>934-0025</td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('COUNTRY')?>:</td>
                            <td>Japan</td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('PHONE')?>/<?=LBL::_x('FAX')?>:</td>
                            <td>Tel: +81 766 50 8767<br/>Fax: + 81 766-50-8768</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="margin-top: 15px;" class="clearfix">
                <div class="col-lg-12">
                    <table class="table" border="0" width="100%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td colspan="2" align="left"><strong><?=LBL::_x('SENT_TO')?></strong></td>
                        </tr>
                        <tr>
                            <td style="width:9%!important;"><?=LBL::_x('DEPARTMENT');?>:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('ADDRESS_1')?>:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('CITY')?>/<?=LBL::_x('ZIPCODE')?>:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('COUNTRY')?>:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                        <tr>
                            <td><?=LBL::_x('PHONE')?>/<?=LBL::_x('FAX')?>:</td>
                            <td style="border-bottom:1px solid;width:25%;"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="margin-top: 15px;" class="clearfix">
                <h3 style="text-align: center;"><?=TBase::ShowLbl('PAYMENT');?> (JPY)</h3>
                <table width="100%" cellspacing="0" cellpadding="10">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr class="notranslate">
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;"><?=TBase::ShowLbl('SERVICE'); ?></td>
                        <td style="border-top:1px solid #f2f2f2; border-bottom: 1px solid #f2f2f2;"><?=TBase::ShowLbl('TOTAL')?></td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                            <?=LBL::_x('ADDING_FUNDS_TO_DEPOSIT_MSG')?>
                        </td>
                        <td><?=$tModel->convert($model->total,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</div>