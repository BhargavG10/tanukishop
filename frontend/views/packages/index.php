<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\TBase as LBL;
$this->title = LBL::_x('PACKAGES');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .table tbody td:first-child {width: 10%;}
    .add-new-btn{margin-right: 15px;  margin-top: 10px;  }
    .add-new-btn a{background: #7ac0c8; border: none;}
    .table thead tr th{text-align: center;}
    .grid-view .fa{background: none;padding: 0;}
    .btn-primary{border-radius: 0;}
</style>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=LBL::_x('PACKAGES')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
        <?=$this->render('/user/_left_nav')?>
        <div class="dashboard">
            <div class="clearfix">
                <h3 class="inner-head "><?=LBL::_x('PACKAGES')?></h3>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;">
                <?= Html::a(LBL::_x('CREATE_PACKAGES'), ['create'], ['class' => 'btn btn-primary pull-right']) ?>
            </div>
            <div class="col-md-12">
                <div class="table-responsive text-center">
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'emptyText' => \common\components\TBase::_x('NO_DATA_FOUND'),
                            //'filterModel' => $searchModel,
                            'summary' => '',
                            'columns' => [
    //                            ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute'=>'id',
                                    'value'=>function($model) {
                                        return $model->id;
                                    }
                                ],
                                [
                                    'attribute'=>'shipping_methods_id',
                                    'value' => function($model) {
                                        return ($model->shipping_methods_id > 0) ?$model->shippingMethod->title  : 'No set';
                                    }
                                ],
	                            'tracking_number',
	                            'status',
                                [
                                    'attribute'=>'created_on',
                                    'value' => function($model) {
                                        return $model->created_on;
                                    },
                                ],
                                [
                                    'content' => function ($model, $key, $index, $column) {
                                        if ($model->status == 'pending') {
                                            $html = Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', ['delete', 'id' => $model->id], ['data-method' => 'post', 'data-confirm' => LBL::_x('DELETE_PACKAGE_CONFIRM_MSG'),'class'=>'btn btn-primary']);
                                            $html .= '&nbsp;&nbsp;';
	                                        return $html .= Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['view', 'id' => $model->id], ['class'=>'btn btn-primary']);
                                        } else if ($model->status == 'shipped') {
                                            return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['view', 'id' => $model->id], ['class'=>'btn btn-primary']);
                                        } else {
                                            return $model->status;
                                        }
                                    }
                                ]
                            ],
                        ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>