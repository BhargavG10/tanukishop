<?php

use yii\widgets\ActiveForm;

use common\components\TBase as LBL;
$this->title = LBL::_x('CREATE_PACKAGES');
$this->params['breadcrumbs'][] = ['label' => LBL::_x('PACKAGES'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <div class="col-lg-12" style="border-bottom: 2px solid #dedede;margin-bottom: 33px;">
                <div class="packages-create">
                    <h4><?=LBL::_x('PACKAGE_ID')?> : <?=$packageModel->name?></h4>
                </div>
                <hr/>
            </div>
            <div class="col-lg-12" >
			<?php $form = ActiveForm::begin(); ?>
				<h3 style="margin-bottom: 22px;" class="row inner-head">
                    <?=LBL::_x('PACKAGE_SHIPPING_ADDRESS')?>
                </h3>
                    <?php if ($shippingList) {
                        foreach($shippingList as $list) { ?>
                            <div class="col-lg-3" style="border-bottom: 1px solid #dedede;font-size: 16px;padding: 21px 0;background:#aec8cc;border-radius:10px;margin:0 5px">
                                <div class="col-md-1 text-center">
                                    <input style="margin-top: 30px;" type="radio" name="shipping" id="shipping<?=$list->id?>" value="<?=$list->id?>"/>
                                </div>
                                <div class="col-md-10">
                                    <label for='shipping<?=$list->id?>'>
					                    <?=$list->address_1?><br/>
					                    <?=$list->city?><br/>
					                    <?=$list->state?><br/>
					                    <?=$list->country?>
                                    </label>
                                </div>
                            </div>

	                    <?php }
                    } else {
                        echo \yii\helpers\Html::a(LBL::_x('CREATE_NEW_SHIPPING_ADDRESS'),['user-shipping-address/index']);
                    } ?>
					
					<div class='clearfix'></div>
					 <h3 style="margin-bottom: 22px;margin-top: 22px;" class="row inner-head"><?=LBL::_x('SELECT_SHIPPING_METHOD'); ?></h3>
					<?php foreach($shippingMethods as $methods) { ?>
                        <div class="col-lg-12" >
                            <div class="col-md-1 text-center">
                                <input  type="radio" name="shipping_methods_id" id="shipping_methods_id<?=$methods->id?>" value="<?=$methods->id?>"/>
                            </div>
                            <div class="col-md-10" >
                                <label for='shipping_methods_id<?=$methods->id?>'><?=$methods->title?></label>
                            </div>
                        </div>
                        <br/>
                    <?php } ?>
					<div class='clearfix'></div>
					<div>
					<br />
					<br />
					 <p class="pull-left"> Read more about shipping reates >><br />(go to https://www.tanukishop.com/site/shipping)</p>
                    <input type="submit" class="btn btn-warning pull-right" name="save" value="Proceed">
					</div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>