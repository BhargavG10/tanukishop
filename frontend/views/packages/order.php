<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase as LBL;
$tModel = new \common\components\TCurrencyConvertor;
$this->title = LBL::_x('CREATE_PACKAGES');
$this->params['breadcrumbs'][] = ['label' => LBL::_x('PACKAGES'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table{background: #fff;} .table-bordered {  margin-bottom: 47px;} .border-div{border-bottom: 2px solid #dedede;margin-bottom: 33px} .margin-bottom-27{margin-bottom: 27px;} .margin-bottom-22{margin-bottom: 22px;} #order-detail{z-index: 999999;} #order-detail .modal-content {margin: 10% auto;width: 163%;left:-162px;}
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <div class="col-lg-12 border-div">
                <div class="packages-create">
                    <h4><?=LBL::_x('PACKAGE_ID');?> : <?=$packageModel->name?></h4>
                </div>
                <hr/>
            </div>

            <div class="col-lg-12 border-div">
                <h3 style="margin-bottom: 22px;" class="row inner-head">
                    <?=LBL::_x('SHIPPING_ADDRESS')?>
                </h3>
                <table class="table table-bordered">
                    <tr>
                        <th><?=LBL::_x('NAME');?> : </th>
                        <td><?=$packageModel->first_name?> <?=$packageModel->last_name?></td>
                    </tr>
                    <tr>
                        <th><?=LBL::_x('COMPANY_NAME')?> : </th>
                        <td><?=$packageModel->company?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('ADDRESS_1')?> :</th>
                        <td><?=$packageModel->address_1?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('ADDRESS_2')?> :</th>
                        <td><?=$packageModel->address_2?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('APARTMENT_NUMBER')?> :</th>
                        <td><?=$packageModel->appt_no?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('CITY')?> :</th>
                        <td><?=$packageModel->city?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('STATE')?> :</th>
                        <td><?=$packageModel->state?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('COUNTRY')?> :</th>
                        <td><?=\common\components\TBase::countryDetail($packageModel->country)?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('ZIPCODE')?> :</th>
                        <td><?=$packageModel->zipcode?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('PHONE')?> :</th>
                        <td><?=$packageModel->phone?></td>
                    </tr>
                    <tr>
                        <th> <?=LBL::_x('EMAIL')?> :</th>
                        <td><?=$packageModel->email?></td>
                    </tr>
                </table>
            </div>

            <div class="col-lg-12 " >
                <h3 class="row inner-head margin-bottom-22">
                    <?=LBL::_x('SELECT_ORDER_FROM_LIST');?>
                </h3>

                <?php $form = ActiveForm::begin(); ?>
                <div class="row margin-bottom-27">
                    <table class="table-bordered table">
                        <tr>
                            <th><input type="checkbox" name="all" id="select_all" id="all" /> <label for='all'>All</label></th>
                            <th><?=LBL::_x('ORDER_ID');?></th>
                            <th><?=LBL::_x('ORDER_DATE');?></th>
                            <th><?=LBL::_x('INVOICE_NUMBER');?></th>
                            <th><?=LBL::_x('TOTAL');?></th>
                            <th>Detail</th>
                        </tr>
                        <?php  foreach($orderModel as $data) { ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="order_id[]" value="<?=$data->id?>">
                                </td>
                                <td><?='TNK0000'.$data->id?></td>
                                <td><?=date('d-m-Y',strtotime($data->date))?></td>
                                <td><?=$data->invoice?></td>
                                <td><?=$tModel->convert($data->total,'JPY','JPY')?></td>
                                <td><a href="#" class="order-detail-view" data-id="<?=$data->id?>">View Detail</a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <input type="submit" class="btn btn-success" name="save" value="Complete">
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
    <div class="modal fade" id="order-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

        </div>
    </div>
<?php
$this->registerJs('
var order_detail_url = "'.\yii\helpers\Url::to(['order/order-partial-view'],true).'"
',\yii\web\View::POS_BEGIN);
?>


<?php
$this->registerJs('
$("#select_all").click(function(event) {
  if(this.checked) {
      // Iterate each checkbox
      $(":checkbox").each(function() {
          this.checked = true;
      });
  }
  else {
    $(":checkbox").each(function() {
          this.checked = false;
      });
  }
});
',\yii\web\View::POS_READY);
?>