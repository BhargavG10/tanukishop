<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\TBase as LBL;
/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?=LBL::_x('PACKAGES')?></li>
    </ol>
</div>
<section>
    <div  class="notranslate container">
		<?=$this->render('/user/_left_nav')?>
        <div class="dashboard">
            <div class="clearfix">
                <h3 class="inner-head "><?=LBL::_x('PACKAGES')?></h3>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;">
            </div>
            <div class="col-md-12">
                <div class="table-responsive text-center">
                <table class="table">
                    <tr>
                        <td>
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
                                    'status',
                                    'first_name',
                                    'last_name',
                                    'company',
                                    'address_1:ntext',
                                    'address_2:ntext',
                                    'appt_no',
                                ],
                            ]);
                            ?>
                        </td>
                        <td>
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'city',
                                    'state',
                                    [
                                        'attribute'=>'country',
                                        'value' => (isset($model->countryName)) ? $model->countryName : ''
                                    ],
                                    'zipcode',
                                    'phone',
                                    'email:email',
                                    'created_on',
	                                [
		                                'attribute'=>'shipping_methods_id',
		                                'value' => (isset($model->shipping->title)) ? $model->shipping->title : ''
	                                ],
                                ],
                            ]);
                            ?>
                        </td>
                    </tr>
                </table>

                    <h3>Order Detail</h3>

                    <table class="table table-striped table-bordered detail-view ">
                        <tr>
                            <th class="text-center">Order ID</th>
                            <th class="text-center">Order Total</th>
                            <th class="text-center">Order Status</th>
                            <th class="text-center">Order Payment Status</th>
                            <th class="text-center">Order Date</th>
                            <th></th>
                        </tr>
                        <?php if ($model->packageOrders) {

                            foreach ($model->packageOrders as $orders) { ?>
                                <tr>
                                    <td>TNK0000<?=$orders->order_id?></td>
                                    <td><?=$orders->order->total?></td>
                                    <td><?=$orders->order->getStatus()?></td>
                                    <td><?=$orders->order->status?></td>
                                    <td><?=date('d-m-Y',strtotime($orders->order->date))?></td>
                                    <td><?=Html::a('View Detail',['order/view','id'=>$orders->order_id],['target'=>'_blank'])?></td>
                                </tr>
                            <?php
                            }
                        }?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

