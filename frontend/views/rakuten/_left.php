<?php
use yii\helpers\Html;
use common\models\Rakuten;
use yii\widgets\ActiveForm;
use common\components\TBase;
?>
<style>
    button.accordion{background:0 0;color:#444;cursor:pointer;width:100%;border:none;text-align:left;outline:0;font-size:15px;transition:.4s;padding:7px 4px}button.accordion.active,button.accordion:hover{background-color:#ddd}button.accordion:after{content:'\002B';color:#777;font-weight:700;float:right;margin-left:5px}button.accordion.active:after{content:"\2212"}div.panel{padding:0 6px 0 10px;max-height:0;overflow:hidden;transition:max-height .2s ease-out}
</style>
<h4 class="notranslate"><?=\common\components\TBase::ShowLbl('CATEGORIES');?></h4>
<div class="panel-group left-side-bar translate" id="accordion">
    <label>
        <?php
        if (Yii::$app->request->get('cid')) {
        $data = Rakuten::categorySearch( Yii::$app->request->get( 'cid' ) );
        if ( isset( $data['current']['genreName'] ) ) {
            echo $data['current']['genreName'];
        }
        ?>
    </label>
    <hr>
	<?php

        if ( isset( $data['current']['genreName']) ) {
            if ( isset( $data['children'] ) ) {
                foreach ( $data['children'] as $key => $category ) { ?>
                    <button class="accordion"><?= $category['child']['genreName'] ?></button>
                    <div class="panel">
                        <a href="<?= \yii\helpers\Url::to( [
                            Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
                            'cid' => $category['child']['genreId']
                        ] ) ?>">
                            <?= $category['child']['genreName'] ?> (All)<br/>
                        </a>
                        <?php
                        $subData = Rakuten::categorySearch( $category['child']['genreId']);
                        if ( isset( $subData['children'] ) ) {
                            foreach ( $subData['children'] as $key => $subcategory ) {
                                ?>
                                <a href="<?= \yii\helpers\Url::to( [
                                    Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
                                    'cid' => $subcategory['child']['genreId']
                                ] ) ?>">
                                    <?= $subcategory['child']['genreName'] ?>
                                </a><br/>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
            } else if ( isset( $data['current']['genreName'] ) ) { ?>
                <a>
                    <?php
                    if ( isset( $data['current']['genreName'] ) ) {
                        echo $data['current']['genreName'] . ' (All)';
                    }
                    ?>
                </a>
            <?php }
        }
	} else {
        $data = \common\models\Category::findAll(['shop'=>'rakuten','parent_id'=>0]);
        if ($data) {
            foreach ($data as $cat) { ?>
                <button class="accordion"><?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?></button>
                <div class="panel">
                    <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $cat->ref_id]) ?>">
                        <?= (\common\components\TBase::CLang() == 'ru-RU') ? $cat->title_ru : $cat->title_en; ?> (All)
                    </a>
                    <?php
                    $child = \common\models\Category::findAll(['parent_id' => $cat->id,'shop'=>'rakuten']); ?>
                    <?php foreach ($child as $childCat) { ?>
                        <div>
                            <a href="<?= \yii\helpers\Url::to([Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, 'cid' => $childCat->ref_id]) ?>">
                                <?= (\common\components\TBase::CLang() == 'ru-RU') ? $childCat->title_ru : $childCat->title_en; ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
    }
    ?>
</div>

<h4 class="notranslate"><?=\common\components\TBase::ShowLbl('FILTER_BY_PRICE')?></h4>
<hr>
<div class="row min-max notranslate">
    <?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl($param),'method' => 'GET']) ?>
    <input type="hidden" name="page" id="page" value="1">

    <div class="col-lg-12 right-pad-zero margin-bottom-7">
		<?=TBase::_x('CURRENT_PRICE')?><br/><hr/>
        <div class="col-xs-5 right-pad-zero " style="padding-left: 0px">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmin']) && $_REQUEST['pmin']!= '') ? $_REQUEST['pmin'] : ''; ?>" name="pmin" placeholder="<?=TBase::ShowLbl('MIN')?>">
        </div>
        <div class="col-xs-5 right-pad-zero " style="padding-left: 0px;margin-left: 27px;">
            <input type="number" class="form-control" value="<?=(isset($_REQUEST['pmax']) && $_REQUEST['pmax']!= '') ? $_REQUEST['pmax'] : ''; ?>" name="pmax" placeholder="<?=TBase::ShowLbl('MAX')?>">
        </div>
    </div>
    <input type="submit" name="search" value="<?=\common\components\TBase::ShowLbl('FILTER')?>" class="btn filter-btn">
    <?php ActiveForm::end() ?>
</div>
<div class="clearfix"></div>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
</script>