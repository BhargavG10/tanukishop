<?php
    use yii\helpers\Html;
    use \common\components\TBase;
    $this->title = TBase::ShowLbl('RAKUTEN_CATEGORY');
    $lang = TBase::CLang();
?>
<!-- listing page -->
<div class="m_container rakuten">
    <div class="container notranslate"><?=TBase::ShowLbl('rakuten'); ?></div>
</div>
<div class="container notranslate rakuten-cats">
    <div class="row marg-l0">
        <?php
        $j = 1;
        foreach ($categories as $cat) {
            if ($cat->parent_id == 0) {
                echo ($j == 1 || ($j % 4 == 0)) ? '<div class="row margin-bottom-20">' : '';
        ?>
                <div class="col-md-3 col-sm-3 cat-list <?php echo $j.'_'.($j % 4); ?>">
                    <p><?php $title = ($lang == 'en-US') ? $cat->title_en : $cat->title_ru;
                    echo Html::a($title,['rakuten/list','cid'=>$cat->ref_id],['style'=>'color:#fff;']);?> </p>
                    <ul>
                        <?php
                        foreach ($categories as $catSub) {
                            if ($cat->id == $catSub->parent_id) {
                                $title = ($lang == 'en-US') ? $catSub->title_en : $catSub->title_ru;
                                echo '<li>'.Html::a($title,['rakuten/list','cid'=>$catSub->ref_id]).'</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <?php
                echo ($j %4 == 0 ) ? '</div>' : '';
                $j++;
            }
        }
        ?>
        <div class="clearfix"></div>
    </div>
</div>
<!-- listing page -->
