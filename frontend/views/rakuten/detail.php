<?php
$model = new \common\components\TCurrencyConvertor;
use yii\helpers\Html;

use common\components\TBase as LBL;
$categoryID = $data['data']['data'][0]['Item']['genreId'];
$name = $data['data']['data'][0]['Item']['itemName'];
$this->title = $name;
$code = $data['data']['data'][0]['Item']['itemCode'];
$review = $data['data']['data'][0]['Item']['reviewAverage'];
$price = $data['data']['data'][0]['Item']['itemPrice'];
$shopCode = $data['data']['data'][0]['Item']['shopCode'];
$affiliateUrl = $data['data']['data'][0]['Item']['affiliateUrl'];
$itemCaption = $data['data']['data'][0]['Item']['itemCaption'];
$this->title = LBL::_x('RAKUTEN_PRODUCT_DETAIL');
$countries = \yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(), 'id', 'title');
$language = ['JPY'=>'JPY','USD'=>'USD','RUB'=>'RUB'];
?>
<style>
    .tab_link{color: #fff !important;text-decoration: none;}
</style>
<div class="m_container rakuten margin-bottom-20 notranslate">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a href="<?=\yii\helpers\Url::to(['rakuten/index']) ?>"><?=LBL::_x('rakuten')?></a></li>
            <?php if ($category) { ?>
                <li> <a href="<?=\yii\helpers\Url::to(['rakuten/list','title'=>$category->slug]) ?>"><?php
                    if (\common\components\Language::CLang() == 'en-US') {
                        echo $category->title_en;
                    } else {
                        echo $category->title_ru;
                    }
                    ?></a></li>
            <?php } ?>
        </ol>

    </div>
</div>
<section class="product-page">
    <div class="container">
        <div class="product-plug">
            <div class="row">
                <div class="col-md-5 notranslate">
                    <div class="row product-bx">
                        <div class="col-md-12 col-sm-12 bx-slid  wow fadeInUp white-bg bxsliderWrapper">
                            <ul id="bxslider-de">
                                <?php foreach($data['data']['data'][0]['Item']['mediumImageUrls'] as $item) { ?>
                                    <li><a href="<?=substr($item['imageUrl'],0,strpos($item['imageUrl'],'?')); ?>" data-lightbox="example-set"><img src="<?=substr($item['imageUrl'],0,strpos($item['imageUrl'],'?')); ?>"></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-12 col-sm-12  wow fadeInUp">
                            <div class="bxpager-slid">
                                <ul id="bxslider-pager">
                                    <?php
                                    $i = 0;
                                    foreach($data['data']['data'][0]['Item']['smallImageUrls'] as $item) { ?>
                                        <li data-slideIndex="<?=$i?>">
                                            <a><img src="<?=substr($item['imageUrl'],0,strpos($item['imageUrl'],'?')); ?>" height="110"></a>
                                        </li>
                                        <?php
                                        $i++;
                                    } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <h2 data-title="<?=Yii::t('app',$name);?>" class="product_title translate" data-short="0"><?=Yii::t('app',$name);?></h2>
                    <ul class="smartpay notranslate  margin-top-25">
                        <li><a href="javascript:void(0)"><?=Yii::t('app',$code);?></a></li>
                        <li>
                            <div id="rate1_rateblock" title="Rated 4.29 out of 5" class="star-rating notranslate">
                                <span id="rate1_stars" style="width:<?=(80*($review/5))?>px;" class="notranslate">
                            </div>
                        </li>
                    </ul>
                    <hr>
                    <form id="add-to-cart" action="/order/add-to-cart" method="GET">
                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
                    <ul class="select-box notranslate margin-top-25">
                        <li style="width:41%;">
                            <p style="font-size: 24px;">
                                <?=LBL::_x('PRICE')?>: <a style="margin-top: 7px;" class="amount" href="javascript:void(0)" data-usd="<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>" data-rub="<?=\common\models\CurrencyTable::convert($price,'JPY','RUB'); ?>" data-jpy="<?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?>">
                                    <?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?>
                                </a>
                                <span style="font-size:15px; ">(~<?=\common\models\CurrencyTable::convert($price,'JPY','USD'); ?>)</span>
                            </p>
                        </li>
                        <li style="width: 10%;">
                            <select class="form-control" name="quantity" id="quantity_class">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <input type="hidden" name="code" value="<?=$code?>">
                            <input type="hidden" name="sh" value="rakuten">
                        </li>

                        <li style="width:25%;">
                            <?php if (Yii::$app->request->get('cart_id')) { ?>
                                <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                    <?=LBL::_x('UPDATE_TO_CART'); ?>
                                </button>
                                <input type="hidden" name="cart_id" value="<?=Yii::$app->request->get('cart_id')?>">
                            <?php } else { ?>
                                <button class="btn btn-cart1"><img src="/tnk/images/cart-white.png" alt="cart" /> <img src="/tnk/images/cart-orange.png" alt="cart" />
                                    <?=LBL::_x('ADD_TO_CART'); ?>
                                </button>
                            <?php } ?>
                        </li>

                        <li><h5><a href="<?= \yii\helpers\Url::to(['favorite-product/create','shop'=>'rakuten','code'=>$data['data']['data'][0]['Item']['itemCode']])?>"><img src="/tnk/images/wishlist.png" alt="wishlist" /><?=LBL::_x('ADD_TO_WISHLIST')?></a></h5></li>
                    </ul>
                    </form>
                    <hr>
                    <ul class="seller-ul notranslate">
                        <li class="width-22-percent">
                            <h5><?=LBL::_x('SELLER_ITEMS'); ?></h5>
                            <h4><?=Yii::t('app',$shopCode);?></h4>
                        </li>
                        <li class="width-25-percent"><h6><a target="_blank" href="<?=$affiliateUrl?>"><?=LBL::_x('THIS_ITEM_PAGE_ON_RAKUTEN'); ?></a></h6></li>
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <li class="width-32-percent"><textarea class="form-control" placeholder="<?=LBL::_x('PLEASE_LOGIN_TO_SEND_MESSAGE')?>"></textarea></li>
                            <li>
                                <a href="<?=\yii\helpers\Url::to(['site/login','returnurl'=>\yii\helpers\Url::to(Yii::$app->request->getUrl(),true)])?>">
                                    <button class="btn btn-post">
                                        <?=LBL::_x('LOGIN')?>
                                    </button>
                                </a>
                            </li>
                        <?php } else { ?>
                            <?=Html::hiddenInput('mail-path',\yii\helpers\Url::to(['site/send-mail']),['id'=>'mail-path'])?>
                            <li class="width-32-percent"><textarea class="form-control" id="msg" placeholder="<?=LBL::_x('COMMENTS'); ?>"></textarea></li>
                            <li><button id="send-mail" class="btn btn-post"><?=LBL::_x('POST');?></button></li>
                        <?php }?>
                    </ul>
                </div>

                <div class="col-md-7 how-much1 position-relative notranslate">
                    <div class="how-much">
                        <div class="absolute"><?=LBL::_x('PLEASE_WAIT'); ?></div>
                        <h4><?=LBL::_x('HOW_MUCH_WILL_IT_COST');?> </h4>
                        <form class="shipping_calculator" id="shipping_calculator">
                        <ul class="select-box">
                            <li><input type="text" class="form-control form-control1" name="package_weight" id="package_weight" placeholder="<?=LBL::_x('PACKAGE_WEIGHT');?>" /></li>
                            <li><?php echo Html::dropDownList('country_id', null, $countries,['class'=>'form-control','prompt'=>LBL::_x('SHIP_TO')]) ; ?></li>
                            <li><?php echo Html::dropDownList('language_id', 'jpy', $language,['class'=>'form-control','prompt'=>LBL::_x('CURRENCY')]) ; ?></li>
                            <li> <button class="btn btn-cart1" id="calculate-shipping"><i class="fa fa-search" aria-hidden="true"></i></button></li>
                        </ul>
                            <input type="hidden" name="price" id="price" value="<?=$price?>">
                            <input type="hidden" name="tanuki_charge" id="tanuki_charge" value="300">
                        </form>
                        <p><?=LBL::_x('THE_LIMIT_OF_WEIGHT_OF_PACKAGE_IS_30KG');?></p>
                        <div class="no-shipping-msg" style="display: none;color:#df7b2b;">
                        </div>
                        <ul class="item-price hidden" id="item-shipping-calculation">
                            <li><strong><?=LBL::_x('PRICE');?>:</strong><?=\common\models\CurrencyTable::convert($price,'JPY','JPY'); ?></span></li>
                            <li><strong><?=LBL::_x('TANUKI_FEE');?>: </strong>¥ 300</li>
                            <li><strong><?=LBL::_x('INTERNATIONAL_SHIPPING');?>: </strong> ¥<label class="shipping_charge">4000</label></li>
                            <li><a href="javascript:void(0)"><strong><?=LBL::_x('TOTAL'); ?>: </strong>¥<label class="total_cost">10000</label> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-tabs">
            <div id="horizontalTab">
                <ul class="resp-tabs-list notranslate">
                    <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><?=LBL::_x('DESCRIPTION');?></li>
                    <li class="resp-tab-item" aria-controls="tab_item-1" role="tab">
                        <?=Html::a(LBL::_x('APPROXIMATE_WEIGHT_OF_DIFFERENT_GOODS'),['site/page','slug'=>\common\models\Page::getSlug(8)],['class'=>'tab_link'])?>
                    </li>
                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab">
                        <?=Html::a(LBL::_x('JAPANESE_CLOTHING_AND_SHOE_SIZES'),['site/page','slug'=>\common\models\Page::getSlug(9)],['class'=>'tab_link'])?>
                    </li>
                </ul>
                <div class="resp-tabs-container">
                    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><?=LBL::_x('DESCRIPTION');?></h2>
                    <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0">
                        <p data-short="0" data-title="<?=Yii::t('app',$itemCaption);?>" class="product_description translate"><?=Yii::t('app',$itemCaption);?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-similer">
            <h3 class="heading_bg"><span><?=LBL::_x('OTHER_SIMILAR_PRODUCTS');?></span></h3>
            <div class="row">
            <?php
                $similar_products = $rModel->RakutenAPI(['genreId'=>$categoryID], 1, 4);
                foreach ($similar_products['data']['data'] as $sproduct) {
                    if ($category) { ?>
                        <a class="no-hover" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$sproduct['Item']['itemCode'].'__'.$category->id])?>">
                    <?php } else { ?>
                        <a class="no-hover" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$sproduct['Item']['itemCode']])?>">
                    <?php } ?>

                    <div class="col-md-3 col-sm-6">
                        <div class="other-image">
                            <?php $img = (isset($sproduct['Item']['mediumImageUrls'][0])) ? $sproduct['Item']['mediumImageUrls'][0]['imageUrl'] : (isset($sproduct['Item']['smallImageUrls'][0])) ? $sproduct['Item']['smallImageUrls'][0]['imageUrl'] : ''; ?>
                            <img src="<?=substr($img,0,strpos($img,'?')); ?>" alt="product" height="122"/>
                        </div>
                        <div class="other-para"><p style="display: block;height: 83px;overflow: hidden;" class="translate" data-title="<?=$sproduct['Item']['itemName']; ?>"><?=$sproduct['Item']['itemName']; ?></p>
                            <h6><a class="no-hover">¥<?=$sproduct['Item']['itemPrice']; ?></a></h6>
                        </div>
                    </div>
                </a>
                <?php
                }
            ?>
            </div>
        </div>
    </div>
</section>
<?=$this->registerCss('
.form-control1{
margin-left:0px!important;
margin-bottom:0px!important;
}
a.amount{text-decoration:none;}

#quantity_class{
    color: #000;font-size: 17px;text-align: center;padding-left: 17px;
}
');?>
<?=$this->registerCssFile('@web/tnk/css/lightbox.min.css');?>
<?=$this->registerJsFile('@web/tnk/js/lightbox-plus-jquery.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?=$this->registerJsFile('@web/tnk/js/modernizr-2.6.2.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>