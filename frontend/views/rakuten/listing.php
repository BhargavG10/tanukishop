<?php
use common\components\TBase as LBL;
$model = new \common\components\TCurrencyConvertor;
?>
<div class="m_container rakuten margin-bottom-20">
    <div class="container ">
        <ol class="breadcrumb">
            <li><a href="<?=\yii\helpers\Url::to(['rakuten/index'])?>"><?=LBL::_x('rakuten')?></a></li>
            <?php if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') { ?>
                <li><a href="#"><?=$_REQUEST['s']; ?> </a></li>
            <?php } else if(isset($category->title_en)){ ?>
                <li><a href="#"><?=$category->title_en; ?> </a></li>
            <?php } ?>
        </ol>
    </div>
</div>
<div class="container">
    <!-- listing page -->
    <section class="listing-panel">
        <div class="container" style="position: relative;">
            <div class="row">
                <div class="col-md-2 col-sm-4 left-bar">
                    <div class="well">
                        <?php echo $this->render('_left',['category'=>$category,'param'=>$param]); ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-8 product-list-page">
                    <div class="well">
                        <div class="row notranslate">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <ul class="filters">
                                        <li><?=LBL::_x('SORT_BY');?>:</li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='lth') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'lth'])); ?>"><?=LBL::_x('LOW_TO_HIGH')?></a></li>
                                        <li class="<?=(isset($_REQUEST['sort']) && $_REQUEST['sort']=='htl') ? 'active' : '';?>"><a href="<?=Yii::$app->urlManager->createUrl(array_merge($param,['sort'=>'htl'])); ?>"><?=LBL::_x('HIGH_TO_LOW')?></a></li>
                                    </ul>
                                </div>
                                <div class="pull-left text-enter">
                                    <ul class="filters filter-result-count">
                                        <li class="text-center"><?=LBL::_x('TOTAL_RESULT');?>&nbsp;&nbsp;<label style="color: #782f4a;"><?=(isset($data['data']['totalRecords'])) ? number_format($data['data']['totalRecords']) : 0; ?></label></li>
                                    </ul>
                                </div>
                                <div class="pull-right">
                                    <ul class="filters currency-filters">
                                        <li><?=LBL::_x('CURRENCY');?>:</li>
                                        <li><a class="current" id="currency-jpy" data-currency="jpy"><?=LBL::_x('JAPANESE_YEN');?></a></li>
                                        <li><a id="currency-usd"  data-currency = "usd"><?=LBL::_x('US_DOLLAR');?></a></li>
                                        <li><a id="currency-rub"  data-currency = "rub"><?=LBL::_x('RUSSIAN_RUBLE');?></a></li>
                                        <li><a id="currency-cny"  data-currency = "cny"><?=LBL::_x('CHINESE_YUAN');?></a></li>
                                        <li><a id="currency-eur"  data-currency = "eur"><?=LBL::_x('EURO');?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="top_p list-brand">
                            <?php
                            $cnt = 0;
                            if (isset($data['data']['data']) && count($data['data']['data'])>0) {
                                    $j=1;
                                    foreach ($data['data']['data'] as $key => $value){
                                        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="product-lst position-relative">
                                            <div class="loading hidden">
                                                <?=LBL::_x('PLEASE_WAIT'); ?>
                                            </div>
                                            <?php if ($category) { ?>
                                                <a target="_blank" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode'],'cid'=>$category->ref_id])?>">
                                            <?php } else { ?>
                                                <a target="_blank" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode']])?>">
                                            <?php } ?>
                                                <div style="height: 128px;">
                                                    <?php if ((isset($value['Item']['mediumImageUrls'][0]['imageUrl']))){ ?>
                                                        <img src="<?=$value['Item']['mediumImageUrls'][0]['imageUrl']; ?>" alt="product">
                                                    <?php } else { ?>
                                                        <img src="<?='/tnk/images/no_image.gif'; ?>" alt="product" style="    width: 70%;">
                                                    <?php } ?>
                                                </div>
                                                <div class="product-list-product translate" id="product_title_<?=$cnt;?>" data-title="<?=$value['Item']['itemName']; ?>"><?=$value['Item']['itemName']; ?></div>
                                            </a>
                                            <h6 class="notranslate">
                                                <a class="amount" href="javascript:void(0)"
                                                   data-usd="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','USD'); ?>"
                                                   data-rub="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','RUB'); ?>"
                                                   data-jpy="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','JPY'); ?>"
                                                   data-cny="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','CNY'); ?>"
                                                   data-eur="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','EUR'); ?>">
                                                    <?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','JPY'); ?>
                                                </a>
                                            </h6>
                                            <div class="cate-list product-icon-listing">
                                                <div class="clearfix">
                                                    <?php
                                                    if ($category) { ?>
                                                        <div class="pull-left"><a  target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode'],'cid'=>$category->ref_id])?>"></a></div>
                                                    <?php } else {
                                                    ?>
                                                        <div class="pull-left"><a  target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode']])?>"></a></div>
                                                    <?php } ?>
                                                    <div class="pull-left"><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'rakuten','code'=>$value['Item']['itemCode']])?>"></a></div>
                                                    <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'rakuten','code'=>$value['Item']['itemCode']],true)?>"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
                                        $j++;
                                        $cnt++;
                                } ?>
                        </div>
                            <?php
                            } else {
                                echo "<div class='text-center'>".LBL::_x('NO_PRODUCT_FOUND')."</div>";
                            }
                            ?>
                        </div>
                        <hr>
                    <?php if (isset($data['data']['data']) && count($data['data']['data'])>15) { ?>
                    <div class="row">
                            <div class="col-md-12 notranslate">
                                <div class="text-center">
                                    <a href="#" class="product-load-more" data-shop="rakuten"><?=LBL::_x('OPEN_MORE_PRODUCTS')?></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
	                <?php if ($category) { ?>
                        <div class="seo-txt">
			                <?=$category->seo_text; ?>
                        </div>
	                <?php } ?>
                    </div>
                </div>
                <!-- slide left side bar div -->
                <div class="col-md-2 scroll-left-side-bar" style="position: absolute; display: none;">
                    <div class="well">
                        <a id="back-to-top" style="margin-top: 10px;width: 90%;" href="" class="btn filter-btn"><?=LBL::_x('GO_TO_FILTER_AREA'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?=$this->registerJS('
var ajax_url = "'.\yii\helpers\Url::to(['rakuten/list']).'";
',yii\web\View::POS_BEGIN)?>
<div class="loading-extend" style="display: none;"></div>
<div class="loading-image" style="display: none;"><?=\yii\helpers\Html::img('@web/tnk/images/ajax-loader.gif')?></div>
