<?php
use common\components\TBase;
if (isset($data['data']['data']) && count($data['data']['data'])>0) {
$model = new \common\components\TCurrencyConvertor;
$cnt = 0;
    $j=1;
    foreach ($data['data']['data'] as $key => $value){
        echo ($j == 1) ? '<div class="row '.$j.'">' : '';
    ?>
    <div class="col-sm-3">
        <div class="product-lst position-relative">
            <div class="loading hidden">
                <?=TBase::ShowLbl('PLEASE_WAIT'); ?>
            </div>
            <?php if ($category) { ?>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode'],'cid'=>$category->ref_id])?>">
            <?php } else { ?>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode']])?>">
            <?php } ?>
                <div style="height: 128px;">
                    <?php if ((isset($value['Item']['mediumImageUrls'][0]['imageUrl']))){ ?>
                        <img src="<?=$value['Item']['mediumImageUrls'][0]['imageUrl']; ?>" alt="product">
                    <?php } else { ?>
                        <img src="<?='/tnk/images/no_image.gif'; ?>" alt="product" style="    width: 70%;">
                    <?php } ?>
                </div>
                <div class="product-list-product translate" id="product_title_<?=$cnt;?>" data-title="<?=$value['Item']['itemName']; ?>"><?=$value['Item']['itemName']; ?></div>
            </a>
            <h6 class="notranslate">
                <a class="amount" href="javascript:void(0)" data-usd="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','USD'); ?>" data-rub="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','RUB'); ?>" data-jpy="<?=\common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','JPY'); ?>">
                    <?php
                    if (isset($_REQUEST['lang'])) {
                        if ($_REQUEST['lang'] == 'jpy') {
                            echo \common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','JPY');
                        } else if ($_REQUEST['lang'] == 'usd') {
                            echo \common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','USD');
                        } else if ($_REQUEST['lang'] == 'cny') {
                            echo \common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','CNY');
                        } else if ($_REQUEST['lang'] == 'eur') {
                            echo \common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','EUR');
                        } else {
                            echo \common\models\CurrencyTable::convert($value['Item']['itemPrice'],'JPY','RUB');
                        }
                    }
                    ?>
                </a>
            </h6>
            <div class="cate-list product-icon-listing">
                <div class="clearfix">
                    <?php
                    if ($category) { ?>
                        <div class="pull-left"><a  target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode'],'cid'=>$category->ref_id])?>"></a></div>
                    <?php } else {
                    ?>
                    <div class="pull-left" ><a target="_blank" class="detail-i" href="<?= \yii\helpers\Url::to(['rakuten/detail','id'=>$value['Item']['itemCode']])?>"></a></div>
                    <?php } ?>
                        <div class="pull-left" ><a class="cart-i"  href="<?= \yii\helpers\Url::to(['order/add-to-cart','sh'=>'rakuten','code'=>$value['Item']['itemCode']])?>"></a></div>
                    <div class="pull-left no-border"><a class="wish-i" data-href="<?= \yii\helpers\Url::toRoute(['favorite-product/create','shop'=>'rakuten','code'=>$value['Item']['itemCode']],true)?>"></a></div>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo (($j % 4 == 0)) ? '</div><div class="row '.$j.'">' : '';
        $j++;
        $cnt++;
} ?>
</div>
<?php
} else {
    echo "0";
    exit;
}
?>
