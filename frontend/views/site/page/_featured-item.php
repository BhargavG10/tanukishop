<?php
use \common\components\TBase;
$model = new \common\helper\Tanuki;
$modelT = new \common\components\TCurrencyConvertor;

?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <ul class="top_p">
                <?php
                if ($rakuten) {
                    foreach ($rakuten as $result) { ?>
                        <li class="position-relative">
                            <div class="loading hidden">Please wait...</div>
                            <a
                               href="<?= \yii\helpers\Url::to(['rakuten/detail', 'id' => $result]) ?>">
                        <?php $data = $model->RakutenAPI(['itemCode'=>$result]);
                        if (isset($data['data']) && isset($data['data']['data']) && isset($data['data']['data'][0]) && isset($data['data']['data'][0]['Item'])) {

                            if (isset($data['data']['data'][0]['Item']['mediumImageUrls']) &&
                                isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]) &&
                                isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'])
                            ) {
                                $img = $data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'];
                                $img = substr($img,0,strpos($img,'?'));
                            } else if (isset($data['data']['data'][0]['Item']['smallImageUrls']) &&
                                isset($data['data']['data'][0]['Item']['smallImageUrls'][0]) &&
                                isset($data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'])
                            ) {
                                $img = $data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'];
                                $img = substr($img,0,strpos($img,'?'));
                            }
                            ?>
                            <img height="128" width="128" src="<?=$img; ?>" alt="product"><br><div class="translate" data-title="<?=$data['data']['data'][0]['Item']['itemName']; ?>"><?=$data['data']['data'][0]['Item']['itemName']; ?></div>
                            <p><?= $modelT->convert($data['data']['data'][0]['Item']['itemPrice'], 'JPY', 'JPY'); ?></p>
                            <?php
                        } else {
                            echo $result;
                        } ?>
                            </a>
                            </li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<!-- top product sec end -->
<!-- top selling product sec start -->
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <h3 class="heading_bg"><span></span></h3>
        </div>
        <div class="row">
            <ul class="top_p">
                <ul class="top_p">
                    <?php

                    if ($yahoo) {
                        $YImg='';
                        foreach ($yahoo as $result) {
                            $YImg = '';
                            $data = $model->YahooApiProductsDetail($result);
                            if (
                                isset($data['ResultSet']) &&
                                isset($data['ResultSet'][0]) &&
                                isset($data['ResultSet'][0]['Result']) &&
                                isset($data['ResultSet'][0]['Result'][0])
                            ) {

                                if (!isset($data['ResultSet'][0]['Result'][0]['Name'])) {
                                    continue;
                                }

                                if (
                                    isset($data['ResultSet'][0]['Result'][0]['Image']) &&
                                    isset($data['ResultSet'][0]['Result'][0]['Image']['Medium'])
                                ) {
                                    $YImg = $data['ResultSet'][0]['Result'][0]['Image']['Medium'];
                                    $YImg = str_replace('/g/','/l/',$YImg);
                                } else if (isset($data['ResultSet'][0]['Result'][0]['RelatedImages']) &&
                                    isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]) &&
                                    isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'])
                                ) {
                                    $YImg = $data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'];
                                    $YImg = str_replace('/g/','/l/',$YImg);
                                } else {
                                    $YImg = 'no-image';
                                }

                                ?>
                                <li class="position-relative">
                                    <div class="loading hidden">
                                        Please wait...
                                    </div>
                                    <a
                                       href="<?= \yii\helpers\Url::to(['yahoo/detail', 'id' => $result]) ?>">
                                        <img height="128" width="128" src="<?= $YImg ?>" alt="product"><br>
                                        <div class="translate" data-title="<?=$data['ResultSet'][0]['Result'][0]['Name']; ?>"><?=$data['ResultSet'][0]['Result'][0]['Name']; ?></div>
                                        <p><?= $modelT->convert($data['ResultSet'][0]['Result'][0]['Price']['_value'], 'JPY', 'JPY'); ?></p>
                                    </a>
                                </li>
                                <?php
                            }
                        }
                    }
                    ?>
                </ul>
            </ul>
        </div>
    </div>
</div>