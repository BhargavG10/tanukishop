<?php
$lang = \common\components\TBase::CLang();
$this->title = ($lang == 'en-US') ? $pageData->title_en : $pageData->title_ru;

$this->registerMetaTag(['name' => 'title', 'content' => $pageData->meta_title]);
$this->registerMetaTag(['name' => 'description', 'content' => $pageData->meta_desc]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);

?>
<div class="container page notranslate">
    <h3 class="about-head"><?=$this->title; ?></h3>
    <ol class="breadcrumb"></ol>
    <?=($lang == 'en-US') ? $pageData->detail_en : $pageData->detail_ru; ?>
</div>
<?php
$this->registerCss("
.container.page{
min-height:300px;
margin-top: 19px;
}
.container.page ul li{
    margin-left: 25px;
}
");
?>