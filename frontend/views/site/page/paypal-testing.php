<div class="notranslate">
<?php

// Express paypal
// ref : https://www.smashingmagazine.com/2011/09/getting-started-with-the-paypal-api/

//http://tanukishop.local/site/paypal-testing?success=1&token=EC-5R6575881N521825Y&PayerID=D264YJWCDT6MU
if (!isset($_REQUEST['token'])) {
//Our request parameters
    $requestParams = array(
        'RETURNURL' => 'http://tanukishop.local/site/paypal-testing?success=1',
        'CANCELURL' => 'http://tanukishop.local/site/paypal-testing?success=0'
    );

    $orderParams = array(
        'PAYMENTREQUEST_0_AMT' => '500',
        'PAYMENTREQUEST_0_SHIPPINGAMT' => '4',
        'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP',
        'PAYMENTREQUEST_0_ITEMAMT' => '496'
    );

//    $item = array(
//        'L_PAYMENTREQUEST_0_NAME0' => 'iPhone',
//        'L_PAYMENTREQUEST_0_DESC0' => 'White iPhone, 16GB',
//        'L_PAYMENTREQUEST_0_AMT0' => '496',
//        'L_PAYMENTREQUEST_0_QTY0' => '1'
//    );

    $paypal = new \common\components\Paypal();
    //$response = $paypal->request('SetExpressCheckout', $requestParams + $orderParams + $item);
    $response = $paypal->request('SetExpressCheckout', $requestParams + $orderParams);


    if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
        $token = $response['TOKEN'];
        header('Location: https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
        exit;
    }
}

if( isset($_GET['token']) && !empty($_GET['token']) ) { // Token parameter exists
    // Get checkout details, including buyer information.
    // We can save it for future reference or cross-check with the data we have
    $paypal = new \common\components\Paypal();
    $checkoutDetails = $paypal -> request('GetExpressCheckoutDetails', array('TOKEN' => $_GET['token']));

    // Complete the checkout transaction
    $requestParams = array(
        'TOKEN' => $_GET['token'],
        'PAYMENTACTION' => 'Sale',
        'PAYERID' => $_GET['PayerID'],
        'PAYMENTREQUEST_0_AMT' => '500.00', // Same amount as in the original request
        'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP' // Same currency as the original request
    );

    $response1 = $paypal->request('DoExpressCheckoutPayment',$requestParams);
    echo "<pre>";
    print_r($response1);
    exit;
    if( is_array($response1) && $response1['ACK'] == 'Success') { // Payment successful
        // We'll fetch the transaction ID for internal bookkeeping
        $transactionId = $response1['PAYMENTINFO_0_TRANSACTIONID'];
        print_r($response1);
        exit;
    }
}

/*
 *response
 * Array
(
    [TOKEN] => EC-1HM90431GJ713425U
    [SUCCESSPAGEREDIRECTREQUESTED] => false
    [TIMESTAMP] => 2016-12-14T05:51:58Z
    [CORRELATIONID] => f12a84678438a
    [ACK] => Success
    [VERSION] => 74.0
    [BUILD] => 000000
    [INSURANCEOPTIONSELECTED] => false
    [SHIPPINGOPTIONISDEFAULT] => false
    [PAYMENTINFO_0_TRANSACTIONID] => 33R468549B077805V
    [PAYMENTINFO_0_TRANSACTIONTYPE] => expresscheckout
    [PAYMENTINFO_0_PAYMENTTYPE] => instant
    [PAYMENTINFO_0_ORDERTIME] => 2016-12-14T05:51:58Z
    [PAYMENTINFO_0_AMT] => 500.00
    [PAYMENTINFO_0_TAXAMT] => 0.00
    [PAYMENTINFO_0_CURRENCYCODE] => GBP
    [PAYMENTINFO_0_PAYMENTSTATUS] => Pending
    [PAYMENTINFO_0_PENDINGREASON] => multicurrency
    [PAYMENTINFO_0_REASONCODE] => None
    [PAYMENTINFO_0_PROTECTIONELIGIBILITY] => Ineligible
    [PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE] => None
    [PAYMENTINFO_0_SECUREMERCHANTACCOUNTID] => 7HRQJ5VSDFC4N
    [PAYMENTINFO_0_ERRORCODE] => 0
    [PAYMENTINFO_0_ACK] => Success
)
 *
 * */

/*
$requestParams = array(
    'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
    'PAYMENTACTION' => 'Sale'
);

$creditCardDetails = array(
    'CREDITCARDTYPE' => 'Visa',
    'ACCT' => '4311191151620799',
    'EXPDATE' => '072020',
    'CVV2' => '123'
);

$payerDetails = array(
    'FIRSTNAME' => 'John',
    'LASTNAME' => 'Doe',
    'COUNTRYCODE' => 'US',
    'STATE' => 'NY',
    'CITY' => 'New York',
    'STREET' => '14 Argyle Rd.',
    'ZIP' => '10010'
);

$orderParams = array(
    'AMT' => '500',
    'ITEMAMT' => '496',
    'SHIPPINGAMT' => '4',
    'CURRENCYCODE' => 'JPY'
);

$item = array(
    'L_NAME0' => 'iPhone',
    'L_DESC0' => 'White iPhone, 16GB',
    'L_AMT0' => '496',
    'L_QTY0' => '1'
);

$paypal = new \common\components\Paypal();
$response = $paypal->request('DoDirectPayment',
    $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item
);

echo "<pre>";
print_r($response);
echo "</pre>";
exit;

if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
    // We'll fetch the transaction ID for internal bookkeeping
    echo "<pre>";
    print_r($response);
    echo "</pre>";
    $transactionId = $response['TRANSACTIONID'];
}
*/
/*
 *
 * response
 *
 * Array
(
    [TIMESTAMP] => 2016-12-14T05:50:43Z
    [CORRELATIONID] => 46603877e649c
    [ACK] => Success
    [VERSION] => 74.0
    [BUILD] => 24616352
    [AMT] => 500
    [CURRENCYCODE] => JPY
    [AVSCODE] => X
    [CVV2MATCH] => M
    [TRANSACTIONID] => 16G11681CV273733H
)
 * */
?>
</div>
