<?php
$sections = \common\models\HomeSection::find()->where(['status'=>1])->orderBy('serial_no ASC')->all();

if ($sections) {
    foreach ($sections as $section) {

        if (count($section->products) > 0) {
            ?>
            <div class="clearfix notranslate">
                <div class="row">
                    <h3 class="heading_bg">
                        <span><?= (\common\components\TBase::CLang() == 'en-US') ? $section->title_en : $section->title_ru; ?></span>
                    </h3>
                </div>
                <div class="rown translate clearfix">
                    <div class="col-md-12">
                        <div class="row">
                            <ul class="top_p">
                                <?php
                                if ($section->products) {
                                    foreach ($section->products as $product) {
                                        ?>
                                        <li class="position-relative">
                                            <a href="<?= \yii\helpers\Url::to([$product->shop . '/detail', 'id' => $product->product_id]) ?>">
                                                <?php $img_path = ($product->image_url) ? $product->image_url : Yii::$app->params['no-image'] ?>
                                                <img height="140" width="140" src="<?= $img_path ?>" alt="<?=$product->title; ?>"><br>

                                                <div style="height: 35px;overflow: hidden;" class="translate"><?=$product->title; ?></div>
                                                <p>￥<?=number_format($product->price); ?></p>
                                            </a>
                                        </li>
                                    <?php }
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}
?>