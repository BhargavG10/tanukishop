<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\components\TBase;
$model = new \common\helper\Tanuki;
$modelT = new \common\components\TCurrencyConvertor;

$this->title = $pageData->title_en;

$this->registerMetaTag(['name' => 'title', 'content' => $pageData->meta_title]);
$this->registerMetaTag(['name' => 'description', 'content' => $pageData->meta_desc]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);

?>
<section class="home">
    <div class="m_container">
        <div class="banner">
            <img src="tnk/images/main_b.jpg" alt="banner">
            <div class="b_search_cont mt20">
                <div class="col-lg-12"><?=TBase::ShowLbl('SLIDER_TITLE')?></div>
                <div class="col-lg-12 search_bg">
                    <?php $form = ActiveForm::begin([
                            'id' => 'main-searching-form',
                            'options' => ['class' => 'form-horizontal'],
                            'action' => ['search'],
                            'method' => 'GET',
                        ]) ?>
                        <div class="row">
                            <div class="col-sm-11 search_wb">
                                <div class="col-sm-3 col-xs-6">
                                    <select name="shop" class="c-select">
                                        <option value="amazon"><?=TBase::ShowLbl('amazon'); ?></option>
                                        <option value="rakuten"><?=TBase::ShowLbl('rakuten'); ?></option>
                                        <option value="yahoo"><?=TBase::ShowLbl('yahoo_shopping'); ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-9 col-xs-12"><input class="form-control" type="text" name="x" placeholder="<?=TBase::ShowLbl('find_order')?>"></div>
                            </div>
                            <div class="col-sm-1 col-xs-6">
                                <?php echo Html::submitButton('', ['class' => 'search_btn']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end() ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<!-- banner end -->
<!-- banner bottom sec start -->
<div class="container free-register">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="icon_bg ">
                    <div><img src="tnk/images/cheep_s.png" alt="cheap shipping" ></div>
                    <div><?=TBase::ShowLbl('cheep_shipping_price')?></div>
                </div>
                <div class="icon_bg1 ">
                    <div><img src="tnk/images/24_s.png" alt="shipping" ></div>
                    <div><?=TBase::ShowLbl('24_hrs_shipping')?></div>
                </div>
                <div class="icon_bg ">
                    <div><img src="tnk/images/guarantee.png" alt="Guarantee" ></div>
                    <div><?=TBase::ShowLbl('guarantee')?></div>
                </div>
                <div class="icon_bg1 ">
                    <div><img src="tnk/images/one-sc.png" alt="one shipping" ></div>
                    <div><?=TBase::ShowLbl('one_shopping_cart')?></div>
                </div>
                <div class="icon_bg ">
                    <div><img src="tnk/images/help.png" alt="help" ></div>
                    <div><?=TBase::ShowLbl('help')?></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mt10"><a href="<?php echo \yii\helpers\Url::to(['site/login'])?>"><img src="tnk/images/free_regi.jpg" alt="free registration"></a></div>
    </div>
</div>
<!-- banner bottom sec end -->
<div class="container home">
    <div class="featured-product">
        <div class="row">
            <h3 class="heading_bg"><span><?=TBase::ShowLbl('top_products')?></span></h3>
        </div>
        <!-- top product sec start -->
        <?php /* ?><div class="row">
            <div class="col-md-12">

                <div class="row">
                    <ul class="top_p">
                        <?php
                        if ($rakuten) {
                            foreach ($rakuten as $result) {
                                $data = $model->RakutenAPI(['itemCode'=>$result]);
                                if (isset($data['data']) && isset($data['data']['data']) && isset($data['data']['data'][0]) && isset($data['data']['data'][0]['Item'])) {

                                    if (isset($data['data']['data'][0]['Item']['mediumImageUrls']) &&
                                        isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]) &&
                                        isset($data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'])
                                    ) {
                                        $img = $data['data']['data'][0]['Item']['mediumImageUrls'][0]['imageUrl'];
                                        $img = substr($img,0,strpos($img,'?'));
                                    } else if (isset($data['data']['data'][0]['Item']['smallImageUrls']) &&
                                        isset($data['data']['data'][0]['Item']['smallImageUrls'][0]) &&
                                        isset($data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'])
                                    ) {
                                        $img = $data['data']['data'][0]['Item']['smallImageUrls'][0]['imageUrl'];
                                        $img = substr($img,0,strpos($img,'?'));
                                    }
                                    ?>
                                    <li class="position-relative">
                                        <div class="loading">
                                            Please wait...
                                        </div>
                                        <a target="_blank"
                                           href="<?= \yii\helpers\Url::to(['rakuten/detail', 'id' => $result]) ?>">
                                            <img height="128" width="128" src="<?=$img; ?>" alt="product"><br><span class="translate" data-title="<?=$data['data']['data'][0]['Item']['itemName']; ?>"><?=$data['data']['data'][0]['Item']['itemName']; ?></span>
                                            <p><?= $modelT->convert($data['data']['data'][0]['Item']['itemPrice'], 'JPY', 'JPY'); ?></p>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div><?php */?>
        <!-- top product sec end -->
        <!-- top selling product sec start -->
        <?php /*?><div class="row">
            <div class="col-md-12">
                <div class="row">
                    <h3 class="heading_bg"><span></span></h3>
                </div>
                <div class="row">
                    <ul class="top_p">
                        <ul class="top_p">
                            <?php

                            if ($yahoo) {
                                $YImg='';
                                foreach ($yahoo as $result) {
                                    $YImg = '';
                                    $data = $model->YahooApiProductsDetail($result);
                                    if (
                                        isset($data['ResultSet']) &&
                                        isset($data['ResultSet'][0]) &&
                                        isset($data['ResultSet'][0]['Result']) &&
                                        isset($data['ResultSet'][0]['Result'][0])
                                    ) {

                                        if (!isset($data['ResultSet'][0]['Result'][0]['Name'])) {
                                            continue;
                                        }

                                        if (
                                            isset($data['ResultSet'][0]['Result'][0]['Image']) &&
                                            isset($data['ResultSet'][0]['Result'][0]['Image']['Medium'])
                                        ) {
                                            $YImg = $data['ResultSet'][0]['Result'][0]['Image']['Medium'];
                                            $YImg = str_replace('/g/','/l/',$YImg);
                                        } else if (isset($data['ResultSet'][0]['Result'][0]['RelatedImages']) &&
                                            isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]) &&
                                            isset($data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'])
                                        ) {
                                            $YImg = $data['ResultSet'][0]['Result'][0]['RelatedImages'][0]['Medium'];
                                            $YImg = str_replace('/g/','/l/',$YImg);
                                        } else {
                                            $YImg = 'no-image';
                                        }

                                        ?>
                                        <li class="position-relative">
                                            <div class="loading">
                                                Please wait...
                                            </div>
                                            <a target="_blank"
                                               href="<?= \yii\helpers\Url::to(['yahoo/detail', 'id' => $result]) ?>">
                                                <img height="128" width="128" src="<?= $YImg ?>" alt="product"><br><span class="translate" data-title="<?=$data['ResultSet'][0]['Result'][0]['Name']; ?>"><?=$data['ResultSet'][0]['Result'][0]['Name']; ?></span>
                                                <p><?= $modelT->convert($data['ResultSet'][0]['Result'][0]['Price']['_value'], 'JPY', 'JPY'); ?></p>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </ul>
                </div>
            </div>
        </div><?php */?>
    </div>
    <?php
    if (TBase::CLang() == 'en-US') {
        echo $pageData->detail_en;
    } else {
        echo $pageData->detail_ru;
    }

    ?>
</div>