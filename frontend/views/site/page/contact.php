<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\components\TBase;
$lang = TBase::CLang();
$this->title = ($lang == 'en-US') ? $pageData->title_en : $pageData->title_ru;

$this->registerMetaTag(['name' => 'title', 'content' => $pageData->meta_title]);
$this->registerMetaTag(['name' => 'description', 'content' => $pageData->meta_desc]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);
?>
<!-- contact page -->
<section>
    <div class="container page notranslate">
        <h3 class="about-head"><?=$this->title; ?></h3>
        <ol class="breadcrumb"></ol>
        <div class="col-sm-7 mt20">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <div class="cont-form mt20"><?=$form->field($model, 'name')->textInput(['autofocus' => true]) ?></div>
                <div class="cont-form "><?= $form->field($model, 'email') ?></div>
                    <?php echo $form->field($model, 'subject')->hiddenInput()->label(false); ?>
                <div class="cont-form "><?= $form->field($model, 'body')->textArea(['rows' => 6]) ?></div>
                <div class="cont-form "><?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?></div>
                <div class="cont-form "><label ></label><input type="submit" class="btn btn-info submit" value="<?=TBase::ShowLbl('SUBMIT')?>" id="submit" name="submit"></div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-sm-5">
            <h4><?=TBase::ShowLbl('ADDRESS')?></h4>
            <div class="add_bg">
                <ul>
                    <?=($lang == 'en-US') ? $pageData->detail_en : $pageData->detail_ru; ?>
                </ul>
                <ul class="social-cont">
                    <li><a href="<?=TBase::TanukiSetting('facebook_url');?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="<?=TBase::TanukiSetting('twitter_url');?>"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    <li><a href="<?=TBase::TanukiSetting('google_plus');?>"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-12 mt20">
            <?=TBase::TanukiSetting('contact_us_address_iframe')?>
        </div>
    </div>
</section>
<?php
$this->registerCss("
.container.page{
min-height:300px;
margin-top: 19px;
}
");
?>