<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\components\TBase;
$model = new \common\helper\Tanuki;
$modelT = new \common\components\TCurrencyConvertor;

$this->title = $pageData->title_en;

$this->registerMetaTag(['name' => 'title', 'content' => $pageData->meta_title]);
$this->registerMetaTag(['name' => 'description', 'content' => $pageData->meta_desc]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $pageData->meta_keywords]);

?>
<style>
    .slider-text{  display: none;  }
    .search_bg{background: none;}
    .search_bg{margin-top: 168px;}
    .search_wb{box-shadow: 2px 6px 10px #888;}
    .search_btn {
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
        box-shadow: 2px 6px 10px #888;
    }
    .b_search_cont {
        left: 50%;
        transform: translateX(-50%);
    }
	
</style>
<section class="home notranslate home-banner">
    <div class="m_container">

        <div class="banner">
		<?php $slider = (TBase::CLang() == 'en-US')  ? 'main_slider_en.jpg' : 'main_slider_ru.jpg'; ?>
         <?=Html::a(Html::img('@web/uploads/'.$slider),['/yahoo-auctions/index'],['class'=>'banner-image'])?>
           
            <div class="b_search_cont">
                <div class="col-lg-12 slider-text"><?=TBase::ShowLbl('SLIDER_TITLE')?></div>
                <div class="col-lg-12 search_bg" style="margin-top: 0px;">
                    <?php $form = ActiveForm::begin([
                            'id' => 'main-searching-form',
                            'options' => ['class' => 'form-horizontal'],
                            'action' => ['search'],
                            'method' => 'GET',
                        ]) ?>
                        <div class="row">
                            <div class="col-sm-11 search_wb">
                                <div class="col-sm-3 col-xs-6">
                                    <select name="shop" class="c-select">
                                        <option value="auction"><?=TBase::ShowLbl('yahoo_auction'); ?></option>
                                        <option value="yahoo"><?=TBase::ShowLbl('yahoo_shopping'); ?></option>
<!--                                        <option value="amazon">--><?//=TBase::ShowLbl('amazon'); ?><!--</option>-->
                                        <option value="rakuten"><?=TBase::ShowLbl('rakuten'); ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-9 col-xs-12"><input class="form-control" type="text" name="x" placeholder="<?=TBase::ShowLbl('find_order')?>"></div>
                            </div>
                            <div class="col-sm-1 col-xs-6" style="padding-left: 0px">
                                <?php echo Html::submitButton('', ['class' => 'search_btn']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<!-- banner end -->
<!-- banner bottom sec start -->
<div class="container free-register notranslate">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="icon_bg ">
                    <div><?=Html::img('@web/tnk/images/cheep_s.png'); ?></div>
                    <div><?=TBase::ShowLbl('cheep_shipping_price')?></div>
                </div>
                <div class="icon_bg1 ">
                    <div><?=Html::img('@web/tnk/images/24_s.png'); ?></div>
                    <div><?=TBase::ShowLbl('24_hrs_shipping')?></div>
                </div>
                <div class="icon_bg ">
                    <div><?=Html::img('@web/tnk/images/guarantee.png'); ?></div>
                    <div><?=TBase::ShowLbl('guarantee')?></div>
                </div>
                <div class="icon_bg1 ">
                    <div><?=Html::img('@web/tnk/images/one-sc.png'); ?></div>
                    <div><?=TBase::ShowLbl('one_shopping_cart')?></div>
                </div>
                <div class="icon_bg ">
                    <div><?=Html::img('@web/tnk/images/help.png'); ?></div>
                    <div><?=TBase::ShowLbl('help')?></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mt10">
            <a href="<?php echo \yii\helpers\Url::to(['site/login'])?>">
                <?=Html::img('@web/tnk/images/free_regi.jpg'); ?>
            </a>
        </div>
    </div>
</div>
<!-- banner bottom sec end -->
<div class="container home ">
    <div class="home-featured-product">
        <div class="text-center lb-loader"><?=Html::img('@web/tnk/images/ajax-loader.gif'); ?></div>
    </div>
    <div class="notranslate">
        <?php
        if (TBase::CLang() == 'en-US') {
            echo $pageData->detail_en;
        } else {
            echo $pageData->detail_ru;
        }
        ?>
    </div>
</div>
<?php
$this->registerJs("
var featureURl = '".\yii\helpers\Url::to(['site/featured-product'])."';
",yii\web\View::POS_BEGIN);
?>