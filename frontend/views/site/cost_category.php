<?php
    use \common\components\TBase;
    use \common\models\CurrencyTable;
    ?>

<style>
    .table>tbody>tr>td,.table>tbody>tr>th,.table>tfoot>tr>td,.table>tfoot>tr>th,.table>thead>tr>td,.table>thead>tr>th{padding:7px;font-size:15px}.table{margin-bottom:4px}.recommend{color:#df7b2b}th.text-center{background-color:#df7b2b;color:#fff;padding:8px!important}.ibox-content{clear:both;background-color:#fff;color:inherit;padding:15px 20px 20px;border-color:#e7eaec;border-image:none;border-style:solid solid none;border-width:1px 0}
    #item-shipping-calculation_category .ibox-content {
        border: none;
    }
</style>
<?php
$Fprice = 0;

$Fprice = ($result->weight_base ==0) ? $result->international_shipping_cost : ($data['package_weight']*$result->international_shipping_cost);
$total =0;
$optionCharge = ($data['option']==1)?TBase::TanukiSetting('Category_shipping_option_1'):TBase::TanukiSetting('Category_shipping_option_2');
$total = $data['price'] + $result->delivery_in_japan_cost + $result->tanuki_fee + $Fprice + $optionCharge;

?>
<div class="ibox-content">
<table class="table-striped table notranslate">
    <tr>
        <td ><?=TBase::ShowLbl('PRICE')?></td>
        <td ><?=CurrencyTable::convert($data['price'],'JPY','JPY'); ?></td>
        <td ><?=CurrencyTable::convert($data['price'],'JPY','RUB'); ?></td>
    </tr>
    <tr>
        <td ><?=TBase::ShowLbl('Shipping_in_japan')?></td>
        <td ><?=CurrencyTable::convert($result->delivery_in_japan_cost,'JPY','JPY'); ?></td>
        <td ><?=CurrencyTable::convert($result->delivery_in_japan_cost,'JPY','RUB'); ?></td>
    </tr>
    <tr>
        <td ><?=TBase::ShowLbl('TANUKI_CHARGES')?></td>
        <td ><?=CurrencyTable::convert($result->tanuki_fee,'JPY','JPY'); ?></td>
        <td ><?=CurrencyTable::convert($result->tanuki_fee,'JPY','RUB'); ?></td>
    </tr>
    <tr>
        <td ><?=TBase::ShowLbl('International_shipping_label')?></td>
        <td ><?=CurrencyTable::convert($Fprice,'JPY','JPY'); ?></td>
        <td ><?=CurrencyTable::convert($Fprice,'JPY','RUB'); ?></td>
    </tr>
    <tr>
        <td ><?=TBase::ShowLbl('Option_label')?></td>
        <td ><?=($data['option']==1)?CurrencyTable::convert(TBase::TanukiSetting('Category_shipping_option_1'),'JPY','JPY'):CurrencyTable::convert(TBase::TanukiSetting('Category_shipping_option_2'),'JPY','JPY');  ?></td>
        <td ><?=($data['option']==1)?CurrencyTable::convert(TBase::TanukiSetting('Category_shipping_option_1'),'JPY','RUB'):CurrencyTable::convert(TBase::TanukiSetting('Category_shipping_option_2'),'JPY','RUB');  ?></td>
    </tr>
    <tr>
        <td><strong><?=TBase::ShowLbl('ИТОГО')?></strong></td>
        <td ><?=CurrencyTable::convert($total,'JPY','JPY'); ?></td>
        <td ><?=CurrencyTable::convert($total,'JPY','RUB'); ?></td>
    </tr>
</table>
    </div>
<?=\yii\bootstrap\Html::a(TBase::ShowLbl('MORE_ABOUT_INTERNATIONAL_SHIPPING'),['/site/shipping'],['style'=>"color:#df7b2b;"])?>