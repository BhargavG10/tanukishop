<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\TBase;
$this->title = TBase::ShowLbl('RESET_PASSWORD_REQUEST');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="notranslate">
    <div class="container">
        <h3 class="heading_bg"><span><?=TBase::ShowLbl('RESET_PASSWORD')?></span></h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-sm-12 mt20">
                    <div class="clearfix"></div>
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                    <div class="col-sm-6 reg-form">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="col-sm-12 mt20"><label ></label><input type="submit" class="btn btn-info register" value="<?=TBase::ShowLbl('SEND_PASSWORD')?>" id="submit" name="submit"></div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>