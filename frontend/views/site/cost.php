<?php
    use \common\components\TBase;
    $model = new \common\components\TCurrencyConvertor;
?>

<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 3px;
    }
    .table {  margin-bottom: 4px;  }
    .recommend{color:#df7b2b;}
    th.text-center{background-color:#df7b2b;color:#fff;padding: 8px!important; }
</style>
<p class="recommend"><?=TBase::ShowLbl('RECOMMENDED');?>:</p>
<table class="table-striped table">
    <tr>
        <th class="text-center"><?=TBase::ShowLbl('PRICE')?></th>
        <th class="text-center"><?=TBase::ShowLbl('QTY')?></th>
        <th class="text-center"><?=TBase::ShowLbl('TANUKI_CHARGES')?></th>
        <th class="text-center"><?=TBase::ShowLbl('shipping')?></th>
        <th class="text-center"><?=TBase::ShowLbl('TOTAL')?></th>
    </tr>
<?php

foreach($result as $key => $list) {
    if ($key == 0) {
        echo '<tr style="font-weight: bold;">';
    } else {
        echo '<tr>';
    }
	$language_id = Yii::$app->request->post('language_id');
	$language = ($language_id) ? $language_id : 'JPY';
	$amount = ((($data['price']*(int)$data['quantity'])+(int)$list['cost']+$data['tanuki_charge']));
    ?>
        <td class="text-center" style="width: 14%! important"><?=\common\models\CurrencyTable::convert($data['price'],'JPY',$language)?> </td>
        <td class="text-center" style="width:7%"><?=$data['quantity']?></td>
        <td class="text-center" style="width:19%"><?=\common\models\CurrencyTable::convert($data['tanuki_charge'],'JPY',$language);?></td>
        <td class="text-center">(<?=$list->method->title; ?>): <?=\common\models\CurrencyTable::convert($list['cost'],'JPY',$language)?> </td>

        <td class="text-center" style="color:#df7b2b;"><?=\common\models\CurrencyTable::convert($amount,'JPY',$language)?> </td>
    </tr>
<?php
}
?>
</table>
<?=\yii\bootstrap\Html::a(TBase::ShowLbl('MORE_ABOUT_INTERNATIONAL_SHIPPING'),['/site/shipping'],['style'=>"color:#df7b2b;"])?>