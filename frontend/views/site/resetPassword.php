<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\TBase;
$this->title = TBase::ShowLbl('RESET_PASSWORD');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="notranslate">
    <div class="container">
        <h3 style="text-align:center;" class="heading_bg"><span><?=TBase::ShowLbl('SIGN_UP_LOGIN')?></span></h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-sm-12 mt20">
                    <div class="col-sm-6"><h3 class="reg-h1"><?=TBase::ShowLbl('NEW_PASSWORD')?></h3></div>
                    <div class="clearfix"></div>
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                    <div class="col-sm-6 reg-form">
                        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                    </div>
                    <div class="col-sm-12 mt20"><label ></label><input type="submit" class="btn btn-info register" value="<?=TBase::ShowLbl('SUBMIT')?>" id="submit" name="submit"></div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>