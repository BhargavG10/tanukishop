<?php
/*
// Request Token Endpoint
$req_url = 'https://auth.login.yahoo.co.jp/oauth/v2/get_request_token';
// AuthZ Endpoint
$authurl = 'https://auth.login.yahoo.co.jp/oauth/v2/request_auth';
// Access Token Endpoint
$acc_url = 'https://auth.login.yahoo.co.jp/oauth/v2/get_token';
// callback_url
$cbc_url = 'https://www.tanukishop.com/';
// sample API
$api_url = 'http://auctions.yahooapis.jp/AuctionWebService/V2/openWatchList';
// Consumerkey
$conskey = 'dj0zaiZpPWFHaGNEMUtTRFJvayZzPWNvbnN1bWVyc2VjcmV0Jng9MTk-';
// Consumersecret
$conssec = '3a10fbe08dfb2d6e4198dccd456b2e959504ce9a';
session_name("pecloauthyjp");
session_start();
if(isset($_GET['clear']) && $_GET['clear']==1){
  $_SESSION=array();
}
if(!isset($_GET['oauth_token']) && (isset($_SESSION['state']) && $_SESSION['state']==1)) {$_SESSION['state'] = 0;}
try {
  $oauth = new OAuth($conskey,$conssec,OAUTH_SIG_METHOD_HMACSHA1,OAUTH_AUTH_TYPE_URI);
  $oauth->enableDebug();
  if(!isset($_GET['oauth_token']) && !$_SESSION['state'] || $_SESSION['state']==0 ) {
    $request_token_info = $oauth->getRequestToken($req_url,$cbc_url);
    $_SESSION['secret'] = $request_token_info['oauth_token_secret'];
    $_SESSION['state'] = 1;
    header('Location: '.$authurl.'?oauth_token='.$request_token_info['oauth_token']);
    exit;
  } else if($_SESSION['state']==1) {
    $oauth->setToken($_GET['oauth_token'],$_SESSION['secret']);
    $access_token_info = $oauth->getAccessToken($acc_url,'',$_GET['oauth_verifier']);
    $token_res = var_export($access_token_info, true);
    $_SESSION['state'] = 2;
    $_SESSION['token'] = $access_token_info['oauth_token'];
    $_SESSION['secret'] = $access_token_info['oauth_token_secret'];
    $_SESSION['osh'] = $access_token_info['oauth_session_handle'];
    $_SESSION['guid'] = $access_token_info['xoauth_yahoo_guid'];
  }
  if(isset($_GET['refresh']) && $_GET['refresh']==1){
    // update Access Token
    $oauth->setToken($_SESSION['token'],$_SESSION['secret']);
    $access_token_info = $oauth->getAccessToken($acc_url, $_SESSION['osh']);
    $token_res = var_export($access_token_info, true);
  }
  // API Access
  $oauth->setToken($_SESSION['token'],$_SESSION['secret']);
  $oauth->fetch($api_url);
  $res = $oauth->getLastResponse();
} catch(OAuthException $E) {
  print_r($E);
}
?>
<html>
<head>
<title>PECL OAuth Library Sample with Yahoo! JAPAN</title>
</head>
<body>
<h1>PECL OAuth Library Sample with Yahoo! JAPAN</h1>
<a href="./">reload</a>
<a href="./?refresh=1">Access Token Refresh</a>
<a href="./?clear=1">restart</a>
<p>Access Token Response : </p>
<pre><?php echo htmlspecialchars(@$token_res); ?></pre>
<p>API Response : </p>
<pre><?php echo htmlspecialchars(@$res); ?></pre>
</body>
*/
?>

<?php
/**
 * Example of making API calls for the Yahoo service
 *
 * @author     Pieter Hordijk <info@pieterhordijk.com>
 * @copyright  Copyright (c) 2014 The authors
 * @license    http://www.opensource.org/licenses/mit-license.html  MIT License
 */
use OAuth\OAuth1\Service\Yahoo;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;
/**
 * Bootstrap the example
 */
$cbc_url = 'https://www.tanukishop.com/';
// sample API
$api_url = 'http://auctions.yahooapis.jp/AuctionWebService/V2/openWatchList';
// Consumerkey
$conskey = 'dj0zaiZpPWFHaGNEMUtTRFJvayZzPWNvbnN1bWVyc2VjcmV0Jng9MTk-';
// Consumersecret
$conssec = '3a10fbe08dfb2d6e4198dccd456b2e959504ce9a';
// Session storage
$storage = new Session();
// Setup the credentials for the requests
$credentials = new Credentials(
    $conskey,
    $conssec,
    $currentUri->getAbsoluteUri()
);
// Instantiate the Yahoo service using the credentials, http client and storage mechanism for the token
$yahooService = $serviceFactory->createService('Yahoo', $credentials, $storage);
if (!empty($_GET['oauth_token'])) {
    $token = $storage->retrieveAccessToken('Yahoo');
    // This was a callback request from Yahoo, get the token
    $yahooService->requestAccessToken(
        $_GET['oauth_token'],
        $_GET['oauth_verifier'],
        $token->getRequestTokenSecret()
    );
    // Send a request now that we have access token
    $result = json_decode($yahooService->request('profile'));
    echo 'result: <pre>' . print_r($result, true) . '</pre>';
} elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
    // extra request needed for oauth1 to request a request token :-)
    $token = $yahooService->requestRequestToken();
    $url = $yahooService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
    header('Location: ' . $url);
} else {
    $url = $currentUri->getRelativeUri() . '?go=go';
    echo "<a href='$url'>Login with Yahoo!</a>";
}
