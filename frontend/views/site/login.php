<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
USE \common\components\TBase;
$this->title = TBase::ShowLbl('SIGN_UP_LOGIN');
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="notranslate">
    <div class="container">
        <h3 style="text-align:center;" class="heading_bg"><span><?=TBase::ShowLbl('SIGN_UP_LOGIN')?></span></h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="col-sm-12 mt20">
                    <div class="col-sm-6"><h3 class="reg-h1"><?=TBase::ShowLbl('REGISTER')?></h3></div>
                    <div class="col-sm-6 mt10 txt-r"><span class="star">*</span><?=TBase::ShowLbl('REQUIRED_FIELD')?></div>
                    <div class="clearfix"></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-signup',
                        'options' => [
                            'onsubmit' => "yaCounter48497555.reachGoal ('registrationtanuki'); return true;"
                        ]

                    ]); ?>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'first_name')->textInput() ?>
                        </div>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'last_name')->textInput() ?>
                        </div>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'password')->passwordInput() ?>
                        </div>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'confirmPassword')->passwordInput() ?>
                        </div>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'email') ?>
                        </div>
                        <div class="col-sm-6 reg-form">
                            <?= $form->field($signUpModel, 'language')->dropDownList(['en-US'=>'English','ru-RU'=>'Russian']) ?>
                        </div>

                        <div class="col-sm-12 reg-form">
                            <?= $form->field($signUpModel, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
                                'template' => '<div class="">{image} <a href="javascript:;" id="fox">'.TBase::ShowLbl('REFRESH').'</a></div><div class="">{input}</div>',
                                'captchaAction'=>'site/captcha',
                            ])?>

                        </div>
                        <div class="col-sm-12">
                            <?php echo $form->field($signUpModel, 'terms', ['options' => ['tag' => 'span',], 'template' => "{input}"])->checkbox(['checked' => false,'class'=>'mr10']);?>
                        </div>
                        <div class="col-sm-12 mt20"><label ></label>
                            <?= Html::submitButton(TBase::ShowLbl('CREATE_ACCOUNT'), ['class' => 'btn btn-info register', 'name' => 'register-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-sm-1 c-line">
                <?=Html::img('@web/tnk/images/center-line.jpg') ?>
            </div>
            <div class="col-sm-5">
                <div class="col-sm-12 mt100">
                    <div class="col-sm-12"><h3 class="reg-h1"><?=TBase::ShowLbl('ALREADY_ACCOUNT')?></h3></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                    ]); ?>
                        <div class="col-sm-12 reg-form">
                            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                        </div>
                        <div class="col-sm-12 reg-form">
                            <?= $form->field($model, 'password')->passwordInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'rememberMe')->checkbox(['class'=>'mr10']) ?>
                        </div>
                        <div class="col-sm-6 txt-r"><?= Html::a(TBase::ShowLbl('FORGET_PASSWORD'), ['site/request-password-reset']) ?></div>
                        <div class="col-sm-12 mt20">
                            <label ></label>
                            <?= Html::submitButton(TBase::ShowLbl('LOGIN'), ['class' => 'btn btn-info login', 'name' => 'login-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->registerJs('
    $(document).ready(function () {
        $("#fox").click(function (event) {
            event.preventDefault();
            $(this).parent().children(\'img\').click();
        })
    });
    ');

$this->registerJs('
    $("#signupform-first_name").focus();
    ',yii\web\View::POS_READY);

$this->registerCss('
.reg-form{position:relative;}
.help-block-error{position: absolute;text-align: right;right: 14px;}
');

?>

