
<?php
use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use common\components\TBase as LBL;

$this->title = LBL::_x('AUCTION_FAVORITE_PRODUCTS');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-cart1 { width: 34% !important; } a.btn-primary{border:none;border-radius: 0px;} .dashboard{ width:87% !important; }.summary{    text-align: right;}
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=LBL::_x('home'); ?></a></li>
        <li class="active"><?= Html::encode($this->title) ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>

        <div class=" dashboard">
            <h3 class="inner-head"><?=LBL::_x('AUCTION_FAVORITE_PRODUCTS')?></h3>
            <div class="table-responsive">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table min-w700 table-bordered table-striped">
                    <tr>
						<th  style="width: 10%;text-align: center;" class="txt-c"><?=LBL::_x('IMAGE');?></th>
						<th  style="text-align: center;" width="" class="txt-c"><?=LBL::_x('AUCTION_ID');?></th>
                        <th  style="width: 26%;text-align: left;" width="" class="txt-c text-left"><?=LBL::_x('AUCTION_TITLE');?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('CURRENT_PRICE');?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('NUMBER_OF_BIDS')?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('SELLER');?></th>
                        <th style="width: 11%;text-align: center;" class="txt-c"><?=LBL::_x('TIME_LEFT');?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('LEADING_BIDDER');?></th>
                        <th  style="width: 11%;text-align: left;"  class="txt-c"></th>
                    </tr>
                    <?php
                    if ($listDataProvider->getCount()) {
                        Pjax::begin();
                        echo ListView::widget([
                            'dataProvider' => $listDataProvider,
                            'itemView' => '_auction_item',
                        ]);
                        Pjax::end();
                    } else{ ?>
                        <tr><td align="center" colspan="9"><?=LBL::_x('NO_PRODUCT_FOUND')?></td></tr>
                    <?php }  ?>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>