
<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use \common\components\TBase as LBL;
$this->title = LBL::_x('FAVORITE_PRODUCT');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-cart1 {width: 34% !important;}a.btn-primary{border:none;border-radius: 0px;} .dashboard{ width:87% !important; }
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=LBL::_x('home'); ?></a></li>
        <li class="active"><?= Html::encode($this->title) ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <h3 class="inner-head "><?=LBL::_x('FAVORITE_PRODUCT')?></h3>
            <div class="table-responsive">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table min-w700 table-bordered table-striped">
                    <tr>
                        <th  width="" class="txt-c"><?=LBL::_x('IMAGE')?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('PRODUCT_NAME')?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('SHOP')?></th>
                        <th  width="" class="txt-c"><?=LBL::_x('ACTION')?></th>
                    </tr>
                    <?php
                    if ($listDataProvider->getCount()) {
                        Pjax::begin();
                        echo ListView::widget([
                            'dataProvider' => $listDataProvider,
                            'itemView' => '_item',
                            'summary' => '',

                        ]);
                        Pjax::end();
                    } else{ ?>
                        <tr><td align="center" colspan="5"><?=LBL::_x('NO_PRODUCT_FOUND')?></td></tr>
                    <?php }  ?>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>