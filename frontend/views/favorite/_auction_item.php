<?php

use yii\helpers\Html;
$acModel = new \common\models\YahooAuctions();
$acModel->productsDetail($model->code,'small');
if (
        isset($acModel->YahooAction['ResultSet']['Result']['Title'])
) {
?>
    <style> .icon-eye-open,.icon-trash {  font-size: 16px;  color: #000;} table td { padding: 3px !important; vertical-align: middle ! important; } </style>
    <?php if(isset($acModel->YahooAction['ResultSet']['Result']['Status']) && $acModel->YahooAction['ResultSet']['Result']['Status'] != 'closed') { ?>
    <tr class="txt-c">
        <td class="text-center translate">
            <?=Html::a($acModel->getImage(), ['yahoo-auctions/detail', 'id' => $model->code]);?>
        </td>
        <td class="text-center translate">
            <?=Html::a($acModel->YahooAction['ResultSet']['Result']['AuctionID'], ['yahoo-auctions/detail', 'id' => $model->code]);?>
        </td>
        <td class="text-left translate"><?=$acModel->YahooAction['ResultSet']['Result']['Title']?></td>
        <td><?=$acModel->getPrice(); ?></td>
        <td><?=$acModel->YahooAction['ResultSet']['Result']['Bids']?></td>
        <td><?=Html::a($acModel->YahooAction['ResultSet']['Result']['Seller']['Id'],['yahoo-auctions/seller-list','sellerID'=>$acModel->YahooAction['ResultSet']['Result']['Seller']['Id']],['target'=>'_blank'])?></td>
        <td><?=$acModel->getTimeLeft(); ?></td>
        <td><?=$acModel->getTopBidder(); ?></td>
        <td width="">
            <?=Html::a('<i class="fa fa-eye"></i>', ['yahoo-auctions/detail', 'id' => $model->code],['class'=>'btn btn-primary','target'=>'_blank']);?>
            <?=Html::a('<i class="fa fa-trash"></i>', ['user/auction-delete-fav', 'id' => $model->id], [
                'class'=>'btn btn-primary',
                'data' => [
                    'confirm' => \common\components\TBase::ShowLbl('DELETE_CONFIRMATION'),
                    'method' => 'post',
                ]
            ])?>
        </td>
    </tr>
    <?php
    } else {
	    $model->delete();
    }
 } ?>