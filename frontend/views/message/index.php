<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = \common\components\TBase::ShowLbl('Messages');
?>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <ol class="breadcrumb"></ol>
        <?=$this->render('/user/_left_nav')?>
        <div class="dashboard">
            <h3 class="inner-head"><?= Html::encode($this->title) ?></h3>
            <div class="slimScrollDiv">
                    <?php
                    foreach($message as $msg) { ?>
                        <div class="msg-div clearfix">
                            <?php if ($msg->message_from == 'admin') { ?>
                            <div class="col-lg-1">
                                <?=\common\components\TBase::getNameChatTitle($msg->message_from)?>
                            </div>
                            <div class="updated-style col-lg-10">
                                <div class="admin_message message">
                                    <?=nl2br($msg->msg)?>
                                    <div class="date">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <?=date('d M, Y H:i:s a',strtotime($msg->date))?>
                                    </div>
                                </div>
                            </div>
                            <?php } else { ?>
                                <div class=" col-lg-1">
                                    <div class="bg-b1c6d8 avtar img-circle">
                                        <?=\common\components\TBase::getNameChatTitle($msg->message_from)?>
                                    </div>
                                </div>
                                <div class="bg-b1c6d8 updated-style col-lg-10">
                                    <div class="my_message message">
                                        <?=nl2br($msg->msg)?>
                                        <div class="date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?=date('d M, Y H:i:s a',strtotime($msg->date))?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <div class="msg-div clearfix message-fixed-div">
                <div class=" col-lg-1">
                    <div class="bg-b1c6d8 avtar img-circle">ME</div>
                </div>
                <div class="bg-b1c6d8 updated-style-1 col-lg-10">
                    <div class="my_message message">
                        <?php $form = ActiveForm::begin(['id' => 'form-message']); ?>
                        <?=$form->field($model,'msg')->textarea(['placeholder'=>\common\components\TBase::ShowLbl('WRITE_DOWN_YOUR_QUESTIONS')])->label(false);?>
                        <?= Html::submitButton(\common\components\TBase::_x('SUBMIT'), ['class' => 'btn btn-primary send-btn', 'name' => 'signup-button']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->registerJs('$(".slimScrollDiv").animate({ scrollTop: $(".slimScrollDiv").prop("scrollHeight")}, 1000);')?>

