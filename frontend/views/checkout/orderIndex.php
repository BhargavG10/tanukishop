<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\TBase as LBL;
$this->title = LBL::_x('ORDER_CONFIRM');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><?= Html::encode($this->title) ?></strong>
        </div>
    </div>
    <div class="panel-body">
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            'id',
            'subtotal',
            'total',
            'tanuki_charges',
            'currency',
            ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
</div>
