<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\helper\Tanuki;
$model = new Tanuki();
use \common\components\TBase;
$CurrencyModel = new \common\components\TCurrencyConvertor;
$tanukiCharges = TBase::TanukiCharges();
$this->title = TBase::_x('ORDER_CONFIRMATION');
?>
<section>
    <div class="container">
        <ol class="breadcrumb"></ol>
        <div class="left-menu checkout-left-bar">
            <h3 class="inner-head notranslate checkout-header-top"><?=TBase::_x('CHECKOUT_PROCESS')?></h3>
            <?=TBase::checkoutSideBarMenu($carts)?>
        </div>
        <div class=" dashboard checkout">
            <h3 class="inner-head border-b notranslate"><?=TBase::_x('ORDER_CONFIRMATION') ?></h3>
                <div class="table-responsive">
                    <table width="100%" border="1" cellspacing="0" cellpadding="0" class="table min-w700 border-1">
                        <tr class="color-7ac0c8 notranslate">
                            <th class="text-center"><?=TBase::_x('IMAGE'); ?></th>
                            <th><?=TBase::_x('ITEM_NAME'); ?></th>
                            <th class="text-center"><?=TBase::_x('SHOP');?></th>
                            <th class="text-center"><?=TBase::_x('PRICE'); ?></th>
                            <th class="text-center"><?=TBase::_x('QUANTITY'); ?></th>
                            <th class="text-center"><?=TBase::_x('TANUKI_CHARGES')?></th>
                            <th class="text-center"><?=TBase::_x('DOMESTIC_SHIPPING'); ?></th>
                            <th class="text-center"><?=TBase::_x('TOTAL'); ?></th>
                        </tr>
                        <?php
                        $count = $subtotal = $payable = $total = $payable_shipping = 0;
                        if (isset($carts) && count($carts)>0){
                            foreach($carts as $key => $item) {
                                $product_code = $item['product_id']; $shop = $item['shop']; $quantity = $item['quantity'];
                                ++$count;
                                $model->singleProductDetail($product_code,$shop);
                                $amount = $model->shopProductDetail($shop,'price');
                                $total = ($amount * $quantity) + $tanukiCharges + $item['domestic_shipping'];
                                $subtotal += $total;
                                $payable = $subtotal;
                                $name = $model->shopProductDetail($shop,'name');
                                ?>
                                <tr>
                                    <td style="width:10%" class="text-center notranslate">
                                        <img src="<?=$model->shopProductDetail($shop,'image'); ?>" height="60" width="60"/>
                                    </td>
                                    <td style="width:34%" data-title="<?=$name; ?>" data-limit="50" class="translate">
                                        <?=$name; ?>
                                        <hr/>
                                        <?php
                                        if (isset($item['params']) && count($item['params'])>0) {
                                            foreach ($item['params'] as $params) {
                                                echo '<strong>'.$params['attribute_name'].'</strong>:'.$params['attribute_value'].'<br/>';
                                            }
                                        }
                                        ?>
                                    </td>

                                    <td style="width:10%" class="text-center notranslate"><?=ucfirst($shop); ?></td>
                                    <td style="width:10%" class="text-center notranslate"><?=$CurrencyModel->convert($amount,'JPY','JPY'); ?></td>
                                    <td style="width:10%" class="text-center notranslate"><?=$quantity; ?></td>
                                    <td style="width:10%" class="text-center notranslate"><?=$CurrencyModel->convert($tanukiCharges,'JPY','JPY'); ?></td>
                                    <td style="width:10%" class="text-center notranslate"><?=$CurrencyModel->convert($item['domestic_shipping'],'JPY','JPY'); ?></td>
                                    <td style="width:10%" class="text-center notranslate"><?=$CurrencyModel->convert($total,'JPY','JPY'); ?></td>
                                </tr>
                                <?php
                            } ?>
                                <tr class="notranslate">
                                    <td colspan="7" align="right"><strong><?=TBase::_x('SUB_TOTAL')?></strong></td>
                                    <td class="text-center"><strong><?=$CurrencyModel->convert($subtotal,'JPY','JPY'); ?></strong></td>
                                </tr>
<!--                                <tr>-->
<!--                                    <td colspan="7" align="right"><strong>--><?//=TBase::_x('DOMESTIC_SHIPPING')?><!--</strong></td>-->
<!--                                    <td class="text-center"><strong>--><?//=$CurrencyModel->convert($payable_shipping,'JPY','JPY'); ?><!--</strong></td>-->
<!--                                </tr>-->
                                <tr class="background-white notranslate">
                                    <td colspan="7" align="right"><strong><?=TBase::_x('TOTAL_AMOUNT')?></strong></td>
                                    <td class="text-center"><strong><?=$CurrencyModel->convert($payable,'JPY','JPY'); ?></strong></td>
                                </tr>
                        <?php } else { ?>
                            <tr>
                                <td class="notranslate" colspan="7" align="center"><?=TBase::_x('EMPTY_CART'); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <?php if (count($carts)>0){ ?>
                    <div class="pull-right mt20 notranslate">
                        <?php echo Html::a(TBase::_x('PAYMENT'),['checkout/pay'],['class'=>'color-df7b2b padding-10-64 btn btn-default  ']); ?><!--  pla-ord -->
                    </div>
                    <div class="mt20 notranslate">
                        <?php echo Html::a(TBase::_x('BACK'),['checkout/shipping'],['class'=>'color-7ac0c8 padding-10-64 btn btn-default  ' ]); ?> <!--  cont-shop -->
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
    </div>
</section>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>
