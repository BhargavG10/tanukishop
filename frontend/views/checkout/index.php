<?php
use yii\helpers\Html;
use common\components\TBase;
use common\components\TBase as LBL;
use \common\helper\Tanuki;
$CurrencyModel = new \common\components\TCurrencyConvertor;
$model = new Tanuki();
/* @var $this yii\web\View */

$this->title = LBL::_x('CART_PRODUCTS');
?>
<section class="margin-top-20">
    <div class="container">
        <div class="left-menu checkout-left-bar">
            <h3 class="inner-head notranslate checkout-header-top"><?=LBL::_x('CHECKOUT_PROCESS')?></h3>
            <?=TBase::checkoutSideBarMenu($carts)?>
        </div>

        <div class="dashboard checkout">
            <h3 class="inner-head notranslate"><?=LBL::_x('CART_DETAIL');?></h3>
            <div class="table-responsive"><table width="100%" border="1" cellspacing="0" cellpadding="0" class="table min-w700 border-1">
                    <tr class="color-7ac0c8 notranslate" >
                        <th class="text-center"><?=LBL::_x('IMAGE'); ?></th>
                        <th><?=LBL::_x('ITEM_NAME'); ?></th>
                        <th class="text-center"><?=LBL::_x('ITEM_CODE'); ?></th>
                        <th class="text-center"><?=LBL::_x('SHOP');?></th>
                        <th class="text-center"><?=LBL::_x('QUANTITY'); ?></th>
                        <th class="text-center"><?=LBL::_x('PRICE'); ?></th>
                        <th class="text-center"><?=LBL::_x('DOMESTIC_SHIPPING'); ?></th>
                        <th class="text-center">&nbsp;</th>
                    </tr>
                    <?php
                    if (count($carts)>0)
                    {
                        foreach($carts as $key => $item) {
                            $product_id = $item['product_id'];
                            $shop = $item['shop'];
                            $quantity = $item['quantity'];
                            $model->singleProductDetail($product_id,$shop);
                            $name = $model->shopProductDetail($shop,'name');
                            ?>
                            <tr>
                                <td class="text-center notranslate">
                                    <img src="<?=$model->shopProductDetail($item['shop'],'image'); ?>" height="60" width="60"/>
                                </td>
                                <td style="width:34%" data-title="<?=$name; ?>" data-limit="50" class="translate">
                                    <?=$name; ?>
                                    <hr/>
                                    <?php
                                    if (isset($item['params']) && count($item['params'])>0) {
                                        foreach ($item['params'] as $params) {
                                            echo '<strong>'.$params['attribute_name'].'</strong>:'.$params['attribute_value'].'<br/>';
                                        }
                                    }
                                    ?>
                                </td>
                                <td class="text-center notranslate"><?=$product_id; ?></td>
                                <td class="text-center notranslate"><?=ucfirst($shop); ?></td>
                                <td style="width:8%" class="text-center" align="center notranslate"><?=$quantity; ?></td>
                                <td class="text-center notranslate"><?=$CurrencyModel->convert($model->shopProductDetail($shop,'price'),'JPY','JPY'); ?></td>
                                <td class="text-center notranslate"><?=$CurrencyModel->convert($item['domestic_shipping'],'JPY','JPY'); ?></td>
                                <td class="text-center notranslate">
                                    <?=Html::a('<i class="fa fa-trash delete-icon" aria-hidden="true"></i>',['cart-products/delete','cart_id'=>$item['id']],['class'=>'btn']); ?><br/>
                                </td>
                            </tr>
                            <?php
                        } ?>
                    <?php } else { ?>
                        <tr class="notranslate"><td colspan="7" align="center"><?=LBL::_x('EMPTY_CART'); ?></td></tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php if (count($carts)>0){ ?>

            <div class="pull-right mt20 notranslate">
            <?php echo Html::a(LBL::_x('CHECKOUT'),['checkout/shipping'],['class'=>'color-df7b2b padding-10-64 btn btn-default ']); ?><!-- pla-ord-->
            </div>
            <div class="mt20 notranslate">
                <?php echo Html::a(LBL::_x('CONTINUE_SHOPPING'),['site/index'],['class'=>'color-7ac0c8 padding-10-64 btn btn-default ']); ?>
            </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>