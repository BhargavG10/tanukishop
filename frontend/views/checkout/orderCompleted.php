<?php
    use common\components\TBase as LBL;
    $this->title = 'Tanukishop | '.LBL::_x('ORDERED_SUCCESSFULLY');
?>
<div class="order-confirm notranslate">
    <div class="jumbotron"></div>
    <div class="body-content">
        <div class="container">
            <h2 class="text-center"><?=LBL::_x('ORDERED_SUCCESSFULLY');?></h2>
        </div>
    </div>
</div>

<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>