<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;
use common\components\TBase as LBL;
use \common\helper\Tanuki;
$CurrencyModel = new \common\components\TCurrencyConvertor;
$TModel = new Tanuki();
$tanukiCharges = \common\components\TBase::TanukiCharges();
/* @var $this yii\web\View */
$this->title = 'Payment';
$month = ['01'=>'01 (Jan)','02'=>'02 (Feb)','03'=>'03 (March)','04'=>'04 (April)','05'=>'05 (May)','06'=>'06 (June)'];
$month += ['07'=>'07 (July)','08' =>'08 (Aug)','09'=>'09 (Sep)','10'=>'10 (Oct)','11'=>'11 (Nov)','12'=>'12 (Dec)'];
$year_range = range(date('Y'),date('Y')+15);
$year = array_combine($year_range,$year_range);
?>
<style>
.tab-content a.accordion-link{display: none;}
a.accordion-link{color:#fff;}
.table tbody td:first-child{width:50%!important;font-weight: bold;}
.table tbody tr:last-child{background: gainsboro;}
.mbal{
    font-size: 11pt;font-family: Arial;color: #000000;background-color: transparent;font-weight: 400;
    font-style: normal;font-variant: normal;text-decoration: none;vertical-align: baseline;white-space: pre-wrap;}
.payp{width: 80%;padding: 0px 8px;float: right;margin-top: 16%;}
</style>
<section>
    <div class="container notranslate">
        <ol class="breadcrumb"></ol>
        <div class="left-menu checkout-left-bar">
            <h3 class="inner-head notranslate checkout-header-top"><?=LBL::_x('CHECKOUT_PROCESS')?></h3>
            <?=TBase::checkoutSideBarMenu($carts)?>
        </div>
        <div class=" dashboard checkout">
            <h3 class="inner-head border-b"><?=LBL::_x('PAYMENT')?></h3>
            <div class="col-sm-8 col-xs-7 mt20">
                <p>
                    <?php
                    $lang = (TBase::CLang() == 'ru-RU') ? 'payment_page_text_russian' : 'payment_page_text_english';
                    echo TBase::TanukiSetting($lang);
                    ?>
                </p>
            </div>
            <div class="col-sm-4 col-xs-5"><?=Html::img(yii\helpers\Url::to('@web/tnk/images/ssl.png', true),['alt'=>'ssl img','class'=>"pull-right"]);?></div>
            <div class="clearfix"></div>
            <div class="col-sm-12 ">
                <ul class="nav nav-tabs responsive-tabs notranslate">
                    <li class="active" ><a href="#moneta"><?=LBL::_x('MONETA')?></a></li>
                    <li><a href="#paypal"><?=LBL::_x('PAYPAL')?></a></li>
                    <li><a href="#Bank"><?=LBL::_x('BANK_TRANSFER')?></a></li>
                    <li><a href="#pay_by_balance"><?=LBL::_x('PAY_BY_BALANCE');?></a></li>
                    <!--                    <li><a href="#Card">--><?//=LBL::_x('CREDIT_CARD')?><!--</a></li>-->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active clearfix" id="moneta">
		                    <?php $data = \common\models\Page::findOne(['slug'=>'moneta']); ?>
                            <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
			                    <?php echo (TBase::CLang() == 'ru-RU') ? $data->detail_ru : $data->detail_en ?>
                            </div>
		                    <?php
		                    $coefficient = (40+($total*0.002));
		                    $percentage = 0.039;

		                    $paypalCharges = round(($total * $percentage) + $coefficient);
		                    $totalCharges = ($paypalCharges + $total)
		                    ?>
                            <div class="col-md-7">
                                <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL')?></div>
						                    <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
						                    <?=$CurrencyModel->convert($paypalCharges,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div> <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                                <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('MONETA_REDIRECTED_MSG');?></p>
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="paypal-payment payment-div">
                                        <div class="clearfix">
                                            <div class="col-md-offset-3 col-md-6">
							                    <?=Html::a(LBL::_x('PAY'),['moneta-payment/index'],['class' => 'btn btn-default paysubmit show-model','style'=>'margin:0px!important;width:100%!important','data-msg'=>LBL::_x('PAY_BY_MONETA_PAYMENT_PROCCESS')]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="tab-pane clearfix" id="paypal">
		                    <?php $data = \common\models\Page::findAll(['slug'=>['paypal','credit-card','bank-transfer']]); ?>
                            <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
			                    <?php echo (TBase::CLang() == 'ru-RU') ? $data[0]->detail_ru : $data[0]->detail_en ?>
                            </div>
		                    <?php
		                    $paypalCharges =  (($total * 0.039 + 40 ) * 1.0402);
		                    $totalCharges = ($paypalCharges + $total)
		                    ?>
                            <div class="col-md-7">
                                <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL')?></div>
						                    <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
						                    <?=$CurrencyModel->convert($paypalCharges,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div> <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                                <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('PAYPAL_REDIRECTED_MSG');?></p>
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="paypal-payment payment-div">
                                        <div class="clearfix">
                                            <div class="col-md-offset-3 col-md-6">
							                    <?=Html::a(LBL::_x('PAY'),['payment/paypal-payment'],['class' => 'btn btn-default paysubmit show-model','style'=>'margin:0px!important;width:100%!important','data-msg'=>LBL::_x('PAY_BY_PAYPAL_PAYMENT_PROCCESS')]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="tab-pane" id="Bank">

                        <div class="tab-pane active clearfix" id="paypal">
                            <?php $data = \common\models\Page::findAll(['slug'=>['paypal','credit-card','bank-transfer']]); ?>
                            <div class="col-md-12" style="border-bottom: 1px solid #e2e2e2;padding-bottom: 27px;">
                                <?=(TBase::CLang() == 'ru-RU') ? $data[2]->detail_ru : $data[2]->detail_en ?>
                            </div>
                            <div class="col-md-7">
                                <h5><strong><?=LBL::_x('ORDER_SUMMARY');?></strong></h5>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('BANK_TRANSFER_AMOUNT');?></div>
                                            <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
                                            <?=LBL::_x('YOUR_BANK_FEES');?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                            <?=$CurrencyModel->convert($total,'JPY','JPY')?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                                <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('INVOICE_ISSUED_MSG');?></p>
                                <div class="panel-body" style="padding: 0px;">
                                    <div class="paypal-payment payment-div">
                                        <div class="clearfix">
                                            <div class="col-md-offset-3 col-md-6">
                                                <?=Html::a(LBL::_x('COMPLETE_ORDER'),['payment/bank-transfer-payment'],['class'=>'btn btn-default paysubmit show-model','style'=>'margin:0px!important;width:100%!important' ,'data-msg'=>LBL::_x('PAY_BY_BANK_PAYMENT_PROCCESS')]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pay_by_balance">
                        <div class="tab-pane active clearfix" id="paypal">
							<p class='mbal'><?=LBL::_x('MY_BALANCE')?></p>
							<div style="width:50%;float:left;">
								<p  class='mbal'><?=LBL::_x('USE_EXISTING_BALANCE_MSG');?>.</p>
								<p  class='mbal'><strong><?=LBL::_x('YOUR_EXISTING_BALANCE');?></strong>:  <?php $urbalance =  Yii::$app->user->identity->credit;?><?=$CurrencyModel->convert($urbalance,'JPY','JPY');?></p>
								<p  class='mbal'><strong><?=LBL::_x('TO_PAY'); ?></strong>:  <?=$CurrencyModel->convert($total,'JPY','JPY');?></p>
									----------------------------------<br />
								<?php
									$pay_by_balance = round($tanukiCharges) + round($total);
									if (Yii::$app->user->identity->credit > $pay_by_balance ) { ?>
										<i>	<?=LBL::_x('FUNDS_DEDUCTION_MSG');?></i><br /><br />
                                    <?php } else { ?>
										<i><?=LBL::_x('INSUFFICIENT_FUNDS_BALANCE_MSG');?>.<?=Html::a('click here',['user/add-fund']); ?></i>
									<?php } ?>
							</div> 
							<div style="width:20%;float:left;">
                                <?php if (Yii::$app->user->identity->credit > $pay_by_balance ) { ?>
                                    <?=Html::img('@web/img/tanuki_wallet.png',['alt'=>"tanuki_wallet", 'width'=>'70%'])?>
                                <?php } ?>
							</div>
							<div style="width:30%;float:left;">
							<?php
									if (Yii::$app->user->identity->credit > $pay_by_balance ) { ?>
										
										<p class ='payp'>
										    <?=Html::a(LBL::_x('PAY'),['payment/pay-balance-transfer'],['class'=>' col-lg-2 btn btn-default paysubmit show-model-balance','style'=>'margin:0px!important;width:100%!important','data-msg'=>LBL::_x('PAY_BY_BALANCE_PAYMENT_PROCCESS')]); ?>
										 </p>
										<?php
									} ?>
							</div>
                    </div>
                    </div>
	                <?php /*?><div class="tab-pane clearfix" id="Card"  style="padding-bottom: 27px;">
                        <div class="col-md-12">
                            <?=(TBase::CLang() == 'ru-RU') ? $data[1]->detail_ru : $data[1]->detail_en ?>
                        </div>
                        <?php
                        $form = ActiveForm::begin([
                            'action' =>['order/create-order'],
                            'id' => 'payment-form',
                            'method' => 'post',
                        ]); ?>
                        <div class="clearfix col-md-12" >
                            <div class="credit-card-payment payment-div">
                                <div class="clearfix">

                                    <div class="col-md-6">
                                        <h4><?=LBL::_x('CARD_DETAIL');?></h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'number')->textInput(['placeholder'=>'Credit Card Number','maxlength'=>16])->label(false); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'security_code')->textInput(['placeholder'=>'Cvv Code','maxlength'=>4])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'expire_month')->dropDownList($month)->label(false); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'expire_year')->dropDownList($year)->label(false);?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'first_name')->textInput(['placeholder'=>'First Name'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'last_name')->textInput(['placeholder'=>'Last Name'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4><?=LBL::_x('BILLING_ADDRESS')?></h4>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'STREET')->textInput(['placeholder'=>'Street'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'CITY')->textInput(['placeholder'=>'City'])->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'STATE')->textInput(['placeholder'=>'State'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'COUNTRYCODE')->dropDownList(
                                                    TBase::countryList())->label(false); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'ZIP')->textInput(['placeholder'=>'Zip Code'])->label(false); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?= $form->field($model, 'type')->dropDownList(
                                                    [
                                                        'Visa' => 'Visa',
                                                        'MasterCard' => 'Master Card',
                                                        'Discover' => 'Discover',
                                                        'Amex' => 'Amex',
                                                        'JCB' => 'JCB',
                                                        'Maestro' => 'Maestro',
                                                    ])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    echo Html::hiddenInput('method','Credit Card');
                                    echo Html::hiddenInput('orderID','',['id'=>'orderID']);
                                    echo Html::hiddenInput('paymentAction',\yii\helpers\Url::to(['payment/create-order']),['id'=>'paymentAction']);
                                    echo Html::hiddenInput('successAction',\yii\helpers\Url::to(['order/payment-completed']),['id'=>'successAction']);
                                    ?>

                                </div>
                                <div class="clearfix" id="order-error" style="color:red;padding: 17px;"></div>
                            </div>
                        </div>

                        <div class="col-md-7">
	                        <?php
	                        $coefficient = (40+($total*0.002));
	                        $percentage = 0.039;

	                        $cardCharges = round(($total * $percentage) + $coefficient);
	                        $totalCharges = ($cardCharges + $total);
	                        ?>

                            <h5><strong><?=LBL::_x('ORDER_SUMMARY')?></strong></h5>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL');?></div>
                                        <?=$CurrencyModel->convert($total,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('PAYPAL_PAYMENT_CHARGES')?></div>
                                        <?=$CurrencyModel->convert($cardCharges,'JPY','JPY');?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-md-8"><?=LBL::_x('TOTAL_AMOUNT')?></div>
                                        <?=$CurrencyModel->convert($totalCharges,'JPY','JPY')?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-lg-5">

                        </div>
                        <div class="col-md-5">
                            <h5 style="border-bottom: 1px solid #e2e2e2;margin: 0px;padding: 10px;"><strong>&nbsp;</strong></h5>
                            <p style="text-align: center;margin-top: 14px;"><?=LBL::_x('CREDIT_CARD_PROCESS_MSG')?>.</p>
                            <div class="panel-body" style="padding: 0px;">
                                <div class="paypal-payment payment-div">
                                    <div class="clearfix">
                                        <div class="col-md-offset-3 col-md-6">
                                            <?= Html::submitButton(LBL::_x('PAY'),['class' => 'btn btn-default paysubmit show-model-with-validation','style'=>'margin:0px!important;width:100%!important']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div><?php */ ?>
                </div>
            </div>
        </div>
</section>

<div id="myModal" class="modal">
    <div class="modal-content">
        <p></p>
    </div>
</div>


<?php
$this->registerJs("

    jQuery('.show-model').click(function() {
        jQuery('#myModal .modal-content p').html($(this).data('msg'));
        jQuery('.modal').show();
    });

    jQuery('.show-model-balance').click(function() {
//        ,'onclick'=>'return (confirm(\"Are you sure you want to pay by your available credits\"))'
        if (confirm(\"Are you sure you want to pay by your available credits\")) {
            jQuery('#myModal .modal-content p').html($(this).data('msg'));
            jQuery('.modal').show();
        } else  {
            return false;
        }
    });
    
    jQuery('.responsive-tabs').responsiveTabs({
        accordionOn: ['xs'] // xs, sm, md, lg
    });
",\yii\web\View::POS_READY);

?>
<?php $this->registerJsFile("@web/tnk/js/jquery.bootstrap-responsive-tabs.min.js",['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>