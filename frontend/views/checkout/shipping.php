<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\TBase;

/* @var $this yii\web\View */

$this->title = TBase::ShowLbl('SHIPPING_ADDRESS');
\Yii::$app->view->registerMetaTag([
    'name' => 'google',
    'content' => 'translate'
]);
?>

<section>
    <div class="container notranslate">
        <ol class="breadcrumb"></ol>
        <div class="left-menu notranslate checkout-left-bar">
            <h3 class="inner-head notranslate checkout-header-top"><?=TBase::ShowLbl('CHECKOUT_PROCESS')?></h3>
            <?=TBase::checkoutSideBarMenu($carts)?>
        </div>
        <div class=" dashboard checkout">
            <h3 class="inner-head border-b"><?=TBase::ShowLbl('SHIPPING_ADDRESS') ?></h3>
            <div class="col-sm-12 ">
                <?php $form = ActiveForm::begin(['id' => 'shipping-form',
                'options'=>['class'=>'form-horizontal'],
                    'fieldConfig'=>[
                    'template'=>"{label}\n<div class=\"col-lg-8\">
                                {input}\n
                                {error}</div>",
                    'labelOptions'=>['class'=>'col-lg-4 control-label'],
                ]]); ?>
                <div class="clearfix row">
                    <div class="col-lg-6">
                        <div class=""><?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?></div>
                        <div class=""><?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?></div>
                        <div class=""><?= $form->field($model, 'company')->textInput(); ?></div>
                        <div class=""><?= $form->field($model, 'address_1')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'address_2')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'appt_no')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'city')->textInput() ?></div>
                    </div>
                    <div class="col-lg-6">
                        <div class=""><?= $form->field($model, 'state')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'country')->dropDownList(TBase::countryList(),['prompt'=>'Select']) ?></div>
                        <div class=""><?= $form->field($model, 'zipcode')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'phone')->textInput() ?></div>
                        <div class=""><?= $form->field($model, 'email')->textInput() ?></div>
                        <div class=""><?php echo $form->field($model, 'shipping_method')->dropDownList(
                                \yii\helpers\ArrayHelper::map(\common\models\ShippingMethods::findAll(['status'=>1]),'id','title'),['prompt'=>'Select']); ?></div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="ship-form pull-right">
                        <?= Html::submitButton(TBase::ShowLbl('ORDER_CONFIRM'), ['class' => 'color-df7b2b padding-10-64  btn btn-default']) ?> <!--save-->
                    </div>
                    <div>
                        <?php echo Html::a(TBase::ShowLbl('BACK'),['checkout/index'],['class'=>'color-7ac0c8 padding-10-64  btn btn-default']); ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="clearfix"></div>
        </div>
</section>
<?php $this->registerCss(".header-bottom {display:none!important;}header{border-bottom: 1px solid #f2f2f2!important;padding-bottom: 15px!important;}"); ?>