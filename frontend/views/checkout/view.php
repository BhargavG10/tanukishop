<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$tanuki = new \common\helper\Tanuki();
$carts = Yii::$app->session['carts'];
?>

<div class="order-view">

    <div class="panel panel-default">
        <div class="panel-heading panel-heading-transparent"><strong><?= Html::encode($this->title) ?></strong></div>

        <div class="panel-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'subtotal',
                    'total',
                    'tanuki_charges',
                    'currency',
                    'user_id',
                    'method',
                    'status',
                    'date',
                    'response:ntext',
                    'invoice',
                    'paypal_id',
                    'failure_reason:ntext',
                ],
            ]) ?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <h4>Order Details</h4>
            <table class="table table-striped table-bordered detail-view">
                <?php

                $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);

                echo"<tr><td>Serial No</td>
                <td>Product name</td>
                <td>Product Image</td>
                <td>product id</td>
                <td>Shop name</td>
                <td>Quantity</td>
                <td>Price</td>
                <td>Action</td>
                    </tr>";
                ?>
                <?php
                $count = 0;;
                if (count($carts)>0){
                    foreach($carts as $key => $item) {
                        $itemcode = $item['product_code'];

                       // $data = $tanuki->RakutenAPI(['itemCode'=>$itemcode]);
                        ++$count;
                        $tanuki->singleProductDetail($item['product_code'],$item['shop']);
                        ?>
                        <tr>
                            <td><?=$count; ?></td>
                            <td><?=$tanuki->shopProductDetail($item['shop'],'name'); ?></td>
                            <td><?=$item['product_code']; ?></td>
                            <td><?=$item['shop']; ?></td>
                            <td><?=$item['quantity']; ?></td>
                            <td><?=$tanuki->shopProductDetail($item['shop'],'price'); ?></td>
                            <td><?=Html::a('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>',['order/delete','code'=>$item['product_code']]); ?></td>
                        </tr>
                        <?php
                    }
                } else { ?>
                    <tr>
                        <td colspan="6" align="center"><?=TBase::ShowLbl('EMPTY_CART'); ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <h4>Shipping Address</h4>
            <table class="table table-striped table-bordered detail-view">
                <?php
                $shippingaddress  = \common\models\ShippingAddress::findAll(['order_id'=>$model->id]);
                foreach($shippingaddress as $shipping){
                    echo"<tr><td>First Name</td>
                 <td>Last Name</td>
                 <td>Company</td>
                 <td>Address Line 1</td>
                 <td>Address Line 2</td>
                 <td>Apartment No</td>
                 <td>City</td>
                 <td>State</td>
                 <td>PinCode</td>
                 <td>Country</td>
                 <td>Phone</td>
                 <td>Email Address</td>
                 <td>Shipping method</td>
                 <td>Order Id</td>
                 </tr>";
                    echo"<tr><td>$shipping->first_name</td>
                 <td>$shipping->last_name</td>
                 <td>$shipping->company</td>
                 <td>$shipping->address_1</td>
                 <td>$shipping->address_2</td>
                 <td>$shipping->appt_no</td>
                 <td>$shipping->city</td>
                 <td>$shipping->state</td>
                 <td>$shipping->zipcode</td>
                 <td>$shipping->country</td>
                 <td>$shipping->phone</td>
                 <td>$shipping->email</td>
                 <td>$shipping->shipping_method</td>
                 <td>$shipping->order_id</td>
                 </tr>";
                }
                ?>
            </table>
        </div>
