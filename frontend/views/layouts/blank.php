<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \common\components\TBase;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon.png">
    <title><?=Yii::$app->name; ?> | <?= Html::encode($this->title) ?></title>
    <?php $this->head(); ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109776575-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109776575-1');
    </script>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="notranslate">
        <?= Alert::widget() ?>
    </div>
    <?= $content ?>
<?php $this->endBody();?>

</body>
</html>
<?php $this->endPage() ?>
