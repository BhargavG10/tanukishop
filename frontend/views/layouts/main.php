<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \common\components\TBase;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?=\common\components\TBase::CLang()?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
<!--        <meta name="google-translate-customization" content="e6d13f48b4352bb5-f08d3373b31c17a6-g7407ad622769509b-12"/>-->
        <meta name="google-translate-customization" content="32c2c762573f4c29-f694ecbfa1146c29-gb7889196ce7269c6-14" />
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?=Yii::$app->name; ?> | <?= Html::encode($this->title) ?></title>
    <?php $this->head(); ?>

    <style>
        body {
            top: 0px !important;
            position: static !important;
        }
        .goog-te-banner-frame {
            display: none !important
        }
    </style>
    <?php if (\common\components\Language::CLang() != 'en-US') { ?>
        <style>
            .product-page ul.seller-ul li.width-32-percent {
                width: 25%!important;
            }
        </style>
    <?php } ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109776575-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109776575-1');
    </script>

    <!-- Yandex.Metrika informer -->
    <!-- /Yandex.Metrika informer -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48497555 = new Ya.Metrika({
                        id:48497555,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        ecommerce:"dataLayer"
                    });
                } catch(e) { }
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/48497555" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter →-->

</head>
<body>
    <?php $this->beginBody() ?>

    <style>#google_translate_element,.skiptranslate{display:none;}body{top:0!important;}</style>
    <div id="google_translate_element"></div><script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: true, multilanguagePage: true}, 'google_translate_element');
        }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <?=TBase::header(); ?>

    <div class="">
        <div class="notranslate">
            <?= Alert::widget() ?>
        </div>
        <?=$content ?>
    </div>
    <?=TBase::footer()?>
    <?php

    $this->registerJs("
        var CREATING_YOUR_ORDER_PLEASE_WAIT = '".TBase::ShowLbl('CREATING_YOUR_ORDER_PLEASE_WAIT')."';
        var ORDER_CREATED_PAYMENT_PROCESSING_PLEASE_WAIT = '".TBase::ShowLbl('ORDER_CREATED_PAYMENT_PROCESSING_PLEASE_WAIT')."';
        var ERROR_WHILE_ORDER_CREATION_PLEASE_TRY_AGAIN = '".TBase::ShowLbl('ERROR_WHILE_ORDER_CREATION_PLEASE_TRY_AGAIN')."';
        var MESSAGE_SEND_SUCCESSFULLY = '".TBase::ShowLbl('MESSAGE_SEND_SUCCESSFULLY')."';
        var ERROR_WHILE_SENDING_MAIL_PLEASE_TRY_AGAIN_LATER = '".TBase::ShowLbl('ERROR_WHILE_SENDING_MAIL_PLEASE_TRY_AGAIN_LATER')."';
        var ERROR_WHILE_LOADING_SERVER_DATA = '".TBase::ShowLbl('ERROR_WHILE_LOADING_SERVER_DATA')."';
        var ERROR_WHILE_LOADING_SERVER_DATA = '".TBase::ShowLbl('ERROR_WHILE_LOADING_SERVER_DATA')."';
        ", yii\web\View::POS_HEAD, 'my-initialse');


    if (Yii::$app->controller->action->id == 'detail') {
        $this->registerJsFile('tnk/js/easy-responsive-tabs.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
        $this->registerJsFile('tnk/js/jquery.bxslider.min.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    }

    $this->registerJs("
        var shipping_url = '';
		 var shipping_url_category = '';
        var language = '';
        shipping_url = '".\yii\helpers\Url::to(['/site/product-shipping-cost'])."',
		 shipping_url_category = '".\yii\helpers\Url::to(['/site/category-shipping-cost'])."',
        language = '".\common\components\Language::CLang()."';
        ", yii\web\View::POS_END, 'my-options');

    if (Yii::$app->controller->action->id == 'detail') {
        $this->registerJsFile('js/_detailtnk.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    } else {
        $this->registerJsFile('js/_tnk.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    }
    $this->endBody();
    ?>

    </body>
<?php /*if($_SERVER['HTTP_HOST'] != 'tanukishop.local'){ ?>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = 'Sgm17hHXK4';var d=document;var w=window;function l(){
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
    <!-- {/literal} END JIVOSITE CODE -->

<?php }*/ ?>
</html>
<?php $this->endPage() ?>
<script>
    if ($('.product-list-page').length>0)
    {
        jQuery(window).on('scroll', function () {
            var top = jQuery(window).scrollTop(),
                divBottom = jQuery('.left-bar').offset().top + jQuery('.left-bar').outerHeight();
            if (divBottom > top) {
                jQuery('.left-bar').removeClass('outOfView');
                jQuery('.scroll-left-side-bar').hide();
            } else {
                jQuery('.scroll-left-side-bar').show(function () {
                    $(window).scroll(function () {
                        $(".scroll-left-side-bar").css({
                            "top": ($(window).scrollTop()) + "px",
                            "left": ($(window).scrollLeft()) + "px"
                        });
                    });
                });
            }
        });
        $('#back-to-top').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
    }
</script>

