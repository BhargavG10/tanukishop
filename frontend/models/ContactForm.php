<?php

namespace frontend\models;

use common\components\MailCamp;
use Yii;
use yii\base\Model;
use \common\components\TBase as LBL;
use \common\components\TBase;
/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required','message' => Yii::t('app', LBL::_x('CANNOT_BLANK'))],
            // email has to be a valid email address
            ['email', 'email','message' => Yii::t('yii', LBL::_x('VALID_EMAIL'))],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha','message' => Yii::t('yii', LBL::_x('NOT_CORRECT'))],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => LBL::_x('VCODE'),
            'name' => TBase::_x('NAME'),
            'email' => TBase::_x('EMAIL'),
            'subject' => TBase::_x('SUBJECT'),
            'body' => TBase::_x('BODY'),
            'captcha' => TBase::_x('CAPTCHA'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
    	return MailCamp::adminContactEmail($this,$email);
    }
}
