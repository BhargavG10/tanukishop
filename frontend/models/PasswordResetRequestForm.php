<?php
namespace frontend\models;

use common\components\TBase;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $type;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['email', 'email','message' => Yii::t('yii', TBase::ShowLbl('VALID_EMAIL'))],
            ['email', function ($attribute,$params)
                {
                    $user = User::findOne([
                        'status' => User::STATUS_ACTIVE,
                        'email' => $this->email,
                        'type' => $this->type,
                    ]);
                    if (!$user)
                        $this->addError($attribute, TBase::ShowLbl('EMAIL_NOT_EXIST'));

                }],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
        }
        
        if (!$user->save()) {
            return false;
        }

        return Yii::$app
            ->mailer
            ->compose(
                'registration/password-reset-token-'.$user->language,
                ['user' => $user]
            )
            ->setFrom([\common\components\TBase::TanukiSetting('support-email') => \Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . \Yii::$app->name)
            ->send();
    }

    public function existence($attribute,$params)
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
            'type' => $this->type,
        ]);
        if (!$user)
            $this->addError($attribute, TBase::ShowLbl('EMAIL_NOT_EXIST'));

    }
}
