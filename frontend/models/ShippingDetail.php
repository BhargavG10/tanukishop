<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\TBase;
/**
 * ContactForm is the model behind the contact form.
 */
class ShippingDetail extends Model
{

    public $first_name;
    public $last_name;
    public $company;
    public $address_1;
    public $address_2;
    public $appt_no;
    public $city;
    public $state;
    public $country;
    public $zipcode;
    public $phone;
    public $email;
    public $shipping_method;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name','last_name','address_1','city','country','phone','email'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['email', 'email','message' => Yii::t('yii', TBase::ShowLbl('VALID_EMAIL'))],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', TBase::ShowLbl('FIRST_NAME')),
            'last_name' => Yii::t('app', TBase::ShowLbl('LAST_NAME')),
            'company' => Yii::t('app', TBase::ShowLbl('COMPANY_NAME')),
            'address_1' => Yii::t('app', TBase::ShowLbl('ADDRESS_1')),
            'address_2' => Yii::t('app', TBase::ShowLbl('ADDRESS_2')),
            'appt_no' => Yii::t('app', TBase::ShowLbl('APPT_NO')),
            'city' => Yii::t('app', TBase::ShowLbl('CITY')),
            'state' => Yii::t('app', TBase::ShowLbl('STATE')),
            'country' => Yii::t('app', TBase::ShowLbl('COUNTRY')),
            'zipcode' => Yii::t('app', TBase::ShowLbl('ZIPCODE')),
            'phone' => Yii::t('app', TBase::ShowLbl('PHONE')),
            'email' => Yii::t('app', TBase::ShowLbl('EMAIL')),
            'shipping_method' => Yii::t('app', TBase::ShowLbl('SHIPPING_METHOD')),
            'order_id' => Yii::t('app', TBase::ShowLbl('ORDER_ID')),
        ];
    }
}
