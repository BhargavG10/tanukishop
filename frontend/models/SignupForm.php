<?php
namespace frontend\models;

use common\components\MailCamp;
use common\components\TBase;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $confirmPassword;
    public $captcha;
    public $terms;
    public $language;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name','last_name','confirmPassword','language'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['confirmPassword','compare','compareAttribute'=>'password','message'=>TBase::ShowLbl("PASSWORDS_DONT_MATCH")],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['email', 'email','message' => Yii::t('yii', TBase::ShowLbl('VALID_EMAIL'))],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => TBase::ShowLbl('EMAIL_ADDRESS_TAKEN.')],
            ['password', 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['password', 'string', 'min' => 6],
            ['captcha', 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            ['captcha', 'captcha','message' => Yii::t('app', TBase::ShowLbl('INVALID_CAPTCHA'))],
            ['terms', 'required','requiredValue' => 1,'message'=>TBase::ShowLbl('AGREE_TERMS_AND_CONDITIONS')]

            //['verifyCode', 'captcha','captchaAction'=>'/login/default/captcha']
        ];
    }

    public function attributeLabels()
    {
        return [
            'terms' => TBase::ShowLbl('JOIN_AGREEMENT'),
            'first_name' => TBase::ShowLbl('FIRST_NAME'),
            'last_name' => TBase::ShowLbl('LAST_NAME'),
            'password' => TBase::ShowLbl('PASSWORD'),
            'confirmPassword' => TBase::ShowLbl('CONFIRM_PASSWORD'),
            'email' => TBase::ShowLbl('EMAIL_USERNAME'),
            'status' => TBase::ShowLbl('STATUS'),
            'type' => TBase::ShowLbl('User Type'),
            'captcha' => TBase::ShowLbl('CAPTCHA'),
            'language' => TBase::ShowLbl('LANGUAGE'),
            'last_login' => TBase::ShowLbl('Last Login'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->email = $this->email;
        $user->status = 9; // in-active
        $user->type = 'user'; // in-active
        $user->language = $this->language; // in-active
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }


    public function sendEmail($password)
    {
        $user = User::findOne([
            'email' => $this->email,
        ]);
        $link = Yii::$app->urlManager->createAbsoluteUrl(['site/user-validate', 'l'=>base64_encode($this->email),'t' => base64_encode($user->auth_key)]);
        MailCamp::userRegistrationEmail($user,$link,$password);
        MailCamp::adminNewUserRegistration($user);
        return true;
    }

    public function adminEmail()
    {
        $user = User::findOne([
            'email' => $this->email,
        ]);

        return Yii::$app
            ->mailer
            ->compose(
                ['new-user'],
                ['user' => $user]
            )
            ->setFrom([TBase::TanukiSetting('support-email') => \Yii::$app->name])
            ->setTo('anilkumar.dhiman1@gmail.com')
            ->setSubject('New User Registration '.\Yii::$app->name)
            ->send();
    }
}
