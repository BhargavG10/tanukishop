<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\TBase;
/**
 * ContactForm is the model behind the contact form.
 */
class OrderPayment extends Model
{

    public $first_name;
    public $last_name;

    public $number;
    public $security_code;
    public $type;
    public $expire_month;
    public $expire_year;

    public $STREET;
    public $CITY;
    public $STATE;
    public $COUNTRYCODE;
    public $ZIP;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        // can use https://packagist.org/packages/andrewblake1/yii2-credit-card
        return [
            [['first_name','last_name','number','security_code','type','expire_month','expire_year'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            [['STREET','CITY','STATE','COUNTRYCODE','ZIP'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            [['first_name','last_name'], 'string'],
            [['number','security_code','expire_month','expire_year'], 'integer','message' => Yii::t('app', TBase::ShowLbl('INTEGER_ONLY'))],
            [['security_code'], 'integer','min'=>'4'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'number' => 'Card card Number',
            'security_code' => 'Cvv Code',
            'expire_month' => 'Expiration Month',
            'expire_year' => 'Expiration Year',
            'STREET' => 'Street',
            'CITY' => 'City',
            'STATE' => 'State',
            'COUNTRYCODE' => 'Country',
            'ZIP' => 'Zipcode'
        ];
    }
}
