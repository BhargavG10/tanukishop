<?php
namespace frontend\models;

use common\components\MailCamp;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class Chat extends Model
{
    public $msg;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msg'], 'required','message' => Yii::t('app', MailCamp::ShowLbl('CANNOT_BLANK'))],
        ];
    }

    public function attributeLabels()
    {
        return [
            'msg' => MailCamp::ShowLbl('MESSAGES'),
        ];
    }

    public function sendEmail()
    {
	    return MailCamp::adminChatQuery($this,Yii::$app->user->identity);
    }
}
