<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tnk_favorite_product}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $shop
 * @property integer $user_id
 * @property string $created_on
 */
class FavoriteProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tnk_favorite_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'shop', 'user_id', 'created_on'], 'required'],
            [['user_id'], 'integer'],
            [['created_on'], 'safe'],
            [['code'], 'string', 'max' => 100],
            [['shop'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'shop' => Yii::t('app', 'Shop'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }
}
