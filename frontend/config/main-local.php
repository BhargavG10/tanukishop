<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3T82a4QnNVNgGm7lA_eSNzUc6ssVXFZU',
        ],
    ],
];
