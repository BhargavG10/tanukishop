<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'dashboard/css/bootstrap.min.css',
        'dashboard/font-awesome/css/font-awesome.css',
        'css/site.css',
//        'dashboard/css/animate.css',
        'dashboard/css/style.css',
    ];
    public $js = [
        //'dashboard/js/jquery-2.1.1.js',
        'dashboard/js/bootstrap.min.js',
        'dashboard/js/plugins/metisMenu/jquery.metisMenu.js',
        'dashboard/js/plugins/slimscroll/jquery.slimscroll.min.js',
        'dashboard/js/inspinia.js',
//        'dashboard/js/plugins/pace/pace.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
