<?php

namespace backend\controllers;

use common\models\OrderDetail;
use common\models\Rakuten;
use common\models\ShippingAddress;
use common\models\ShippingMethodCalculationFinal;
use common\models\ShippingMethods;
use common\models\User;
use common\models\UserAuctionBlockedAmount;
use common\models\UserAuctions;
use Stripe\OrderReturn;
use Yii;
use common\models\Order;
use common\models\OrderSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
//    	$orders = Order::find()
////		    ->where(['in','type',['auction','buyout']])
//		    ->where(['in','type',['auction']])
//	                  ->all();
//    	foreach ($orders as $order) {
//		    echo "<pre>";
//    		print_r($order->id);
//    		echo "<br/>";

//    		if (isset($order->orderDetail[0]->product_id)) {
//    			echo $order->orderDetail[0]->product_id;
//			    $record = UserAuctions::findOne( [
//				    'auction_id' => $order->orderDetail[0]->product_id,
//				    'user_id'    => $order->user_id
//			    ] );
//
//			    print_r($record);


//			    print_r( $order->blockedAmount );
//			    if ( isset( $order->blockedAmount->id ) ) {
//				    print_r( $order->blockedAmount->id );
//			    } else {
//				    echo $order->id;
//			    }
//			    echo "</pre>";

//		    }
//		    echo "</pre>";
//    		Order::updateAll(['user_auction_id'=>$order->blockedAmount->id],['order_id'=>$order->id]);
//	    }

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('update_status')) {
			Yii::$app->session->setFlash('success','Order updated successfully');
            if ($model->order_status != Yii::$app->request->post('status')) {
                $model->order_status = Yii::$app->request->post('status');
                Yii::$app->session->setFlash('success','Order status changed to `'.$model->getStatus() .'` successfully');
            }

            //update shipping method
	        if ( $model->type != Order::ADD_FUND) {
		        $shippingMethod = ShippingAddress::findOne( [ 'order_id' => $id ] );
		        if ( $shippingMethod ) {
			        $shippingParams = Yii::$app->request->post( 'shipping' );

			        $shippingMethod->first_name      = $shippingParams['first_name'];
			        $shippingMethod->last_name       = $shippingParams['last_name'];
			        $shippingMethod->company         = $shippingParams['company'];
			        $shippingMethod->appt_no         = $shippingParams['appt_no'];
			        $shippingMethod->address_1       = $shippingParams['address_1'];
			        $shippingMethod->address_2       = $shippingParams['address_2'];
			        $shippingMethod->phone           = $shippingParams['phone'];
			        $shippingMethod->city            = $shippingParams['city'];
			        $shippingMethod->state           = $shippingParams['state'];
			        $shippingMethod->country         = $shippingParams['country'];
			        $shippingMethod->shipping_weight = $shippingParams['shipping_weight'];
			        $shippingMethod->shipping_method = $shippingParams['shipping_method'];
			        $shippingMethod->save( false );
		        } else {
			        $shippingMethod                  = new ShippingAddress;
			        $shippingParams                  = Yii::$app->request->post( 'shipping' );
			        $shippingMethod->first_name      = $shippingParams['first_name'];
			        $shippingMethod->last_name       = $shippingParams['last_name'];
			        $shippingMethod->company         = $shippingParams['company'];
			        $shippingMethod->appt_no         = $shippingParams['appt_no'];
			        $shippingMethod->address_1       = $shippingParams['address_1'];
			        $shippingMethod->address_2       = $shippingParams['address_2'];
			        $shippingMethod->phone           = $shippingParams['phone'];
			        $shippingMethod->city            = $shippingParams['city'];
			        $shippingMethod->state           = $shippingParams['state'];
			        $shippingMethod->country         = $shippingParams['country'];
			        $shippingMethod->shipping_weight = $shippingParams['shipping_weight'];
			        $shippingMethod->shipping_method = $shippingParams['shipping_method'];
			        $shippingMethod->order_id        = $id;
			        $shippingMethod->save( false );
		        }

		        $domestic_shipping = Yii::$app->request->post( 'domestic_shipping' );
		        $weight            = Yii::$app->request->post( 'weight' );
		        $bank_fees         = Yii::$app->request->post( 'bank_fees' );
		        $other_fees        = Yii::$app->request->post( 'other_fees' );
		        $subtotal          = $tanuki_fee = 0;
		        foreach ( $domestic_shipping as $key => $shipping ) {
			        $detailModel                    = OrderDetail::findOne( [ 'id' => $key ] );
			        $detailModel->domestic_shipping = $shipping;
			        $detailModel->weight            = $weight[ $key ];
			        $detailModel->bank_fees         = $bank_fees[ $key ];
			        $detailModel->other_fees        = $other_fees[ $key ];
			        $detailModel->save( false );
			        $subtotal   += ( $detailModel->price * $detailModel->quantity ) + $detailModel->tanuki_fee + $detailModel->domestic_shipping + $detailModel->bank_fees + $detailModel->other_fees;
			        $tanuki_fee += $detailModel->tanuki_fee;
		        }

		        if (
			        $shippingParams['country'] &&
			        $shippingParams['shipping_weight'] &&
			        $shippingParams['shipping_method']
		        ){
			        $gramWeight = $shippingParams['shipping_weight'] * 1000;
			        $result = ShippingMethodCalculationFinal::find()
                        ->andWhere(['shipping_method_id'=>$shippingParams['shipping_method'],'country_id' => $shippingParams['country']])
                        ->andWhere("'{$gramWeight}' BETWEEN weight_from and weight_to")->one();
			        if ($result) {
				        $model->shipping_charges = $result->cost;
					}
	            } else {
			        $model->shipping_charges = $_REQUEST['order']['shipping_charges'];
		        }

		        $model->subtotal         = $subtotal;
//		        $model->tanuki_charges   = 0;
		        $model->consolidation    = $_REQUEST['order']['consolidation'];

		        $model->total            = ( $model->consolidation + $model->shipping_charges + $model->subtotal );
	        }
	        $model->save(false);

            if($model->type == Order::ADD_FUND && Yii::$app->request->post('status') == 8) {
                User::updateFund($model->user_id, $model->total, $model->method); // update user credits
            }
        }

        $view = ($model->type =='add_fund') ? 'fund' : 'view';
        return $this->render($view, [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public static function userName(){
        $firstName = Yii::$app->user->identity->first_name;
        $lastName = Yii::$app->user->identity->last_name;
        return $firstName." ".$lastName;
    }

    /**
     * @param $id
     * @return $this
     */
    public function actionInvoiceMail($id){
        if (Order::invoiceMail($id)) {
            Yii::$app->session->setFlash('success','Order invoice sent to user successfully');
            return Yii::$app->getResponse()->redirect(Url::to(['order/view','id'=>$id]));
        }
    }

    public function actionReceiptMail($id){
        if (Order::invoiceMail($id,'receipt')) {
            Yii::$app->session->setFlash('success','Order receipt sent to user successfully');
            return Yii::$app->getResponse()->redirect(Url::to(['order/view','id'=>$id]));
        }
    }

    public function actionDeductAmount($id) {
		$model = Order::findOne($id);
		$actualBlockedAmount = (($model->type == 'buyout' || $model->type == 'auction')) ? $model->blocked_amount : 0;
		$pending_amount = $model->total - $model->paid_amount;


	    if ($actualBlockedAmount) {
		    $pending_balance = $model->total;
		} else {
		    $pending_balance = $pending_amount;
		}

	    if (($model->user->credit + $actualBlockedAmount) > $pending_balance) {
		    $model->paid_amount = $model->total;
		    $model->blocked_amount = 0;
		    $model->save(false);
		    User::updateAll(['credit'=>(($model->user->credit + $actualBlockedAmount) - $pending_balance)],['id'=>$model->user->id]);

		    Yii::$app->session->setFlash('success','Amount adjustment successfully.');
		    return Yii::$app->getResponse()->redirect(Url::to(['order/view','id'=>$id]));

	    } else {
		    Yii::$app->session->setFlash('danger','User has low balance to deduct.');
		    return Yii::$app->getResponse()->redirect(Url::to(['order/view','id'=>$id]));
	    }
    }

    public function actionRakutenUrl($itemCode)
    {
	    $model = new Rakuten();
	    $data = $model->RakutenAPI(['itemCode'=>$itemCode]);
	    return $this->redirect($data['data']['data'][0]['Item']['itemUrl']);
    }
}
