<?php
namespace backend\controllers;

use common\models\OrderDetail;
use Yii;
use common\models\UserAuctions;
use common\models\UserAuctionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Order;

use common\components\TBase;
use common\models\OrderSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use common\models\User;
use common\models\Messages;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => null,
            ],
        ];
    }

    public function actionProfile()
    {
        $user = \common\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        return $this->render('profile', [
            'user' => $user,
        ]);
    }

    public function actionUpdate()
    {
        $model = new \common\models\PasswordForm;
        $user = \common\models\User::findOne(['id' => Yii::$app->user->identity->id]);

        if (isset($_POST['User']) && $user->load(Yii::$app->request->post())) {
            $mydata = $_POST['User'];
            $user->first_name = $mydata['first_name'];
            $user->last_name = $mydata['last_name'];
            $user->email = $mydata['email'];
            if ($user->save()) {

	            Yii::$app->session->setFlash( 'success', TBase::ShowLbl( 'PROFILE_UPDATED' ) );

	            return $this->redirect( [ 'update' ] );
            }
        }


        if($model->load(Yii::$app->request->post()) && $model->validate() && isset($_POST['PasswordForm'])){
            $user->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
            if($user->save()){
                Yii::$app->getSession()->setFlash('success',TBase::ShowLbl('PASSWORD_CHANGED_SUCCESS'));
            }else{
                Yii::$app->getSession()->setFlash('danger',TBase::ShowLbl('PASSWORD_NOT_CHANGED'));
            }
            return $this->redirect(['update']);
        }

        return $this->render('update', [
            'user' => $user,
            'model'=>$model
        ]);
    }

    public function actionChangePassword(){
        $model = new \common\models\PasswordForm;
        $modeluser = User::findOne(Yii::$app->user->getId());

        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash('success',TBase::ShowLbl('PASSWORD_CHANGED_SUCCESS'));
                    }else{
                        Yii::$app->getSession()->setFlash('danger',TBase::ShowLbl('PASSWORD_NOT_CHANGED'));
                    }
                    return $this->redirect(['change-password']);
                }catch(\Exception $e){
                    Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
                    return $this->render('change-password',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('change-password',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('change-password',[
                'model'=>$model
            ]);
        }
    }
}
