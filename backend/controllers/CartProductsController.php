<?php

namespace backend\controllers;

use common\models\CartProductAttributes;
use common\models\User;
use Yii;
use common\models\CartProducts;
use common\models\CartProductsSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * CartProductsController implements the CRUD actions for CartProducts model.
 */
class CartProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CartProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CartProductsSearch();
        $dataProvider = $searchModel->groupByUserSearch(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CartProducts model.
     * @param $buyer_id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($buyer_id)
    {
        if (empty($buyer_id)) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        $searchModel = new CartProductsSearch();
        $searchModel->buyer_id= $buyer_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Updates an existing CartProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return array
     */
    public function actionChangeStatus()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $status = Yii::$app->request->get('status');

        if ($id && $status) {
            $product = CartProducts::findOne($id);
            if ($product) {
                $product->status = $status;
                if ($product->save(false)) {
                    return [
                        'type' => 'success',
                        'message' => 'Status changed to '.$status. ' successfully'
                    ];
                }
            }
        } else {
            return [
                'type' => 'error',
                'message' => 'Invalid Credentials'
            ];
        }
    }
    public function actionDomesticShipping()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $cost = Yii::$app->request->get('cost');

        if ($id && $cost) {
            $product = CartProducts::findOne($id);
            if ($product) {
                $product->domestic_shipping = $cost;
                if ($product->save(false)) {
                    return [
                        'type' => 'success',
                        'message' => 'Domestic cost set '.$cost. ' successfully'
                    ];
                }
            }
        } else {
            return [
                'type' => 'error',
                'message' => 'Invalid Credentials'
            ];
        }
    }

    public function actionQuantity()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $quantity = Yii::$app->request->get('quantity');

        if ($id && $quantity) {
            $product = CartProducts::findOne($id);
            if ($product) {
                $product->quantity = $quantity;
                if ($product->save(false)) {
                    return [
                        'type' => 'success',
                        'message' => 'Product quantity updated successfully'
                    ];
                }
            }
        } else {
            return [
                'type' => 'error',
                'message' => 'Invalid Credentials'
            ];
        }
    }
    /**
     * Finds the CartProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CartProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CartProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $buyer_id
     * @return \yii\web\Response
     */
    public function actionNotification($buyer_id){
        $user = User::findOne($buyer_id);
        $cartList = CartProducts::find()->where(['buyer_id'=>$buyer_id])->count();
        if ($cartList) {
            $sent = CartProducts::notifyUser($user);
            if ($sent) {
                CartProducts::updateAll(['notify'=>'1'],'buyer_id='.$buyer_id);
                Yii::$app->session->setFlash('success', 'notification mail sent successfully');
            } else {
                Yii::$app->session->setFlash('warning', 'error while sending notification. Please try again');
            }
            return $this->redirect(['cart-products/view','buyer_id'=>$buyer_id]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        CartProductAttributes::deleteAll(['cart_id'=>$id]);
        $cart = CartProducts::findOne($id);
        if ($cart->delete()) {
            $buyer_id = $cart->buyer_id;
            Yii::$app->session->setFlash('success', 'Item deleted from cart successfully');
            return $this->redirect(['cart-products/view', 'buyer_id' => $buyer_id]);
        }
    }
}
