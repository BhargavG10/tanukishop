<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProfile()
    {
        $model  =   \backend\models\Profile::findOne(Yii::$app->user->id);
        $modelP  =   new \common\models\ChangePassword;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Profile updated Successfully');
                return $this->refresh();
            }
        }

        if ($modelP->load(Yii::$app->request->post()) && $modelP->validate()) {

            $model->password_hash = Yii::$app->security->generatePasswordHash($modelP->confirm_password);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Password updated Successfully');
                return $this->refresh();
            }
        }

        $model->password_hash = '';
        return $this->render('profile',['model'=>$model,'modelp'=>$modelP]);

    }
}
