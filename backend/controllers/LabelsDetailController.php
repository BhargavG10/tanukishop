<?php

namespace backend\controllers;

use common\components\TBase;
use common\models\Labels;
use Yii;
use common\models\LabelsDetail;
use common\models\LabelsDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LabelsDetailController implements the CRUD actions for LabelsDetail model.
 */
class LabelsDetailController extends Controller
{
    /**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

    /**
     * Lists all LabelsDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LabelsDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LabelsDetail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LabelsDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LabelsDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success','Label Created Successfully');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LabelsDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success','Label Updated Successfully');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LabelsDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateLabels($id)
    {
        $model = LabelsDetail::findAll(['labels_id'=>$id]);
        if (isset($_POST['LabelsDetail'])) {

            $modelDetail = LabelsDetail::findOne($_POST['LabelsDetail']['id']);
            $modelDetail->title = $_POST['LabelsDetail']['title'];
            if ($modelDetail->save()) {
                Yii::$app->session->setFlash('success', '` '.$modelDetail->title.' ` Label Updated Successfully');
                $this->refresh();
            }
        } else {

            return $this->render('update-labels', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LabelsDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $data = $this->findModel($id);
        $labels_id = $data->labels_id;
        LabelsDetail::deleteAll(['labels_id'=>$labels_id]);
        Labels::deleteAll(['id'=>$labels_id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the LabelsDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LabelsDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LabelsDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
