<?php

namespace backend\controllers;

use common\models\Order;
use Yii;
use backend\models\Customer;
use backend\models\CustomerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PageController implements the CRUD actions for Page model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $searchModel->type = 'user';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (isset($_POST['change_password']) && $_POST['Customer']['password_hash'] != '') {
                $model->setPassword($model->password_hash);
            } else if (isset($_POST['change_password']) && $_POST['Customer']['password_hash'] == '') {
                $model->password_hash = $model->OldAttributes['password_hash'];
                Yii::$app->session->setFlash('danger','password not updated due to empty password');
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success','Profile Updated Successfully');
                return $this->redirect(['index']);
            }
        } else {
	        $model->password_hash = '';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success','Page Deleted Successfully');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionAddFund() {

        if (Yii::$app->request->isPost) {
	        $amount = Yii::$app->request->post('amount');
        	if (Yii::$app->request->post('action') == 'sub') {
		        $amount = '-'.$amount;
	        }
            $model = new Order();
            $model->type = 'add_fund';
            $model->subtotal = $amount;
            $model->total = $model->subtotal;
            $model->tanuki_charges = 0;
            $model->shipping_charges = 0;
            $model->currency = 'JPY';
            $model->user_id = Yii::$app->request->post('user_id');
            $model->method = 'Bank Deposit';
            $model->status = 'Success';
            $model->date = date('Y-m-d H:i:s');
            $model->response = 'Fund added by Administrator';
            $model->invoice = uniqid();
            $model->order_status = 9;
            if ($model->save(false)) {
                $user = \common\models\User::findOne(Yii::$app->request->post('user_id'));
                $user->credit = $user->credit+$model->subtotal;
                $user->save();
                return $this->redirect(['customer/index']);
            }
        }
    }
}
