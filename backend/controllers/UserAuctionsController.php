<?php

namespace backend\controllers;

use Yii;
use common\models\UserAuctions;
use common\models\UserAuctionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
 * UserAuctionsController implements the CRUD actions for UserAuctions model.
 */
class UserAuctionsController extends Controller
{
    /**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

    /**
     * Lists all UserAuctions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserAuctionsSearch();
        $searchModel->is_deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all UserAuctions models.
     * @return mixed
     */
    public function actionAll()
    {
        $searchModel = new UserAuctionsSearch();
        $searchModel->is_deleted = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserAuctions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserAuctions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAuctions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserAuctions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserAuctions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserAuctions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAuctions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAuctions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $auction_id
     * @param $user_id
     * @return \yii\web\Response
     */
    public function actionWinnerMail($auction_id,$user_id) {
        $data = UserAuctions::findOne(['user_id'=>$user_id,'auction_id'=>$auction_id]);
        if (UserAuctions::winnerMail($data)) {
            Yii::$app->session->setFlash('success','mail sent successfully');
            return $this->redirect(['user-auctions/index']);
        }
        Yii::$app->session->setFlash('warning','Error while sending mail');
        return $this->redirect(['user-auctions/index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionMarkWinner($id) {

	    $result = UserAuctions::markWinner($id);
    	if ($result) {
		    Yii::$app->session->setFlash( 'success', 'order created & mail sent successfully' );
		    return $this->redirect( [ 'user-auctions/index' ] );
	    }
        Yii::$app->session->setFlash('warning','Error while sending mail');
        return $this->redirect(['user-auctions/index']);

    }
}
