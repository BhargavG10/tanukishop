<?php

namespace backend\controllers;

use Yii;
use common\models\Messages;
use common\models\MessagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MessagesController implements the CRUD actions for Messages model.
 */
class MessagesController extends Controller
{
    /**
     * @inheritdoc
     */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

    /**
     * Lists all Messages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Messages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = new Messages();
        $user_id = $this->findModel($id)->user_id;

        $model->user_id = $user_id;
        $model->message_from = 'admin';
        $model->date = new \yii\db\Expression('NOW()');
        $model->is_read_by_user = 0;
        $model->is_read_by_admin = 1;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->msg = $model->msg;
                if ($model->save(false)) {
                    if ($model->sendEmail($model->msg,$this->findModel($id)->user)) {
                        Yii::$app->session->setFlash('success', 'Message Sent Successfully');
                    }
                }

            }
        }

        return $this->render('view', [
            'allMessages' => Messages::findAll(['user_id'=>$user_id]),
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Messages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $model = new Messages();
	    $model->date = date('Y-m-d H:i:s');
	    $model->is_read_by_admin = 1;
	    $model->is_read_by_user = 0;
	    $model->message_from = 'admin';
	    if ($id) {
	    	$model->user_id = $id;
	    }
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Messages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Messages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Messages::deleteAll(['user_id'=>$this->findModel($id)->user_id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Messages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Messages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Messages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
