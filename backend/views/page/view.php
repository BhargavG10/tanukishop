<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title_en;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row page-view">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
                <div class="col-md-12" style="padding:12px;">Page Url : <a target="_blank" href="https:/www.tanukishop.com/<?=$model->slug?>"'>https:/www.tanukishop.com/<?=$model->slug?></a></div>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'title_en',
                        'title_ru',
                        'detail_en:html',
                        'detail_ru:html',
                        'slug',
                        'meta_title',
                        'meta_desc:ntext',
                        'meta_keywords:ntext',
                        [
                            'attribute'=>'status',
                            'format' =>'text',
                            'value' => ($model->status) ? 'Enable' : 'Disable'
                        ],
                        'created_on',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>