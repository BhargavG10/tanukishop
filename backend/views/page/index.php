<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row page-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?= Html::a(Yii::t('app', '+ Add New'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        //'filterModel' => $searchModel,
                        'tableOptions' => ['class'=>'table table-bordered'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'title_en',
                                'format' => 'text',
                                'value' => function($model){
                                    return (strlen($model->title_en)>50) ? substr($model->title_en,0,50).'...' : $model->title_en;
                                },
                            ],
                            [
                                'attribute' => 'title_ru',
                                'format' => 'text',
                                'value' => function($model){
                                    return (strlen($model->title_ru)>50) ? substr($model->title_ru,0,50).'...' : $model->title_ru;
                                },
                            ],
                            // 'slug',
                            // 'meta_title',
                            // 'meta_desc:ntext',
                            // 'meta_keywords:ntext',
                            // 'status',
                            // 'created_on',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} | {update}'
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
