<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form clearfix">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'detail_en')->widget(CKEditor::className(), [
            'preset' => 'full',
            'clientOptions' => ['allowedContent' => true ]
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'detail_ru')->widget(CKEditor::className(), [
            'preset' => 'full',
            'clientOptions' => ['allowedContent' => true ]
        ]) ?>
    </div>

    <div class="col-lg-6">
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'status')->dropDownList([ 1 => 'Enable', 0 => 'Disable']) ?>
    </div>

    <div class="col-lg-6 form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
