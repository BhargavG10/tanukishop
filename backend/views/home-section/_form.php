<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomeSection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial_no')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(['0'=>'Disable','1'=>'Enable']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button onclick="history.go(-1)" class="btn btn-default"> << Back</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
