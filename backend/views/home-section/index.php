<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HomeSectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Home Sections');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row home-section-index">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <h5 class="pull-right"><?= Html::a(Yii::t('app', 'Create Home Section'), ['create'], ['class' => 'btn btn-success']) ?></h5>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'pager' => [
	                            'firstPageLabel' => 'First',
	                            'lastPageLabel'  => 'Last'
                            ],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'title_en',
                                'title_ru',
                                'serial_no',
                                [
                                   'attribute' => 'status',
                                   'value' => function($data) {
                                        return ($data->status) ? 'Enable' : 'Disable';
                                   },
                                ],
                                [
                                    'label'=>'Total Product',
                                    'format'=>'raw',
                                    'value'=> function($data) {
                                        return count($data->products);
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{update} {delete}'
                                ],
                            ],
                        ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
