<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShippingCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipping-category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'delivery_in_japan_cost')->textInput() ?>
        <?= $form->field($model, 'is_active')->checkbox() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'international_shipping_cost')->textInput() ?>
        <?= $form->field($model, 'tanuki_fee')->textInput() ?>
        <?= $form->field($model, 'weight_base')->checkbox() ?>
    </div>
    <?php if($model->isNewRecord){
      echo  $form->field($model, 'created_on')->hiddenInput(['value'=>date('Y-m-d')])->label(false);
    }else{
        echo   $form->field($model, 'updated_on')->hiddenInput(['value'=>date('Y-m-d')])->label(false);
    }
     ?>
    <div class="form-group col-lg-12 clearfix">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clearfix">
</div>
</div>
