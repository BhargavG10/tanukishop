<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ShippingCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shipping Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-category-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?= Html::a(Yii::t('app', '+ Add New'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                           // 'id',
                            'name',
                            'delivery_in_japan_cost',
                            'tanuki_fee',
                            'international_shipping_cost',
                            //'weight_base',
                            //'created_on',
                            //'updated_on',
                            [
                                'attribute' => 'status',
                                'format' => 'text',
                                'label' => 'Status',
                                'value' => function($model){
                                    return ($model->is_active) ? 'Enable' : 'Disable';
                                },
                            ],

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
