<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShippingCountry */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shipping Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row testimonial-view">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                          //  'id',
                            'name',
                            'code',
                          [
                              'attribute'=>'status',
                              'format' =>'text',
                              'value' => ($model->is_active) ? 'Enable' : 'Disable'
                          ],
                        ],
                    ]) ?>
            </div>
        </div>
    </div>
</div>
