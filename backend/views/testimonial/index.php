<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TestimonialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Testimonials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row testimonial-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?= Html::a(Yii::t('app', '+ Add New'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </div>
            <div class="ibox-content">

                <?php Pjax::begin(); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'tableOptions' => ['class'=>'table table-bordered'],
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                //'class' => DataColumn::className(), // this line is optional
                                'attribute' => 'name',
                                'format' => 'text',
                                'label' => 'Name',
                                'value' => function($model){
                                    return substr($model->name,0,50);
                                },
                            ],
                            [
                                //'class' => DataColumn::className(), // this line is optional
                                'attribute' => 'detail',
                                'format' => 'text',
                                'label' => 'Testimony',
                                'value' => function($model){
                                    return substr($model->detail,0,50);
                                },
                            ],
                            'date',
                            [
                                //'class' => DataColumn::className(), // this line is optional
                                'attribute' => 'status',
                                'format' => 'text',
                                'label' => 'Status',
                                'value' => function($model){
                                    return ($model->status) ? 'Enable' : 'Disable';
                                },
                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?></div>
        </div>
    </div>
</div>
