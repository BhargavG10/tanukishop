<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
$pages = ['page','labels-detail','home-section','home-product', 'shipping-methods', 'shipping-method-calculation-final','shipping-category','shipping-country','order-status']
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                             </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><?=Html::a('Profile', ['/dashboard/profile']); ?></li>
                            <li class="divider"></li>
                            <li><?=Html::a('Logout', ['/site/logout'], ['data' => ['method' => 'post',]]); ?></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="<?=(Yii::$app->controller->id == 'dashboard') ? 'selected' : ''; ?>" >
                    <a href="<?=\yii\helpers\Url::to(['dashboard/index']); ?>">
                        <i class="fa fa-th-large"></i><span class="nav-label"><?=Yii::t('app','Dashboard')?></span>
                    </a>
                </li>
                <li class="<?=(Yii::$app->controller->id == 'cart-products') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['cart-products/index']); ?>"><i class="fa fa-shopping-cart"></i> <span class="nav-label"><?=Yii::t('app','Cart Products')?></span>&nbsp;&nbsp;<?=\common\models\CartProducts::notification()?></a></li>


                <li class="<?=(in_array(Yii::$app->controller->id, ['user-auctions'])) ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-gavel"></i><span class="nav-label">
                            <?=Yii::t('app','Auctions')?></span><span class="fa arrow">
                        </span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?=(Yii::$app->controller->id == 'user-auctions') ? 'selected' : ''; ?>" >
                            <a href="<?=\yii\helpers\Url::to(['user-auctions/all']); ?>">
                                <span class="nav-label"><?=Yii::t('app','All')?></span>
                            </a>
                        </li>
                        <li class="<?=(Yii::$app->controller->id == 'user-auctions') ? 'selected' : ''; ?>" >
                            <a href="<?=\yii\helpers\Url::to(['user-auctions/index']); ?>">

                                <span class="nav-label"><?=Yii::t('app','Current Auctions')?></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?=(Yii::$app->controller->id == 'order') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['order/index']); ?>"><i class="fa fa-file-text"></i> <span class="nav-label"><?=Yii::t('app','Orders')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'category') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['category/index']); ?>"><i class="fa fa-tags"></i> <span class="nav-label"><?=Yii::t('app','Categories')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'testimonial') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['testimonial/index']); ?>"><i class="fa fa-quote-left"></i> <span class="nav-label"><?=Yii::t('app','Testimonials')?></span></a></li>
                <li class="<?=(in_array(Yii::$app->controller->id, $pages)) ? 'active' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-newspaper-o"></i> <span class="nav-label"><?=Yii::t('app','Content Sections')?></span><span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?=(Yii::$app->controller->id == 'page') ? 'active' : ''; ?>" >
                            <a href="<?=\yii\helpers\Url::to(['page/index']); ?>">
                                <?=Yii::t('app','Pages')?>
                            </a>
                        </li>
                        <li class="<?=(Yii::$app->controller->id == 'labels-detail') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['labels-detail/index']); ?>"><span class="nav-label"><?=Yii::t('app','Labels')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'home-section') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['home-section/index']); ?>"><span class="nav-label"><?=Yii::t('app','Home Page Section')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'home-product') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['home-product/index']); ?>"><span class="nav-label"><?=Yii::t('app','Home Recommended')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'shipping-methods') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['shipping-methods/index']); ?>"><span class="nav-label"><?=Yii::t('app','Shipping Methods')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'shipping-method-calculation-final') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['shipping-method-calculation-final/index']); ?>"><span class="nav-label"><?=Yii::t('app','Shipping Calculator')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'shipping-country') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['shipping-country/index']); ?>"><span class="nav-label"><?=Yii::t('app','Shipping Country')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'shipping-category') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['shipping-category/index']); ?>"><span class="nav-label"><?=Yii::t('app','Shipping Category')?></span></a></li>
                        <li class="<?=(Yii::$app->controller->id == 'order-status') ? 'active' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['order-status/index']); ?>"><span class="nav-label"><?=Yii::t('app','Order Status')?></span></a></li>

                    </ul>
                </li>
                <li class="<?=(Yii::$app->controller->id == 'customer') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['customer/index']); ?>"><i class="fa fa-users"></i> <span class="nav-label"><?=Yii::t('app','Clients')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'setting') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['setting/index']); ?>"><i class="fa fa-chain"></i> <span class="nav-label"><?=Yii::t('app','Settings')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'messages') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['messages/index']); ?>"><i class="fa fa-envelope-o"></i> <span class="nav-label"><?=Yii::t('app','Message')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'packages') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['packages/index']); ?>"><i class="fa fa-suitcase"></i> <span class="nav-label"><?=Yii::t('app','Package')?></span></a></li>
                <li class="<?=(Yii::$app->controller->id == 'user') ? 'selected' : ''; ?>" ><a href="<?=\yii\helpers\Url::to(['user/update']); ?>"><i class="fa fa-suitcase"></i> <span class="nav-label"><?=Yii::t('app','Profile')?></span></a></li>
            </ul>

        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-1"> <div class=" row navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
            </div>
            <div class="col-lg-6">
                <h2 class="text-uppercase"><?= (Yii::$app->controller->id == 'customer') ? 'Clients' : Yii::$app->controller->id; ?></h2>
                <?= Breadcrumbs::widget([
                    'homeLink' => ['label' => 'Dashboard',
                    'url' => \yii\helpers\Url::to(['dashboard/index'])],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]); ?>
            </div>
            <div class="col-lg-5">
                <ul class="nav navbar-top-links navbar-right" style="margin-top: 11px;">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome to Admin Section.</span>
                    </li>
                    <li>
                        <?=Html::a('<i class="fa fa-sign-out"></i> Logout',
                            ['/site/logout'],
                            ['data' => ['method' => 'post',]]);
                        ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight table-responsive clearfix">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong>&copy; DOKO Link <?= date('Y') ?>
            </div>
        </div>

    </div>
</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
