<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = \common\components\TBase::ShowLbl('CHANGE_PASSWORD');
?>
<style>
    .btn-cart1 {
        width: 20% !important;
    }
</style>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home'); ?></a></li>
        <li class="active"><?= Html::encode($this->title) ?></li>
    </ol>
</div>
<section>
    <div class="container notranslate">
        <?=$this->render('/user/_left_nav')?>
        <div class=" dashboard">
            <h3 class="inner-head "> <?= Html::encode($this->title) ?>	</h3>
            <div class="">
                <?php $form = ActiveForm::begin([
                    'id'=>'changepassword-form',
                    'options'=>['class'=>'form-horizontal'],
                    'fieldConfig'=>[
                        'template'=>"{label}\n<div class=\"col-lg-3\">
                                {input}</div>\n<div class=\"col-lg-5\">
                                {error}</div>",
                        'labelOptions'=>['class'=>'col-lg-2 control-label'],
                    ],
                ]); ?>
                <?= $form->field($model,'oldpass',['inputOptions'=>['placeholder'=>\common\components\TBase::ShowLbl('OLD_PASSWORD')]])->passwordInput() ?>
                <?= $form->field($model,'newpass',['inputOptions'=>['placeholder'=>\common\components\TBase::ShowLbl('NEW_PASSWORD')]])->passwordInput() ?>
                <?= $form->field($model,'repeatnewpass',['inputOptions'=>['placeholder'=>\common\components\TBase::ShowLbl('REPEAT_PASSWORD')]])->passwordInput() ?>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-8">
                        <?= Html::submitButton(\common\components\TBase::ShowLbl('CHANGE_PASSWORD'),[
                            'class'=>'btn width-204 color-782f4a'
                        ]) ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>