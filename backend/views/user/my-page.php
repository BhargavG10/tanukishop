<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use common\components\TBase as LBL;
$this->title = LBL::_x('MY_PAGE');
$user_id = Yii::$app->user->id;
$auctions = \common\models\UserAuctions::find()->where(['user_id'=>$user_id,'is_deleted'=>0])->count();
$watchlist = \app\models\FavoriteProduct::find()->where(['shop'=>'yahoo_auction','user_id'=>$user_id])->count();
$favorite = \app\models\FavoriteProduct::find()->where(['user_id'=>$user_id])->count();
$cartProduct = \common\models\CartProducts::find()->where(['buyer_id'=>$user_id])->count();
$orders = \common\models\Order::find()->where(['user_id'=>$user_id])->count();
$packages = \common\models\Packages::find()->where(['created_by'=>$user_id])->count();
$blockedAmount = \common\models\UserAuctionBlockedAmount::find()
    ->select('sum(blocked_amount) as blocked_amount, sum(auction_amount) as auction_amount')
    ->where(['user_id'=>$user_id])->one();
if ($blockedAmount) {
    $blocked = (int)$blockedAmount['blocked_amount'] + (int)$blockedAmount['auction_amount'];
}
?>
<style>
    .btn-cart1 {width: 20% !important;}
    .my_balance_block{padding: 0px 21px 16px;font-size: 16px;}
    .left-block img{width: 52px;}
    .margin-top-23{margin-top: 40px;}
    .left {float: left;margin-right: 17px;  }
    .right {float: left;}
    p.top{margin: 0px;margin-top: 4px;border-bottom: 1px solid;padding: 3px 0px; font-weight: bold;}
    p.bottom{padding: 1px 0px;}
    p a{color: #000;}
    p{font-size: 16px;}
    p.one{font-weight:bold; margin-top: 17px;margin-bottom: 0px;}
    p.event{    margin-left: 12px;margin-top: 19px;font-style: italic;color: #ee7907;font-weight: bold;}
    .dashboard .inner-head {padding-left: 27px;}
    .container .dashboard {padding-bottom: 123px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
</style>
<div class="notranslate">
<div class="container notranslate">
    <ol class="breadcrumb">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home');?></a></li>
        <li class="active"><?= Html::encode($this->title) ?></li>
    </ol>
</div>
    <section class="notranslate">
        <div class="container">
            <ol class="breadcrumb"></ol>
            <?=$this->render('/user/_left_nav')?>

            <div class=" dashboard">
                <h3 class="inner-head"><?=LBL::_x('MY_PAGE')?></h3>
                <div class="row">
                    <div class="col-md-8 left-block">
                        <div class="col-md-12 my_balance_block">
                            <table>
                                <tr>
                                    <td style="padding: 2px 6px;"><?=LBL::_x('MY_BALANCE')?> </td>
                                    <td>
                                        <span style="font-size: 20px;color:#428bca;"><?=number_format(Yii::$app->user->identity->credit)?> JPY</span>
                                    </td>
                                    <td>
                                        <?=Html::a('+ '.LBL::_x('ADD_FUND'),['user/add-fund'],['class'=>'add_fund_btn','style'=>'margin-left: 11px;'])?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 2px 6px;"><?=LBL::_x('BLOCKED')?></td>
                                    <td><span style="    font-size: 20px;color: #ee7907;"> <?=number_format($blocked)?> JPY</span></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <p class="event"><?=LBL::_x('NEW_EVENT')?></p>
                        </div>
                        <div class="col-md-12 margin-top-23">
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/gavel-icon.png')?>
                                </div>
                                <div class="right">
                                    <p class="top"><?=Html::a(LBL::_x('USER_AUCTIONS'),['user-auctions/index'])?>&nbsp;(<?=$auctions?>)</p>
                                    <p class="bottom"><?=Html::a(LBL::_x('WATCHLIST'),['user/yahoo-auction-fav-products'])?>&nbsp;(<?=$watchlist?>)</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/cart.png')?>
                                </div>
                                <div class="right">
                                    <p class="top"><?=Html::a(LBL::_x('CART_PRODUCTS'),['cart-products/index'])?>&nbsp;(<?=$cartProduct?>)</p>
                                    <p class="bottom"><?=Html::a(LBL::_x('FAVORITE'),['user/fav-products'])?>&nbsp;(<?=$favorite-$watchlist?>)</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margin-top-23">
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/transcript-upload.png')?>
                                </div>
                                <div class="right">
                                    <p class="one"><?=Html::a(LBL::_x('MY_ORDERS'),['order/index'])?>&nbsp;(<?=$orders?>)</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/treasure.png')?>
                                </div>
                                <div class="right">
                                    <p class="one"><?=Html::a(LBL::_x('ADD_FUND'),['user/add-fund'])?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 margin-top-23">
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/img-shipping.png')?>
                                </div>
                                <div class="right">
                                    <p class="top"><?=Html::a(LBL::_x('PACKAGES'),['packages/index'])?>&nbsp;(<?=$packages?>)</p>
                                    <p class="bottom"><?=Html::a(LBL::_x('NAVIGATION_SHIPPING_ADDRESS'),['user-shipping-address/index'])?></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left">
	                                <?=Html::img('@web/img/user.png')?>
                                </div>
                                <div class="right">
                                    <p class="top"><?=Html::a(LBL::_x('EDIT_PROFILE'),['user/update'])?></p>
                                    <p class="bottom"><?=Html::a(LBL::_x('MESSAGES'),['user/inbox'])?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 right-block" style="margin-top: 35px;">
                        <?=Html::img('@web/img/mypage_right.png')?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>