<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'update profile';
?>

<div class="row page-update">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content clearfix">
                <div class="col-lg-6">
                <h3 class="inner-head "> <?= \common\components\TBase::ShowLbl('UPDATE_PROFILE') ?>	</h3>
                <div class="">
		            <?php $form = ActiveForm::begin(['id' => 'form-update',
		                                             'options'=>['class'=>'form-horizontal'],
		                                             'fieldConfig'=>[
			                                             'template'=>"{label}\n<div class=\"col-lg-6\">
                                    {input}</div>\n<div class=\"col-lg-12 clearfix\">
                                    {error}</div>",
			                                             'labelOptions'=>['class'=>'col-lg-4 control-label'],
		                                             ],]); ?>
		            <?= $form->field($user, 'first_name')->textInput()->label('First Name') ?>
		            <?= $form->field($user, 'last_name')->textInput()->label('Last Name') ?>
		            <?= $form->field($user, 'email')->textInput();?>
                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
				            <?= Html::submitButton(\common\components\TBase::ShowLbl('UPDATE_PROFILE'), ['class'=>'btn width-204 color-782f4a']) ?>
                        </div>
                    </div>
		            <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="inner-head "> <?= \common\components\TBase::ShowLbl('CHANGE_PASSWORD') ?>	</h3>
                <div class="">
			        <?php $form = ActiveForm::begin([
				        'id'=>'changepassword-form',
				        'options'=>['class'=>'form-horizontal'],
				        'fieldConfig'=>[
					        'template'=>"{label}\n<div class=\"col-lg-6\">
                                {input}</div>\n<div class=\"col-lg-12 clearfix\">
                                {error}</div>",
					        'labelOptions'=>['class'=>'col-lg-4 control-label'],
				        ],
			        ]); ?>
			        <?= $form->field($model,'oldpass')->passwordInput() ?>
			        <?= $form->field($model,'newpass')->passwordInput() ?>
			        <?= $form->field($model,'repeatnewpass')->passwordInput() ?>

                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
					        <?= Html::submitButton(\common\components\TBase::ShowLbl('CHANGE_PASSWORD'),[
						        'class'=>'btn width-204 color-782f4a'
					        ]) ?>
                        </div>
                    </div>
			        <?php ActiveForm::end(); ?>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>