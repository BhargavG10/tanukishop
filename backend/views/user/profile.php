<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = \common\components\TBase::ShowLbl('PROFILE');
?>
<style>
    .btn-cart1 {
        width: 20% !important;
    }
</style>
<div class="notranslate">
<div class="container notranslate">
    <ol class="breadcrumb">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home');?></a></li>
        <li class="active"><?= Html::encode($this->title) ?></li>
    </ol>
</div>
    <section class="notranslate">
        <div class="container">
            <ol class="breadcrumb"></ol>
            <?=$this->render('/user/_left_nav')?>

            <div class=" dashboard">
                <h3 class="inner-head"> <?= Html::encode($this->title) ?>	</h3>
                <div class="table-responsive">
                    <p><b><?=\common\components\TBase::ShowLbl('FIRST_NAME')?> :</b> <?= $user->first_name?></p>
                    <p><b><?=\common\components\TBase::ShowLbl('LAST_NAME')?> : </b><?= $user->last_name?></p>
                    <p><b><?=\common\components\TBase::ShowLbl('EMAIL')?> : </b><?= $user->email?></p>

                </div>
            </div>
        </div>
    </section>
</div>