<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login | Tanuki';
?>

<div>
    <div>
        <h1 class="logo-name">
            <?=Html::img('/dashboard/img/logo.png')?>
        </h1>
    </div>

    <h3>Admin Login</h3>
    <p>Login in. To see it in action.</p>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true,'placeholder'=>"Username",'class'=>"form-control"])->label(false); ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>"Password",'class'=>"form-control"])->label(false); ?>
        </div>
        <?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>
    <?php ActiveForm::end(); ?>
</div>