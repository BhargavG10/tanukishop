<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row page-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Package : <?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">

        <div class="packages-view">
            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        <table class="table">
            <tr>
                <td>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name',
                            'status',
                            'first_name',
                            'last_name',
                            'company',
                            'address_1:ntext',
                            'address_2:ntext',
                            'appt_no',
	                        [
		                        'attribute'=>'shipping_methods_id',
		                        'value' => (isset($model->shipping->title)) ? $model->shipping->title : ''
	                        ],
                        ],
                    ]);
                    ?>
                </td>
                <td>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'city',
                            'state',
                            [
                                'attribute'=>'country',
                                'value' => (isset($model->countryName)) ? $model->countryName : ''
                            ],
                            'zipcode',
                            'phone',
                            'email:email',
                            'created_on',
                            [
                                 'attribute'=>'created_by',
                                 'label'=>'User',
                                 'value' => $model->user->first_name.' '.$model->user->last_name
                            ]
                        ],
                    ]);
                    ?>
                </td>
            </tr>
        </table>

            <h3>Order Detail</h3>

            <table class="table table-striped table-bordered detail-view">
                <tr>
                    <th>Order ID</th>
                    <th>Order Total</th>
                    <th>Order Status</th>
                    <th>Order Payment Status</th>
                    <th>Order Date</th>
                    <th></th>
                </tr>
                <?php if ($model->packageOrders) {

                    foreach ($model->packageOrders as $orders) { ?>
                        <tr>
                            <td>TNK0000<?=$orders->order_id?></td>
                            <td><?=$orders->order->total?></td>
                            <td><?=$orders->order->getStatus()?></td>
                            <td><?=$orders->order->status?></td>
                            <td><?=$orders->order->date?></td>
                            <td><?=Html::a('View Detail',['order/view','id'=>$orders->order_id],['target'=>'_blank'])?></td>
                        </tr>
                    <?php
                    }
                }?>
            </table>

        </div>
            </div>
        </div>
    </div>
</div>

