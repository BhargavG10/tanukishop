<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="packages-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=$form->errorSummary($model); ?>
	<?= $form->field($model, 'cost')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-3">
	        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
	        <?= $form->field($model, 'status')->dropDownList(['pending'=>'Pending','processing'=>'Processing','shipped'=>'Shipped','delivered'=>'Delivered']) ?>
        </div>
        <div class="col-md-3">
	        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
	        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'appt_no')->textInput() ?>
        </div>
        <div class="col-md-3">
		    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
		    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3">
            <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
		    <?= $form->field($model, 'phone')->textInput() ?>
        </div>
        <div class="col-md-3">
		    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
		    <?= $form->field($model, 'address_1')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-4">
		    <?= $form->field($model, 'address_2')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-4">
		    <?= $form->field($model, 'tracking_number')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3" style="margin-top: 25px;">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('back', ['index'],['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<br/>
<br/>
<br/>
<h3>Order Detail</h3>

<table class="table table-striped table-bordered detail-view">
    <tr>
        <th>Order ID</th>
        <th>Order Total</th>
        <th>Order Status</th>
        <th>Order Payment Status</th>
        <th>Order Date</th>
        <th></th>
    </tr>
	<?php if ($model->packageOrders) {

		foreach ($model->packageOrders as $orders) { ?>
            <tr>
                <td>TNK0000<?=$orders->order_id?></td>
                <td><?=$orders->order->total?></td>
                <td><?=$orders->order->getStatus()?></td>
                <td><?=$orders->order->status?></td>
                <td><?=$orders->order->date?></td>
                <td><?=Html::a('View Detail',['order/view','id'=>$orders->order_id],['target'=>'_blank'])?></td>
            </tr>
			<?php
		}
	}?>
</table>
