<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = Yii::t('app', 'Create Packages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container notranslate">
    <ol class="breadcrumb breadcrumb-dashboard-bar">
        <li><a href="#"><?=\common\components\TBase::ShowLbl('home')?></a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</div>
<section class="notranslate">
    <div class="container">
        <div class="left-menu">
            <h3 class="inner-head "><?=\common\components\TBase::ShowLbl('DASHBOARD');?></h3>
            <?= \common\components\TBase::getSideBarMenu(); ?>
        </div>
        <div class=" dashboard">

            <div class="col-lg-12 clearfix">
                <h3 style="margin-bottom: 22px;" class="row inner-head"><?= Html::encode($this->title) ?></h3>
                <div class="packages-create">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>