<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PackagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Packages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row messages-index">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'summary' => '',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute'=>'id',
                                'label'=>'Package ID',
                                'value'=>function($model) {
                                    return $model->id;
                                }
                            ],
                            'tracking_number',
                            [
                                'attribute'=>'shipping_methods_id',
                                'value' => function($model) {
                                    return ($model->shipping_methods_id > 0) ?$model->shippingMethod->title  : 'No set';
                                },
                                'filter' => \yii\helpers\ArrayHelper::map(\common\models\ShippingMethods::find()->all(),'id','title')

                            ],
                            [
                                'attribute'=>'status',
                                'filter'=>['pending'=>'Pending','processing'=>'Processing','shipped'=>'Shipped','delivered'=>'Delivered']
                            ],
	                        [
		                        'attribute'=>'created_on',
		                        'format'=>'date',
		                        'value' => function($model) {
			                        return $model->created_on;
		                        },
                                'filter' => false
	                        ],
//                            'first_name',
                            // 'last_name',
                            // 'company',
                            // 'address_1:ntext',
                            // 'address_2:ntext',
                            // 'appt_no',
                            // 'city',
                            // 'state',
                            // 'country',
                            // 'zipcode',
                            // 'phone',
                            // 'email:email',
                            // 'created_on',
                            // 'created_by',
                            // 'modified_on',

                            [
                                'class' => 'yii\grid\ActionColumn',
//                                'template' =>'{update} {delete}',
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>