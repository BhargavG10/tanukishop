<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .fa-sign-in,.fa-weixin,.fa-money {
        background-color: #1a7bb9;
        color: #FFFFFF;
        border-radius: 3px;
        display: inline-block;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        touch-action: manipulation;
        cursor: pointer;
    }
    .fa-ban{color: red;}
    .fa-check-circle{color: green;}
    table.table tr td:nth-child(8),
    table.table tr td:nth-child(11),
    table.table tr td:nth-child(10),
    table.table tr td:nth-child(6),
    table.table tr td:nth-child(9),
    table.table tr td:nth-child(7){text-align: center;}
    .fa-sign-in,.fa-weixin,.fa-money,.glyphicon-pencil, .glyphicon-eye-open, .glyphicon-trash, .glyphicon-envelope {
        padding: 0px 3px;
        font-size: 11px;
    }
</style>
<div class="row page-index">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <?php Pjax::begin(); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'tableOptions' => ['class'=>'table table-bordered'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
	                        [
		                        'attribute' => 'first_name',
		                        'label' => 'First Name',
	                        ],
                            [
		                        'attribute' => 'last_name',
		                        'label' => 'Last Name',
	                        ],
                            [
		                        'attribute' => 'email',
		                        'label' => 'Email',
	                        ],
                            [
                                'attribute' => 'credit',
                                'format' => 'html',
                                'value' => function($model){
                                    return '&#165; '.number_format($model->credit);
                                },
                                'filter' => Html::activeTextInput($searchModel, 'credit',['class'=>'form-control']),
                            ],
                            [
                                'label' => 'Blocked Amount',
                                'format' => 'html',
                                'value' => function($model){
	                                return '&#165; '.number_format($model->blockedAmount);
                                },
                            ],
	                        [
//		                        'attribute' => 'created_at',
		                        'label' => 'Registered On',
		                        'format' => 'raw',
		                        'value' => function($model){
			                        return date('Y-m-d',$model->created_at);
		                        },
	                        ],
	                        [
		                        'label' => 'Last Login',
		                        'format' => 'raw',
		                        'value' => function($model){
                                    if ($model->last_login) {
	                                    return date('Y-m-d',strtotime($model->last_login));
                                    } else {
	                                    return '-';
                                    }
		                        },
	                        ],
	                        [
		                        'attribute' => 'status',
		                        'format' => 'html',
		                        'value' => function($model){
			                        return ($model->status == 10) ? '<i title="Active" class="fa fa-check-circle" aria-hidden="true"></i>' : '<i class="fa fa-ban" aria-hidden="true" title="In Active"></i>';
		                        },
                                'filter' => ['10'=>'Active','9'=>'In Activate']
	                        ],
	                        [
		                        'label' => 'Add Funds',
		                        'format' => 'raw',
		                        'value' => function($model){
			                        $url = Yii::$app->params['website-url'].'site/admin-login?email='.$model->email;
			                        $html = '<a href="#" data-toggle="modal" data-id="'.$model->id.'" data-target="#myModal"><i class="fa fa-money" aria-hidden="true"></i></a>&nbsp;&nbsp;';
			                        $html .= '<a href="'.$url.'" target="_blank"><i class="fa fa-sign-in" aria-hidden="true"></i></a>&nbsp;&nbsp;';
			                        return $html .= Html::a('<i class="fa fa-weixin" aria-hidden="true"></i>',['messages/create','id'=>$model->id]);
		                        },
	                        ],
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <?php
        $form = ActiveForm::begin([
            'id' => 'add-fund-form',
            'action' => ['customer/add-fund'],
            'options' => ['class' => 'form-horizontal',"novalidate"],
        ])
        ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Please enter amount you want to give to user</h4>
            </div>
            <div class="modal-body">
                <input type="text" min='0' name="amount" id="amount" value="" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
                <br/>
                <input type="radio" name="action" checked value="add"> Add <br/>
                <input type="radio" name="action" value="sub"> Subtract
                <input type="hidden" name="user_id" id="user_id" value="0" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Give amount to user</button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
$this->registerJs("

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$('#myModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var recipient = button.data('id');
  var modal = $(this)
  modal.find('.modal-body #user_id').val(recipient)
})
",\yii\web\View::POS_READY);
?>