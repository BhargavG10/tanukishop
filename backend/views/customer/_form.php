<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form clearfix">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'last_name')->textInput(['rows' => 6]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'email')->textInput(['rows' => 6]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'status')->dropDownList([ 10 => 'Enable', 9 => 'Disable']) ?>
    </div>
    <div class="col-lg-6">
		<?= $form->field($model, 'language')->dropDownList(['en-US'=>'English','ru-RU'=>'Russian']) ?>
    </div>
    <div class="col-lg-6">
		<?= $form->field($model, 'password_hash')->textInput(['rows' => 6,'disabled'=>'disabled']) ?>
    </div>
    <div class="col-lg-6">
        <input type="checkbox" name="change_password" id="change_password" value ='1' onclick="return enablePassword(this) "/> Change Password?
    </div>
        <div class="col-lg-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function enablePassword(change)
    {
        if (document.getElementById("change_password").checked) {
            document.getElementById("customer-password_hash").disabled = false;
        } else {
            document.getElementById("customer-password_hash").disabled = true;
        }
    }
</script>