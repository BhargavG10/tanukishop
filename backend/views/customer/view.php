<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row page-view">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        [
                            'attribute'=>'status',
                            'format' =>'text',
                            'value' => ($model->status == 10) ? 'Enable' : 'Disable'
                        ],
                        [
                            'label'=>'Total Balance',
                            'format' =>'text',
                            'value' => 'JPY '.number_format($model->credit)
                        ],
                        [
                            'label'=>'Total Blocked Amount',
                            'format' =>'text',
                            'value' => 'JPY '.number_format($model->getBlockedAmount())
                        ],
                        [
                            'label'=>'Registered On',
                            'format' =>'text',
                            'value' => date('Y-m-d',$model->created_at)
                        ],
                        'last_login:datetime'

                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>