<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if ($cat == 'auction') {
    $model->shop = 'yahoo_auctions';
} else if ($cat == 'a') {
    $model->shop = 'amazon';
} else if ($cat == 'y') {
    $model->shop = 'yahoo';
} else if ($cat == 'r') {
    $model->shop = 'rakuten';
}
$data = \common\models\Category::find()->where(['shop'=>$model->shop])->asArray()->all();

$tree = \common\components\TBase::buildTree($data);
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'ref_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <div class="form-group field-category-parent_id ">
                <label class="control-label" for="category-parent_id">Parent ID</label>
                <select id="category-parent_id" class="form-control" name="Category[parent_id]" aria-invalid="true">
                    <option value="0">root category</option>
	                <?=\common\components\TBase::printTree($tree,0,null,$model->parent_id,$model->id);?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'shop')->dropDownList(['yahoo_auctions'=>'Yahoo Auctions', 'rakuten'=>'Rakuten', 'amazon' => 'Amazon', 'yahoo' => 'Yahoo' ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'status')->dropDownList([ 1 => 'Enable', 0 => 'Disable', ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'detail_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'detail_ru')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
	        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
	        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
	        <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
		    <?= $form->field($model, 'serial_no')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-12">
		    <?= $form->field($model, 'seo_text')->widget(\dosamigos\ckeditor\CKEditor::className(), [
			    'preset' => 'full',
			    'clientOptions' => ['allowedContent' => true ]
		    ]) ?>
        </div>
        <div class="col-lg-4">
		    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?php
            if ($cat == 'a') {
                echo $form->field($model, 'amazon_search_index')->textInput(['maxlength' => true]);
            } else {
                $form->field($model, 'amazon_search_index')->hiddenInput(['maxlength' => true])->label(false);
            }
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
