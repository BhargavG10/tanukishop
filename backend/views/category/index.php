<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table.table > tbody > tr > td:first-child {
        width:100px;
        text-align:center;
    }
    table.table > tbody > tr > td:nth-child(4),table.table > tbody > tr > td:nth-child(5) {
        width:100px;
        text-align:center;
    }
    table.table > tbody > tr > td:nth-child(7) {
        width:100px;
        text-align:center;
    }
    .glyphicon-pencil,.glyphicon-trash
    {
        background-color: #23c6c8;
        border-color: #23c6c8;
        color: #FFFFFF;
        border-radius: 3px;
        display: inline-block;
        padding: 6px 12px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        touch-action: manipulation;
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?= Html::a(Yii::t('app', '+ Add Yahoo Auction Category'), ['create','cat'=>'auction'], ['class' => 'btn btn-success pull-right']) ?>
                <?= Html::a(Yii::t('app', '+ Add Yahoo Category'), ['create','cat'=>'y'], ['class' => 'btn btn-success pull-right','style'=>'margin-right: 17px;']) ?>
                <?= Html::a(Yii::t('app', '+ Add Rakuten Category'), ['create','cat'=>'r'], ['class' => 'btn btn-success pull-right','style'=>'margin-right: 17px;']) ?>
                <?= Html::a(Yii::t('app', '+ Add Amazon Category'), ['create','cat'=>'a'], ['class' => 'btn btn-success pull-right','style'=>'margin-right: 17px;']) ?>
            </div>
            <div class="ibox-content">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'pager' => [
	                    'firstPageLabel' => 'First',
	                    'lastPageLabel'  => 'Last'
                    ],
                    'tableOptions' => ['class'=>'table table-bordered'],
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        'ref_id',
                        [
                            'attribute' => 'title_en',
                            'value' => function($model){
                                return (strlen($model->title_en)>30) ? substr($model->title_en,0,30).'...' : $model->title_en;
                            },
                        ],
                        [
                            'attribute' => 'title_ru',
                            'value' => function($model){
                                return (strlen($model->title_ru)>30) ? substr($model->title_ru,0,30).'...' : $model->title_ru;
                            },
                        ],
                        [
                            'attribute'=>'shop',
                            'label' => 'Shop',
                            'value' => function($model){
                                return ($model->shop == 'yahoo_auctions') ? 'Yahoo Auction' : $model->shop;
                            },
                            'filter'=>['yahoo_auctions'=>'Auction','yahoo'=>'Yahoo','amazon'=>'Amazon','rakuten'=>'Rakuten']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'text',
                            'label' => 'Status',
                            'value' => function($model){
                                return ($model->status) ? 'Enable' : 'Disable';
                            },
                            'filter'=>[1=>'Enable',0=>'Disable']
                        ],
                        'serial_no',
                        [
                            'attribute' => 'parent_id',
                            'format' => 'text',
                            'label' => 'Parent Category',
                            'value' => function($model){
                                if ($model->parent_id) {
                                    $values = \common\models\Category::findOne($model->parent_id);
                                    if ($values && isset($values->title_en)) {
	                                    return ( strlen( $values->title_en ) > 30 ) ? substr( $values->title_en, 0, 30 ) . '...' : $values->title_en;
                                    } else {
	                                    return ' Root Category ';
                                    }
                                } else {
                                    return ' Root Category ';
                                }
                            },
                            'filter' => false
                        ],
                        // 'created_on',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
