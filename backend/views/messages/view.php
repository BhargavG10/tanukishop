<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Chat With '.$model->user->first_name.' '.$model->user->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-cart1 {  width: 20% !important;  }
    textarea {  resize: none;padding: 12px;font-size: 15px;   width: 723px;  border: none;  margin: 0px;
        border-radius:0px!important;  }
    .send-btn{  width: 100%;  padding: 11px;  border-radius: 0px;  }
    .padding-left-0 {padding-left:0px;}
    .padding-right-0 {padding-right:0px;}
    .msg-div{margin-bottom: 23px;}
    .updated-style {padding: 7px 1px 8px 1px;background: #e9c4a7;color: #fff;border-radius: 10px;position: relative;}
    .date{position: absolute;top: 0px;font-size: 11px;right: 17px}
    .avtar{background-color: #df7b2b;width: 46px;height: 46px;
        color: #fff;text-align: center;line-height: 46px;font-weight: bold;font-size: 22px;}
    .message:before {content: "";display: block;position: absolute;width: 0;height: 0;left: 0;top: 0;
        border-top: 7px solid transparent;border-bottom: 7px solid transparent;border-right: 7px solid #e9c4a7;margin: 15px 0 0 -6px;}
    .my_message:before{border-right: 7px solid #6792B9;}
    .bg-3071a9{background:#6792B9;color:#fff!important;}
    .message{background-color:inherit;border:none;margin:1px;color:darkslategray;padding-top: 18px;}
    .margin-bottom-48{margin-bottom:48px;}
    .field-messages-msg{margin-left: 83px;}
</style>
<div class="row messages-index">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class=""><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                <div class="messages-view">
                    <?php
                    foreach($allMessages as $msg) { ?>
                        <div class="msg-div clearfix">
                            <?php if ($msg->message_from == 'admin') { ?>
                                <div class="col-lg-1">
                                    <div class="img-circle">
                                        <?=\common\components\TBase::getNameChatTitle($msg->message_from)?>
                                    </div>
                                </div>
                                <div class="updated-style col-lg-10">
                                    <div class="admin_message message">
                                        <?=nl2br($msg->msg)?>
                                        <div class="date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?=date('d M, Y H:i:s a',strtotime($msg->date))?>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class=" col-lg-1">
                                    <div class="bg-3071a9 avtar img-circle">
                                        <?=($msg->message_from=='admin') ? 'TS' : 'USR'?>
                                    </div>
                                </div>
                                <div class="bg-3071a9 updated-style col-lg-10">
                                    <div class="my_message message">
                                        <?=nl2br($msg->msg)?>
                                        <div class="date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?=date('d M, Y H:i:s a',strtotime($msg->date))?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                        <div class="clearfix margin-bottom-48 message-form">
                            <?php $form = ActiveForm::begin(['id' => 'form-message']);
                            $model->msg = '';
                            ?>
                            <div class="col-lg-9 padding-right-0" id="text-field">
                                <?=$form->field($model,'msg')->textarea()->label(false);?>
                            </div>
                            <div class="col-lg-2 padding-left-0">
                                <div class="form-group">
                                    <?= Html::submitButton('Reply', ['class' => 'btn btn-primary send-btn', 'name' => 'signup-button','style'=>'padding:16px']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.scrollTo(0,document.body.scrollHeight);
</script>
<?php

$this->registerjs("
$('body').on('click','.send-btn',function() {
        $(this).html('Please Wait...');
    })
    ",\yii\web\View::POS_READY,'click-btn');

?>