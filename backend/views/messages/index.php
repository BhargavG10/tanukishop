<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row messages-index">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <h5 class="pull-right"><?= Html::a('New Chat',['create'],['class'=>'btn btn-primary']) ?></h5>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'summary'=>'',
                        'columns' => [
	                        [
		                        'attribute'=>'date',
		                        'value' => function($data) {
			                        $date = $data->getLatestDate();
			                        return date('Y-m-d',strtotime($date['date']));
		                        },
		                        'filter'=> Html::activeTextInput($searchModel,'date',['class'=>'form-control','type'=>'date'])
	                        ],
                            [
                                'attribute'=>'user_id',
                                'value' => function($model) {
                                    return (isset($model->user)) ? $model->user->first_name.' '.$model->user->last_name : '';
                                },
                                'filter'=>ArrayHelper::map(Customer::find()->all(),'id','fullName')
                            ],
                            [
                                'attribute' => 'msg',
                                'value' => function($model) {
                                    return (strlen(strip_tags($model->msg))>50) ? substr($model->msg,0,40).'...' : $model->msg;
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' =>'{view} {delete}',
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
