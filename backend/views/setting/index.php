<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row page-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
    <!--<p>
        <?php //Html::a(Yii::t('app', 'Create Setting'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
	        'firstPageLabel' => 'First',
	        'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'setting_key',
                'format'=>'html',
                'value'=>function($model) {
                    $stng = str_replace('-',' ',$model->setting_key);
                    return '<span style="text-transform:uppercase ">'.str_replace('_',' ',$stng).'</span>';
                }
            ],

            [
                'attribute'=>'setting_value',
                'value'=>function($model) {
                    return (strlen($model->setting_value)>100) ? substr($model->setting_value,0,100).'...':$model->setting_value;
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
