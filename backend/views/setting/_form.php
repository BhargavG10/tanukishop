<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'setting_key')->textInput(['maxlength' => true]);
    } else {
        echo $form->field($model, 'setting_key')->textInput(['disabled' => 'disabled']);
    }
    if (
        $model->setting_key == 'Yahoo-Shopping' ||
        $model->setting_key == 'Rakuten' ||
        $model->setting_key == 'Amazon'
    ){
        echo $form->field($model, 'setting_value')->dropDownList(['Disabled'=>'Disabled','Enable'=>'Enable']);
    } else {
        echo $form->field($model, 'setting_value')->textInput(['maxlength' => true]);
    }
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
