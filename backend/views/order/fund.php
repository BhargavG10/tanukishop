<?php
use yii\helpers\Html;
use common\components\TBase;
use yii\widgets\ActiveForm;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();


$tanuki = new \common\helper\Tanuki();
$this->title = 'TNK0000'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    table.less-padding tr td,table.less-padding tr th{padding: 2px;text-align: center!important;}
</style>
<div class="panel panel-default">
    <div class="panel-heading panel-heading-transparent"><strong><?= Html::encode($this->title) ?></strong></div>

    <div class="panel-body">
        <div class="row clearfix">
            <div class="order-summary-table col-lg-12 clearfix">
                <h3 class="inner-head">Order Summary</h3>
                <table class="table table-bordered">
                    <tr><th>Order ID </th><td>TNK0000<?= $model->id?></td></tr>
                    <tr><th>Total </th><td><?=$tModel->convert($model['subtotal'],'JPY','JPY'); ?></td></tr>
                    <tr><th>Currency </th><td><?=$model->currency; ?></td></tr>
                    <tr><th>Payment Method </th><td><?=$model->method; ?></td></tr>
                    <tr><th>Order Status </th>
                        <td>
                            <?php $form = ActiveForm::begin(); ?>
                            <?=Html::dropDownList('status',$model->order_status,
                                \yii\helpers\ArrayHelper::map(\common\models\OrderStatus::find()->all(),'id','title'));
                            ?>
                            <input type="submit" name="update_status" value="Update" class="btn btn-success pull-right">
                            <?php ActiveForm::end(); ?>
                        </td></tr>
                    <tr><th>Payment Status </th><td><?=$model->status; ?></td></tr>
                    <tr><th>Ordered On Date </th><td><?=$model->date; ?></td></tr>
                    <tr><th>Invoice </th><td><?=$model->invoice; ?></td></tr>
                    <tr><th>Response </th><td><?=$model->response; ?></td></tr>
                </table>
            </div>

            <div class="col-lg-12 clearfix margin-top-20">
                <h3 class="inner-head">Ordered Product(s) Summary</h3>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table min-w700 table-order-view-detail table-bordered">
                    <tr class="notranslate">
                        <td><?=TBase::ShowLbl('SERVICE'); ?></td>
                        <td><?=TBase::ShowLbl('TOTAL')?></td>
                    </tr>
                    <tr class="notranslate">
                        <td>
                        Adding funds to deposit to pay for goods or service
                        </td>
                        <td><?=$tModel->convert($model->subtotal,'JPY','JPY')?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>