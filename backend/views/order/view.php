<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$tModel = new \common\components\TCurrencyConvertor;
$tbase = new \common\components\TBase();
/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = 'TNK0000'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$shipping  = $model->shipping;
$first_name = (isset($shipping->first_name)) ? $shipping->first_name : Yii::$app->user->identity->first_name;
$last_name = (isset($shipping->last_name)) ? $shipping->last_name : Yii::$app->user->identity->last_name;
$company = (isset($shipping->company)) ? $shipping->company:'';
$appt_no = (isset($shipping->appt_no)) ? $shipping->appt_no:'';
$address_1 = (isset($shipping->address_1)) ? $shipping->address_1:'';
$address_2 = (isset($shipping->address_2)) ? $shipping->address_2:'';
$city = (isset($shipping->city)) ? $shipping->city:'';
$state = (isset($shipping->state)) ? $shipping->state:'';
$country = (isset($shipping->country)) ? $shipping->country:'';
$phone = (isset($shipping->phone)) ? $shipping->phone:'';
$method = (isset($shipping->shipping_method)) ? $shipping->shipping_method:'';
$weight = (isset($shipping->shipping_weight)) ? $shipping->shipping_weight:'';
$zipcode = (isset($shipping->zipcode)) ? $shipping->zipcode:'';
$phone = (isset($shipping->phone)) ? $shipping->phone:'';
$email = (isset($shipping->email)) ? $shipping->email:'';
?>
<style>
    table.less-padding tr td,table.less-padding tr th{padding: 2px;text-align: center!important;}
    .width-63{ width:63px;}
</style>
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-transparent"><strong><?= Html::encode($this->title) ?></strong></div>
	    <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row clearfix">
                <div class="order-summary-table col-lg-6">
                    <h3 class="inner-head">Order Summary</h3>
                    <table class="table table-bordered">
                        <tr><th>Order ID </th><td>TNK0000<?= $model->id?></td></tr>
                        <tr><th>Order type </th><td><?=$model->getOrderType();?></td></tr>
                        <tr><th>Total </th><td>
                                <?php
                                if ($model->type == 'add_fund') {
	                                $total = $model['subtotal'];
                                } else {
	                                $total = $model['total'];
                                }
                                echo $tModel->convert( $total, 'JPY', 'JPY' );
                                ?>
                            </td></tr>
                        <tr><th>Currency </th><td><?=$model->currency; ?></td></tr>
                        <tr><th>Payment Method </th><td><?=$model->method; ?></td></tr>
                        <tr><th>Order Status </th>
                        <td>
                            <?=Html::dropDownList('status',$model->order_status,
                                \yii\helpers\ArrayHelper::map(\common\models\OrderStatus::find()->all(),'id','title'));
                            ?>
                        </td></tr>
                        <tr><th>Payment Status </th><td><?=$model->status; ?></td></tr>
                        <tr><th>Ordered On Date </th><td><?=$model->date; ?></td></tr>
                        <tr><th>Invoice </th><td><?=$model->invoice; ?></td></tr>
                        <tr><th>Order User </th><td><?=$model->user->first_name.' '.$model->user->last_name; ?></td></tr>
                        <tr><th>User Balance</th><td>¥<?=number_format($model->user->credit); ?></td></tr>
                        <?php if ($model->external == 1) { ?>
                            <tr><th>Response </th><td><?=$model->response?></td></tr>
                        <?php } else { ?>
                            <tr><th>Response </th><td><pre><?php print_r(json_decode($model->response)); ?></pre></td></tr>
                        <?php } ?>
                    </table>
                </div>
                <div class="order-shipping-summary col-lg-6">
                    <h3 class="inner-head">Shipping Summary</h3>
                    <table class="table table-bordered">
                        <tr><th>Name</th><td><input type="text" name="shipping[first_name]" value="<?=$first_name?>" placeholder="First Name"/>&nbsp;<input type="text" name="shipping[last_name]" value="<?=$last_name?>" placeholder="Last Name"/></td></tr>
                        <tr><th>Company Name</th><td><input type="text" name="shipping[company]" value="<?=$company?>"/></td></tr>
                        <tr><th>Address 1</th><td><textarea name="shipping[address_1]" cols="40" rows="1"><?=$address_1?></textarea></td></tr>
                        <tr><th>Address 2</th><td><textarea name="shipping[address_2]" cols="40" rows="1"><?=$address_2?></textarea></td></tr>
                        <tr><th>Apt. / house no.</th><td><input type="text" name="shipping[appt_no]" value="<?=$appt_no?>"/></td></tr>
                        <tr><th>City</th><td><input type="text" name="shipping[city]" value="<?=$city?>"></td></tr>
                        <tr><th>State / District</th><td><input type="text" name="shipping[state]" value="<?=$state?>"></td></tr>
                        <tr><th>Postal code</th><td><input type="text" name="shipping[zipcode]" value="<?=$zipcode?>"></td></tr>
                        <tr><th>Phone</th><td><input type="text" name="shipping[phone]" value="<?=$phone?>"></td></tr>
                        <tr><th>Email</th><td><input type="text" name="shipping[email]" value="<?=$email?>"></td></tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-12 clearfix margin-top-20 table-responsive">
                <h3 class="inner-head">Ordered Product(s) Summary</h3>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table min-w700 table-order-view-detail table-bordered">
                    <?php $orderDetails = \common\models\OrderDetail::findAll(['order_id'=>$model->id]);?>
                    <tr>
                        <td width="10%" height="30" bgcolor="#7ac0c8" class="cw txt-c">Image</td>
                        <td width="30%" bgcolor="#7ac0c8" class="cw txt-c">Product Name</td>
                        <td width="18%" bgcolor="#7ac0c8" class="cw txt-c">Product ID</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Shop name</td>
                        <td width="13%" bgcolor="#7ac0c8" class="cw txt-c">Price</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Quantity</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Tanuki Fee</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Domestic Shipping</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Weight (kg)</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Bank Fees</td>
                        <td width="8%" bgcolor="#7ac0c8" class="cw txt-c">Other Fees</td>
                        <td width="13%" bgcolor="#7ac0c8" class="cw txt-c">Total</td>
                    </tr>
	                <?php
	                $blocked_amount = $actualBlockedAmount = 0;
	                if (($model->type == 'buyout' || $model->type == 'auction')) {

		                $blocked_amount = $model->blocked_amount;
		                $actualBlockedAmount = $blocked_amount;
	                }
//	                $pendingAmount = $model->total - $actualBlockedAmount - $model->paid_amount;
	                $pendingAmount = $model->total - $model->paid_amount;
                    $count = $price = $total = 0 ;
                    if (count($model->orderDetail)>0){
                        foreach($model->orderDetail as $key => $item) {

                            $price = ($item->price * $item['quantity']) + $item['tanuki_fee'] + $item['domestic_shipping'] + $item['bank_fees'] + $item['other_fees'];
                            $total += $price;
                            ?>
                            <tr class="txt-c">
                                <td width="10%"><?=$item->getImage()?></td>
                                <td width="50%" class="translate" data-title="<?=ucfirst($item['product_name']); ?>" data-short="0">
                                    <?=ucfirst($item['product_name']);?>
                                    <?php
                                    if (isset($item->productAttributes) && count($item->productAttributes)>0) { ?>
                                        <table class="less-padding table table-bordered" style="margin-top: 30px">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Attribute Name</th>
                                                <th class="text-center">Attribute Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($item->productAttributes as $attribute) { ?>
                                                <tr>
                                                    <td><?=$attribute->attribute_name?></td>
                                                    <td><?=$attribute->attribute_value?></td>
                                                </tr>
                                                <?php
                                            } ?>
                                            </tbody>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td width="18%"><?=Html::a(ucfirst($item['product_id']),$item->getOuterLink(),['target'=>'_blank']);?></td>
                                <td width="8%"><?=ucfirst($item['shop']); ?></td>
                                <td width="13%"><?=$tModel->convert($item->price,'JPY','JPY'); ?></td>
                                <td width="8%"><?=$item['quantity']; ?></td>
                                <td width="8%"><?=number_format($item['tanuki_fee']); ?></td>
                                <td width="8%">
                                    <?php if (($model->type == 'buyout' || $model->type == 'auction')) { ?>
                                        <input type="text" class="width-63" tabindex="1" name="domestic_shipping[<?=$item['id']?>]" value="<?=$item['domestic_shipping']; ?>">
                                    <?php } else { ?>
	                                    <?=number_format($item['domestic_shipping'])?>
                                        <input type="hidden" name="domestic_shipping[<?=$item['id']?>]" value="<?=$item['domestic_shipping']; ?>">
                                    <?php }?>
                                </td>
                                <td width="8%"><input type="text" class="width-63 weight-class" tabindex="2" name="weight[<?=$item['id']?>]" value="<?=$item['weight']; ?>"></td>
                                <td width="8%"><input type="text" class="width-63" tabindex="3" name="bank_fees[<?=$item['id']?>]" value="<?=$item['bank_fees']; ?>"></td>
                                <td width="8%"><input type="text" class="width-63" tabindex="4" name="other_fees[<?=$item['id']?>]" value="<?=$item['other_fees']; ?>"></td>
                                <td width="13%"><?=$tModel->convert($price,'JPY','JPY'); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <th colspan="11" align="right">Sub Total</th>
                            <td align="center"><?=$tModel->convert($model->subtotal,'JPY','JPY'); ?></td>
                        </tr>
                        <tr>
                            <th colspan="11" align="right">Consolidation</th>
                            <td align="center">
                                <input type="text" class="width-63" name="order[consolidation]" value="<?=$model->consolidation; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="11" align="right">
                                <div class="col-md-offset-3 col-md-3 text-center">
                                    Shipping Method <br/>
		                            <?=Html::dropDownList('shipping[shipping_method]',$method,\yii\helpers\ArrayHelper::map(\common\models\ShippingMethods::find()->all(),'id','title'),['prompt'=>'Please Select Shipping Method','class'=>'shipping_method_dropdown']); ?>
                                </div>
                                <div class="col-md-4 text-center">
                                    Shipping Country <br/>
                                    <div class="shipping_country_list">
                                        <?php
                                        if ($method) {
	                                        $list = \common\models\ShippingMethods::getCountryList($method);
                                        } else {
	                                        $list = $tbase->countryList();
                                        }
                                        ?>
		                                <?=Html::dropDownList('shipping[country]',$country,$list,['prompt'=>'Please Select Shipping Country']); ?>
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    Shipping Weight (kg)<br/>
                                    <input type="text" class="shipping_weight" name="shipping[shipping_weight]" value="<?=$weight; ?>"/>
                                </div>
                            </th>
                            <td>
                                <br/>
                                <input type="text" class="width-63" name="order[shipping_charges]" value="<?=$model->shipping_charges; ?>"/>
                            </td>
                        </tr>
                        <tr style="border-top: 1px solid #fff;">
                            <th colspan="11" align="right">Total</th>
                            <td align="center"><?=$tModel->convert(($model->total),'JPY','JPY'); ?></td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td colspan="12" align="center">Empty Cart</td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <div class="clearfix">
                    <div class=" pull-right">
			            <?=Html::a('Send Payment Invoice',['invoice-mail','id'=>$model->id],[
				            'class'=>'btn btn-info',
				            'data' => [
					            'confirm' => 'Are you sure you want to send invoice to user?'
				            ]
			            ])?>
			            <?=Html::a('Send Receipt',['receipt-mail','id'=>$model->id],[
				            'class'=>'btn btn-warning',
				            'data' => [
					            'confirm' => 'Are you sure you want to send Receipt to user?'
				            ]
			            ])?>
                        <input type="submit" name="update_status" value="Update Order" class="btn btn-primary">
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-6 pull-right">
                        <table class="table table-bordered">
                            <tr>
                                    <td>
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Order Amount : </th><td>¥<?=number_format($model->total)?></td>
                                            </tr>
                                            <tr>
                                                <th>Blocked Amount : </th><td>¥<?=number_format($blocked_amount)?></td>
                                            </tr>
                                            <?php
                                            $color  = '';
                                            if (($model->total - $model->paid_amount)>0) { ?>
                                                <tr style="color: #ed5565">
                                                    <th>Pending Amount : </th>
                                                    <td>
                                                        ¥<?=number_format($pendingAmount)?><br/>
                                                        <?php

	                                                    if ($pendingAmount > 0) {
		                                                    echo Html::a( 'Deduct Amount', [ 'deduct-amount', 'id' => $model->id ], [
                                                                'class'=>'badge badge-danger',
			                                                    'data'  => [
				                                                    'confirm' => 'Are you sure you want to deduct amount from user account?'
			                                                    ]
		                                                    ] );
	                                                    }
	                                                    ?></td>
                                                </tr>
                                            <?php } else { ?>
                                                <tr style="color: #1ab394">
                                                    <th>Pending Amount : </th>
                                                    <td>
                                                        ¥<?=number_format($pendingAmount)?><br/>
	                                                    <?php

	                                                    if ($pendingAmount < 0) {
		                                                    echo Html::a( 'Refund Amount', [ 'deduct-amount', 'id' => $model->id ], [
			                                                    'class'=>'badge badge-primary',
			                                                    'data'  => [
				                                                    'confirm' => 'Are you sure you want to deduct amount from user account?'
			                                                    ]
		                                                    ] );
	                                                    }
	                                                    ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>

                                            <?php
                                            $color  = '';
                                            if ($model->user->credit > 10 && $model->user->credit > ($pendingAmount)) {
                                                $color = "#1ab394";
                                            } else {
                                                $color = "#ed5565";
                                            }
                                            ?>
                                            <tr style="color: <?=$color?>;">
                                                <th>User Balance : </th>
                                                <td>¥<?=number_format($model->user->credit)?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	    <?php ActiveForm::end(); ?>
    </div>
</section>
<?php
$script = "

$('.weight-class').change(function(){
    var total = 0;
    $('.weight-class').each(function(key,value){
        total += parseFloat($(this).val());
        $('.shipping_weight').val(total);
    });
}); 


$('.shipping_method_dropdown').change(function(){
     $.ajax({ 
          url: '".\yii\helpers\Url::to(['shipping-methods/countries-list'])."',
          data: {id: $(this).val()},
          type: \"POST\",
          success: function(data){
              $('.shipping_country_list').html(data);
          },
          error: function(){
              console.log(\"failure\");
          }
    });
}); 
";
$this->registerJs($script, \yii\web\View::POS_END);
?>