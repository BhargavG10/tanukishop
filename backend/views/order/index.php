<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row page-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">

            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pager' => [
	                    'firstPageLabel' => 'First',
	                    'lastPageLabel'  => 'Last'
                    ],
                    'columns' => [

	                    [
		                    'label'=>'Image',
		                    'format'=>'html',
		                    'value'=> function($data){
			                    return Html::a(Html::img($data->image,['width'=>50,'height'=>50]),['view','id'=>$data->id]);
		                    },
	                    ],
                        [
                            'attribute'=>'id',
                            'label'=>'Order ID',
                            'value'=> function($data){
                                return 'TNK0000'.$data->id;
                            },
                        ],
                        [
                            'attribute'=>'auction_id',
                            'format'=>'raw',
                            'value'=> function($data){
                                return ($data->auction_id) ?
	                                '<a href="https://page.auctions.yahoo.co.jp/jp/auction/'.$data->auction_id.'" target="_blank">'.$data->auction_id.'</a>':
                                    '--';
                            },
                        ],
                        [
                            'attribute'=>'type',
                            'label'=>'Order Type',
                            'value'=> function($data){
                                return $data->getOrderType();
                            },
                            'filter'=>['add_fund'=>'Add Fund','product_purchase'=>'Shop Order','buyout'=>'Yahoo Buyout','auction'=>'Yahoo Auction']
                        ],
                        [
                            'attribute'=>'method',
                            'label'=>'Order method',
                            'value'=> function($data){
                                return $data->method;
                            },
                            'filter'=>\common\models\Payments::getPaymentMethod()
                        ],
                        [
                            'attribute'=>'user_id',
                            'format'=>'html',
                            'value'=> function($data){
                                if (isset($data->user)) {
                                    return Html::a($data->user->first_name . ' ' . $data->user->last_name, ['customer/view', 'id' => $data->user->id], ['target' => '_blank']);
                                }
                            },
                        ],
                        [
                            'attribute'=>'total',
                            'label'=>'Order Total',
                            'format'=>'html',
                            'value'=> function($data){
                                $tModel = new \common\components\TCurrencyConvertor;
	                            if ($data->type == 'add_fund') {
	                                $total = $data->subtotal;
	                            } else {
		                            $total = $data->total;
                                }
	                            return $tModel->convert($total,'JPY','JPY');

                            },
                        ],
                        [
                            'label'=>'Blocked Amount',
                            'format'=>'raw',
                            'value'=> function($data){
                                $tModel = new \common\components\TCurrencyConvertor;
                                return $tModel->convert($data->blocked_amount,'JPY','JPY');
                            },
                        ],
                        [
                            'attribute'=>'order_status',
                            'value'=> function($data){
                                if ($data->type == 'add_fund') {
	                                return ($data->status) ? $data->status : 'Not Successful';
                                } else {
	                                return ( isset( $data->statusDetail->title ) ) ? $data->statusDetail->title : '-';
                                }
                            },
                            'filter'=>\yii\helpers\ArrayHelper::map(\common\models\OrderStatus::find()->all(),'id','title')
                        ],
                        [
                            'attribute'=>'date',
                            'value'=> function($data){
                                return date('d-m-Y',strtotime($data->date));
                            },
                            'filter'=>false
                        ],

                        ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                    ],
                ]); ?>
                <strong>Note: Use last digits after 'TNK0000' for searching order by order id</strong>
            </div>
        </div>
    </div>
</div>