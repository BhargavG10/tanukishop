<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row page-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">

                <div class="page-form">

                    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
                    <div class="form-group"><label class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10"><?= $form->field($model, 'username')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10"><?= $form->field($model, 'email')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-10"><?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-10"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>
                    <div class="form-group ">
                        <div class="col-lg-offset-2 col-lg-10">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row page-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Change Password</h5>
            </div>
            <div class="ibox-content">

                <div class="page-form">

                    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
                    <div class="form-group"><label class="col-lg-2 control-label">New Password</label>
                        <div class="col-lg-10"><?= $form->field($modelp, 'new_password')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Confirm Password</label>
                        <div class="col-lg-10"><?= $form->field($modelp, 'confirm_password')->textInput(['maxlength' => true])->label(false); ?></span></div>
                    </div>
                    <div class="form-group ">
                        <div class="col-lg-offset-2 col-lg-10">
                            <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
