<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$items = \yii\helpers\ArrayHelper::map(\common\models\Labels::find()->all(), 'id', 'slug');
?>
<div class="labels-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textarea(['cols' => 5]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'russian')->textarea(['cols' => 5]) ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-3 margin-top-20">
            <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
