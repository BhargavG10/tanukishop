<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LabelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Labels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?php if (isset($_REQUEST['q'])) {?>
                    <?= Html::a(Yii::t('app', '+ Add New Label'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
                <?php } ?>
            </div>
            <div class="ibox-content">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class'=>'table table-bordered'],
                    'pager' => [
	                    'firstPageLabel' => 'First',
	                    'lastPageLabel'  => 'Last'
                    ],
                    'filterModel' => $searchModel,
                    'columns' => [
                        'title',
                        'slug',
                        'category',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' =>'{detail}',
                            'buttons' => [
                                'detail' => function ($url,$model) {
                                    $url = \yii\helpers\Url::to(['labels-detail/update-labels','id'=>$model->id]);
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-list-alt"></span>',
                                        $url,
                                        [
                                            'title' => 'Download',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                            ],


                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?></div>
        </div>
    </div>
</div>
