<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LabelsDetail */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Labels Detail',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row labels-update">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>