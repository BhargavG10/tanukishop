<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LabelsDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="labels-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'lang_code')
            ->dropDownList(
                ['en-US'=>'English','ru-RU'=>'Russian']
            );


            $items = \yii\helpers\ArrayHelper::map(\common\models\Labels::find()->all(), 'id', 'slug');

            echo $form->field($model, 'labels_id')
                 ->dropDownList(
                     $items
                 );
        }
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['labels-detail/index'],['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
