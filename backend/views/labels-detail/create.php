<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LabelsDetail */

$this->title = Yii::t('app', 'Create Labels');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row labels-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <div class="pull-left">
                    <h5><?= Html::encode($this->title) ?></h5>
                </div>
                <div class="pull-right">
                    <h5><?= Html::a('+ New',['labels/create']) ?></h5>
                </div>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
