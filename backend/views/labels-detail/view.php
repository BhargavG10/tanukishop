<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LabelsDetail */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labels Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row page-view">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                //    'id',
                    'title:ntext',
                    'lang_code',
                    [
                        'attribute'=>'labels_id',
                        'format' =>'text',
                        'label' =>'Slug',
                        'value' => $model->labels->slug
                    ],
                ],
            ]) ?>
            </div>
        </div>
    </div>
</div>