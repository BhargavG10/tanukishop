<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\LabelsDetail */


$this->title = Yii::t('app', 'Update {modelClass} ', [
    'modelClass' => 'Labels Detail',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row labels-update">
    <div class="col-lg-12 clearfix">

        <?php foreach($model as $key=>$labels) {
            ?>
            <?php $form = ActiveForm::begin(); ?>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Language : <?= Html::encode($labels->language->title) ?></h5>
            </div>
            <div class="ibox-content">
                <div class="labels-detail-form">
                    <?=$form->field($labels, 'title')->textarea(['rows' => 6]) ?>
                    <?=$form->field($labels, 'id')->hiddenInput()->label(false); ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['labels-detail/index'],['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
            <?php ActiveForm::end(); ?>
        <?php } ?>
    </div>
</div>