<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LabelsDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Labels Details');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <h5 class="pull-right"><?=Html::a('+ Add New Label', ['/labels/create'], ['class' => 'btn btn-default']) ?></h5>
            </div>
            <div class="ibox-content">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pager' => [
	                    'firstPageLabel' => 'First',
	                    'lastPageLabel'  => 'Last'
                    ],
                    'tableOptions' => ['class'=>'table table-bordered'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute'=>'title',
                            'format'=>'html',
                            'value' => function($model){
                                return (strlen($model->title)>40) ? mb_substr($model->title, 0, 40, 'UTF-8').'...' : $model->title;
                            },
                        ],
                        [
                            'attribute'=>'lang_code',
                            'value' => 'lang_code',
                            'filter'=>['en-US'=>'English','ru-RU'=>'Russian']
                        ],
                        [
                            'attribute'=>'slug',
                            'value'=>'labels.slug',
                        ],

                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{view}  | {update} | {delete}',
                            'buttons' => [
                                'update' => function ($url,$model) {
                                    $url = \yii\helpers\Url::to(['labels-detail/update-labels','id'=>$model->labels_id]);
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-pencil"></span>',
                                        $url,
                                        [
                                            'title' => 'Download',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?></div>
        </div>
    </div>
</div>

