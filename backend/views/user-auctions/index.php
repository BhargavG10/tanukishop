<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = Yii::t('app', 'User Auctions');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row user-auctions-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
	                            'attribute'=>'auction_id',
	                            'format'=>'html',
	                            'value'=> function($data){
		                            return Html::a($data->auction_id,'https://page.auctions.yahoo.co.jp/jp/auction/'.$data->auction_id,['target'=>'_blank', 'class'=>"outer-link"]);
	                            },
                            ],
                            [
                                'attribute'=>'user_id',
                                'format'=>'html',
                                'value'=> function($data){
                                    if (isset($data->user->first_name)) {
	                                    return Html::a( $data->user->first_name . ' ' . $data->user->last_name, [
		                                    'customer/view',
		                                    'id' => $data->user->id
	                                    ], [ 'target' => '_blank' ] );
                                    }
                                },
                                'filter'=>Html::activeDropDownList($searchModel, 'user_id', \common\models\User::allUser(),['class'=>'form-control','prompt' => 'All']),
                            ],
                            [
                                'attribute'=>'amount',
                                'format'=>'html',
                                'value'=> function($data){
                                    return $data->getFormattedAmount();
                                },
                            ],
                            [
                                'attribute'=>'auction_ends',
                                'filter'=>false,
                                'format'=>'html',
                                'value'=> function($data){
	                                $html = $data->getTimeLeft();
                                    if ($data->is_deleted) {
                                        if ($data->auction_type == 'buyout') {
	                                        $html = 'Yahoo Auction Buyout';
                                        } else {
	                                        if ( $data->is_deleted ) {
		                                        $html .= ' At <br/><small>'.$data->auction_ends.'</small>';
	                                        }
                                        }

                                    }
                                    return $html;
                                },
                            ],
                            [
                                'attribute'=>'auction_type',
                                'value'=> function($data){
                                    return $data->auction_type;
                                },
                                'filter'=>['auction'=>'Auction','buyout'=>'Buyout']
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {winner}',
                                'buttons' => [
                                    'winner' => function ($url, $model) {
                                        if ($model->is_deleted) {
                                            return false;
                                        }
                                        $url = \yii\helpers\Url::toRoute(['mark-winner','id'=>$model->id]);
                                        return Html::a('Mark Winner',
                                            $url, [
                                                'title' => \Yii::t('yii', 'mark as winner'),
                                                'data-confirm' =>
                                                    \Yii::t('yii', 'Are you sure you want to mark this auction as winning auction?'),
                                                'data-pjax' => '0',
                                                'class'=>'btn btn-danger'
                                            ]);
                                    },
                                    'view' => function ($url, $model) {
                                        return Html::a('View Detail',
                                            $url, [
                                                'title' => \Yii::t('yii', 'Detail'),
                                                'class'=>'btn btn-warning'
                                            ]);
                                    },
                                    'mail' => function ($url, $model) {
                                        $url = \yii\helpers\Url::toRoute(['winner-mail','id'=>$model->auction_id,'user_id'=>$model->user_id]);
                                        return Html::a('Send Mail',
                                            $url, [
                                                'title' => \Yii::t('yii', 'Mail'),
                                                'data-confirm' =>
                                                    \Yii::t('yii', 'Are you sure you want to send mail to this user?'),
                                                'data-pjax' => '0',
                                                'class'=>'btn btn-primary'
                                            ]);
                                    }
                                ]
                            ],
                        ],
                    ]); ?>
            </div>
        </div>
    </div>
</div>
