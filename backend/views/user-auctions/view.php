<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserAuctions */

$this->title = $model->auction_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Auctions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-auctions-view">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-transparent"><strong><?= Html::encode($this->title) ?></strong></div>
        <div class="panel-body">
            <div class="row clearfix">
                <div class="order-summary-table col-md-12">
                    <h3 class="inner-head">Auction Summary</h3>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'auction_id',
                            [
                                'attribute'=>'user_id',
                                'label'=>'Auction By',
                                'format'=>'html',
                                'value'=> function($data){
	                                if (isset($data->user->first_name)) {
		                                return Html::a( $data->user->first_name . ' ' . $data->user->last_name, [
			                                'customer/view',
			                                'id' => $data->user->id
		                                ], [ 'target' => '_blank' ] );
	                                }
                                },
                            ],
                            'amount:integer',
                            'ip_address',
                            'auction_ends:datetime',
                            'auction_type',
                            'blocked_amount:integer',
                            'created_on:datetime',
                            'modified_on',
                            'api_response',
                        ],
                    ]); ?>
            <button onclick="history.go(-1)" class="btn btn-default"> << Back</button>
            <h2>Bid History</h2>
            <?php
            if ($model->auctionBids) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Bid Amount</th>
                        <th>Bid Date</th>
                    </tr>
                    </thead>
                    <tbody>
	            <?php
                foreach ( $model->auctionBids as $bid) { ?>
                    <tr>
                        <td><?=$bid->bid_amount?></td>
                        <td><?=$bid->datetime?></td>
                    </tr>
	            <?php
                }
	            ?>
                    </tbody>
                </table>
<?php

            }
            ?>
        </div>
        </div>
    </div>
</div>
</div>
