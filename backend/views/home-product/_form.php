<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomeProduct */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="home-product-form">
    <?php
    $form = ActiveForm::begin();
//    echo $form->errorSummary($model);
    ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'shop')->dropDownList([ 'rakuten' => 'Rakuten', 'amazon' => 'Amazon', 'yahoo' => 'Yahoo'], ['prompt' => 'Please Select Shop']) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'section_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\HomeSection::findAll(['status'=>'1']),'id','title_en'), ['prompt' => 'Please select Section']) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'product_id')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <?= $form->field($model, 'image_url')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-1">
            <?= $form->field($model, 'serial')->textInput() ?>
        </div>
        <div class="col-lg-4 margin-top-20">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
