<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HomeProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Home Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <?= Html::a(Yii::t('app', '+ Add New Product'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pager' => [
	                        'firstPageLabel' => 'First',
	                        'lastPageLabel'  => 'Last'
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute'=>'shop',
                                'filter'=>Html::activeDropDownList($searchModel, 'shop', ['yahoo'=>'Yahoo','rakuten'=>'Rakuten','amazon'=>'Amazon'],['class'=>'form-control','prompt' => 'All']),
                            ],
                            'product_id',
                            [
                                'attribute'=>'section_id',
                                'value'=>function($data) {
                                    return (isset($data->section->title_en)) ? $data->section->title_en : '-';
                                },
                                'filter'=>Html::activeDropDownList($searchModel, 'section_id', \yii\helpers\ArrayHelper::map(\common\models\HomeSection::findAll(['status'=>'1']),'id','title_en'),['class'=>'form-control','prompt' => 'All']),
                            ],
                            //'status',
                            'serial',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}'
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>