<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomeProduct */

$this->title = Yii::t('app', 'Create Home Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Home Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row labels-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>

