<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;
$this->title = Yii::t('app', 'Cart Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <div class="cart-products-index">
                    <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'pager' => [
	                            'firstPageLabel' => 'First',
	                            'lastPageLabel'  => 'Last'
                            ],
                            'rowOptions'=>function($model){
	                            $data = \common\models\CartProducts::find()->where(['status'=>'pending','buyer_id'=>$model->buyer_id])->count();
	                            if ($data) {
		                            return [ 'style' => 'background:#dfe8ee;' ];
	                            }
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute'=>'buyer_id',
                                    'value' => function($data) {
                                        return ($data->buyer)?$data->buyer->fullName:'';
                                    },
                                ],
                                [
                                    'label'=>'Quantity',
                                    'format'=>'raw',
                                    'value' => function($data) {
                                        return $data->totalProduct();
                                    }
                                ],
	                            [
		                            'label'=>'Added on',
		                            'value' => function($data) {
			                            $date = $data->getLatestDate();
			                            return $date['created_on'];
		                            }
	                            ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}',
                                    'buttons' => [
                                        'view' => function ($url,$model) {
                                            return Html::a('View All Products',['view','buyer_id'=>$model->buyer_id],['class'=>'btn btn-primary']);
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
