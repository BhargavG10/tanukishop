<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;
$this->title = Yii::t('app', 'Cart Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .grid-view td{
        white-space: inherit;
    }
</style>
<div class="row">
    <div class="col-lg-12 clearfix">
        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <div class="col-md-4">
                    <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                </div>
                <div class="col-md-4 msg text-center">

                </div>
                <div class="col-md-4 text-right">
                    <?=Html::a('Send Notification',['/cart-products/notification','buyer_id'=>Yii::$app->request->get('buyer_id')],['class'=>'btn btn-primary']);?>
                </div>
            </div>
            <div class="ibox-content">

                <div class="cart-products-index" style="position: relative;">
                    <div class="_load" style="display: none;">
                        <span>Please wait</span>
                    </div>
                    <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute'=>'buyer_id',
                                    'value' => function($data) {
                                        return ($data->buyer)?$data->buyer->fullName:'';
                                    },
                                ],
                                [
                                    'attribute'=>'product_id',
                                    'format'=>'html',
                                    'value' => function($data) {
	                                    $html = Html::a($data->product_id,$data->getOuterLink(),['class'=>'outer-link']);
	                                    $html .= "<br/>";
                                        if ($data->params) {
	                                        $html .= '<div style="width:300px;">'.$data->listAttr().'</div>';
                                        }
                                        return $html;
                                    },
                                ],
                                'shop',
	                            [
		                            'attribute'=>'quantity',
		                            'format'=>'raw',
		                            'value'=>function($model) {
			                            $html = '';
			                            $html .= Html::textInput('quantity',$model->quantity,['class'=>'domestic_shipping col-md-6 text-center','id'=>'quantity_'.$model->id,'style'=>'padding: 2px;margin-right: 12px;','type'=>'number']);
			                            return $html .= Html::a('Update','#',['class'=>'btn btn-primary _quantity','style'=>'padding: 3px;font-size: 12px;','data-id'=>$model->id]);
		                            }
	                            ],
                                [
                                    'attribute'=>'status',
                                    'format'=>'raw',
                                    'value'=>function($model) {
                                        $status = ['pending'=>'pending','approve'=>'approve','disapprove'=>'disapprove'];
                                        $html = '';
                                        return $html .= Html::dropDownList('status',$model->status,$status,['class'=>'status','id'=>$model->id]);
                                    }
                                ],
                                [
                                    'attribute'=>'domestic_shipping',
                                    'format'=>'raw',
                                    'value'=>function($model) {
                                        $html = '';
                                        $html .= Html::textInput('domestic_shipping',$model->domestic_shipping,['class'=>'domestic_shipping col-md-6 text-center','id'=>'shipping_'.$model->id,'style'=>'padding: 2px;margin-right: 12px;','type'=>'number']);
                                        return $html .= Html::a('Update','#',['class'=>'btn btn-primary shipping_cost','style'=>'padding: 3px;font-size: 12px;','data-id'=>$model->id]);
                                    }
                                ],
                                [
                                    'attribute'=>'notify',
                                    'value'=>function($data) {
                                        return ($data->notify) ? 'Sent' : 'Pending';
                                    }
                                ],
                                [
                                     'attribute'=>'created_on',
                                     'filter'=>false,
                                     'format'=>'date'
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {delete}',
                                    'buttons' => [
                                        'view' => function ($url,$model) {
                                            $url = Yii::$app->params['website-url'].$model->shop.'/'.$model->product_id;
                                            return Html::a('View Product',Yii::$app->params['website-url'].$model->shop.'/detail?id='.$model->product_id,['class'=>'btn btn-primary','target'=>'_blank']);
                                        },
                                        'delete' => function ($url,$model) {
                                            return Html::a('Delete Product',$url,
                                                [
                                                    'class'=>'btn btn-danger',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this item from cart?',
                                                        'method' => 'post'
                                                    ]
                                                ]
                                            );
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$status_change_code =
	'$(".status").on("change", function() {
        $("._load").show();
        $.ajax({
            url: "'.\yii\helpers\Url::to(["change-status"]).'",
            data: {id:this.id,status:$(this).val(),buyer_id:'.Yii::$app->request->get('buyer_id').'},
        })
        .done(function( data ) {
            $("._load").hide();
            $(".msg").html(data.message);
        });
    });';

$shipping_cost_code =
	'$(".shipping_cost").on("click", function() {
	    var shipping_cost = $("#shipping_"+$(this).data("id")).val();
	    var id = $(this).data("id");
        $("._load").show();
        $.ajax({
            url: "'.\yii\helpers\Url::to(["domestic-shipping"]).'",
            data: {id:id,cost:shipping_cost},
        })
        .done(function( data ) {
            $("._load").hide();
            $(".msg").html(data.message);
        });
    });';

$quantity_code =
	'$("._quantity").on("click", function() {
	    var quantity = $("#quantity_"+$(this).data("id")).val();
	    var id = $(this).data("id");
        $("._load").show();
        $.ajax({
            url: "'.\yii\helpers\Url::to(["quantity"]).'",
            data: {id:id,quantity:quantity},
        })
        .done(function( data ) {
            $("._load").hide();
            $(".msg").html(data.message);
        });
    });';

$this->registerJs(
	$status_change_code.$shipping_cost_code.$quantity_code,
    \yii\web\View::POS_READY,
    'my-status-change'
);
?>

<?php
$this->registerCss("
.cart-products-index{position:relative;}
._load{position: absolute;width: 100%;height: 100%;background: #000;opacity: .5;text-align: center;}
._load span{color: #fff;top: 50%;display: block;position: absolute;left: 45%;}

");
?>