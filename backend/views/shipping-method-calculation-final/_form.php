<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShippingMethodCalculationFinal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipping-method-calculation-final-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shipping_method_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\ShippingMethods::findAll(['status'=>1]),'id','title')) ?>

    <?= $form->field($model, 'country_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(),'id','title')) ?>

    <?= $form->field($model, 'weight_from')->textInput() ?>

    <?= $form->field($model, 'weight_to')->textInput() ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Back'), ['shipping-method-calculation-final/index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
