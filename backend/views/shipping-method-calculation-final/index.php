<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ShippingMethodCalculationFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shipping Method Calculation Finals');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row shipping-method-calculation-final-index">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title clearfix">
                <h5 class="pull-left"><?= Html::encode($this->title) ?></h5>
                <h5 class="pull-right"><?=Html::a('+ Add New', ['/shipping-method-calculation-final/create'], ['class' => 'btn btn-default']) ?></h5>
            </div>
            <div class="ibox-content">
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
	        'firstPageLabel' => 'First',
	        'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'shipping_method_id',
                'header'=>'Shipping Method',
                'value'=>function($model) {
                    return $model->method->title;
                },
                'filter'=>Html::activeDropDownList($searchModel, 'shipping_method_id', \yii\helpers\ArrayHelper::map(\common\models\ShippingMethods::findAll(['status'=>1]),'id','title'), ['prompt'=>'All','class'=>'form-control'])
            ],
            [
                'attribute'=>'country_id',
                'value'=>function($model) {
                    return $model->country->title;
                },
              'filter'=>Html::activeDropDownList($searchModel, 'country_id', \yii\helpers\ArrayHelper::map(\common\models\Countries::find()->where(['active'=>1])->orderBy('title')->all(),'id','title'), ['prompt'=>'All','class'=>'form-control'])

    ],
            'weight_from',
            'weight_to',
             'cost',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
        </div>
    </div>
</div>
