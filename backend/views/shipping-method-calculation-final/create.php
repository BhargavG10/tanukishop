<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShippingMethodCalculationFinal */

$this->title = Yii::t('app', 'Create Shipping Method Calculation Final');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shipping Method Calculation Finals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row shipping-method-calculation-final-create">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>