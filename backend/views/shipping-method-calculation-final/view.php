<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShippingMethodCalculationFinal */

$this->title = $model->method->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shipping Method Calculation Finals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row shipping-method-calculation-final-view">
    <div class="col-lg-12 clearfix">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
            </div>
            <div class="ibox-content">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute'=>'shipping_method_id',
                            'header'=>'Shipping Method',
                            'value'=>$model->method->title
                        ],
                        [
                            'attribute'=>'country_id',
                            'value'=> $model->country->title
                        ],
                        'weight_from',
                        'weight_to',
                        'cost',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
