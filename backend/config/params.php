<?php
return [
    'adminEmail' => 'admin@tanuki.com',
    'supportEmail' => 'support@tanuki.com',
];
