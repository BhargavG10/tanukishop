<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShippingMethodCalculationFinal;

/**
 * ShippingMethodCalculationFinalSearch represents the model behind the search form about `common\models\ShippingMethodCalculationFinal`.
 */
class ShippingMethodCalculationFinalSearch extends ShippingMethodCalculationFinal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shipping_method_id', 'country_id', 'weight_from'], 'integer'],
            [['weight_to', 'cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShippingMethodCalculationFinal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'shipping_method_id' => $this->shipping_method_id,
            'country_id' => $this->country_id,
            'weight_from' => $this->weight_from,
            'weight_to' => $this->weight_to,
            'cost' => $this->cost,
        ]);

        return $dataProvider;
    }
}
