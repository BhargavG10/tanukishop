<?php
namespace backend\models;


use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use \common\components\TBase;
use common\models\Order;
use app\models\FavoriteProduct;
use common\models\UserAuctions;
use common\models\Packages;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $name
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $last_login
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Customer extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_DEACTIVE = 9;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name','last_name','email'], 'required','message' => Yii::t('app', TBase::ShowLbl('CANNOT_BLANK'))],
            [['email'], 'unique'],
            [['status'], 'number'],
            [['password_hash','language'], 'string'],
            ['email', 'email','message' => Yii::t('yii', TBase::ShowLbl('VALID_EMAIL'))],
            [['status','last_login','language'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'username' => 'Username',
          'auth_key' => 'Authentication Key',
          'password_hash' => 'Password',
          'password_reset_token' => 'Password Token',
          'name' => 'Name',
          'email' => 'Email',
          'status' => 'Status',
          'last_login' => 'Last Login',
          'created_at' => 'Created At',
          'updated_at' => 'Updated On',
        ];
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getFullName() {
        return $this->first_name.' '.$this->last_name;
    }

	/**
	 * get user blocked amount
	 * @return int
	 */
//	public function getBlockedAmount()
//	{
//		$blocked = 0;
//		$blockedAmount = \common\models\UserAuctionBlockedAmount::find()
//		                                                        ->select('sum(blocked_amount) as blocked_amount, sum(auction_amount) as auction_amount')
//		                                                        ->where(['user_id'=>$this->id])->one();
//		if ($blockedAmount) {
//			$blocked = (int)$blockedAmount['blocked_amount'] + (int)$blockedAmount['auction_amount'];
//		}
//		return $blocked;
//	}

	/**
	 * get user blocked amount
	 * @return int
	 */

	/**
	 * @return int|string
	 */
	public function getAuctionsCount()
	{
		return UserAuctions::find()->where(['user_id'=>Yii::$app->user->id,'is_deleted'=>0])->count();
	}

	/**
	 * @return int|string
	 */
	public function getYahooWatchListCount()
	{
		return FavoriteProduct::find()->where(['shop'=>'yahoo_auction','user_id'=>Yii::$app->user->id])->count();
	}

	/**
	 * @return int|string
	 */
	public function getWatchListCount()
	{
		return FavoriteProduct::find()->where(['user_id'=>Yii::$app->user->id])->count();
	}

	public function getOrder() {
		return $this->hasMany(Order::className(),['user_id'=>'id']);
	}


	public function getUserAuction() {
		return $this->hasMany(UserAuctions::className(),['user_id'=>'id']);
	}

	public function getOrderCount()
	{
		return Order::find()
		            ->where(['user_id'=>Yii::$app->user->id])
		            ->andWhere(['!=','type','add_fund'])
		            ->count();
	}

	public function getPackageCount()
	{
		return Packages::find()->where(['created_by'=>Yii::$app->user->id])->count();
	}

	public static function updateCredit($amount)
	{
		self::updateAll( [ 'credit' => $amount ], [ 'id' => Yii::$app->user->id ] );
	}
	public function getBlockedAmount()
	{
		$blocked = 0;
		$auctions = $this->getUserAuction()
		                 ->select('sum(blocked_amount) as blocked_amount')
		                 ->where(['user_id'=>$this->id,'is_deleted'=>'0'])->one();

		$blocked += ($auctions->blocked_amount) ? $auctions->blocked_amount : 0;
		$order = $this->getOrder()
		              ->select('sum(blocked_amount) as blocked_amount')
		              ->where(['user_id'=>$this->id])->one();
		$blocked  += ($order->blocked_amount) ? $order->blocked_amount : 0;
		return $blocked;
	}

}
