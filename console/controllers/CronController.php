<?php

namespace console\controllers;

use common\components\TCurrencyConvertor;
use common\models\CurrencyTable;
use common\models\UserAuctions;
use yii\helpers\Console;
use yii\console\Controller;
/**
 * All Cron actions related to this project
 */
class CronController extends Controller {

    /**
     * Used for testing only
     */
    public function actionIndex(){
        $this->stdout("Sample Output \n", Console::FG_RED, Console::BOLD);
    }
    /**
     * Method called by cron every minute
     * php yii cron/every-hour
     */
    public function actionEveryHour() {
        $count = UserAuctions::auctionUpdate();
        $this->stdout("$count : Auction(s) Updated; \n", Console::FG_RED, Console::BOLD);
    }

	/**
	 * run every day to update corrency
	 *php yii cron/every-day
	 */

    public function actionEveryDay() {
	    $this->stdout("Sample Output \n", Console::FG_RED, Console::BOLD);
//        $model = new TCurrencyConvertor();
//        $count = $model->update_rates();

        $currency = new CurrencyTable();
	    $count = $currency->check_url($this);
        $this->stdout("$count : conversion rate updated; \n", \yii\helpers\Console::FG_RED, \yii\helpers\Console::BOLD);
    }

    public function actionEveryHalfHour() {
	    $data = \common\models\UserAuctions::storeAuctionResult();
	    $this->stdout("$data: record updated; \n", Console::FG_RED, Console::BOLD);
    }

}
