<?php

namespace api\modules\v1\controllers;

use api\models\Order;
use Yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;

/**
 * City controller - Search Cities 
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [''],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
        ];

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options', 'list','detail'];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
            // optional:
            'collectionOptions' => ['GET', 'POST', 'HEAD', 'OPTIONS'],
            'resourceOptions' => ['GET', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
        ];
        return $actions;
    }

	/**
	 * @return ActiveDataProvider
	 */
    public function actionList()
    {
    	$default = date('Y-m-d');
    	$date = Yii::$app->request->get('date',$default);
    	$end_date = Yii::$app->request->get('end_date',$default);
        $query = Order::find();
        $query->andWhere(['between', 'DATE(date)', $date, $end_date]);
	    return $provider = new ActiveDataProvider([
		 'query' => $query,
			 'pagination' => [
			    'pageSize' => 100,
			 ],
		 ]);
    }


    public function actionDetail($id)
    {
        return Order::findOne($id);
    }
}